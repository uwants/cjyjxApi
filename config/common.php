<?php

return [
    'apiTokenName' => 'api',
    'companyUser' => [
        'defaultAvatar' => 'https://cjyjxqny.missapp.com/default_avatar03.png',
        'defaultRole' => 'ROLE_NORMAL',
        'role' => [
            'supperAdministrator' => 'ROLE_SUPPER_ADMINISTRATOR',
            'administrator' => 'ROLE_ADMINISTRATOR',
            'normal' => 'ROLE_NORMAL',
        ]
    ],
    'company' => [
        'status' => [
            'default' => 'STATUS_CERTIFIED_PRE_COMMIT',
            'audit' => 'STATUS_CERTIFIED_AUDIT',
            'fail' => 'STATUS_CERTIFIED_FAILED',
            'pass' => 'STATUS_CERTIFIED_PASS',
        ]
    ],
    'platformCode' => [
        'pc' => 'PC',
        'h5' => 'H5',
        'mpDingTalk' => 'MP-DINGTALK',
        'mpWeiXin' => 'MP-WEIXIN',
        'mpWorkWeiXin' => 'MP-WORKWEIXIN',
    ],
    'mpDingTalk' => [
        'corpId' => env('MP_DING_TALK_CORP_ID', 'dingb25ddcb88f064b7935c2f4657eb6378f'),
        'token' => env('MP_DING_TALK_TOKEN', 'cjyjx2'),
        'encodingAesKey' => env('MP_DING_TALK_ENCODING_AES_KEY', '3xhybjmccz3xysursyo6sajz65dg74c1pvrzy1ducx3'),
        'suiteKey' => env('MP_DING_TALK_SUITE_KEY', 'suitevdowp2ajomyamq7y'),
        'suiteSecret' => env('MP_DING_TALK_SUITE_SECRET', 'vHZZb2Z9QGYz_loaRTWmWhiMBqeUihZwG8r_S_XtbQGjaDEyqX9b6RiM6QlAGjEt'),

        'templateId' => [
            'receiveComment' => '28384c9c564c40459b894c1f969660d4',
            'receiveReply' => '3e2ed16fd44e4bf0999d788fe8396ffb',
            'adoptComment' => '193dd219e1ab4481b9b31fa126064c94',
            'boxCreate' => 'ab2d998168344e89986c38754b7b35ac',
            'receiveReplyNew'=>'8c4ad85400234c7ead3a450fef33cd01',
        ]
    ],
    'mpWeiXin' => [
        'appId' => env('APP_ID', 'wx99942da2b8166c31 '),
        'secret' => env('APP_SECRET', '30347c16fd3c245ec23486118a35fc08'),
    ],
    'default_company_profile' => env('DEFAULT_COMPANY_PROFILE', '完善简介让更多人认识您的组织'),
    'default_company_logo' => env('DEFAULT_COMPANY_LOGO', 'https://cjyjxqny.missapp.com/default_logo.png'),
    'default_avatar' => env('DEFAULT_AVATAR', 'https://cjyjxqny.missapp.com/default_avatar03.png'),
    'default_company_poster' => env('DEFAULT_COMPANY_POSTER', 'https://cjyjxqny.missapp.com/posterNew.png'),
    'banner_type' => [
        'mp' => 1,
        'pc' => 2,
    ],
    'box' => [
        'feedback_box_id' => 3273
    ],
    'qiniu' => [
        'QN_CDN_URL' => 'https://cjyjxqny.missapp.com',
        'QN_MYSELF_HTTPS' => 'http://hr-20190515jyjs/public/',
        'QN_ACCESS_KEY' => 'RJhka6JxUVnDjUuvWQzqEdCr0L6Bvi4GADNfksVN',
        'QN_SECRET_KEY' => 'ClS8FtdSbTyOC_WA20PTCTZFW0OVDo11mmWfTFlY',
        'QN_BUCKET' => 'my-cjyjx',
    ],
    'sms' => [
        'ACCESS_KEY_ID' => 'LTAI5tAwJyjjNiWjL9zZgAHV',
        'ACCESS_KEY_SECRET' => 'jsUxchX0F6HJMRUWALXMBs8L3bCSgr'
    ],
    'mpWorkWeChat' => [
        'corpId' => env('WE_CHAT_CORP_ID', 'ww41f0d96e84c59766'),
        'providerSecret' => env('WE_CHAT_PROVIDER_SECRET', 'PV3p2CeBIZ3cVbJ-ZRpk_c2N9Vtt0sy-NwL28Htmbg61YKgn64FUcWIkvkrswKIl'),
        'encodingAesKey' => env('WORK_WE_CHAT_ENCODING_AES_KE', 'QkGHxMX6fNlJxzSzVznAGevhugDhFh6sAVBEs93wWdK'),
        'token' => env('WORK_WE_CHAT_TOKEN', 'KLgfwHi4uVS4cf2WHrD8xrmg1'),
        'suiteId' => env('WORK_WE_CHAT_SUITE_ID', 'ww1fe4c80c856d553b'),//小程序
        'suiteSecret' => env('WORK_WE_CHAT_SUITE_SECRET', 'hpu0jB1tD0y1H618cYpSI2VqMmSHGiXpdih3-kgJn1c'),//小程序
    ],
    'weChat' => [
        'appId' => env('APP_ID', 'wx9ba20af453ec39e4 '),//wx9ba20af453ec39e4  wx99942da2b8166c31
        'appSecret' => env('APP_SECRET', '30347c16fd3c245ec23486118a35fc08'),//
        'templateId' => [
            'commentCreate' => env('MP_WEIXIN_COMMENT_CREATE', 'dU_O_LlOBWVuY63ZYK7ci-ITHBvVHqz_H0QkkkVlhb0'),
            'replyCreate' => env('MP_WEIXIN_REPLY_CREATE', '0e-VlRUYn5hC5I4wHB3hZB44ZNq3I1DWTKdStcBS97s'),
            'commentAdopt' => env('MP_WEIXIN_COMMENT_ADOPT', 'YF2iSD1Gghnf_kl6X2eRf2UIZMskAw_NT1o2XQcDEt8'),
        ]
    ],
    //服务号
    'weChatService' => [
        'appId' => env('WX_SER_APP_ID', 'wxfbf2426facc0a103'),
        'appSecret' => env('WX_SER_APP_SECRET', '827e6acf00eec87508fb9ce1f46e1310'),
        'templateId' => [
            'commentCreate' => env('MP_WEIXIN_COMMENT_CREATE', 'dU_O_LlOBWVuY63ZYK7ci-ITHBvVHqz_H0QkkkVlhb0'),
            'replyCreate' => env('MP_WEIXIN_REPLY_CREATE', '0e-VlRUYn5hC5I4wHB3hZB44ZNq3I1DWTKdStcBS97s'),
            'commentAdopt' => env('MP_WEIXIN_COMMENT_ADOPT', 'YF2iSD1Gghnf_kl6X2eRf2UIZMskAw_NT1o2XQcDEt8'),
        ]
    ],
    'alias_avatar' => [
        '',
    ],
    //订阅号
    'weChatSubscribe' => [
        'appId' => 'wxb0ae9a693e04ae07',
        'appSecret' => '57f00630998f44892f690689b3f405a8',
    ],
    'default_avatars' => env('DEFAULT_AVATARS', [
        'https://cjyjxqny.missapp.com/lizi.png',
        'https://cjyjxqny.missapp.com/lizi-3.png',
        'https://cjyjxqny.missapp.com/mogu 2.png',
        'https://cjyjxqny.missapp.com/菠萝-1.png',
        'https://cjyjxqny.missapp.com/番茄.png',
        'https://cjyjxqny.missapp.com/蓝莓.png',
        'https://cjyjxqny.missapp.com/牛油果.png',
        'https://cjyjxqny.missapp.com/茄子.png',
        'https://cjyjxqny.missapp.com/西瓜.png',
        'https://cjyjxqny.missapp.com/香蕉.png',
    ]),
    //天眼查
    'tianYanCha' => [
        'hostUrl' => env('TYC_HOST_URL', 'http://open.api.tianyancha.com/services/open/search/2.0'),
        'hostMsgUrl' => env('TYC_HOST_MSG_URL', 'http://open.api.tianyancha.com/services/open/ic/baseinfo/2.0'),
        'token' => env('TYC_TOKEN', '81ed839b-894f-4e79-bb81-f8ed7d4a7a77'),
    ],
    //机器人
    'robot' => [
        'access_token' => [
            // 升级意见箱审核
            'company_apply' => env('COMPANY_APPLY_ACCESS_TOKEN', '550cff9c6e081813e233b6a01c0825090185b1b086c6e4981f28eac8349ba58a'),
            // 代码报错
            'code_error' => env('CODE_ERROR_ACCESS_TOKEN', '4eb22c9e098b6a771570338ee2ccce921fec7196563b3c94450f80381a0d2052')
        ]
    ],
    'adminTokenName' => 'admin',
    'url' => [
        'company_invite' => env('COMPANY_INVITE', ' https://m.cjyjx.cn/#/pages/companies/invite'),
        'box_invite' => env('BOX_INVITE', 'https://m.cjyjx.cn/#/pages/boxes/detail')
    ],

    //省份简称
    'provinceA' => [
        'bj' => '北京市',
        'tj' => '天津市',
        'heb' => '河北省',
        'sx' => '山西省',
        'nmg' => '内蒙古自治区',
        'ln' => '辽宁省',
        'jl' => '吉林省',
        'hlj' => '黑龙江省',
        'sh' => '上海市',
        'js' => '江苏省',
        'zj' => '浙江省',
        'ah' => '安徽省',
        'fj' => '福建省',
        'jx' => '江西省',
        'sd' => '山东省',
        'hen' => '河南省',
        'hub' => '湖北省',
        'hun' => '湖南省',
        'gd' => '广东省',
        'gx' => '广西壮族自治区',
        'han' => '海南省',
        'cq' => '重庆市',
        'sc' => '四川省',
        'gz' => '贵州省',
        'yn' => '云南省',
        'xz' => '西藏自治区',
        'snx' => '陕西省',
        'gs' => '甘肃省',
        'qh' => '青海省',
        'nx' => '宁夏回族自治区',
        'xj' => '新疆维吾尔自治区',
        'hk' => '香港特别行政区'
    ]
];
