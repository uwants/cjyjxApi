<?php
return [
    'pay' => [
        // APPID
        'app_id' => '2021000119623470',
        // 支付宝 支付成功后 主动通知商户服务器地址  注意 是post请求
        'notify_url' => env('APP_URL').'/admin/alipay/notify',
        // 支付宝 支付成功后 回调页面 get
        'return_url' => env('APP_URL').'/admin/alipay/return',
        // 公钥（注意是支付宝的公钥，不是商家应用公钥）
        'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl7vjMLgPNhirX5SbXmCmqYTtR/sHRPPYXwHtih+wnQOHj0myTBN38cE8hQEqFF+ChrDGa3RyDn0sw7F+vB8vxZZxaxGzHjdOfBLAt8mCHNom3KnB5N+BQoVBItWMCndyOll1uXApF7cUymVTJT3SSeJYM8tNoCKwo519p2cNK5C+XlYT0ZJHlPvdUhyOkRxCyYiR0ZXU1EVkB07kxEI1yVRN/H/GWE9SZ8xVPl1CDQub2xReC9SGBdWtAEdzLJW4QElXVatBN/PgY1Ng/16m/KX4kRob5Cs5We0f1X2aCi+ZXvx94JG3ztvR5/XQmKZQtTFkQA1U6qtTrc/PKyWgcwIDAQAB',
        // 加密方式： **RSA2** 私钥 商家应用私钥
        'private_key' => 'MIIEpQIBAAKCAQEA48M5XGTTR0gYeXld+S40NzHcKcryCE1bJvuZN4N11jdVz/D1134wXfg3GhZuESJD13nqEQ0o36YNit28dKlEbV4FRBOlHsS0a8jZmKauK+73tk0h6dG2Tj6t19T6T8voyJPHJV5FqaRtP+pCuL4UjmwjjfsHFdWpIaxDCmP0+ClUzdZ0b/1QkimaVOXbS09B54SfhKoPg8GOhjhsSj9QXnTwAuXLiUd9GVLvqiFHfx9B9F02kXv3hPqonCtVyTfQxQ2hGsOYKIbuZ2hWhJIs2z4fNBRu305WpnFXyoCIaVndp48zr2KPBYpz3dpWuhoa9NNdRoAVx2RYQLhfoXEt5QIDAQABAoIBAQCQOBvoL0fBSRuGP52LnQAfOwknxYQAUcs4IfRgvHMvE5bYtsFUlGjX98YcVLuZSdMfa+kUpvqMsz68Aa/FRzE292PqOW5Cz1dXMLTn/hMnaoeb/bdNsSb44KN/mTJuuLVL3JcrSEhgJOewowtgo6usizjwPFW+M66GzecdezGwuHjs8ZAkjLa2K7x0Rr8aKFe32K8PQk2ER+b9xtk26hODDEQav5k0hsBaC3ner+hxEy0tG1Qkc0i3ei81p7XzW2WQyRDpmB/ICdFJEfz3rkZbHI9qatRqUSHJWB8yfnLu2Q8ecs1yZcTG197FAayUO9mNIQ5eyh9/Xn0YjHn0nTv5AoGBAP5lanK5KsbSR1mXY8vCEI8ouVUhv5VOhhUFaL/GC/iXYVp58HEAjd54SQEIlNq40e/C8dWWn+H/jJVqg6IHmzyMGuLs3BRkn16YD5A5T+dYiadzYqAko/AIlebhtJM/E+ganVi1jjzM3E/bcOim/IL7ze4/1lRpQY+v+B/lXIFDAoGBAOUy0qbVCGKreV06fxD/riXLoqsg6W+q8JzKqsonUywR2IcRlAto+tUlQNYmzLkqRDOnpp4wmFEfAdR4BmkhPBcwSHgCZPVcFU187wzuRcAnTMB0RZYXRutbjbu8NaXl7vbsUUo+kaPCVKfr2vdfM3hbXPyhKwr6oWR/kR1qiC23AoGBAIK/dvVdq9mh65/lShpINKCPaqmRfdjsEceuDwc5b5+llhidQXGiukeUBNluIqtDgEq9I/QFxUivQf7tTpgxVpzI2Wy2Feq5SJuihaU3f5TEkYMpr4/lJQjSMNEIQfnH2qBZRXN5ZJBlLfoxK5e3c2KSrmV8E9kwe8ywwVMLW/7nAoGBAIgutnNiSOU3tZjrVOyovrS+Vh0QJSOwkd7XnLsYupaI0/sNOsW8zq0uwWN9Ek9DCdsXVizP+vDYV6IC3uxATxAIJEcKRSp0WEP+Y5tvSApwT25ap64SjpTxhTlzDv1PrbkRmQtgOfDvWpWb0jMBYfe1cX651HVljdfE5FhyIA/pAoGAMCl1gQsPk+Q2XbQPlaWUgzmU7uwvi/xhaMQagkLJGO945uj1mJiI6/EpI0hKc43y+W+0MkLkhzgdEA2zlgEGoNghQnbckeWcSG/gN2VbZ6Oh6Hg9PS/DfsTQJ31bgEJ4wotGF1SU90oFPFJtTBVRyWgAMcfY7Rw2wWW2oQ/eBaA=',
        'log' => [ // optional
            'file' => '../storage/logs/alipay.log',
            'level' => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        'mode' => 'dev', // dev/ optional,设置此参数，将进入沙箱模式
    ]
];
