<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_cards', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('标题');
            $table->string('image')->comment('图片链接');
            $table->string('path')->comment('路径');
            $table->boolean('suspend')->default(false)->comment('关闭');
            $table->unsignedInteger('sort')->default(0)->comment('排序');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_cards');
    }
}
