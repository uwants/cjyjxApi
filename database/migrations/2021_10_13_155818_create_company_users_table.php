<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->default(0)->comment('企业id');
            $table->foreignId('user_id')->default(0)->comment('用户id');
            $table->string('name', 60)->default('')->comment('企业用户名');
            $table->string('avatar')->default('')->comment('企业用户logo');
            $table->string('role')->default('')->comment('企业用户角色');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['company_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_users');
    }
}
