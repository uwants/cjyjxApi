<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDingTalkUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ding_talk_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->default(0)->comment('用户id');
            $table->string('corp_id')->default('')->comment('钉钉企业id');
            $table->string('union_id')->default('')->comment('用户unionId');
            $table->string('ding_talk_user_id')->default('')->comment('钉钉用户的userId');
            $table->string('device_id')->default('')->comment('设备id');
            $table->boolean('sys')->default(false)->comment('是否是管理员。true：是；false：不是');
            $table->unsignedTinyInteger('sys_level')->default(0)->comment('级别。1：主管理员；2：子管理员；100：老板；0：其他（如普通员工）');
            $table->string('associated_union_id')->default('')->comment('用户关联的unionId');
            $table->string('name')->default('')->comment('用户名字');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ding_talk_users');
    }
}
