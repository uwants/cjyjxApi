<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxCommentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_comment_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('box_id')->comment('意见箱id');
            $table->foreignId('user_id')->comment('用户Id');
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['box_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_comment_users');
    }
}
