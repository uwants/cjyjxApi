<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminRoleRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_role_relationships', function (Blueprint $table) {
            $table->id();
            $table->foreignId('admin_id')->default(0)->comment('管理员id');
            $table->foreignId('created_admin_id')->default(0)->comment('创建者id');
            $table->foreignId('role_id')->default(0)->comment('角色id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_role_relationships');
    }
}
