<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_applies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('box_id')->default(0)->comment('意见箱id');
            $table->foreignId('company_id')->index()->default(0)->comment('企业id');
            $table->string('id_card_face')->default('')->comment('身份证人像面');
            $table->string('id_card_positive')->default('')->comment('身份证国徽面');
            $table->string('business')->default('')->comment('营业执照');
            $table->string('reject_reason')->default('')->comment('审核不通过原因');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_applies');
    }
}
