<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkWeiXinCompanyCorpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_wei_xin_company_corps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->default(0)->comment('企业id');
            $table->string('corp_id', 64)->default('')->unique()->comment('企业微信企业id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_wei_xin_company_corps');
    }
}
