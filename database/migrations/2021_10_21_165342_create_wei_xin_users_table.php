<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeiXinUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wei_xin_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->default(0)->comment('用户id');
            $table->string('open_id')->default('')->comment('用户唯一标识');
            $table->string('union_id')->default('')->comment('用户unionId');
            $table->string('session_key')->default('')->comment('会话密钥');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wei_xin_users');
    }
}
