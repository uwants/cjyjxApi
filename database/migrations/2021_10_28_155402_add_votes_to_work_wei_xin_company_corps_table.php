<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToWorkWeiXinCompanyCorpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_wei_xin_company_corps', function (Blueprint $table) {
            //
            $table->string('permanent_code')->comment('企业微信企业permanent_code');
            $table->string('agentid')->comment('企业微信agentid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_wei_xin_company_corps', function (Blueprint $table) {
            //
        });
    }
}
