<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->index()->default(0)->comment('企业id');
            $table->foreignId('user_id')->index()->default(0)->comment('意见箱所有者id');
            $table->string('name', 60)->default('')->comment('意见箱名称');
            $table->boolean('suspend')->default(false)->comment('是否暂停');
            $table->boolean('once')->default(false)->comment('是否只提一次');
            $table->boolean('open')->default(false)->comment('是否公开');
            $table->boolean('all')->default(false)->comment('是否所有人');
            $table->boolean('system')->default(false)->comment('是否系统意见箱');
            $table->boolean('show')->default(false)->comment('是否被搜索');
            $table->json('content')->comment('意见箱内容');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
