<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()->comment('用户id');
            $table->foreignId('template_id')->index()->comment('模板id');
            $table->string('title')->comment('标题');
            $table->text('content')->comment('内容');
            $table->boolean('read')->default(false)->comment('是否已读');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_messages');
    }
}
