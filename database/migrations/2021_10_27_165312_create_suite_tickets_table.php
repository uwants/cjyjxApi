<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuiteTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suite_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('corp_id')->comment('平台企业id');
            $table->string('suite_ticket')->comment('平台Ticket');
            $table->foreignId('biz_id')->default(0)->comment('SuiteId');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suite_tickets');
    }
}
