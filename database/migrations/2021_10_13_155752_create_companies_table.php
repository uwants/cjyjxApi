<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('企业名称');
            $table->foreignId('user_id')->index()->default(0)->comment('企业所有者id');
            $table->string('status', 60)->index()->default('')->comment('状态');
            $table->string('logo')->default('')->comment('企业logo');
            $table->text('profile')->comment('简介');
            $table->string('poster')->default('')->comment('海报');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
