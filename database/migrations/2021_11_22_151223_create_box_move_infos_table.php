<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxMoveInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_move_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('box_id')->default(0)->comment('意见箱id');
            $table->integer('new_user_id')->comment('新的意见箱所属user_id');
            $table->integer('old_user_id')->comment('旧的意见箱所属user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_move_infos');
    }
}
