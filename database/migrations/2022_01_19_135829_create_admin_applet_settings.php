<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAppletSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_applet_settings', function (Blueprint $table) {
            $table->id();
            $table->string('app_id')->comment('小程序appId');
            $table->string('type')->comment('类型');
            $table->boolean('state')->default(false)->comment('状态');
            $table->json('setting')->comment('设置参数');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_applet_settings');
    }
}
