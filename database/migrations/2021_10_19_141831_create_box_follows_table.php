<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_follows', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('用户id');
            $table->foreignId('box_id')->comment('意见箱id');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['user_id', 'box_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_follows');
    }
}
