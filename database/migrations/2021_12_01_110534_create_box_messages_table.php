<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()->comment('用户id');
            $table->foreignId('template_id')->index()->comment('模板id');
            $table->foreignId('box_id')->index()->comment('企业id');
            $table->foreignId('company_id')->index()->comment('企业id');
            $table->string('title')->comment('标题');
            $table->text('content')->comment('内容');
            $table->boolean('read')->default(false)->comment('是否已读');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_messages');
    }
}
