<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyUserInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_user_invites', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->comment('企业id');
            $table->foreignId('company_user_id')->comment('企业成员id');
            $table->string('token', 150)->default('')->comment('token');
            $table->string('token_expired_at')->comment('token过期时间');
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['company_id', 'token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_user_invites');
    }
}
