<?php

use Illuminate\Support\Facades\Route;

//发送验证码
Route::post('/sms/send', 'SmsController@sendCodeByMobile');
//登录
Route::post('/login', 'AuthController@login');
//注册
Route::post('/register', 'AuthController@register');
//七牛云
Route::post('/qiniu/token', 'QiNiuController@token');
//banner图
Route::post('/banners/get', 'BannersController@index');
//第三方授权
Route::any('/third', 'ThirdAuthController@index');
//faq
Route::post('/faqs/get', 'FaqController@index');
//金刚区信息
Route::post('/tab-card/get', 'TabCardController@index');
//意见箱总数
Route::post('/boxes/total', 'BoxesController@total');
//收到意见总数
Route::post('/comments/total', 'CommentsController@total');
//微信公众号文章接口接口
Route::post('/news/get', 'NewsController@index');
//公司本周收到意见数量排名
Route::post('/companies/comment/rank', 'CompaniesController@commentRank');
//服务号回调地址
Route::any('/follow', 'WeChatCallbackController@index');
Route::any('/follow/test', 'WeChatCallbackController@callbackTest');

Route::post('/wechat/workSuiteToken', 'WeChatController@workSuiteToken');
//是否订阅
Route::post('/wechat-user-subscribes/check', 'WechatUserSubscribesController@check');
Route::post('/wechat-skin/get', 'WeChatController@getSkin');//皮肤

Route::post('/customer-companies-logo/get', 'CustomerCompanyLogosController@index');//官网在使用我们的产品公司Logo
Route::post('/boxes-old/get', 'BoxesController@old');///根据旧版参数回去新版
Route::post('/companies-old/get', 'BoxesController@getCompany');///根据旧版参数回去新版企业
Route::post('/report', 'ReportsController@index');
Route::post('/festival', 'WeChatController@festival');

Route::any('/comments/export', 'CommentsController@export');


Route::middleware('auth:sanctum')->group(function () {
    //意见箱
    Route::post('/boxes/get', 'BoxesController@index');//意见箱列表
    Route::post('/boxes/create', 'BoxesController@store');//创建意见箱
    Route::post('/boxes/detail', 'BoxesController@show');//意见箱详情
    Route::post('/boxes-comment/detail', 'BoxesController@getComment');
    Route::post('/boxes/update', 'BoxesController@update');//编辑意见箱
    Route::post('/boxes-suspend/update', 'BoxesController@suspend');//暂停意见箱
    Route::post('/boxes-open/update', 'BoxesController@open');//开启意见箱
    Route::post('/boxes/delete', 'BoxesController@destroy');//删除意见箱
    Route::post('/boxes-collection/create', 'BoxesController@collectionStore');//收藏意见箱
    Route::post('/boxes-collection/delete', 'BoxesController@collectionDestroy');//删除收藏
    Route::post('/boxes-collection/get', 'BoxesController@collectionIndex');//收藏列表
    Route::post('/boxes-hot/get', 'BoxesController@hotIndex');//热门意见箱
    Route::post('/boxes-edit/detail', 'BoxesController@editShow');//编辑意见箱详情
    Route::post('/boxes/feedback', 'BoxesController@feedback');//意见反馈

    //消息
    Route::post('/boxes-message/get', 'BoxesMessageController@index');//意见箱消息列表
    Route::post('/boxes-message/receive/comment', 'BoxesMessageController@receiveComment');//意见箱收到意见
    Route::post('/boxes/message/read', 'BoxesMessageController@read');//意见箱消息已读
    Route::post('/message/unread/total', 'BoxesMessageController@unReadTotal');//意见箱未读总数
    Route::post('/systems-message/get', 'SystemMessageController@index');//系统消息列表
    Route::post('/systems-message/read', 'SystemMessageController@read');//系统消息已读

    //意见
    Route::post('/comments/get', 'CommentsController@index');//意见列表
    Route::post('/comments/create', 'CommentsController@store');//提出意见
    Route::post('/comments/detail', 'CommentsController@show');//意见详情
    Route::post('/comments/update', 'CommentsController@update');//更新意见
    Route::post('/comments-adopt/update', 'CommentsController@adopt');//采纳
    Route::post('/comments-cancel-adopt/update', 'CommentsController@cancelAdopt');//取消采纳
    Route::post('/comments/delete', 'CommentsController@destroy');//删除意见
    Route::post('/comments/next', 'CommentsController@next');//下一条意见

    //回复
    Route::post('/replies/get', 'RepliesController@index');//回复列表
    Route::post('/replies/create', 'RepliesController@store');//回复
    Route::post('/replies/detail', 'RepliesController@show');//回复详情
    Route::post('/replies/update', 'RepliesController@update');//更新回复
    Route::post('/replies/delete', 'RepliesController@destroy');//删除回复
    //企业
    Route::post('/companies/detail', 'CompaniesController@show');//企业详情
    Route::post('/companies/get', 'CompaniesController@index');//企业列表
    Route::post('/companies/update', 'CompaniesController@update');//更新企业
    Route::post('/companies/create', 'CompaniesController@store');//创建企业
    Route::post('/companies-apply/create', 'CompaniesController@apply');//企业申请认证
    Route::post('/companies-certified-status/detail', 'CompaniesController@applyStatus');//企业认证状态
    Route::post('/companies-reject-reason/detail', 'CompaniesController@applyReason');//认证失败原因
    Route::post('/companies-main/detail', 'CompaniesController@mainShow');//获取主企业
    Route::post('/companies-main/update', 'CompaniesController@mainUpdate');//主企业更新
    Route::post('/companies/exist', 'CompaniesController@exist');//是否存在企业
    Route::post('/companies-hot/get', 'CompaniesController@hotIndex');//热门企业
    Route::post('/companies/infos/complete', 'CompaniesController@infosComplete');
    //企业成员
    Route::post('/companies-users/get', 'CompanyUsersController@index');//公司成员列表
    Route::post('/companies-users/delete', 'CompanyUsersController@destroy');//企业用户退出或删除
    Route::post('/companies-users-administrator/create', 'CompanyUsersController@storeAdmin');//企业添加管理员
    Route::post('/companies-users-administrator/delete', 'CompanyUsersController@destroyAdmin');//企业删除管理员
    Route::post('/companies-users-name/update', 'CompanyUsersController@updateName');//企业成员名称修改
    Route::post('/companies-users-invite', 'CompanyUserInvitesController@store');//生成邀请企业成员token
    Route::post('/companies-users-invite/detail', 'CompanyUserInvitesController@show');//邀请企业成员详情
    Route::post('/companies-users/accept', 'CompanyUsersController@store');//接受企业成员邀请
    Route::post('/company-users/supper-administrator/exchange', 'CompanyUsersController@supperAdministratorExchange');//超管转移


    //天眼查企业查询
    Route::post('/companies-search/get', 'CompaniesController@searchCompany');//企业查询
    Route::post('/companies-search-tyc/get', 'CompaniesController@getSearchCompany');//天眼查
    Route::post('/companies-search-box/get', 'CompaniesController@getSearchCompanyBox');//
    Route::post('/companies-search-create/detail', 'CompaniesController@createCompany');//企业详情

    //用户
    Route::post('/users/get', 'UserController@index');//用户信息

    //匿名分身
    Route::post('/aliases/get', 'AliasesController@index');//随机列表
    Route::post('/aliases/detail', 'AliasesController@show');//匿名详情

    //微信小程序二维码
    Route::post('/wechat/qrcode', 'WeChatController@qrCode');
    Route::post('/wechat/invite-qrcode', 'WeChatController@inviteQrCode');
    //是否订阅
    Route::post('/wechat-user-subscribes/check', 'WechatUserSubscribesController@check');

    //生成分享链接
    Route::post('/share/url/detail', 'ShareUrlsController@show');
    //意见箱海报
    Route::post('/boxes/poster/detail', 'BoxesPosterController@show');
    Route::post('/boxes/poster/update', 'BoxesPosterController@update');
    //企业二维码海报
    Route::post('/companies/poster/detail', 'CompaniesPosterController@show');
    Route::post('/companies/poster/update', 'CompaniesPosterController@update');

    //转让意见箱
    Route::post('/market-companies-boxes/create', 'CompaniesController@marketStore');//创建
    Route::post('/market-companies-boxes/get', 'BoxesController@marketBoxMessage');//详情
    Route::post('/market-companies-boxes/update', 'BoxesController@moveBox');//转让
    Route::post('/market-companies-boxes-code/create', 'WeChatController@companyBoxCode');//转让码

    //操作指引
    Route::post('/guide/get', 'NewsController@guide');
    Route::post('/guide/show', 'NewsController@show');

    //获取go_token
    Route::post('/go-token/detail','GoTokenController@show');
});
