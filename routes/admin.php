<?php

use Illuminate\Support\Facades\Route;

Route::resource('/boxes', 'BoxesController');
Route::resource('/comments', 'CommentsController');
Route::resource('/replies', 'RepliesController');

Route::post('/adminLogin', 'AdminsController@login');//登录
Route::post('image/create', 'ImagesController@create');//

Route::post('company/automatic-out', 'CompanyController@automatic');//

//支付回调
Route::any('alipay/return', 'PayController@aliPayReturn');//
Route::any('alipay/notify', 'PayController@aliPayNotify');//

Route::any('logout', function () {
    Artisan::call('cache:clear');
    return 'login';
});

Route::any('test', 'TestController@test');//

Route::any('snake', 'GamController@snake');//
Route::any('snakeStart', 'GamController@snakeStart');//

Route::middleware('auth:sanctum')->group(function () {

    //首页
    Route::post('/index/index', 'IndexController@index');
    Route::post('/index/box', 'IndexController@box');
    Route::post('/index/opinion', 'IndexController@opinion');
    Route::post('/index/userThirty', 'IndexController@userThirty');

    //企业中心
    Route::post('/company/get', 'CompanyController@apply');
    Route::post('/company/examine', 'CompanyController@check');//企业审核
    Route::post('/company/list', 'CompanyController@index');//企业列表
    Route::post('/company/update', 'CompanyController@update');//企业更新
    Route::post('/company/message', 'CompanyController@message');//企业详情
    Route::post('/company/box', 'CompanyController@companyBox');//企业意见箱
    Route::post('/company/opinion', 'CompanyController@opinion');//企业收到意见
    Route::post('/company/member', 'CompanyController@member');//企业成员

    Route::post('company/allList', 'CompanyController@select');//企业搜索列表
    Route::post('company/tycSelect', 'CompanyController@tycSelect');//天眼查企业搜索列表
    Route::post('company/getTycMessage', 'CompanyController@getTycMessage');//天眼查企业详情信息
    Route::post('company/hot', 'CompanyController@hot');//热门企业列表
    Route::post('company/updateHot', 'CompanyController@updateHot');//热门企业列表编辑
    Route::post('company/deleteHot', 'CompanyController@deleteHot');//热门企业列表编辑
    Route::post('company/switchHot', 'CompanyController@switchHot');//企业状态编辑
    Route::post('company/automatic', 'CompanyController@automatic');//


    //用户
    Route::post('/users/get', 'UserController@index');
    Route::post('/users/detail', 'UserController@show');
    Route::post('/users/box', 'UserController@box');
    Route::post('/users/opinion', 'UserController@opinion');
    Route::post('/users/transaction/get', 'UserController@getUserTransaction');

    //banner管理
    Route::post('/banners/get', 'BannersController@index');
    Route::post('/banners/update', 'BannersController@update');
    Route::post('/banners/delete', 'BannersController@destroy');
    Route::post('/banners/create', 'BannersController@store');
    Route::post('/banners/detail', 'BannersController@show');
    Route::post('/banners/switch/status', 'BannersController@switchStatus');
    //FAQ管理
    Route::post('/faq/get', 'FaqController@index');
    Route::post('/faq/update', 'FaqController@update');
    Route::post('/faq/delete', 'FaqController@destroy');
    Route::post('/faq/create', 'FaqController@store');
    Route::post('/faq/detail', 'FaqController@get');
    Route::post('/faq/switch/status', 'FaqController@switchStatus');


    //内部用户管理
    Route::post('internalUser/get', 'InternalUsersController@index');
    Route::post('internalUser/delete', 'InternalUsersController@destroy');
    Route::post('internalUser/create', 'InternalUsersController@store');

    //官网在使用我们的产品公司Logo管理
    Route::post('/customer-companies-logo/get', 'CustomerCompanyLogosController@index');
    Route::post('/customer-companies-logo/update', 'CustomerCompanyLogosController@update');
    Route::post('/customer-companies-logo/create', 'CustomerCompanyLogosController@store');
    Route::post('/customer-companies-logo/delete', 'CustomerCompanyLogosController@destroy');
    Route::post('/customer-companies-logo/switch/status', 'CustomerCompanyLogosController@switchStatus');


    //微信公众号自定义菜单
    Route::post('/wechat/index', 'AppletsController@index');
    Route::post('/wechat/update', 'AppletsController@update');


    //意见箱
    Route::any('box/index', 'BoxesController@index');
    Route::post('box/explode', 'BoxesController@explode');
    Route::post('box/get', 'BoxesController@show');
    Route::post('box/getOpinion', 'BoxesController@opinion');

    //意见中心
    Route::post('/opinion/index', 'CommentsController@index');
    Route::post('/opinion/explode', 'CommentsController@explode');
    Route::post('/opinion/get', 'CommentsController@show');

    //意见回复
    Route::post('/reply/index', 'RepliesController@index');
    Route::post('/reply/explode', 'RepliesController@explode');
    Route::post('/reply/get', 'RepliesController@get');

    //意见箱
    Route::any('/box/index', 'BoxesController@index');
    Route::post('/box/explode', 'BoxesController@explode');
    Route::post('/box/get', 'BoxesController@show');
    Route::post('/box/getOpinion', 'BoxesController@opinion');

    //热门意见箱
    Route::post('/box-hots/get', 'BoxesHotController@index');
    Route::post('/box-hots/create', 'BoxesHotController@store');
    Route::post('/box-hots/delete', 'BoxesHotController@destroy');
    Route::post('/box-hots/search', 'BoxesHotController@search');
    Route::post('/box-hots/switch/status', 'BoxesHotController@switchStatus');

    //内部用户管理
    Route::post('internalUser/get', 'InternalUsersController@index');
    Route::post('internalUser/delete', 'InternalUsersController@destroy');
    Route::post('internalUser/create', 'InternalUsersController@store');

    //官网在使用我们的产品公司Logo管理
    Route::post('/customer-companies-logo/get', 'CustomerCompanyLogosController@index');
    Route::post('/customer-companies-logo/update', 'CustomerCompanyLogosController@update');
    Route::post('/customer-companies-logo/create', 'CustomerCompanyLogosController@store');
    Route::post('/customer-companies-logo/delete', 'CustomerCompanyLogosController@destroy');
    Route::post('/customer-companies-logo/switch/status', 'CustomerCompanyLogosController@switchStatus');
    //小程序皮肤管理
    Route::post('/applet-skin/get', 'AppletsController@skin');
    Route::post('/applet-skin/create', 'AppletsController@setSkin');

    //消息中心
    Route::post('/message-template/index', 'MessageController@template');
    Route::post('/message-template/get', 'MessageController@getTemplate');
    Route::post('/message-template/update', 'MessageController@templateStore');
    Route::post('/message-log/get', 'MessageController@messageLog');
    Route::post('/system-log/get', 'MessageController@systemLog');//系统热值


    //菜单管理
    Route::post('admin/createMenu', 'AdminsController@createMenu');
    Route::post('admin/listMenu', 'AdminsController@listMenu');
    Route::post('admin/deleteMenu', 'AdminsController@deleteMenu');

    //管理员中心
    Route::post('admin/show', 'AdminsController@show');
    Route::post('admin/update', 'AdminsController@update');
    Route::post('admin/get', 'AdminsController@index'); //管理员列表
    Route::post('admin/create', 'AdminsController@create');
    Route::post('admin/delete', 'AdminsController@delete');
    Route::post('admin/menu', 'AdminsController@menu');

    //菜单管理
    Route::post('admin-menus/create', 'AdminsController@createMenu');
    Route::post('admin-menus/get', 'AdminsController@listMenu');
    Route::post('admin-menus/delete', 'AdminsController@deleteMenu');

    //角色管理
    Route::post('admin-roles/get', 'AdminsController@role');
    Route::post('admin-roles/create', 'AdminsController@roleCreate');
    Route::post('admin-roles-menus/get', 'AdminsController@roleMenu');
    Route::post('admin-roles/update', 'AdminsController@updateRole');
    Route::post('admin-roles/delete', 'AdminsController@deleteRole');

    //交易中心
    Route::post('transaction/get', 'TransactionController@index');
    Route::post('transaction/show', 'TransactionController@show');
    Route::post('transaction/refuse', 'TransactionController@refuse');
    Route::post('transaction/order/get', 'TransactionController@orderList');
    Route::post('transaction/order/show', 'TransactionController@orderMessage');
    Route::post('transaction/data/get', 'TransactionController@dataIndex');
    Route::any('message/count', 'TransactionController@countNum');

    //提现
    Route::post('alipay/carry', 'PayController@aliPayCarry');

    //文章管理
    Route::post('articles/get', 'ArticlesController@index');
    Route::post('articles/detail', 'ArticlesController@show');
    Route::post('articles/create', 'ArticlesController@store');
    Route::post('articles/delete', 'ArticlesController@destroy');
    Route::post('articles/getAll', 'ArticlesController@all');
    Route::post('articles/switch', 'ArticlesController@switch');

    //

});
