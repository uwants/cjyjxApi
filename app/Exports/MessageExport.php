<?php

namespace App\Exports;

use App\Models\Comment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MessageExport implements WithHeadings, FromCollection, WithColumnWidths
{
    public int $boxId = 0;

    /**
     * MessageExport constructor.
     * @param int $boxId
     */
    public function __construct(int $boxId)
    {
        $this->boxId = $boxId;
    }

    public function collection()
    {
        $model = Comment::query()
            ->select(DB::raw("'one' as name"), 'box_id', 'user_id', 'content',
                DB::raw("'image1' as image1"),
                DB::raw("'image2' as image2"),
                DB::raw("'image3' as image3"),
                DB::raw("'video' as video"),
                'id', 'created_at')
            ->with('user')
            ->where('box_id', $this->boxId)
            ->orderBy('id', 'desc')
            ->get();
        foreach ($model as &$val) {
            $val['name'] = @$val->user->alias_name;
            $val['box_id'] = date($val->created_at);
            $val['user_id'] = Arr::get($val->content, 'content');
            $imageList = Arr::get($val->content, 'images');
            $video = Arr::get($val->content, 'videos');
            $val['image1'] = Arr::get($imageList, '0');
            $val['image2'] = Arr::get($imageList, '1');
            $val['image3'] = Arr::get($imageList, '2');
            $val['video'] = Arr::get($video, '0');
            unset($val['id']);
            unset($val['content']);
            unset($val['created_at']);
            unset($val['user']);
        }
        Log::info('信息', [$model]);
        return $model;
    }

    public function headings(): array
    {
        return ['昵称', '提出时间', '提出内容', '图片1', '图片2', '图片3', '视频'];
    }

    public function ColumnWidths(): array
    {
        return ['A'=>25,'B'=>25,'C'=>40,'D'=>40,'E'=>40,'F'=>40,'G'=>40];
    }


}
