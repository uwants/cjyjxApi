<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRolePermission extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'role_id',
        'menu_id',
        'parent_id',
        'level'
    ];

    public function menu()
    {
        return $this->hasOne(AdminMenu::Class, 'id', 'menu_id');
    }
}
