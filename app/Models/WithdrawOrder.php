<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class WithdrawOrder extends Model
{
    use HasFactory, SoftDeletes;

    const success = 'SUCCESS';
    const wait = 'wait';
    const WAIT = 'WAIT';
    const reject = 'REJECT';

    private static array $statusDesc = [
        self::success => '成功',
        self::wait => '等待审核',
        self::WAIT => '等待审核',
        self::reject => '拒绝',

    ];

    private static array $aliPayOrderState = [
        'SUCCESS' => '提现成功',
        'PAYEE_NOT_EXIST' => '提现失败'
    ];
    protected $fillable = [
        'id',
        'order_id',
        'amount',
        'to_user_id',
        'to_user',
        'alipay_user_id',
        'alipay_user_name',
        'alipay_order_id',
        'alipay_order_state',
        'alipay_fund_order_id',
        'pay_type',
        'state',
        'review_user',
        'reject_reason',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getWithdrawOrderAttribute()
    {
        return Arr::get(self::$statusDesc, $this->getOriginal('state'));
    }

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getWithdrawOrderUserAttribute()
    {
        if (!empty($this->getOriginal('alipay_order_state'))) {
            return Arr::get(self::$aliPayOrderState, $this->getOriginal('alipay_order_state'));
        } else {
            return '--';
        }

    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }
}
