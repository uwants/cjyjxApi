<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkWeiXinCompanyCorp extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'company_id',
        'corp_id',
        'permanent_code',
        'agentid',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
