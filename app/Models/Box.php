<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Box extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'company_id',
        'user_id',
        'name',
        'suspend',
        'once',
        'open',
        'transfer',
        'transfer_content',
        'all',
        'system',
        'show',
        'content',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $casts = [
        'content' => 'array',
        'transfer_content' => 'array',
    ];

    /**
     * 意见箱创建者、管理员、企业超级管理员有权限
     * @param Builder $query
     * @param $userId
     */
    public function scopeUpdate(Builder $query, $userId)
    {
        $query->where(function (Builder $query) use ($userId) {
            $query->where('user_id', $userId)
                ->orWhere(function (Builder $query) use ($userId) {
                    $query->where('company_id', '>', 0)
                        ->whereHas('company', function (Builder $query) use ($userId) {
                            $query->where('user_id', $userId);
                        });
                });
        });
    }

    /**
     * 公开的意见箱,公司内部所有人可见的意见箱(未暂停),自已的意见箱
     * @param Builder $query
     * @param $userId
     */
    public function scopeView(Builder $query, $userId)
    {
        $query->where(function (Builder $query) use ($userId) {
            $query
                ->where('suspend', false)
                ->where(function (Builder $query) {
                    $query->where('all', true)
                        ->orWhere('open', true)
                        ->orWhere('show', true);
                })
                ->orWhere(function (Builder $query) use ($userId) {
                    $query->whereHas('box_users', function ($query) use ($userId) {
                        $query->where('user_id', $userId);
                    });
                })
                ->orWhere(function (Builder $query) use ($userId) {
                    $query->whereHas('box_comment_users', function ($query) use ($userId) {
                        $query->where('user_id', $userId);
                    });
                })
                ->orWhere('user_id', $userId);
        });
    }

    /**
     * 自已提过意见的意见箱
     * @param Builder $query
     * @param $userId
     */
    public function scopeComment(Builder $query, $userId)
    {
        $query->whereHas('comments', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        });
    }

    /**
     * 自已的意见箱
     * @param Builder $query
     * @param $userId
     */
    public function scopeOwner(Builder $query, $userId)
    {
        $query->where(function (Builder $query) use ($userId) {
            $query
                ->where('user_id', $userId);
        });
    }

    public function scopeBox_user(Builder $query, $userId)
    {
        $query->where(function (Builder $query) use ($userId) {
            $query->whereHas('box_users', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            });
        });
    }


    public function getCreatedTimeAttribute(): string
    {
        $createTime = Carbon::make($this->getAttribute('created_at'));
        if ($createTime->isToday()) {
            return $createTime->format('H:i');
        }
        if ($createTime->isYesterday()) {
            return '昨天';
        }
        return $createTime->format('Y-m-d');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function box_users(): HasMany
    {
        return $this->hasMany(BoxUser::class, 'box_id', 'id');
    }

    public function box_comment_users(): HasMany
    {
        return $this->hasMany(BoxCommentUser::class);
    }

    public function boxes_follows(): HasMany
    {
        return $this->hasMany(BoxFollow::class, 'box_id', 'id');
    }

    public function box_messages(): HasMany
    {
        return $this->hasMany(BoxMessage::class);
    }
}
