<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TabCard extends Model
{
    use SoftDeletes, HasFactory;

    protected $fillable = [
        'title',
        'image',
        'path',
        'suspend',
        'sort',
        'created_at',
    ];
}
