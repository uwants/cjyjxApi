<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DingTalkBizData extends Model
{
    use HasFactory;

    protected $connection = 'ding_cloud_push';

    protected $table = 'open_sync_biz_data';
}
