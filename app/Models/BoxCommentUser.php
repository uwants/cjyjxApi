<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoxCommentUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'box_member_ref';

    protected $fillable = [
        'user_id',
        'box_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
