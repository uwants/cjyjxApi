<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'type',
        'suspend',
        'image',
        'url',
        'sort',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
