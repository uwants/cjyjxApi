<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class AdminUser extends Authenticatable
{
    use HasFactory, SoftDeletes, HasApiTokens;

    protected $fillable = [
        'id',
        'name',
        'password',
        'avatar',
        'nick_name',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function relationship(){
        return $this->hasOne(AdminRoleRelationship::Class,'admin_id','id');
    }
}
