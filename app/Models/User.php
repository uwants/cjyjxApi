<?php

namespace App\Models;

use App\Services\AliasUser\Models\UpdateOrCreateAliasUserRandomByUserIdRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, HasApiTokens, SoftDeletes;

    const PC = 'PC';
    const H5 = 'H5';
    const APP = 'APP';
    const MP_DINGTALK = 'MP-DINGTALK';
    const MP_WEIXIN = 'MP-WEIXIN';
    const MP_WORKWEIXIN = 'MP-WORKWEIXIN';
    const MP_WEIXIN_BOSSBOX = 'MP-WEIXIN-BOSSBOX';
    const BUSINESS_WEB = 'BUSINESS-WEB';

    private static array $statusDesc = [
        self::PC => 'PC',
        self::H5 => 'H5',
        self::APP => 'APP',
        self::MP_DINGTALK => '钉钉',
        self::MP_WEIXIN => '超级意见箱',
        self::MP_WORKWEIXIN => '企业微信',
        self::MP_WEIXIN_BOSSBOX => '老板意见箱',
        self::BUSINESS_WEB => 'PC',

    ];
    protected $fillable = [
        'mobile',
        'avatar',
        'name',
        'alias_name',
        'alias_avatar',
        'platform',
        'main_company_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getPlatformDescAttribute()
    {
        if (empty($this->getOriginal('platform'))) {
            return '超级意见箱';
        } else {
            return Arr::get(self::$statusDesc, $this->getOriginal('platform'));
        }

    }

    public function getAliasNamesAttribute(): string
    {
        if (empty($this->alias_name)) {
            $updateOrCreateAliasUserRandomByUserIdRequest = new UpdateOrCreateAliasUserRandomByUserIdRequest();
            $updateOrCreateAliasUserRandomByUserIdRequest->setUserId($this->getKey());
            \App\Facades\AliasUser::UpdateOrCreateAliasUserRandomByUserId($updateOrCreateAliasUserRandomByUserIdRequest);
        }
        return empty($this->alias_name) ? "匿名用户" : $this->alias_name;
    }

    public function getAliasAvatarsAttribute(): string
    {
        if (empty($this->alias_avatar)) {
            $updateOrCreateAliasUserRandomByUserIdRequest = new UpdateOrCreateAliasUserRandomByUserIdRequest();
            $updateOrCreateAliasUserRandomByUserIdRequest->setUserId($this->getKey());
            \App\Facades\AliasUser::UpdateOrCreateAliasUserRandomByUserId($updateOrCreateAliasUserRandomByUserIdRequest);
        }
        return empty($this->alias_avatar) ? config('common.default_avatar') : $this->alias_avatar;
    }

    public function alias_user(): HasOne
    {
        return $this->hasOne(AliasUser::class);
    }

    public function boxes(): HasMany
    {
        return $this->hasMany(Box::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function reply(): HasMany
    {
        return $this->hasMany(Reply::class);
    }

    public function company_user(): HasMany
    {
        return $this->hasMany(CompanyUser::class);
    }

    public function companyUser()
    {
        return $this->hasMany(CompanyUser::class, 'user_id', 'id');
    }

    public function replies(): HasMany
    {
        return $this->hasMany(Reply::class);
    }

    public function userUnionid()
    {
        return $this->hasOne(WeiXinUser::class, 'user_id', 'id');
    }

    public function company()
    {
        return $this->hasMany(Company::class, 'user_id', 'id');
    }

    public function platform()
    {
        return $this->hasOne(UserPlatform::class, 'user_id', 'id');
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id', 'id');
    }


}
