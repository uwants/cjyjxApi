<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPoster extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'company_id',
        'poster',
        'created_at',
        'updated_at',
        'delete_at',
    ];
}
