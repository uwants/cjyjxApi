<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DingTalkAuthCorp extends Model
{
    use HasFactory;

    protected $connection = 'isv_ding_talk_biz';

    protected $table = 'authed_corp';
}
