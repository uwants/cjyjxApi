<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAddress extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'company_id',
        'province',
        'city',
        'area',
        'info',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function getStrAttribute()
    {
        return $this->getAttribute('province') . $this->getAttribute('city') . $this->getAttribute('area') . $this->getAttribute('info');
    }
}
