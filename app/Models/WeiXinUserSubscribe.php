<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WeiXinUserSubscribe extends Model
{
    use SoftDeletes;

    public $fillable = [
        'unionid',
        'openid'
    ];
}
