<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DingTalkUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'corp_id',
        'union_id',
        'ding_talk_user_id',
        'device_id',
        'sys',
        'sys_level',
        'associated_union_id',
        'name',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
