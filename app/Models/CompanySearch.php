<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanySearch extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'company_dictionaries';

    public $fillable = [
        'request',
        'response'
    ];

    public $casts = [
        'request' => 'array',
        'response' => 'array'
    ];
}
