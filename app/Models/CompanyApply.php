<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyApply extends Model
{
    use SoftDeletes, HasFactory;

    public $fillable = [
        'id',
        'box_id',
        'company_id',
        'user_id',
        'id_card_face',
        'id_card_positive',
        'business',
        'reject_reason'
    ];

    public function box()
    {
        return $this->belongsTo(Box::class);
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getCompanyHasAttribute(): string
    {
        $company = company::withTrashed()
            ->select('name')
            ->where('id', $this->getAttribute('company_id'))
            ->first();
        $companyList = company::query()
            ->with('user')
            ->where('name', $company->name)
            ->get();
        $companyString = '';
        foreach ($companyList as $key => $val) {
            if ($val->status == 'STATUS_CERTIFIED_PASS' && !empty($val->user->name)) {
                $companyString .= '企业：' . @$val->name . '【认证人：' . @$val->user->name . '；认证号码：' . @$val->user->mobile . '】';
            }
        }
        return $companyString;
    }
}
