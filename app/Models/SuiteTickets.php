<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuiteTickets extends Model
{
    use SoftDeletes;

    public $fillable = [
        'id',
        'corp_id',
        'suite_ticket',
        'biz_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
