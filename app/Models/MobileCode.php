<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MobileCode extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'mobile',
        'code',
        'expired_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
