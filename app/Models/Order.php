<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    const SUCCESS = 'SUCCESS';
    const REFUND = 'REFUND';
    const NOTPAY = 'NOTPAY';
    const CLOSED = 'CLOSED';
    const REVOKED = 'REVOKED';
    const USERPAYING = 'USERPAYING';
    const PAYERROR = 'PAYERROR';

    private static array $statusDesc = [
        self::SUCCESS => '支付成功',
        self::REFUND => '转入退款',
        self::NOTPAY => '未支付',
        self::CLOSED => '已关闭',
        self::REVOKED => '已撤销',
        self::USERPAYING => '用户支付中',
        self::PAYERROR => '支付失败',

    ];

    protected $fillable = [
        'id',
        'order_id',
        'prepay_id',
        'opinion_id',
        'pay_type',
        'from_user_id',
        'from_user',
        'to_user_id',
        'to_user',
        'amount',
        'order_state',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getOrderStatesAttribute()
    {
        return Arr::get(self::$statusDesc, $this->getOriginal('order_state'));
    }

    public function opinion()
    {
        return $this->hasOne(Comment::class, 'id', 'opinion_id');
    }

    public function fromUser()
    {
        return $this->hasOne(User::class, 'id', 'from_user_id');
    }

    public function toUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }
}
