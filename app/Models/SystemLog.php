<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Laravel\Sanctum\HasApiTokens;

class SystemLog extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    protected $fillable = [
        'id',
        'type',
        'message',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    private static array $statusDesc = [
        'merge_company' => '合并企业信息',
        'admin_login'=>'登录'
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getTypeDescAttribute()
    {
        return Arr::get(self::$statusDesc, $this->getOriginal('type'));
    }
}
