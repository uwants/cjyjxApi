<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class AdminArticle extends Model
{
    use HasFactory, SoftDeletes;

    private static array $statusDesc = [
        'INSTRUCTION_WEIXIN' => '微信小程序',
        'INSTRUCTION_DING' => '钉钉',
        'INSTRUCTION_H5' => 'H5',
    ];

    protected $fillable = [
        'id',
        'type',
        'title',
        'content',
        'suspend',
        'sort',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getArticleTypeAttribute()
    {
        $list = explode(',', $this->getOriginal('type'));
        $listMessage = '';
        foreach ($list as $key => $val) {
            $listMessage .= Arr::get(self::$statusDesc, $val) . '、';
        }
        return trim($listMessage,'、');
    }
}
