<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = [
        'id',
        'user_id',
        'total_amount',
        'frozen_amount',
        'balance',
        'history_balance',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
