<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRole extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'remark',
        'created_admin_id',
    ];

    public function adminUser(){
        return $this->hasOne(AdminUser::Class,'id','created_admin_id');
    }
}
