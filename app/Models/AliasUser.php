<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class AliasUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'alias_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function alias():BelongsTo
    {
        return $this->belongsTo(Alias::class);
    }
}
