<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    // 认证通过
    const STATUS_CERTIFIED_PASS = 'STATUS_CERTIFIED_PASS';
    // 未认证
    const STATUS_CERTIFIED_DEFAULT = 'STATUS_CERTIFIED_PRE_COMMIT';
    // 审核中
    const STATUS_CERTIFIED_AUDIT = 'STATUS_CERTIFIED_AUDIT';
    // 不通过
    const STATUS_CERTIFIED_FAILED = 'STATUS_CERTIFIED_FAILED';

    protected $fillable = [
        'id',
        'name',
        'user_id',
        'logo',
        'status',
        'profile',
        'poster',
        'industry',
        'wx_poster',
        'province',
        'city',
        'area',
        'address',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getAddressStrAttribute()
    {
        return $this->getAttribute('province') . $this->getAttribute('city') . $this->getAttribute('area') . $this->getAttribute('address');
    }

    public function company_users()
    {
        return $this->hasMany(CompanyUser::class);
    }

    public function boxes()
    {
        return $this->hasMany(Box::class);
    }

    public function company_apply()
    {
        return $this->hasOne(CompanyApply::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function platform()
    {
        return $this->hasOne(CompanyPlatform::class, 'company_id', 'id');
    }

    public function company_address(): HasOne
    {
        return $this->hasOne(CompanyAddress::class);
    }
}
