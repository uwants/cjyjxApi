<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRoleRelationship extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'admin_id',
        'created_admin_id',
        'role_id',
    ];

    public function role(){
        return $this->hasOne(AdminRole::class,'id','role_id');
    }
}
