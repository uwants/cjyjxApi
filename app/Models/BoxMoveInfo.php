<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoxMoveInfo extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'box_id',
        'new_user_id',
        'old_user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function box()
    {
        $this->belongsTo(Box::class);
    }
}
