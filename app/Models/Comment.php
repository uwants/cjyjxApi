<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'opinions';

    public $fillable = [
        'box_id',
        'user_id',
        'content',
        'adopt',
        'company_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $casts = [
        'content' => 'array',
    ];

    public function box()
    {
        return $this->belongsTo(Box::class);
    }

    public function reply(): HasOne
    {
        return $this->hasOne(Reply::class);
    }

    public function replies(): HasMany
    {
        return $this->hasMany(Reply::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 关联的意见箱是他可见的
     * @param Builder $query
     * @param $userId
     */
    public function scopeBoxView(Builder $query, $userId)
    {
        $query->whereHas('box', function (Builder $query) use ($userId) {
            $query->scopes(['view' => $userId]);
        });
    }

    public function getCreatedTimeAttribute(): string
    {
        $createTime = Carbon::make($this->getAttribute('created_at'));
        if ($createTime->isToday()) {
            return $createTime->format('H:i');
        }
        if ($createTime->isYesterday()) {
            return '昨天';
        }
        return $createTime->format('Y-m-d');
    }

    public function aliasUser()
    {
        return $this->hasOne(AliasUser::class, 'user_id', 'user_id');
    }

    public function getCommentRewardAttribute()
    {
        return Order::query()
            ->where('opinion_id', $this->getAttribute('id'))
            ->where('order_state', 'SUCCESS')
            ->where('to_user_id', $this->getAttribute('user_id'))
            ->sum('amount');

    }

    public function getCommentRewardAllAttribute()
    {
        return Order::query()
            ->whereIn('opinion_id', function ($query) {
                $query->select('id')->from('opinions')
                    ->where('user_id', $this->getAttribute('user_id'))
                    ->where('box_id', $this->getAttribute('box_id'));
            })
            ->where('order_state', 'SUCCESS')
            ->where('to_user_id', $this->getAttribute('user_id'))
            ->sum('amount');
    }
}
