<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class WeiXinUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'open_id',
        'union_id',
        'app_id',
        'session_key',
        'app_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    private static array $statusDesc = [
        'wx99942da2b8166c31' => '超级意见箱',
        'wxe54ba2e5a7ae2211' => '老板意见箱',
    ];

    /**
     * 获取状态描述
     * @return mixed
     */
    public function getAppletDescAttribute()
    {
        return Arr::get(self::$statusDesc, $this->getOriginal('app_id'), '');
    }

    public function wapOpenId()
    {
        return $this->hasOne(WeiXinUserSubscribe::class, 'unionid', 'union_id');
    }
}
