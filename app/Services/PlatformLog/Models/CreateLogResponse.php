<?php


namespace App\Services\PlatformLog\Models;


class CreateLogResponse
{
    private int $logId = 0;

    /**
     * @return int
     */
    public function getLogId(): int
    {
        return $this->logId;
    }

    /**
     * @param int $logId
     */
    public function setLogId(int $logId): void
    {
        $this->logId = $logId;
    }

}
