<?php

namespace App\Services\PlatformLog;

use App\Services\PlatformLog\Models\CreateLogRequest;
use App\Services\PlatformLog\Models\CreateLogResponse;
use App\Models\User;
use App\Models\Company;

class PlatformLogService
{
    public function createUserPlatformLog(CreateLogRequest $request): CreateLogResponse
    {
        User::query()
            ->where('id', $request->getUserId())
            ->update([
                'platform' => $request->getPlatform()
            ]);
        $createLogResponse = new CreateLogResponse();
        $createLogResponse->setLogId($request->getUserId());
        return $createLogResponse;
    }

    public function createCompanyPlatformLog(CreateLogRequest $request): CreateLogResponse
    {
         Company::query()
            ->where('id',$request->getCompanyId())
            ->update(['platform' => $request->getPlatform()]);
        $createLogResponse = new CreateLogResponse();
        $createLogResponse->setLogId($request->getCompanyId());
        return $createLogResponse;
    }
}
