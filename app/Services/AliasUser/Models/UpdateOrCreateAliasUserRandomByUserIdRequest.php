<?php


namespace App\Services\AliasUser\Models;


class UpdateOrCreateAliasUserRandomByUserIdRequest
{
    private int $userId = 0;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

}