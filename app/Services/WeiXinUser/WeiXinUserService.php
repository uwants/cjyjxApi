<?php

namespace App\Services\WeiXinUser;

use App\Facades\WeiXinApi;
use App\Models\WeiXinUser;
use App\Services\WeiXinApi\Models\Code2SessionRequest;
use App\Services\WeiXinUser\Models\UpdateOrCreateWeiXinUserRequest;
use App\Services\WeiXinUser\Models\UpdateOrCreateWeiXinUserResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WeiXinUserService
{
    /**
     * @param UpdateOrCreateWeiXinUserRequest $request
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function updateOrCreateWeiXinUser(UpdateOrCreateWeiXinUserRequest $request): UpdateOrCreateWeiXinUserResponse
    {
        $response = new UpdateOrCreateWeiXinUserResponse();
        $insertData = [
            'union_id' => $request->getUnionId(),
            'open_id' => $request->getOpenId(),
        ];
        $updateData = array_filter([
            'session_key' => $request->getSessionKey(),
            'user_id' => $request->getUserId(),
            'app_id' => config('common.mpWeiXin.appId')
        ]);
        if ($request->getAuthCode()) {//登录
            $code2SessionRequest = new Code2SessionRequest();
            $code2SessionRequest->setCode($request->getAuthCode());
            $code2SessionResponse = WeiXinApi::code2Session($code2SessionRequest);
            $request->setUnionId($code2SessionResponse->getUnionId());
            $request->setOpenId($code2SessionResponse->getOpenId());
            $insertData = [
                'union_id' => $code2SessionResponse->getUnionId(),
                'open_id' => $code2SessionResponse->getOpenId(),
            ];
            Arr::set($updateData, 'session_key', $code2SessionResponse->getSessionKey());
            Arr::set($updateData, 'open_id', $code2SessionResponse->getOpenId());
            Arr::set($updateData, 'app_id', config('common.mpWeiXin.appId'));
        }
        Log::info('登录返回信息：', [$insertData]);
        $model = WeiXinUser::withTrashed()
            ->updateOrCreate($insertData, $updateData);
        if ($model->trashed()) {
            $model->restore();
        }
        $response->setUserId((int)$model->getOriginal('user_id'));
        $response->setOpenId((string)$model->getOriginal('open_id'));
        $response->setUnionId((string)$model->getOriginal('union_id'));
        $response->setSessionKey((string)$model->getOriginal('session_key'));
        return $response;
    }

    public function checkMessage(UpdateOrCreateWeiXinUserRequest $request)
    {
        $model = WeiXinUser::withTrashed()
            ->where('union_id', $request->getUnionId())
            ->count();
        if ($model > 1) {
            $modelOne = WeiXinUser::withTrashed()
                ->where('union_id', $request->getUnionId())
                ->where('open_id', $request->getOpenId())
                ->where('user_id', '>',0)
                ->first();
            $modelTwo = WeiXinUser::withTrashed()
                ->where('union_id', $request->getUnionId())
                ->where('app_id', config('common.mpWeiXin.appId'))
                ->where('user_id', '>',0)
                ->first();
            if (!empty($modelOne->id)) {
                $insertData = [
                    'union_id' => $request->getUnionId(),
                    'open_id' => $request->getOpenId(),
                ];
            } elseif (!empty($modelTwo->id)) {
                $insertData = [
                    'union_id' => $request->getUnionId(),
                    'app_id' => config('common.mpWeiXin.appId'),
                ];
            } else {
                $insertData = [
                    'union_id' => $request->getUnionId(),
                ];
            }
        } else {
            $insertData = [
                'union_id' => $request->getUnionId(),
            ];
        }
        return $insertData;
    }
}
