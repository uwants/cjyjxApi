<?php

namespace App\Services\WeiXinUser\Models;

class UpdateOrCreateWeiXinUserResponse
{
    private int $id = 0;

    private int $userId = 0;

    private string $openId = '';

    private string $unionId = '';

    private string $sessionKey = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function setId(int $id): UpdateOrCreateWeiXinUserResponse
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function setUserId(int $userId): UpdateOrCreateWeiXinUserResponse
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function setOpenId(string $openId): UpdateOrCreateWeiXinUserResponse
    {
        $this->openId = $openId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function setUnionId(string $unionId): UpdateOrCreateWeiXinUserResponse
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     * @return UpdateOrCreateWeiXinUserResponse
     */
    public function setSessionKey(string $sessionKey): UpdateOrCreateWeiXinUserResponse
    {
        $this->sessionKey = $sessionKey;
        return $this;
    }
}
