<?php

namespace App\Services\WeiXinUser\Models;

class UpdateOrCreateWeiXinUserRequest
{
    private int $userId = 0;

    private string $openId = '';

    private string $unionId = '';

    private string $sessionKey = '';

    private string $authCode = '';

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateWeiXinUserRequest
     */
    public function setUserId(int $userId): UpdateOrCreateWeiXinUserRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     * @return UpdateOrCreateWeiXinUserRequest
     */
    public function setOpenId(string $openId): UpdateOrCreateWeiXinUserRequest
    {
        $this->openId = $openId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return UpdateOrCreateWeiXinUserRequest
     */
    public function setUnionId(string $unionId): UpdateOrCreateWeiXinUserRequest
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     * @return UpdateOrCreateWeiXinUserRequest
     */
    public function setSessionKey(string $sessionKey): UpdateOrCreateWeiXinUserRequest
    {
        $this->sessionKey = $sessionKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthCode(): string
    {
        return $this->authCode;
    }

    /**
     * @param string $authCode
     * @return UpdateOrCreateWeiXinUserRequest
     */
    public function setAuthCode(string $authCode): UpdateOrCreateWeiXinUserRequest
    {
        $this->authCode = $authCode;
        return $this;
    }
}
