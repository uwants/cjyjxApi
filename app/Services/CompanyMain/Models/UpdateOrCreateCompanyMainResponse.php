<?php

namespace App\Services\CompanyMain\Models;

class UpdateOrCreateCompanyMainResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UpdateOrCreateCompanyMainResponse
     */
    public function setId(int $id): UpdateOrCreateCompanyMainResponse
    {
        $this->id = $id;
        return $this;
    }
}
