<?php

namespace App\Services\CompanyMain\Models;

class GetCompanyIdResponse
{
    private int $companyId = 0;

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return GetCompanyIdResponse
     */
    public function setCompanyId(int $companyId): GetCompanyIdResponse
    {
        $this->companyId = $companyId;
        return $this;
    }
}
