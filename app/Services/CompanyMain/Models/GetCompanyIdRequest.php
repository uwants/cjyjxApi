<?php

namespace App\Services\CompanyMain\Models;

class GetCompanyIdRequest
{
    private int $userId = 0;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return GetCompanyIdRequest
     */
    public function setUserId(int $userId): GetCompanyIdRequest
    {
        $this->userId = $userId;
        return $this;
    }
}
