<?php

namespace App\Services\CompanyMain\Models;

class UpdateOrCreateCompanyMainRequest
{
    private int $userId = 0;

    private int $companyId = 0;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateCompanyMainRequest
     */
    public function setUserId(int $userId): UpdateOrCreateCompanyMainRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return UpdateOrCreateCompanyMainRequest
     */
    public function setCompanyId(int $companyId): UpdateOrCreateCompanyMainRequest
    {
        $this->companyId = $companyId;
        return $this;
    }
}
