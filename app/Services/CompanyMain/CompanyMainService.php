<?php

namespace App\Services\CompanyMain;

use App\Models\User;
use App\Models\CompanyUser;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainResponse;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\CompanyMain\Models\GetCompanyIdResponse;
use Exception;
use Illuminate\Support\Facades\Auth;

class CompanyMainService
{
    /**
     * @param UpdateOrCreateCompanyMainRequest $request
     * @return UpdateOrCreateCompanyMainResponse
     * @throws Exception
     */
    public function updateOrCreateCompanyMain(UpdateOrCreateCompanyMainRequest $request): UpdateOrCreateCompanyMainResponse
    {
        $userId = $request->getUserId();
        if (empty($userId)) {
            throw new Exception('用户id不能为空');
        }
        $companyId = $request->getCompanyId();
        if (!empty($companyId)) {
            $isInCompany = CompanyUser::query()
                ->select('id')
                ->where('user_id', $userId)
                ->where('company_id', $companyId)
                ->exists();
            if (!$isInCompany) {
                throw new Exception('成员不在该主企业');
            }
        }
        User::query()
            ->where('id', $userId)
            ->update([
                'main_company_id' => $companyId,
            ]);
        $response = new UpdateOrCreateCompanyMainResponse();
        $response->setId($userId);
        return $response;
    }

    /**
     * @param GetCompanyIdRequest $request
     * @return GetCompanyIdResponse
     * @throws Exception
     */
    public function getCompanyId(GetCompanyIdRequest $request): GetCompanyIdResponse
    {
        $userId = $request->getUserId() ?: Auth::id();
        $model = User::query()
            ->where('id', $userId)
            ->first();

        $response = new GetCompanyIdResponse();
        if (!empty($model->main_company_id)) {
            $exist = CompanyUser::query()
                ->where('user_id', $userId)
                ->where('company_id', $model->getAttribute('main_company_id'))
                ->exists();
            if ($exist) {
                $response->setCompanyId($model->getOriginal('main_company_id'));
                return $response;
            }
        }
        $companyUser = CompanyUser::query()
            ->select('company_id')
            ->where('user_id', $userId)
            ->latest()
            ->first();
        $updateOrCreateCompanyMainRequest = new UpdateOrCreateCompanyMainRequest();
        if (!empty($companyUser)) {
            $updateOrCreateCompanyMainRequest->setCompanyId($companyUser->getOriginal('company_id'));
            $updateOrCreateCompanyMainRequest->setUserId($userId);
            $this->updateOrCreateCompanyMain($updateOrCreateCompanyMainRequest);
            $response->setCompanyId($companyUser->getOriginal('company_id'));
            return $response;
        }
        $updateOrCreateCompanyMainRequest->setCompanyId(0);
        $updateOrCreateCompanyMainRequest->setUserId($userId);
        $this->updateOrCreateCompanyMain($updateOrCreateCompanyMainRequest);
        return $response;
    }

}
