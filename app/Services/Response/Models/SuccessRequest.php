<?php

namespace App\Services\Response\Models;

class SuccessRequest extends Request
{
    public function __construct()
    {
        $this->setCode(0);
        $this->setMessage('success');
    }
}
