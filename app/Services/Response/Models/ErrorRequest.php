<?php

namespace App\Services\Response\Models;

class ErrorRequest extends Request
{
    public function __construct()
    {
        $this->setCode(1);
        $this->setMessage('error');
    }
}
