<?php


namespace App\Services\Response\Models;


class PageRequest extends Request
{

    private int $page = 1;
    private int $pageSize = 10;
    private int $totalPage = 0;
    private int $totalNumber = 0;
    private array $list = [];
    private $extra = [];

    public function __construct(int $page = 1, int $pageSize = 10)
    {
        $this->page = max(1, $page);
        $this->pageSize = min(100, max(1, $pageSize));
    }

    public function execute()
    {
        $this->setData([
            'pageInfo' => [
                'page' => $this->getPage(),
                'pageSize' => $this->getPageSize(),
                'totalPage' => $this->getTotalPage(),
                'totalNumber' => $this->getTotalNumber()
            ],
            'list' => $this->getList(),
            'extra' => $this->getExtra()
        ]);
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param array $list
     */
    public function setList(array $list): void
    {
        $this->list = $list;
    }

    /**
     * @return int
     */
    public function getTotalPage(): int
    {
        return $this->totalPage;
    }

    /**
     * @param int $totalPage
     */
    public function setTotalPage(int $totalPage): void
    {
        $this->totalPage = $totalPage;
    }

    /**
     * @return int
     */
    public function getTotalNumber(): int
    {
        return $this->totalNumber;
    }

    /**
     * @param int $totalNumber
     */
    public function setTotalNumber(int $totalNumber): void
    {
        $this->totalNumber = $totalNumber;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    /**
     * @param array $extra
     */
    public function setExtra(array $extra): void
    {
        $this->extra = $extra;
    }

}
