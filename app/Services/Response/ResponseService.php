<?php

namespace App\Services\Response;

use App\Services\Response\Models\ErrorRequest;
use App\Services\Response\Models\JsonRequest;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ResponseService
{
    /**
     * @param SuccessRequest $request
     * @return HttpResponseException
     */
    public function success(SuccessRequest $request): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'code' => 0,
            'message' => $request->getMessage(),
            'data' => $request->getData()
        ]));
    }

    /**
     * @param ErrorRequest $request
     * @return HttpResponseException
     */
    public function error(ErrorRequest $request): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'code' => max(1, $request->getCode()),
            'message' => $request->getMessage(),
            'data' => $request->getData()
        ]));
    }

    /**
     * @param JsonRequest $request
     * @return HttpResponseException
     */
    public function json(JsonRequest $request): HttpResponseException
    {
        throw new HttpResponseException(response()->json($request->getData()));
    }

    /**
     * @param PageRequest $request
     * @return HttpResponseException
     */
    public function page(PageRequest $request): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'code' => 0,
            'message' => $request->getMessage(),
            'data' => $request->getData()
        ]));
    }
}
