<?php

namespace App\Services\DingTalkApi\Models;

class GetUserRequest
{
    private string $unionId = '';

    private string $accessToken = '';

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return GetUserRequest
     */
    public function setUnionId(string $unionId): GetUserRequest
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetUserRequest
     */
    public function setAccessToken(string $accessToken): GetUserRequest
    {
        $this->accessToken = $accessToken;
        return $this;
    }
}
