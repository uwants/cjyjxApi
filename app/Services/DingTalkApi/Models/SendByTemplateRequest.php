<?php


namespace App\Services\DingTalkApi\Models;


class SendByTemplateRequest
{
    private string $templateId = '';

    private string $agentId = '';

    private string $userList = '';

    private array $data = [];

    private string $accessToken = '';

    /**
     * @return string
     */
    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    /**
     * @param string $templateId
     */
    public function setTemplateId(string $templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @return string
     */
    public function getAgentId(): string
    {
        return $this->agentId;
    }

    /**
     * @param string $agentId
     */
    public function setAgentId(string $agentId): void
    {
        $this->agentId = $agentId;
    }

    /**
     * @return string
     */
    public function getUserList(): string
    {
        return $this->userList;
    }

    /**
     * @param string $userList
     */
    public function setUserList(string $userList): void
    {
        $this->userList = $userList;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

}