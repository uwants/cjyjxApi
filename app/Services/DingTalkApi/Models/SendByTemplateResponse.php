<?php


namespace App\Services\DingTalkApi\Models;


class SendByTemplateResponse
{
    private $taskId = 0;

    /**
     * @return int
     */
    public function getTaskId(): int
    {
        return $this->taskId;
    }

    /**
     * @param int $taskId
     */
    public function setTaskId(int $taskId): void
    {
        $this->taskId = $taskId;
    }
}