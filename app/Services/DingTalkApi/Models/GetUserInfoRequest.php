<?php

namespace App\Services\DingTalkApi\Models;

class GetUserInfoRequest
{
    private string $accessToken = '';

    private string $code = '';

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetUserInfoRequest
     */
    public function setAccessToken(string $accessToken): GetUserInfoRequest
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return GetUserInfoRequest
     */
    public function setCode(string $code): GetUserInfoRequest
    {
        $this->code = $code;
        return $this;
    }
}
