<?php

namespace App\Services\DingTalkApi\Models;

class GetUserAccessTokenResponse
{
    private string $accessToken = '';

    private string $refreshToken = '';

    private int $expireIn = 0;

    private ?string $corpId = null;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return int
     */
    public function getExpireIn(): int
    {
        return $this->expireIn;
    }

    /**
     * @param int $expireIn
     */
    public function setExpireIn(int $expireIn): void
    {
        $this->expireIn = $expireIn;
    }

    /**
     * @return string|null
     */
    public function getCorpId(): ?string
    {
        return $this->corpId;
    }

    /**
     * @param string|null $corpId
     */
    public function setCorpId(?string $corpId): void
    {
        $this->corpId = $corpId;
    }
}
