<?php

namespace App\Services\DingTalkApi\Models;

class GetCorpAccessTokenResponse
{
    private string $accessToken = '';

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetCorpAccessTokenResponse
     */
    public function setAccessToken(string $accessToken): GetCorpAccessTokenResponse
    {
        $this->accessToken = $accessToken;
        return $this;
    }
}
