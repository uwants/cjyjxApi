<?php

namespace App\Services\DingTalkApi\Models;

class GetUserInfoResponse
{
    private string $userId = '';

    private string $deviceId = '';

    private bool $sys = false;

    private int $sysLevel = 0;

    private string $associatedUnionId = '';

    private string $unionId = '';

    private string $name = '';

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return GetUserInfoResponse
     */
    public function setUserId(string $userId): GetUserInfoResponse
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return GetUserInfoResponse
     */
    public function setDeviceId(string $deviceId): GetUserInfoResponse
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSys(): bool
    {
        return $this->sys;
    }

    /**
     * @param bool $sys
     * @return GetUserInfoResponse
     */
    public function setSys(bool $sys): GetUserInfoResponse
    {
        $this->sys = $sys;
        return $this;
    }

    /**
     * @return int
     */
    public function getSysLevel(): int
    {
        return $this->sysLevel;
    }

    /**
     * @param int $sysLevel
     * @return GetUserInfoResponse
     */
    public function setSysLevel(int $sysLevel): GetUserInfoResponse
    {
        $this->sysLevel = $sysLevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssociatedUnionId(): string
    {
        return $this->associatedUnionId;
    }

    /**
     * @param string $associatedUnionId
     * @return GetUserInfoResponse
     */
    public function setAssociatedUnionId(string $associatedUnionId): GetUserInfoResponse
    {
        $this->associatedUnionId = $associatedUnionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return GetUserInfoResponse
     */
    public function setUnionId(string $unionId): GetUserInfoResponse
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return GetUserInfoResponse
     */
    public function setName(string $name): GetUserInfoResponse
    {
        $this->name = $name;
        return $this;
    }
}
