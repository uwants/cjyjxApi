<?php

namespace App\Services\DingTalkApi\Models;

class GetUserResponse
{
    private string $mobile = '';

    private string $nick = '';

    private string $avatarUrl = '';

    private string $openId = '';

    private string $email = '';

    private string $stateCode = '';

    private string $unionId = '';

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return GetUserResponse
     */
    public function setMobile(string $mobile): GetUserResponse
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getNick(): string
    {
        return $this->nick;
    }

    /**
     * @param string $nick
     * @return GetUserResponse
     */
    public function setNick(string $nick): GetUserResponse
    {
        $this->nick = $nick;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatarUrl(): string
    {
        return $this->avatarUrl;
    }

    /**
     * @param string $avatarUrl
     * @return GetUserResponse
     */
    public function setAvatarUrl(string $avatarUrl): GetUserResponse
    {
        $this->avatarUrl = $avatarUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     * @return GetUserResponse
     */
    public function setOpenId(string $openId): GetUserResponse
    {
        $this->openId = $openId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return GetUserResponse
     */
    public function setEmail(string $email): GetUserResponse
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getStateCode(): string
    {
        return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return GetUserResponse
     */
    public function setStateCode(string $stateCode): GetUserResponse
    {
        $this->stateCode = $stateCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return GetUserResponse
     */
    public function setUnionId(string $unionId): GetUserResponse
    {
        $this->unionId = $unionId;
        return $this;
    }
}

