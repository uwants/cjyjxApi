<?php

namespace App\Services\DingTalkApi\Models;

class GetUserByUserIdResponse
{
    private string $name = '';

    private string $avatar = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return GetUserByUserIdResponse
     */
    public function setName(string $name): GetUserByUserIdResponse
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return GetUserByUserIdResponse
     */
    public function setAvatar(string $avatar): GetUserByUserIdResponse
    {
        $this->avatar = $avatar;
        return $this;
    }
}
