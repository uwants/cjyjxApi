<?php

namespace App\Services\DingTalkApi\Models;

class GetCorpAccessTokenRequest
{
    private string $suiteKey = '';

    private string $suiteSecret = '';

    private string $authCorpId = '';

    private string $suiteTicket = '';

    /**
     * @return string
     */
    public function getSuiteKey(): string
    {
        return $this->suiteKey;
    }

    /**
     * @param string $suiteKey
     * @return GetCorpAccessTokenRequest
     */
    public function setSuiteKey(string $suiteKey): GetCorpAccessTokenRequest
    {
        $this->suiteKey = $suiteKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuiteSecret(): string
    {
        return $this->suiteSecret;
    }

    /**
     * @param string $suiteSecret
     * @return GetCorpAccessTokenRequest
     */
    public function setSuiteSecret(string $suiteSecret): GetCorpAccessTokenRequest
    {
        $this->suiteSecret = $suiteSecret;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthCorpId(): string
    {
        return $this->authCorpId;
    }

    /**
     * @param string $authCorpId
     * @return GetCorpAccessTokenRequest
     */
    public function setAuthCorpId(string $authCorpId): GetCorpAccessTokenRequest
    {
        $this->authCorpId = $authCorpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuiteTicket(): string
    {
        return $this->suiteTicket;
    }

    /**
     * @param string $suiteTicket
     * @return GetCorpAccessTokenRequest
     */
    public function setSuiteTicket(string $suiteTicket): GetCorpAccessTokenRequest
    {
        $this->suiteTicket = $suiteTicket;
        return $this;
    }
}
