<?php

namespace App\Services\DingTalkApi\Models;

class GetUserByUserIdRequest
{
    private string $accessToken = '';

    private string $userId = '';

    private string $language = 'zh_CN';

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetUserByUserIdRequest
     */
    public function setAccessToken(string $accessToken): GetUserByUserIdRequest
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return GetUserByUserIdRequest
     */
    public function setUserId(string $userId): GetUserByUserIdRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return GetUserByUserIdRequest
     */
    public function setLanguage(string $language): GetUserByUserIdRequest
    {
        $this->language = $language;
        return $this;
    }
}
