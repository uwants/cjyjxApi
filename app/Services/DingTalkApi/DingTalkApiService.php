<?php

namespace App\Services\DingTalkApi;

use AlibabaCloud\SDK\Dingtalk\Vcontact_1_0\Dingtalk;
use AlibabaCloud\SDK\Dingtalk\Vcontact_1_0\Models\GetUserHeaders;
use AlibabaCloud\SDK\Dingtalk\Voauth2_1_0\Models\GetUserTokenRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use App\Services\DingTalkApi\Models\GetContactUserRequest;
use App\Services\DingTalkApi\Models\GetContactUserResponse;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenResponse;
use App\Services\DingTalkApi\Models\GetUserAccessTokenRequest;
use App\Services\DingTalkApi\Models\GetUserAccessTokenResponse;
use App\Services\DingTalkApi\Models\GetUserByUserIdRequest;
use App\Services\DingTalkApi\Models\GetUserByUserIdResponse;
use App\Services\DingTalkApi\Models\GetUserInfoRequest;
use App\Services\DingTalkApi\Models\GetUserInfoResponse;
use App\Services\DingTalkApi\Models\GetUserRequest;
use App\Services\DingTalkApi\Models\GetUserResponse;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkApi\Models\SendByTemplateResponse;
use Darabonba\OpenApi\Models\Config;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DingTalkApiService
{
    /**
     * @param GetUserAccessTokenRequest $request
     * @return GetUserAccessTokenResponse
     */
    public function getUserAccessToken(GetUserAccessTokenRequest $request): GetUserAccessTokenResponse
    {
        $config = new Config([]);
        $config->protocol = "https";
        $config->regionId = "central";
        $client = new \AlibabaCloud\SDK\Dingtalk\Voauth2_1_0\Dingtalk($config);
        $getUserTokenRequest = new GetUserTokenRequest([
            "clientId" => $request->getAccessKey() ?: config('common.mpDingTalk.suiteKey'),
            "clientSecret" => $request->getAccessSecret() ?: config('common.mpDingTalk.suiteSecret'),
            "code" => $request->getCode(),
            "refreshToken" => $request->getRefreshToken(),
            "grantType" => $request->getGrantType()
        ]);
        $response = new GetUserAccessTokenResponse();
        $body = $client->getUserToken($getUserTokenRequest)->body;
        $response->setAccessToken((string)$body->accessToken);
        $response->setCorpId($body->corpId);
        $response->setRefreshToken($body->refreshToken);
        $response->setExpireIn($body->expireIn);
        return $response;
    }

    /**
     * @param GetContactUserRequest $request
     * @return GetContactUserResponse
     */
    public function getContactUser(GetContactUserRequest $request): GetContactUserResponse
    {
        $config = new Config([]);
        $config->protocol = "https";
        $config->regionId = "central";
        $client = new Dingtalk($config);
        $getUserHeaders = new GetUserHeaders([]);
        $getUserHeaders->xAcsDingtalkAccessToken = $request->getAccessToken();
        $body = $client->getUserWithOptions("me", $getUserHeaders, new RuntimeOptions([]))->body;
        $response = new GetContactUserResponse();
        $response->setUnionId($body->unionId);
        $response->setMobile($body->mobile);
        return $response;
    }

    /**
     * @param GetUserRequest $request
     * @return GetUserResponse
     */
    public function getUser(GetUserRequest $request): GetUserResponse
    {
        $config = new Config([]);
        $config->protocol = "https";
        $config->regionId = "central";
        $client = new Dingtalk($config);
        $getUserHeaders = new GetUserHeaders([]);
        $getUserHeaders->xAcsDingtalkAccessToken = $request->getAccessToken();
        $unionId = $request->getUnionId();
        $body = $client->getUserWithOptions($unionId, $getUserHeaders, new RuntimeOptions([]))->body;
        $response = new GetUserResponse();
        $response->setMobile($body->mobile);
        $response->setNick($body->nick);
        $response->setAvatarUrl($body->avatarUrl);
        $response->setOpenId($body->openId);
        $response->setEmail($body->email);
        $response->setStateCode($body->stateCode);
        $response->setUnionId($body->unionId);
        return $response;
    }

    /**
     * @param GetCorpAccessTokenRequest $request
     * @return GetCorpAccessTokenResponse
     */
    public function getCorpAccessToken(GetCorpAccessTokenRequest $request): GetCorpAccessTokenResponse
    {
        $config = new Config([]);
        $config->protocol = "https";
        $config->regionId = "central";
        $client = new \AlibabaCloud\SDK\Dingtalk\Voauth2_1_0\Dingtalk($config);
        $getCorpAccessTokenRequest = new \AlibabaCloud\SDK\Dingtalk\Voauth2_1_0\Models\GetCorpAccessTokenRequest([
            "suiteKey" => $request->getSuiteKey() ?: config('common.mpDingTalk.suiteKey'),
            "suiteSecret" => $request->getSuiteSecret() ?: config('common.mpDingTalk.suiteSecret'),
            "authCorpId" => $request->getAuthCorpId(),
            "suiteTicket" => $request->getSuiteTicket(),
        ]);
        $body = $client->getCorpAccessToken($getCorpAccessTokenRequest)->body;
        $response = new GetCorpAccessTokenResponse();
        $response->setAccessToken($body->accessToken);
        return $response;
    }

    /**
     * @param GetUserInfoRequest $request
     * @return GetUserInfoResponse
     * @throws Exception
     */
    public function getUserInfo(GetUserInfoRequest $request): GetUserInfoResponse
    {
        $url = 'https://oapi.dingtalk.com/topapi/v2/user/getuserinfo?access_token=' . $request->getAccessToken();
        $http = Http::post($url, [
            'code' => $request->getCode()
        ]);
        $result = $http->json();
        $errCode = Arr::get($result, 'errcode');
        if (!empty($errCode)) {
            throw new Exception(Arr::get($result, 'errmsg'));
        }
        $response = new GetUserInfoResponse();
        $response->setUserId((string)Arr::get($result, 'result.userid'));
        $response->setName((string)Arr::get($result, 'result.name'));
        $response->setUnionId((string)Arr::get($result, 'result.unionid'));
        $response->setSys((bool)Arr::get($result, 'result.sys'));
        $response->setSysLevel((int)Arr::get($result, 'result.sys_level'));
        $response->setDeviceId((string)Arr::get($result, 'result.device_id'));
        $response->setAssociatedUnionId((string)Arr::get($result, 'result.associated_unionid'));
        return $response;
    }


    /**
     * @param GetUserByUserIdRequest $request
     * @return GetUserByUserIdResponse
     * @throws Exception
     */
    public function getUserByUserId(GetUserByUserIdRequest $request): GetUserByUserIdResponse
    {
        $url = 'https://oapi.dingtalk.com/topapi/v2/user/get?access_token=' . $request->getAccessToken();
        $http = Http::post($url, [
            'userid' => $request->getUserId(),
            'lang' => $request->getLanguage() ?: 'zh_CN'
        ]);
        $result = $http->json();
        $errCode = Arr::get($result, 'errcode');
        if (!empty($errCode)) {
            throw new Exception(Arr::get($result, 'errmsg'));
        }
        $response = new GetUserByUserIdResponse();
        $response->setName((string)Arr::get($result, 'result.name'));
        $response->setAvatar((string)Arr::get($result, 'result.avatar'));
        return $response;
    }

    /**
     * @param SendByTemplateRequest $request
     * @return SendByTemplateResponse
     * @throws Exception
     */
    public function sendByTemplate(SendByTemplateRequest $request): SendByTemplateResponse
    {
        $url = 'https://oapi.dingtalk.com/topapi/message/corpconversation/sendbytemplate?access_token=' . $request->getAccessToken();
        $data = [
            'agent_id' => $request->getAgentId(),
            'template_id' => $request->getTemplateId(),
            'userid_list' => $request->getUserList(),
            'data' => $request->getData()
        ];
        $http = Http::post($url, $data);
        $result = $http->json();
        $errCode = Arr::get($result, 'errcode');
        if (!empty($errCode)) {
            Log::error('钉钉推送异常：', $result);
            Log::error('钉钉url：', compact('url'));
            Log::error('钉钉推送异常数据：', $data);
//            throw new Exception(Arr::get($result, 'errmsg'));
        }
        $response = new SendByTemplateResponse();
        $response->setTaskId((int)Arr::get($result, 'task_id'));
        return $response;
    }
}
