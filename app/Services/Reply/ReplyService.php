<?php

namespace App\Services\Reply;

use App\Events\MessageEvent;
use App\Events\ReplyCreateEvent;
use App\Exceptions\MessageException;
use App\Models\Comment;
use App\Models\Reply;
use App\Services\Reply\Models\CreateReplyRequest;
use App\Services\Reply\Models\CreateReplyResponse;

class ReplyService
{
    /**
     * @param CreateReplyRequest $request
     * @return CreateReplyResponse
     * @throws MessageException
     */
    public function createReply(CreateReplyRequest $request): CreateReplyResponse
    {
        /*$repliedComment = Comment::query()
            ->whereHas('replies')
            ->find($request->getCommentId());
        if (!empty($repliedComment)) {
            throw new MessageException('不支持回复多次');
        }*/
        $model = new Reply();
        $model->fill([
            'user_id' => $request->getUserId(),
            'comment_id' => $request->getCommentId(),
            'content' => $request->getContent()
        ]);
        $model->save();
        $response = new CreateReplyResponse();
        $response->setId($model->getKey());
        $response->setContent($model->getAttribute('content'));
        $response->setCreatedAt($model->getAttribute('created_at'));
        event(new ReplyCreateEvent($model));
        //消息推送
        $comment = Comment::query()
            ->where('id', $request->getCommentId())
            ->first();
        event(new MessageEvent('COMMENT-REPLY', 0, $comment->box_id, $comment->user_id, ''));
        return $response;
    }

}
