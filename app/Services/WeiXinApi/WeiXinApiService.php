<?php

namespace App\Services\WeiXinApi;

use App\Services\WeiXinApi\Models\Code2SessionRequest;
use App\Services\WeiXinApi\Models\Code2SessionResponse;
use App\Services\WeiXinApi\Models\GetPhoneNumberRequest;
use App\Services\WeiXinApi\Models\GetPhoneNumberResponse;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendResponse;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenResponse;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;


class WeiXinApiService
{
    const API_HOST = 'https://api.weixin.qq.com';

    /**
     * @param Code2SessionRequest $request
     * @return Code2SessionResponse
     * @throws Exception
     */
    public function code2Session(Code2SessionRequest $request): Code2SessionResponse
    {
        $path = '/sns/jscode2session?' . http_build_query([
                'appid' => $request->getAppId() ?: config('common.mpWeiXin.appId'),
                'secret' => $request->getSecret() ?: config('common.mpWeiXin.secret'),
                'js_code' => $request->getCode(),
                'grant_type' => $request->getGrantType() ?: 'authorization_code',
            ]);
        $http = Http::get(self::API_HOST . $path);
        $result = $http->json();
        Log::info('获取微信接口返回信息：', [$result]);
        if (!empty(Arr::get($result, 'errcode'))) {
            Log::info('获取微信接口返回信息：', [$result]);
            throw new Exception('errmsg');
        }
        $response = new Code2SessionResponse();
        $response->setUnionId(Arr::get($result, 'unionid'));
        $response->setOpenId(Arr::get($result, 'openid'));
        $response->setSessionKey(Arr::get($result, 'session_key'));
        return $response;
    }

    /**
     * @param GetPhoneNumberRequest $request
     * @return GetPhoneNumberResponse
     * @throws Exception
     */
    public function getPhoneNumber(GetPhoneNumberRequest $request): GetPhoneNumberResponse
    {
        $sessionKey = $request->getSessionKey();
        if (!$sessionKey) {
            throw new Exception('sessionKey不能为空');
        }
        if (strlen($sessionKey) != 24) {
            throw new Exception('sessionKey长度错误');
        }
        $aesKey = base64_decode($sessionKey);
        if (strlen($request->getIv()) != 24) {
            throw new Exception('参数iv 非法');
        }
        $aesIV = base64_decode($request->getIv());
        $encryptedData = $request->getEncryptedData();
        if (!$encryptedData) {
            throw new Exception('密文不能为空');
        }
        $aesCipher = base64_decode($encryptedData);
        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
        $data = json_decode($result, true);
        if ($data == NULL) {
            throw new Exception('手机号解密，解密失败');
        }
        $mobile = Arr::get($data, 'purePhoneNumber');
        if (!$mobile) {
            throw new Exception('手机号解密，手机号为空');
        }
        $response = new GetPhoneNumberResponse();
        $response->setMobile($mobile);
        $response->setCountryCode(Arr::get($data, 'country_code', '86'));
        return $response;
    }

    /**
     * @param GetAccessTokenRequest $request
     * @return GetAccessTokenResponse
     */
    public function getAccessToken(GetAccessTokenRequest $request): GetAccessTokenResponse
    {
        $response = new GetAccessTokenResponse();
        $url = "https://api.weixin.qq.com/cgi-bin/token";
        $param = [
            'appid' => $request->getAppId(),
            'secret' => $request->getSecret(),
            'grant_type' => 'client_credential',
        ];
        $accessMsg = Http::get($url, $param);
        $data = $accessMsg->json();
        Log::info('获取token：', [$data]);
        if (!empty(Arr::get($data, 'access_token'))) {
            $response->setAccessToken(Arr::get($data, 'access_token'));
        }
        return $response;
    }

    /**
     * @param UniformMessageSendRequest $request
     * @return UniformMessageSendResponse
     */
    public function sendMsg(UniformMessageSendRequest $request): UniformMessageSendResponse
    {
        $response = new UniformMessageSendResponse();
        $url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=" . $request->getAccessToken();
        $param = [
            'touser' => $request->getOpenId(),
            'mp_template_msg' => [
                'appid' => $request->getAppId(),
                'template_id' => $request->getTemplateId(),
                'url' => $request->getUrl(),
                'miniprogram' => $request->getMiniProgram(),
                'data' => $request->getData(),
            ],
        ];
        $response->setData($param);
        $accessMsg = Http::post($url, $param);
        Log::info('发送消息：', [$accessMsg]);
        return $response;
    }

    public function sendMsgService(UniformMessageSendRequest $request): UniformMessageSendResponse
    {
        $response = new UniformMessageSendResponse();
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $request->getAccessToken();
        $param = [
            'touser' => $request->getOpenId(),
            'template_id' => $request->getTemplateId(),
            'url' => $request->getUrl(),
            'miniprogram' => $request->getMiniProgram(),
            'data' => $request->getData(),
        ];
        $response->setData($param);
        $accessMsg = Http::post($url, $param);
        Log::info('发送消息：', [$accessMsg]);
        return $response;
    }
}
