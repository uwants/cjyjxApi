<?php

namespace App\Services\WeiXinApi\Models;

class Code2SessionResponse
{
    private string $openId = '';

    private string $sessionKey = '';

    private string $unionId = '';

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     * @return Code2SessionResponse
     */
    public function setOpenId(string $openId): Code2SessionResponse
    {
        $this->openId = $openId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     * @return Code2SessionResponse
     */
    public function setSessionKey(string $sessionKey): Code2SessionResponse
    {
        $this->sessionKey = $sessionKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return Code2SessionResponse
     */
    public function setUnionId(string $unionId): Code2SessionResponse
    {
        $this->unionId = $unionId;
        return $this;
    }
}
