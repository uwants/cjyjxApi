<?php

namespace App\Services\WeiXinApi\Models;

class GetPhoneNumberRequest
{
    private string $sessionKey = '';

    private string $iv = '';

    private string $encryptedData = '';

    /**
     * @return string
     */
    public function getSessionKey(): string
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     * @return GetPhoneNumberRequest
     */
    public function setSessionKey(string $sessionKey): GetPhoneNumberRequest
    {
        $this->sessionKey = $sessionKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getIv(): string
    {
        return $this->iv;
    }

    /**
     * @param string $iv
     * @return GetPhoneNumberRequest
     */
    public function setIv(string $iv): GetPhoneNumberRequest
    {
        $this->iv = $iv;
        return $this;
    }

    /**
     * @return string
     */
    public function getEncryptedData(): string
    {
        return $this->encryptedData;
    }

    /**
     * @param string $encryptedData
     * @return GetPhoneNumberRequest
     */
    public function setEncryptedData(string $encryptedData): GetPhoneNumberRequest
    {
        $this->encryptedData = $encryptedData;
        return $this;
    }
}
