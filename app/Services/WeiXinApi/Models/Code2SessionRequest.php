<?php

namespace App\Services\WeiXinApi\Models;

class Code2SessionRequest
{
    private string $code = '';

    private string $appId = '';

    private string $secret = '';

    private string $grantType = '';

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Code2SessionRequest
     */
    public function setCode(string $code): Code2SessionRequest
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     * @return Code2SessionRequest
     */
    public function setAppId(string $appId): Code2SessionRequest
    {
        $this->appId = $appId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     * @return Code2SessionRequest
     */
    public function setSecret(string $secret): Code2SessionRequest
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @return string
     */
    public function getGrantType(): string
    {
        return $this->grantType;
    }

    /**
     * @param string $grantType
     * @return Code2SessionRequest
     */
    public function setGrantType(string $grantType): Code2SessionRequest
    {
        $this->grantType = $grantType;
        return $this;
    }
}
