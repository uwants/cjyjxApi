<?php

namespace App\Services\WeiXinApi\Models;

class GetPhoneNumberResponse
{
    private string $mobile = '';

    private string $countryCode = '';

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return GetPhoneNumberResponse
     */
    public function setMobile(string $mobile): GetPhoneNumberResponse
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return GetPhoneNumberResponse
     */
    public function setCountryCode(string $countryCode): GetPhoneNumberResponse
    {
        $this->countryCode = $countryCode;
        return $this;
    }
}
