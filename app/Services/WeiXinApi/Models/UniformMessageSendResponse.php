<?php


namespace App\Services\WeiXinApi\Models;


class UniformMessageSendResponse
{
    private array $data = [];

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
