<?php


namespace App\Services\Sms;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use App\Models\MobileCode;
use App\Services\Sms\Models\IsValidMobileCodeRequest;
use App\Services\Sms\Models\IsValidMobileCodeResponse;
use App\Services\Sms\Models\SendCodeByMobileRequest;
use App\Services\Sms\Models\SendCodeByMobileResponse;
use Carbon\Carbon;
use Darabonba\OpenApi\Models\Config;
use Illuminate\Support\Facades\Log;

class SmsService
{

    public function sendCodeByMobile(SendCodeByMobileRequest $request): SendCodeByMobileResponse
    {
        $randNumber = rand(100000, 999999);
        //之前的验证码失效
        MobileCode::query()
            ->where('mobile', $request->getMobile())
            ->delete();
        $config = new Config([
            "accessKeyId" => config('common.sms.ACCESS_KEY_ID'),
            "accessKeySecret" => config('common.sms.ACCESS_KEY_SECRET')
        ]);
        $config->endpoint = "dysmsapi.aliyuncs.com";
        $signName = '超级意见箱';
        $templateCode = 'SMS_221083420';
        $code = [
            'code' => $randNumber
        ];
        $client = new Dysmsapi($config);
        $sendSmsRequest = new SendSmsRequest([
            'phoneNumbers' => $request->getMobile(),
            'templateCode' => $templateCode,
            'signName' => $signName,
            'templateParam' => json_encode($code)
        ]);
        $http = $client->sendSms($sendSmsRequest);
        $body = $http->body;
        $code = $body->code;
        $response = new SendCodeByMobileResponse();
        if ($code == 'OK') {
            $model = new MobileCode();
            $model->fill([
                'mobile' => $request->getMobile(),
                'code' => $randNumber,
                'expired_at' => Carbon::now()->addMinutes(5)->toDateTime(),
            ]);
            $model->save();
            $response->setId($model->getKey());
            return $response;
        }
        $message = $body->message;
        Log::error('阿里云发送短信失败(' . $message . ')', compact('body'));
        return $response;
    }

    public function isValidMobileCode(IsValidMobileCodeRequest $request): IsValidMobileCodeResponse
    {
        $model = MobileCode::query()
            ->where('mobile', $request->getMobile())
            ->where('code', $request->getCode())
            ->latest()
            ->first();
        $response = new IsValidMobileCodeResponse();
        //验证码是否过期
        if (!empty($model) && $model->getAttribute('expired_at') < Carbon::now()->toDateTimeString()) {
            $response->setIsExpired(true);
        } else {
            $response->setIsExpired(false);
        }
        //验证码是否正确
        if (!empty($model)) {
            $response->setIsCorrect(true);
            $model->delete();
        } else {
            $response->setIsCorrect(false);
        }
        //验证码是否有效
        if ($response->isCorrect() && !$response->isExpired()) {
            $response->setIsValid(true);
        } else {
            $response->setIsValid(false);
        }
        return $response;
    }

}