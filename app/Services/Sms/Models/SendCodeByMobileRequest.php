<?php


namespace App\Services\Sms\Models;


class SendCodeByMobileRequest
{
    public string $mobile = '';

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(string $mobile): void
    {
        $this->mobile = $mobile;
    }


}