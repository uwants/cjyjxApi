<?php

namespace App\Services\DingTalkBizData;

use App\Events\CodeErrorEvent;
use App\Exceptions\MessageException;
use App\Facades\Company;
use App\Models\DingTalkBizData;
use App\Services\Company\Models\UpdateOrCreateCompanyRequest;
use App\Services\DingTalkBizData\Models\CreateCompanyByCorpIdRequest;
use App\Services\DingTalkBizData\Models\CreateCompanyByCorpIdResponse;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdResponse;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketResponse;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class DingTalkBizDataService
{
    /**
     * @param CreateCompanyByCorpIdRequest $request
     * @return CreateCompanyByCorpIdResponse
     * @throws Exception
     */
    public function createCompanyByCorpId(CreateCompanyByCorpIdRequest $request): CreateCompanyByCorpIdResponse
    {
        Log::info('DingTalkBizDataService createCompanyByCorpId开始');
        $response = new CreateCompanyByCorpIdResponse();
        $corpId = $request->getCorpId();
        if (empty($corpId)) {
            $e = new MessageException($request->getCorpId() . 'createCompanyByCorpId钉钉缺少参数corpId');
            event(new CodeErrorEvent($e));
            return $response;
        }
        $model = DingTalkBizData::query()
            ->where('corp_id', $corpId)
            ->where('biz_type', 4)
            ->orderBy('gmt_create')
            ->first();
        Log::info('DingTalkBizData model', $model->toArray());
        if (empty($model)) {
            return $response;
        }
        $bizData = $model->getOriginal('biz_data');
        $bizData = json_decode($bizData, true);
        $updateOrCreateCompanyRequest = new UpdateOrCreateCompanyRequest();
        $status = Arr::get($bizData, 'auth_corp_info.is_authenticated');
        Log::info('status', [$status]);
        if ($status) {
            $updateOrCreateCompanyRequest->setStatus(config('common.company.status.pass'));
            $updateOrCreateCompanyRequest->setName(Arr::get($bizData, 'auth_corp_info.full_corp_name'));
        } else {
            $updateOrCreateCompanyRequest->setStatus(config('common.company.status.default'));
            $updateOrCreateCompanyRequest->setName(Arr::get($bizData, 'auth_corp_info.corp_name'));
        }
        $updateOrCreateCompanyRequest->setLogo(Arr::get($bizData, 'auth_corp_info.corp_logo_url', config('common.default_company_logo')));
        $updateOrCreateCompanyRequest->setUserId($request->getUserId());
        Log::info('updateOrCreateCompanyRequest', [$updateOrCreateCompanyRequest]);
        $updateOrCreateCompanyResponse = Company::updateOrCreateCompany($updateOrCreateCompanyRequest);
        $response->setCompanyId($updateOrCreateCompanyResponse->getId());
        return $response;
    }

    /**
     * @param GetSuiteTicketRequest $request
     * @return GetSuiteTicketResponse
     * @throws Exception
     */
    public function getSuiteTicket(GetSuiteTicketRequest $request): GetSuiteTicketResponse
    {
        $corpId = $request->getCorpId() ?: config('common.mpDingTalk.corpId');
        $model = DingTalkBizData::query()
            ->where('corp_id', $corpId)
            ->where('biz_type', 2)
            ->orderBy('gmt_create')
            ->first();
        $response = new GetSuiteTicketResponse();
        if (empty($model)) {
            $e = new MessageException($request->getCorpId() . 'getSuiteTicket缺少推送数据suiteTicket');
            event(new CodeErrorEvent($e));
            return $response;
        }
        $bizData = $model->getOriginal('biz_data');
        $bizData = json_decode($bizData, true);
        $response->setSuiteTicket((string)Arr::get($bizData, 'suiteTicket'));
        return $response;
    }

    /**
     * @throws Exception
     */
    public function getAgentIdByCorpId(GetAgentIdByCorpIdRequest $request): GetAgentIdByCorpIdResponse
    {
        $corpId = $request->getCorpId() ?: config('common.mpDingTalk.corpId');
        $model = DingTalkBizData::query()
            ->where('corp_id', $corpId)
            ->where('biz_type', 7)
            ->orderBy('gmt_create')
            ->first();
        $response = new GetAgentIdByCorpIdResponse();
        if (empty($model)) {
            $e = new MessageException($request->getCorpId() . 'getAgentIdByCorpId缺少推送的agentId');
            event(new CodeErrorEvent($e));
            return $response;
        }
        $bizData = $model->getOriginal('biz_data');
        $bizData = json_decode($bizData, true);
        $response->setAgentId((string)Arr::get($bizData, 'agentId'));
        return $response;
    }
}
