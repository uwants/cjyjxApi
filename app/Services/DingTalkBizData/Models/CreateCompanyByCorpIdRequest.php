<?php

namespace App\Services\DingTalkBizData\Models;

class CreateCompanyByCorpIdRequest
{
    private string $corpId = '';

    private int $userId = 0;

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return CreateCompanyByCorpIdRequest
     */
    public function setCorpId(string $corpId): CreateCompanyByCorpIdRequest
    {
        $this->corpId = $corpId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return CreateCompanyByCorpIdRequest
     */
    public function setUserId(int $userId): CreateCompanyByCorpIdRequest
    {
        $this->userId = $userId;
        return $this;
    }
}
