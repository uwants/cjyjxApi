<?php

namespace App\Services\DingTalkBizData\Models;

class GetSuiteTicketRequest
{
    private string $corpId = '';

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return GetSuiteTicketRequest
     */
    public function setCorpId(string $corpId): GetSuiteTicketRequest
    {
        $this->corpId = $corpId;
        return $this;
    }
}
