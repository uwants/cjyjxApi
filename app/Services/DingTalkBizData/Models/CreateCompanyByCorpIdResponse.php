<?php

namespace App\Services\DingTalkBizData\Models;

class CreateCompanyByCorpIdResponse
{
    private int $companyId = 0;

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return CreateCompanyByCorpIdResponse
     */
    public function setCompanyId(int $companyId): CreateCompanyByCorpIdResponse
    {
        $this->companyId = $companyId;
        return $this;
    }
}
