<?php


namespace App\Services\DingTalkBizData\Models;


class GetAgentIdByCorpIdResponse
{
    private string $agentId = '';

    /**
     * @return string
     */
    public function getAgentId(): string
    {
        return $this->agentId;
    }

    /**
     * @param string $agentId
     */
    public function setAgentId(string $agentId): void
    {
        $this->agentId = $agentId;
    }

}