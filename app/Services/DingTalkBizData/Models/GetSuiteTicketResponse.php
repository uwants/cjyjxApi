<?php

namespace App\Services\DingTalkBizData\Models;

class GetSuiteTicketResponse
{
    private string $suiteTicket = '';

    /**
     * @return string
     */
    public function getSuiteTicket(): string
    {
        return $this->suiteTicket;
    }

    /**
     * @param string $suiteTicket
     * @return GetSuiteTicketResponse
     */
    public function setSuiteTicket(string $suiteTicket): GetSuiteTicketResponse
    {
        $this->suiteTicket = $suiteTicket;
        return $this;
    }
}
