<?php


namespace App\Services\DingTalkCompanyCorp\Models;


class GetCorpIdByCompanyIdRequest
{
    private int $companyId = 0;

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

}