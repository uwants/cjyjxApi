<?php

namespace App\Services\DingTalkCompanyCorp\Models;

class FirstOrCreateCompanyCorpRequest
{
    private int $companyId = 0;

    private string $corpId = '';

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return FirstOrCreateCompanyCorpRequest
     */
    public function setCompanyId(int $companyId): FirstOrCreateCompanyCorpRequest
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return FirstOrCreateCompanyCorpRequest
     */
    public function setCorpId(string $corpId): FirstOrCreateCompanyCorpRequest
    {
        $this->corpId = $corpId;
        return $this;
    }
}
