<?php

namespace App\Services\DingTalkCompanyCorp\Models;

class GetCompanyIdByCorpIdRequest
{
    private string $corpId = '';

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return GetCompanyIdByCorpIdRequest
     */
    public function setCorpId(string $corpId): GetCompanyIdByCorpIdRequest
    {
        $this->corpId = $corpId;
        return $this;
    }
}
