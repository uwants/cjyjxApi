<?php

namespace App\Services\DingTalkCompanyCorp\Models;

class FirstOrCreateCompanyCorpResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FirstOrCreateCompanyCorpResponse
     */
    public function setId(int $id): FirstOrCreateCompanyCorpResponse
    {
        $this->id = $id;
        return $this;
    }
}
