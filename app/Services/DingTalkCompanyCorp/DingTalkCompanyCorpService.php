<?php

namespace App\Services\DingTalkCompanyCorp;

use App\Models\DingTalkCompanyCorp;
use App\Services\DingTalkCompanyCorp\Models\FirstOrCreateCompanyCorpRequest;
use App\Services\DingTalkCompanyCorp\Models\FirstOrCreateCompanyCorpResponse;
use App\Services\DingTalkCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCompanyIdByCorpIdResponse;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyResponse;
use Exception;
use Illuminate\Support\Facades\Log;

class DingTalkCompanyCorpService
{
    /**
     * @param GetCompanyIdByCorpIdRequest $request
     * @return GetCompanyIdByCorpIdResponse
     */
    public function getCompanyIdByCorpId(GetCompanyIdByCorpIdRequest $request): GetCompanyIdByCorpIdResponse
    {
        $model = DingTalkCompanyCorp::query()
            ->select('company_id')
            ->where('corp_id', $request->getCorpId())
            ->first();
        $response = new GetCompanyIdByCorpIdResponse();
        if (!empty($model)) {
            $response->setCompanyId($model->getOriginal('company_id'));
        }
        return $response;
    }

    /**
     * @param FirstOrCreateCompanyCorpRequest $request
     * @return FirstOrCreateCompanyCorpResponse
     * @throws Exception
     */
    public function firstOrCreateCompanyCorp(FirstOrCreateCompanyCorpRequest $request): FirstOrCreateCompanyCorpResponse
    {

        Log::info('firstOrCreateCompanyCorp开始');
        $companyId = $request->getCompanyId();
        if (empty($companyId)) {
            throw new Exception('企业id不能为空');
        }
        $corpId = $request->getCorpId();
        if (empty($companyId)) {
            throw new Exception('corpId不能为空');
        }
        Log::info('DingTalkCompanyCorp', [$corpId, $companyId]);
        $model = DingTalkCompanyCorp::withTrashed()
            ->firstOrCreate([
                'corp_id' => $corpId
            ], [
                'company_id' => $companyId,
            ]);
        if ($model->trashed()) {
            $model->restore();
        }
        $response = new FirstOrCreateCompanyCorpResponse();
        $response->setId($model->getKey());
        return $response;
    }

    public function getCorpIdByCompanyId(GetCorpIdByCompanyIdRequest $request): GetCorpIdByCompanyResponse
    {
        $model = DingTalkCompanyCorp::query()
            ->select('corp_id')
            ->where('company_id', $request->getCompanyId())
            ->first();
        $response = new GetCorpIdByCompanyResponse();
        if (!empty($model)) {
            $response->setCorpId($model->getOriginal('corp_id'));
        }
        return $response;
    }
}
