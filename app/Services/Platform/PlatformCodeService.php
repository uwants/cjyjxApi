<?php

namespace App\Services\Platform;

class PlatformCodeService
{
    // 来源端识别码
    private static string $platformCode = '';

    /**
     * @param string $platformCode
     */
    public function setPlatformCode(string $platformCode): void
    {
        self::$platformCode = $platformCode;
    }

    public function getPlatformCode(): string
    {
        return self::$platformCode;
    }

    /**
     * 是否钉钉
     * @return bool
     */
    public function isMpDingTalk(): bool
    {
        return self::$platformCode == config('common.platformCode.mpDingTalk');
    }

    /**
     * 是否h5
     * @return bool
     */
    public function isH5(): bool
    {
        return self::$platformCode == config('common.platformCode.h5');
    }

    /**
     * 是否微信小程序
     * @return bool
     */
    public function isMpWeiXin(): bool
    {
        return self::$platformCode == config('common.platformCode.mpWeiXin');
    }

    /**
     * 是否企业微信
     * @return bool
     */
    public function isMpWorkWeiXin(): bool
    {
        return self::$platformCode == config('common.platformCode.mpWorkWeiXin');
    }

    /**
     * 是否企业微信
     * @return bool
     */
    public function isPc(): bool
    {
        return self::$platformCode == config('common.platformCode.pc');
    }
}
