<?php

namespace App\Services\CompanyUser;

use App\Facades\DingTalkApi;
use App\Facades\DingTalkAuthCorp;
use App\Models\CompanyUser;
use App\Services\CompanyUser\Models\FirstOrCreateCompanyUserRequest;
use App\Services\CompanyUser\Models\FirstOrCreateCompanyUserResponse;
use App\Services\DingTalkApi\Models\GetUserByUserIdRequest;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdRequest;
use Exception;

class CompanyUserService
{
    /**
     * @param FirstOrCreateCompanyUserRequest $request
     * @return FirstOrCreateCompanyUserResponse
     * @throws Exception
     */
    public function firstOrCreateCompanyUser(FirstOrCreateCompanyUserRequest $request): FirstOrCreateCompanyUserResponse
    {
        $companyId = $request->getCompanyId();
        if (empty($companyId)) {
            throw new Exception('缺少参数企业id');
        }
        $userId = $request->getUserId();
        if (empty($companyId)) {
            throw new Exception('缺少参数用户id');
        }
        $model = CompanyUser::withTrashed()
            ->where('user_id', $userId)
            ->where('company_id', $companyId)
            ->first();
        $response = new FirstOrCreateCompanyUserResponse();
        if (!empty($model)) {
            if ($model->trashed()) {
                $model->restore();
            }
            $response->setId($model->getKey());
            return $response;
        }
        if ($request->getDingTalkUserId()) {
            $getAccessTokenByCorpIdRequest = new GetAccessTokenByCorpIdRequest();
            $getAccessTokenByCorpIdRequest->setCorpId($request->getDingTalkCorpId());
            $getAccessTokenByCorpIdResponse = DingTalkAuthCorp::getAccessTokenByCorpId($getAccessTokenByCorpIdRequest);
            $accessToken = $getAccessTokenByCorpIdResponse->getAccessToken();
            if (empty($accessToken)) {
                throw new Exception('缺少企业accessToken');
            }
            $getUserByUserIdRequest = new GetUserByUserIdRequest();
            $getUserByUserIdRequest->setAccessToken($accessToken);
            $getUserByUserIdRequest->setUserId($request->getDingTalkUserId());
            $getUserByUserIdResponse = DingTalkApi::getUserByUserId($getUserByUserIdRequest);
            $model = new CompanyUser();
            $model->fill([
                'user_id' => $userId,
                'company_id' => $companyId,
                'name' => $getUserByUserIdResponse->getName(),
                'avatar' => empty($getUserByUserIdResponse->getAvatar()) ? config('common.companyUser.defaultAvatar') : $getUserByUserIdResponse->getAvatar(),
                'role' => $request->getRole()
            ]);
            $model->save();
            $response->setId($model->getKey());
            return $response;
        }
        return $response;
    }
}
