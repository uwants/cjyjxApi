<?php

namespace App\Services\CompanyUser\Models;

class FirstOrCreateCompanyUserResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FirstOrCreateCompanyUserResponse
     */
    public function setId(int $id): FirstOrCreateCompanyUserResponse
    {
        $this->id = $id;
        return $this;
    }
}
