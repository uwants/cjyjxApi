<?php

namespace App\Services\CompanyUser\Models;

class FirstOrCreateCompanyUserRequest
{
    private int $companyId = 0;

    private int $userId = 0;

    private string $name = '';

    private string $avatar = '';

    private string $role = '';

    private string $dingTalkUserId = '';

    private string $dingTalkCorpId = '';

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setCompanyId(int $companyId): FirstOrCreateCompanyUserRequest
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setUserId(int $userId): FirstOrCreateCompanyUserRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setName(string $name): FirstOrCreateCompanyUserRequest
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setAvatar(string $avatar): FirstOrCreateCompanyUserRequest
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setRole(string $role): FirstOrCreateCompanyUserRequest
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkUserId(): string
    {
        return $this->dingTalkUserId;
    }

    /**
     * @param string $dingTalkUserId
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setDingTalkUserId(string $dingTalkUserId): FirstOrCreateCompanyUserRequest
    {
        $this->dingTalkUserId = $dingTalkUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkCorpId(): string
    {
        return $this->dingTalkCorpId;
    }

    /**
     * @param string $dingTalkCorpId
     * @return FirstOrCreateCompanyUserRequest
     */
    public function setDingTalkCorpId(string $dingTalkCorpId): FirstOrCreateCompanyUserRequest
    {
        $this->dingTalkCorpId = $dingTalkCorpId;
        return $this;
    }
}
