<?php

namespace App\Services\Box\Models;

class GetByBoxIdRequest
{
    private int $boxId = 0;

    /**
     * @return int
     */
    public function getBoxId(): int
    {
        return $this->boxId;
    }

    /**
     * @param int $boxId
     */
    public function setBoxId(int $boxId): void
    {
        $this->boxId = $boxId;
    }
}
