<?php


namespace App\Services\Box\Models;


class CreateBoxRequest
{
    private int $boxId = 0;
    private int $boxCompanyId = 0;
    private int $boxUserId = 0;
    private string $boxName = '';
    private bool $boxSuspend = false;
    private bool $boxOnce = false;
    private bool $boxOpen = false;
    private bool $boxAll = false;
    private bool $boxSystem = false;
    private bool $boxShow = false;
    private bool $boxTransfer  = false;
    private array $transferContent = [];
    private array $content = [];
    private array $boxCommentUserIds = [];
    private array $boxUserIds = [];

    /**
     * @return int
     */
    public function getBoxId(): int
    {
        return $this->boxId;
    }

    /**
     * @param int $boxId
     */
    public function setBoxId(int $boxId): void
    {
        $this->boxId = $boxId;
    }

    /**
     * @return array
     */
    public function getBoxCommentUserIds(): array
    {
        return $this->boxCommentUserIds;
    }

    /**
     * @param array $boxCommentUserIds
     */
    public function setBoxCommentUserIds(array $boxCommentUserIds): void
    {
        $this->boxCommentUserIds = $boxCommentUserIds;
    }

    /**
     * @return array
     */
    public function getBoxUserIds(): array
    {
        return $this->boxUserIds;
    }

    /**
     * @param array $boxUserIds
     */
    public function setBoxUserIds(array $boxUserIds): void
    {
        $this->boxUserIds = $boxUserIds;
    }

    /**
     * @return int
     */
    public function getBoxCompanyId(): int
    {
        return $this->boxCompanyId;
    }

    /**
     * @param int $boxCompanyId
     */
    public function setBoxCompanyId(int $boxCompanyId): void
    {
        $this->boxCompanyId = $boxCompanyId;
    }

    /**
     * @return int
     */
    public function getBoxUserId(): int
    {
        return $this->boxUserId;
    }

    /**
     * @param int $boxUserId
     */
    public function setBoxUserId(int $boxUserId): void
    {
        $this->boxUserId = $boxUserId;
    }

    /**
     * @return string
     */
    public function getBoxName(): string
    {
        return $this->boxName;
    }

    /**
     * @param string $boxName
     */
    public function setBoxName(string $boxName): void
    {
        $this->boxName = $boxName;
    }

    /**
     * @return bool
     */
    public function isBoxSuspend(): bool
    {
        return $this->boxSuspend;
    }

    /**
     * @param bool $boxSuspend
     */
    public function setBoxSuspend(bool $boxSuspend): void
    {
        $this->boxSuspend = $boxSuspend;
    }

    /**
     * @return bool
     */
    public function isBoxOnce(): bool
    {
        return $this->boxOnce;
    }

    /**
     * @param bool $boxOnce
     */
    public function setBoxOnce(bool $boxOnce): void
    {
        $this->boxOnce = $boxOnce;
    }

    /**
     * @return bool
     */
    public function isBoxOpen(): bool
    {
        return $this->boxOpen;
    }

    /**
     * @param bool $boxOpen
     */
    public function setBoxOpen(bool $boxOpen): void
    {
        $this->boxOpen = $boxOpen;
    }

    /**
     * @return bool
     */
    public function isBoxAll(): bool
    {
        return $this->boxAll;
    }

    /**
     * @param bool $boxAll
     */
    public function setBoxAll(bool $boxAll): void
    {
        $this->boxAll = $boxAll;
    }

    /**
     * @return bool
     */
    public function isBoxSystem(): bool
    {
        return $this->boxSystem;
    }

    /**
     * @param bool $boxSystem
     */
    public function setBoxSystem(bool $boxSystem): void
    {
        $this->boxSystem = $boxSystem;
    }

    /**
     * @return bool
     */
    public function isBoxShow(): bool
    {
        return $this->boxShow;
    }

    /**
     * @param bool $boxShow
     */
    public function setBoxShow(bool $boxShow): void
    {
        $this->boxShow = $boxShow;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent(array $content): void
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isBoxTransfer(): bool
    {
        return $this->boxTransfer;
    }

    /**
     * @param bool $boxTransfer
     */
    public function setBoxTransfer(bool $boxTransfer): void
    {
        $this->boxTransfer = $boxTransfer;
    }

    /**
     * @return array
     */
    public function getTransferContent(): array
    {
        return $this->transferContent;
    }

    /**
     * @param array $transferContent
     */
    public function setTransferContent(array $transferContent): void
    {
        $this->transferContent = $transferContent;
    }

}
