<?php


namespace App\Services\Box\Models;


class MoveByBoxIdsRequest
{
    private int $oldUserId=0;
    private int $newUserId=0;
    private array $boxIds =[];

    /**
     * @return int
     */
    public function getOldUserId(): int
    {
        return $this->oldUserId;
    }

    /**
     * @param int $oldUserId
     */
    public function setOldUserId(int $oldUserId): void
    {
        $this->oldUserId = $oldUserId;
    }

    /**
     * @return int
     */
    public function getNewUserId(): int
    {
        return $this->newUserId;
    }

    /**
     * @param int $newUserId
     */
    public function setNewUserId(int $newUserId): void
    {
        $this->newUserId = $newUserId;
    }

    /**
     * @return array
     */
    public function getBoxIds(): array
    {
        return $this->boxIds;
    }

    /**
     * @param array $boxIds
     */
    public function setBoxIds(array $boxIds): void
    {
        $this->boxIds = $boxIds;
    }

}