<?php


namespace App\Services\Box;


use App\Events\BoxCreateEvent;
use App\Events\MessageEvent;
use App\Models\Box;
use App\Models\BoxMoveInfo;
use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\Box\Models\GetByBoxIdResponse;
use App\Models\BoxCommentUser;
use App\Models\BoxUser;
use App\Models\CompanyUser;
use App\Services\Box\Models\CreateBoxRequest;
use App\Services\Box\Models\CreateBoxResponse;
use App\Services\Box\Models\MoveByBoxIdsRequest;
use App\Services\Box\Models\MoveByBoxIdsResponse;
use Illuminate\Support\Carbon;


class BoxService
{
    public function createBox(CreateBoxRequest $request): CreateBoxResponse
    {

        if ($request->getBoxId() == 0) {
            $model = new Box();
            $model->fill([
                'user_id' => $request->getBoxUserId()
            ]);
        } else {
            $model = Box::query()
                ->findOrFail($request->getBoxId());
            if ($model->getAttribute('name') != $request->getBoxName()) {
                Box::withTrashed()
                    ->where('id', $request->getBoxId())
                    ->update([
                        'poster' => ''
                    ]);
            }
            $request->setBoxCompanyId($model->getAttribute('company_id'));
            $request->setBoxSuspend($model->getAttribute('suspend'));
            $request->setBoxTransfer($model->getAttribute('transfer'));
            $transfer_content = empty($model->getAttribute('transfer_content')) ? [] : $model->getAttribute('transfer_content');
            $request->setTransferContent($transfer_content);
        }
        $model->fill([
            'company_id' => $request->getBoxCompanyId(),
            'name' => $request->getBoxName(),
            'suspend' => $request->isBoxSuspend(),
            'once' => $request->isBoxOnce(),
            'open' => $request->isBoxOpen(),
            'all' => $request->isBoxAll(),
            'system' => $request->isBoxSystem(),
            'show' => $request->isBoxShow(),
            'transfer' => $request->isBoxTransfer(),
            'transfer_content' => $request->getTransferContent(),
            'content' => $request->getContent(),
        ]);
        $model->save();
        $userSendIds = '';
        if (!empty($request->getBoxUserIds())) {
            $boxUserIds = CompanyUser::query()
                ->where('company_id', $model->getAttribute('company_id'))
                ->whereIn('user_id', $request->getBoxUserIds())
                ->pluck('user_id')
                ->toArray();
            foreach ($boxUserIds as $boxUserId) {
                $boxUser = BoxUser::withTrashed()
                    ->firstOrCreate([
                        'box_id' => $model->getKey(),
                        'user_id' => $boxUserId,
                    ]);

                if ($boxUser->wasRecentlyCreated) {
                    $userSendIds .= $boxUserId . ',';
                }
                if ($boxUser->trashed()) {
                    $boxUser->restore();
                }
            }
        }
        if (!empty($request->getBoxCommentUserIds())) {
            $boxCommentUserIds = CompanyUser::query()
                ->where('company_id', $model->getAttribute('company_id'))
                ->whereIn('user_id', $request->getBoxCommentUserIds())
                ->pluck('user_id')
                ->toArray();
            foreach ($boxCommentUserIds as $boxCommentUserId) {
                $boxCommentUser = BoxCommentUser::withTrashed()
                    ->firstOrCreate([
                        'box_id' => $model->getKey(),
                        'user_id' => $boxCommentUserId,
                    ]);

                if ($boxCommentUser->trashed()) {
                    $boxCommentUser->restore();
                }
            }
        }
        if ($request->getBoxId() == 0) {
            event(new BoxCreateEvent($model));
        }
        if (!empty($userSendIds)) {//系统消息
            event(new MessageEvent('BOX-HELP', $request->getBoxCompanyId(), $model->getKey(), 0, $userSendIds));
        }
        $createBoxResponse = new CreateBoxResponse();
        $createBoxResponse->setBoxId($model->getKey());
        return $createBoxResponse;
    }

    /**
     * @param GetByBoxIdRequest $request
     * @return GetByBoxIdResponse
     */
    public function getByBoxId(GetByBoxIdRequest $request): GetByBoxIdResponse
    {
        $model = Box::query()
            ->find($request->getBoxId());
        $response = new GetByBoxIdResponse();
        if (!empty($model)) {
            $response->setCompanyId($model->getOriginal('company_id'));
            $response->setName($model->getOriginal('name'));
        }
        return $response;
    }

    public function moveByBoxIds(MoveByBoxIdsRequest $request): MoveByBoxIdsResponse
    {
        $oldUserId = $request->getOldUserId();
        $newUserId = $request->getNewUserId();
        $moveData = [];
        $time = Carbon::now();
        foreach ($request->getBoxIds() as $id) {
            $moveData[] = [
                'box_id' => $id,
                'old_user_id' => $oldUserId,
                'new_user_id' => $newUserId,
                'created_at' => $time,
                'updated_at' => $time
            ];
        }
        if (!empty($moveData)) {
            //记录意见箱的转移
            BoxMoveInfo::query()
                ->insert($moveData);
        }
        //转移意见箱
        Box::query()
            ->whereIn('id', $request->getBoxIds())
            ->update([
                'user_id' => $request->getNewUserId()
            ]);
        //转移后删除助手
        BoxUser::query()
            ->whereIn('box_id', $request->getBoxIds())
            ->where('user_id', $newUserId)
            ->delete();

        return new MoveByBoxIdsResponse();
    }
}
