<?php

namespace App\Services\DingTalkUser;

use App\Facades\DingTalkApi;
use App\Facades\DingTalkAuthCorp;
use App\Models\DingTalkUser;
use App\Services\DingTalkApi\Models\GetUserInfoRequest;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdRequest;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserRequest;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserResponse;
use Exception;

class DingTalkUserService
{
    /**
     * @param UpdateOrCreateDingTalkUserRequest $request
     * @return UpdateOrCreateDingTalkUserResponse
     * @throws Exception
     */
    public function updateOrCreateDingTalkUser(UpdateOrCreateDingTalkUserRequest $request): UpdateOrCreateDingTalkUserResponse
    {
        $response = new UpdateOrCreateDingTalkUserResponse();
        $insertData = [
            'corp_id' => $request->getCorpId(),
            'union_id' => $request->getUnionId(),
        ];
        $updateData = array_filter([
            'ding_talk_user_id' => $request->getDingTalkUserId(),
            'device_id' => $request->getDeviceId(),
            'sys' => $request->isSys(),
            'sys_level' => $request->getSysLevel(),
            'associated_union_id' => $request->getAssociatedUnionId(),
            'name' => $request->getName(),
            'user_id' => $request->getUserId()
        ]);
        if ($request->getAuthCode()) {
            $getAccessTokenByCorpIdRequest = new GetAccessTokenByCorpIdRequest();
            $getAccessTokenByCorpIdRequest->setCorpId($request->getCorpId());
            $getAccessTokenByCorpIdResponse = DingTalkAuthCorp::getAccessTokenByCorpId($getAccessTokenByCorpIdRequest);
            $accessToken = $getAccessTokenByCorpIdResponse->getAccessToken();
            if (empty($accessToken)) {
                throw new Exception('缺少企业accessToken');
            }
            $getUserInfoRequest = new GetUserInfoRequest();
            $getUserInfoRequest->setCode($request->getAuthCode());
            $getUserInfoRequest->setAccessToken($accessToken);
            $getUserInfoResponse = DingTalkApi::getUserInfo($getUserInfoRequest);
            $insertData = [
                'corp_id' => $request->getCorpId(),
                'union_id' => $getUserInfoResponse->getUnionId(),
            ];
            $updateData = [
                'ding_talk_user_id' => $getUserInfoResponse->getUserId(),
                'device_id' => $getUserInfoResponse->getDeviceId(),
                'sys' => $getUserInfoResponse->isSys(),
                'sys_level' => $getUserInfoResponse->getSysLevel(),
                'associated_union_id' => $getUserInfoResponse->getAssociatedUnionId(),
                'name' => $getUserInfoResponse->getName(),
            ];
        }
        $model = DingTalkUser::withTrashed()
            ->updateOrCreate($insertData, $updateData);
        if ($model->trashed()) {
            $model->restore();
        }
        $response->setUserId((int) $model->getOriginal('user_id'));
        $response->setCorpId((string) $model->getOriginal('corp_id'));
        $response->setDingTalkUserId((string) $model->getOriginal('ding_talk_user_id'));
        $response->setUnionId((string) $model->getOriginal('union_id'));
        $response->setDeviceId((string) $model->getOriginal('device_id'));
        $response->setSys((bool) $model->getOriginal('sys'));
        $response->setSysLevel((int) $model->getOriginal('sys_level'));
        $response->setAssociatedUnionId((string) $model->getOriginal('associated_union_id'));
        $response->setName((string) $model->getOriginal('name'));
        return $response;
    }
}
