<?php

namespace App\Services\DingTalkUser\Models;

class UpdateOrCreateDingTalkUserResponse
{
    private int $id = 0;

    private int $userId = 0;

    private string $corpId = '';

    private string $dingTalkUserId = '';

    private string $deviceId = '';

    private bool $sys = false;

    private int $sysLevel = 0;

    private string $associatedUnionId = '';

    private string $unionId = '';

    private string $name = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setId(int $id): UpdateOrCreateDingTalkUserResponse
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setUserId(int $userId): UpdateOrCreateDingTalkUserResponse
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setCorpId(string $corpId): UpdateOrCreateDingTalkUserResponse
    {
        $this->corpId = $corpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkUserId(): string
    {
        return $this->dingTalkUserId;
    }

    /**
     * @param string $dingTalkUserId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setDingTalkUserId(string $dingTalkUserId): UpdateOrCreateDingTalkUserResponse
    {
        $this->dingTalkUserId = $dingTalkUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setDeviceId(string $deviceId): UpdateOrCreateDingTalkUserResponse
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSys(): bool
    {
        return $this->sys;
    }

    /**
     * @param bool $sys
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setSys(bool $sys): UpdateOrCreateDingTalkUserResponse
    {
        $this->sys = $sys;
        return $this;
    }

    /**
     * @return int
     */
    public function getSysLevel(): int
    {
        return $this->sysLevel;
    }

    /**
     * @param int $sysLevel
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setSysLevel(int $sysLevel): UpdateOrCreateDingTalkUserResponse
    {
        $this->sysLevel = $sysLevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssociatedUnionId(): string
    {
        return $this->associatedUnionId;
    }

    /**
     * @param string $associatedUnionId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setAssociatedUnionId(string $associatedUnionId): UpdateOrCreateDingTalkUserResponse
    {
        $this->associatedUnionId = $associatedUnionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setUnionId(string $unionId): UpdateOrCreateDingTalkUserResponse
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UpdateOrCreateDingTalkUserResponse
     */
    public function setName(string $name): UpdateOrCreateDingTalkUserResponse
    {
        $this->name = $name;
        return $this;
    }
}
