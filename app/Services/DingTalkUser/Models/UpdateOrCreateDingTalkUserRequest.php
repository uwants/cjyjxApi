<?php

namespace App\Services\DingTalkUser\Models;

class UpdateOrCreateDingTalkUserRequest
{
    private string $authCode = '';

    private string $corpId = '';

    private int $userId = 0;

    private string $dingTalkUserId = '';

    private string $deviceId = '';

    private bool $sys = false;

    private int $sysLevel = 0;

    private string $associatedUnionId = '';

    private string $unionId = '';

    private string $name = '';

    /**
     * @return string
     */
    public function getAuthCode(): string
    {
        return $this->authCode;
    }

    /**
     * @param string $authCode
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setAuthCode(string $authCode): UpdateOrCreateDingTalkUserRequest
    {
        $this->authCode = $authCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setCorpId(string $corpId): UpdateOrCreateDingTalkUserRequest
    {
        $this->corpId = $corpId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setUserId(int $userId): UpdateOrCreateDingTalkUserRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkUserId(): string
    {
        return $this->dingTalkUserId;
    }

    /**
     * @param string $dingTalkUserId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setDingTalkUserId(string $dingTalkUserId): UpdateOrCreateDingTalkUserRequest
    {
        $this->dingTalkUserId = $dingTalkUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setDeviceId(string $deviceId): UpdateOrCreateDingTalkUserRequest
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSys(): bool
    {
        return $this->sys;
    }

    /**
     * @param bool $sys
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setSys(bool $sys): UpdateOrCreateDingTalkUserRequest
    {
        $this->sys = $sys;
        return $this;
    }

    /**
     * @return int
     */
    public function getSysLevel(): int
    {
        return $this->sysLevel;
    }

    /**
     * @param int $sysLevel
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setSysLevel(int $sysLevel): UpdateOrCreateDingTalkUserRequest
    {
        $this->sysLevel = $sysLevel;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssociatedUnionId(): string
    {
        return $this->associatedUnionId;
    }

    /**
     * @param string $associatedUnionId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setAssociatedUnionId(string $associatedUnionId): UpdateOrCreateDingTalkUserRequest
    {
        $this->associatedUnionId = $associatedUnionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnionId(): string
    {
        return $this->unionId;
    }

    /**
     * @param string $unionId
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setUnionId(string $unionId): UpdateOrCreateDingTalkUserRequest
    {
        $this->unionId = $unionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UpdateOrCreateDingTalkUserRequest
     */
    public function setName(string $name): UpdateOrCreateDingTalkUserRequest
    {
        $this->name = $name;
        return $this;
    }
}
