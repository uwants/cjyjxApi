<?php

namespace App\Services\DingTalkAuthCorp;

use App\Facades\DingTalkApi;
use App\Facades\DingTalkBizData;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdRequest;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdResponse;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;

class DingTalkAuthCorpService
{
    /**
     * @param GetAccessTokenByCorpIdRequest $request
     * @return GetAccessTokenByCorpIdResponse
     */
    public function getAccessTokenByCorpId(GetAccessTokenByCorpIdRequest $request): GetAccessTokenByCorpIdResponse
    {
        $getSuiteTicketRequest = new GetSuiteTicketRequest();
        $getSuiteTicketResponse = DingTalkBizData::getSuiteTicket($getSuiteTicketRequest);
        $getCorpAccessTokenRequest = new GetCorpAccessTokenRequest();
        $getCorpAccessTokenRequest->setAuthCorpId($request->getCorpId());
        $getCorpAccessTokenRequest->setSuiteTicket($getSuiteTicketResponse->getSuiteTicket());
        $getCorpAccessTokenResponse = DingTalkApi::getCorpAccessToken($getCorpAccessTokenRequest);
        $response = new GetAccessTokenByCorpIdResponse();
        $response->setAccessToken($getCorpAccessTokenResponse->getAccessToken());
        return $response;
    }
}
