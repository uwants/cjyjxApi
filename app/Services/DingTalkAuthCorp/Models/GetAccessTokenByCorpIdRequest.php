<?php

namespace App\Services\DingTalkAuthCorp\Models;

class GetAccessTokenByCorpIdRequest
{
    private string $corpId = '';

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     * @return GetAccessTokenByCorpIdRequest
     */
    public function setCorpId(string $corpId): GetAccessTokenByCorpIdRequest
    {
        $this->corpId = $corpId;
        return $this;
    }
}
