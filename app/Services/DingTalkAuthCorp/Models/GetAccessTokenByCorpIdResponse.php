<?php

namespace App\Services\DingTalkAuthCorp\Models;

class GetAccessTokenByCorpIdResponse
{
    private string $accessToken = '';

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetAccessTokenByCorpIdResponse
     */
    public function setAccessToken(string $accessToken): GetAccessTokenByCorpIdResponse
    {
        $this->accessToken = $accessToken;
        return $this;
    }
}
