<?php

namespace App\Services\User\Models;

class CreateUserResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CreateUserResponse
     */
    public function setId(int $id): CreateUserResponse
    {
        $this->id = $id;
        return $this;
    }
}
