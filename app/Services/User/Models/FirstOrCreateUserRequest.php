<?php

namespace App\Services\User\Models;

class FirstOrCreateUserRequest
{
    private string $mobile = '';

    private string $dingTalkAuthCode = '';

    private string $dingTalkCorpId = '';

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return FirstOrCreateUserRequest
     */
    public function setMobile(string $mobile): FirstOrCreateUserRequest
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkAuthCode(): string
    {
        return $this->dingTalkAuthCode;
    }

    /**
     * @param string $dingTalkAuthCode
     * @return FirstOrCreateUserRequest
     */
    public function setDingTalkAuthCode(string $dingTalkAuthCode): FirstOrCreateUserRequest
    {
        $this->dingTalkAuthCode = $dingTalkAuthCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getDingTalkCorpId(): string
    {
        return $this->dingTalkCorpId;
    }

    /**
     * @param string $dingTalkCorpId
     * @return FirstOrCreateUserRequest
     */
    public function setDingTalkCorpId(string $dingTalkCorpId): FirstOrCreateUserRequest
    {
        $this->dingTalkCorpId = $dingTalkCorpId;
        return $this;
    }
}
