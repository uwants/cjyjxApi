<?php

namespace App\Services\User\Models;

class FirstOrCreateUserResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FirstOrCreateUserResponse
     */
    public function setId(int $id): FirstOrCreateUserResponse
    {
        $this->id = $id;
        return $this;
    }
}
