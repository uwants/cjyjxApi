<?php

namespace App\Services\User\Models;

class GetAccessTokenByUserIdRequest
{
    private int $userId = 0;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return GetAccessTokenByUserIdRequest
     */
    public function setUserId(int $userId): GetAccessTokenByUserIdRequest
    {
        $this->userId = $userId;
        return $this;
    }
}
