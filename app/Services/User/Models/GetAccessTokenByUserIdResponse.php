<?php

namespace App\Services\User\Models;

class GetAccessTokenByUserIdResponse
{
    private string $accessToken = '';

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return GetAccessTokenByUserIdResponse
     */
    public function setAccessToken(string $accessToken): GetAccessTokenByUserIdResponse
    {
        $this->accessToken = $accessToken;
        return $this;
    }
}
