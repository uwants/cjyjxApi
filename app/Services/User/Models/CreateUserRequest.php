<?php

namespace App\Services\User\Models;

class CreateUserRequest
{
    private string $mobile = '';

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return CreateUserRequest
     */
    public function setMobile(string $mobile): CreateUserRequest
    {
        $this->mobile = $mobile;
        return $this;
    }
}
