<?php

namespace App\Services\User;

use App\Facades\DingTalkApi;
use App\Facades\DingTalkAuthCorp;
use App\Facades\DingTalkUser;
use App\Models\Box;
use App\Models\CompanyUser;
use App\Models\User;
use App\Services\DingTalkApi\Models\GetContactUserRequest;
use App\Services\DingTalkApi\Models\GetUserAccessTokenRequest;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserRequest;
use App\Services\User\Models\GetAccessTokenByUserIdRequest;
use App\Services\User\Models\GetAccessTokenByUserIdResponse;
use App\Services\User\Models\GetUserInfoByCompanyIdRequest;
use App\Services\User\Models\GetUserInfoByCompanyIdResponse;
use App\Services\User\Models\FirstOrCreateUserRequest;
use App\Services\User\Models\FirstOrCreateUserResponse;
use Exception;
use Illuminate\Support\Arr;

class UserService
{
    /**
     * @param FirstOrCreateUserRequest $request
     * @return FirstOrCreateUserResponse
     * @throws Exception
     */
    public function firstOrCreateUser(FirstOrCreateUserRequest $request): FirstOrCreateUserResponse
    {
        if ($request->getDingTalkAuthCode()) {
            $getUserAccessTokenRequest = new GetUserAccessTokenRequest();
            $getUserAccessTokenRequest->setCode($request->getDingTalkAuthCode());
            $getAccessTokenByCorpIdResponse = DingTalkApi::getUserAccessToken($getUserAccessTokenRequest);
            $accessToken = $getAccessTokenByCorpIdResponse->getAccessToken();
            if (empty($accessToken)) {
                throw new Exception('钉钉授权企业accessToken不能为空');
            }
            $getUserInfoRequest = new GetContactUserRequest();
            $getUserInfoRequest->setAccessToken($accessToken);
            $getUserInfoResponse = DingTalkApi::getContactUser($getUserInfoRequest);
            $mobile = $getUserInfoResponse->getMobile();
            $model = User::withTrashed()
                ->updateOrCreate([
                    'mobile' => $mobile
                ], [
                    'name' => substr($mobile, -4, 4),
                    'avatar' => config('common.default_avatar')
                ]);
            if ($model->trashed()) {
                $model->restore();
            }
            $userId = $model->getKey();
            $updateOrCreateDingTalkUserRequest = new UpdateOrCreateDingTalkUserRequest();
            $updateOrCreateDingTalkUserRequest->setUserId($userId);
            $updateOrCreateDingTalkUserRequest->setUnionId($getUserInfoResponse->getUnionId());
            $updateOrCreateDingTalkUserRequest->setCorpId($request->getDingTalkCorpId());
            DingTalkUser::updateOrCreateDingTalkUser($updateOrCreateDingTalkUserRequest);
            $response = new FirstOrCreateUserResponse();
            $response->setId($model->getKey());
            return $response;
        }
        $mobile = $request->getMobile();
        if (empty($mobile)) {
            throw new Exception('手机号不能为空');
        }
        $model = User::withTrashed()
            ->updateOrCreate([
                'mobile' => $mobile,
            ], [
                    'name' => substr($mobile, -4, 4),
                    'avatar' => config('common.default_avatar')
                ]
            );
        if ($model->trashed()) {
            $model->restore();
        }
        $response = new FirstOrCreateUserResponse();
        $response->setId($model->getKey());
        return $response;
    }

    /**
     * @param GetAccessTokenByUserIdRequest $request
     * @return GetAccessTokenByUserIdResponse
     */
    public function getAccessTokenByUserId(GetAccessTokenByUserIdRequest $request): GetAccessTokenByUserIdResponse
    {
        $model = User::query()
            ->findOrFail($request->getUserId());
        $response = new GetAccessTokenByUserIdResponse();
        if ($model instanceof User) {
            $token = $model->createToken(config('common.apiTokenName'));
            $response->setAccessToken($token->plainTextToken);
        }
        return $response;
    }

    public function getUserInfoByCompanyId(GetUserInfoByCompanyIdRequest $request): GetUserInfoByCompanyIdResponse
    {
        $response = new GetUserInfoByCompanyIdResponse();
        $model = CompanyUser::query()
            ->with('user')
            ->select('role', 'name', 'user_id', 'avatar', 'company_id')
            ->where('company_id', $request->getCompanyId())
            ->where('user_id', $request->getUserId())
            ->first();
        $userInfo = [];
        if (empty($model->id)) {
            $userInfo = User::query()
                ->where('id', $request->getUserId())
                ->first();
        }
        $data = [
            'userRole' => empty(Arr::get($model, 'role')) ? 'ROLE_NORMAL' : Arr::get($model, 'role'),
            'userInfo' => [
                'userId' => empty(Arr::get($model, 'user_id')) ? $userInfo->id : Arr::get($model, 'user_id'),
                'userName' => empty(Arr::get($model, 'name')) ? $userInfo->name : Arr::get($model, 'name'),
                'userAvatar' => empty(Arr::get($model, 'avatar')) ? $userInfo->avatar : Arr::get($model, 'avatar'),
                'userMobile' => empty(Arr::get($model, 'user.mobile')) ? $userInfo->mobile : Arr::get($model, 'user.mobile'),
            ]
        ];
        $response->setData($data);
        return $response;
    }
}
