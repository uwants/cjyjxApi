<?php


namespace App\Services\CompanyAddress;


use App\Models\Company;
use App\Services\CompanyAddress\Models\UpdateOrCreateCompanyAddressRequest;
use App\Services\CompanyAddress\Models\UpdateOrCreateCompanyAddressResponse;
use Illuminate\Support\Arr;

class CompanyAddressService
{
    public function updateOrCreateCompanyAddress(UpdateOrCreateCompanyAddressRequest $request): UpdateOrCreateCompanyAddressResponse
    {
        $model = Company::withTrashed()
            ->where('id', $request->getCompanyAddressCompanyId())
            ->first();
        $data = [
            'province' => $request->getCompanyAddressProvince(),
            'city' => $request->getCompanyAddressCity(),
            'area' => $request->getCompanyAddressArea(),
            'address' => $request->getCompanyAddressInfo()
        ];
        $model->fill(Arr::where($data, function ($item) {
            return !is_null($item);
        }));
        $model->save();
        $response = new UpdateOrCreateCompanyAddressResponse();
        $id = $model->getKey();
        $response->setCompanyAddressId($id);
        return $response;
    }
}