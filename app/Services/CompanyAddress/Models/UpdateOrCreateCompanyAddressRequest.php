<?php


namespace App\Services\CompanyAddress\Models;


class UpdateOrCreateCompanyAddressRequest
{
    private string $companyAddressProvince = '';
    private string $companyAddressCity = '';
    private string $companyAddressArea = '';
    private string $companyAddressInfo='';
    private int $companyAddressCompanyId = 0;

    /**
     * @return string
     */
    public function getCompanyAddressInfo(): string
    {
        return $this->companyAddressInfo;
    }

    /**
     * @param string $companyAddressInfo
     */
    public function setCompanyAddressInfo(string $companyAddressInfo): void
    {
        $this->companyAddressInfo = $companyAddressInfo;
    }

    /**
     * @return string
     */
    public function getCompanyAddressProvince(): string
    {
        return $this->companyAddressProvince;
    }

    /**
     * @param string $companyAddressProvince
     */
    public function setCompanyAddressProvince(string $companyAddressProvince): void
    {
        $this->companyAddressProvince = $companyAddressProvince;
    }

    /**
     * @return string
     */
    public function getCompanyAddressCity(): string
    {
        return $this->companyAddressCity;
    }

    /**
     * @param string $companyAddressCity
     */
    public function setCompanyAddressCity(string $companyAddressCity): void
    {
        $this->companyAddressCity = $companyAddressCity;
    }

    /**
     * @return string
     */
    public function getCompanyAddressArea(): string
    {
        return $this->companyAddressArea;
    }

    /**
     * @param string $companyAddressArea
     */
    public function setCompanyAddressArea(string $companyAddressArea): void
    {
        $this->companyAddressArea = $companyAddressArea;
    }

    /**
     * @return int
     */
    public function getCompanyAddressCompanyId(): int
    {
        return $this->companyAddressCompanyId;
    }

    /**
     * @param int $companyAddressCompanyId
     */
    public function setCompanyAddressCompanyId(int $companyAddressCompanyId): void
    {
        $this->companyAddressCompanyId = $companyAddressCompanyId;
    }


}