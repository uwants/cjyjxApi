<?php


namespace App\Services\CompanyAddress\Models;


class UpdateOrCreateCompanyAddressResponse
{
    private int $companyAddressId =0;

    /**
     * @return int
     */
    public function getCompanyAddressId(): int
    {
        return $this->companyAddressId;
    }

    /**
     * @param int $companyAddressId
     */
    public function setCompanyAddressId(int $companyAddressId): void
    {
        $this->companyAddressId = $companyAddressId;
    }

}