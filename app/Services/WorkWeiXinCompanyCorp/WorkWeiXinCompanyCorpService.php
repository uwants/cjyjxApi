<?php

namespace App\Services\WorkWeiXinCompanyCorp;

use App\Models\WorkWeiXinCompanyCorp;
use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdResponse;
use App\Services\WorkWeiXinCompanyCorp\Models\CreateCompanyPlatformRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\CreateCompanyPlatformResponse;

class WorkWeiXinCompanyCorpService
{
    /**
     * @param GetCompanyIdByCorpIdRequest $request
     * @return GetCompanyIdByCorpIdResponse
     */
    public function getCompanyIdByCorpId(GetCompanyIdByCorpIdRequest $request): GetCompanyIdByCorpIdResponse
    {
        $model = WorkWeiXinCompanyCorp::query()
            ->select('company_id')
            ->where('corp_id', $request->getCorpId())
            ->first();
        $response = new GetCompanyIdByCorpIdResponse();
        if (!empty($model)) {
            $response->setCompanyId($model->getOriginal('company_id'));
        }
        return $response;
    }

    public function createWeiXinCompany(CreateCompanyPlatformRequest $request): CreateCompanyPlatformResponse
    {
        $model = WorkWeiXinCompanyCorp::withTrashed()
            ->updateOrCreate([
                'corp_id' => $request->getCorpId()
            ], [
                'company_id' => $request->getCompanyId(),
                'permanent_code' => $request->getPermanentCode(),
                'agentid' => $request->getAgentId(),
            ]);
        if ($model->trashed()) {
            $model->trashed();
        }
        $response = new CreateCompanyPlatformResponse();
        $response->setId($model->getKey());
        return $response;
    }
}
