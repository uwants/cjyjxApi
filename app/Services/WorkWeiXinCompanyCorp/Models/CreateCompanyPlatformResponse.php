<?php

namespace App\Services\WorkWeiXinCompanyCorp\Models;

class CreateCompanyPlatformResponse
{
    private int $id = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
