<?php

namespace App\Services\WorkWeiXinCompanyCorp\Models;

class GetCompanyIdByCorpIdResponse
{
    private int $companyId = 0;

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return GetCompanyIdByCorpIdResponse
     */
    public function setCompanyId(int $companyId): GetCompanyIdByCorpIdResponse
    {
        $this->companyId = $companyId;
        return $this;
    }
}
