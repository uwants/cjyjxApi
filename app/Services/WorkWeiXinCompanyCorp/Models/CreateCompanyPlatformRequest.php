<?php


namespace App\Services\WorkWeiXinCompanyCorp\Models;


class CreateCompanyPlatformRequest
{
    private string $corpId = '';

    private int $companyId = 0;

    private string $permanentCode = '';

    private string $agentId = '';

    /**
     * @return string
     */
    public function getAgentId(): string
    {
        return $this->agentId;
    }

    /**
     * @param string $agentId
     */
    public function setAgentId(string $agentId): void
    {
        $this->agentId = $agentId;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return string
     */
    public function getPermanentCode(): string
    {
        return $this->permanentCode;
    }

    /**
     * @param string $permanentCode
     */
    public function setPermanentCode(string $permanentCode): void
    {
        $this->permanentCode = $permanentCode;
    }


}
