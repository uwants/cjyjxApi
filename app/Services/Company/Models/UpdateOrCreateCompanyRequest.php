<?php

namespace App\Services\Company\Models;

class UpdateOrCreateCompanyRequest
{
    private string $dingTalkCorpId = '';

    private string $name = '';

    private int $userId = 0;

    private string $status = '';

    private string $logo = '';

    private string $profile = '';

    /**
     * @return string
     */
    public function getDingTalkCorpId(): string
    {
        return $this->dingTalkCorpId;
    }

    /**
     * @param string $dingTalkCorpId
     * @return UpdateOrCreateCompanyRequest
     */
    public function setDingTalkCorpId(string $dingTalkCorpId): UpdateOrCreateCompanyRequest
    {
        $this->dingTalkCorpId = $dingTalkCorpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UpdateOrCreateCompanyRequest
     */
    public function setName(string $name): UpdateOrCreateCompanyRequest
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateCompanyRequest
     */
    public function setUserId(int $userId): UpdateOrCreateCompanyRequest
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return UpdateOrCreateCompanyRequest
     */
    public function setStatus(string $status): UpdateOrCreateCompanyRequest
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return UpdateOrCreateCompanyRequest
     */
    public function setLogo(string $logo): UpdateOrCreateCompanyRequest
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfile(): string
    {
        return $this->profile;
    }

    /**
     * @param string $profile
     * @return UpdateOrCreateCompanyRequest
     */
    public function setProfile(string $profile): UpdateOrCreateCompanyRequest
    {
        $this->profile = $profile;
        return $this;
    }
}
