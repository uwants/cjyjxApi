<?php


namespace App\Services\Company\Models;


class UpdateCompanyByUserRequest
{
    private int $companyId = 0;
    private string $companyName = '';
    private string $companyLogo = '';
    private string $companyProfile = '';
    private string $companyPoster = '';
    private int $companyUserId = 0;
    private string $companyIndustry='';

    /**
     * @return string
     */
    public function getCompanyIndustry(): string
    {
        return $this->companyIndustry;
    }

    /**
     * @param string $companyIndustry
     */
    public function setCompanyIndustry(string $companyIndustry): void
    {
        $this->companyIndustry = $companyIndustry;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName(string $companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getCompanyLogo(): string
    {
        return $this->companyLogo;
    }

    /**
     * @param string $companyLogo
     */
    public function setCompanyLogo(string $companyLogo): void
    {
        $this->companyLogo = $companyLogo;
    }

    /**
     * @return string
     */
    public function getCompanyProfile(): string
    {
        return $this->companyProfile;
    }

    /**
     * @param string $companyProfile
     */
    public function setCompanyProfile(string $companyProfile): void
    {
        $this->companyProfile = $companyProfile;
    }

    /**
     * @return string
     */
    public function getCompanyPoster(): string
    {
        return $this->companyPoster;
    }

    /**
     * @param string $companyPoster
     */
    public function setCompanyPoster(string $companyPoster): void
    {
        $this->companyPoster = $companyPoster;
    }

    /**
     * @return int
     */
    public function getCompanyUserId(): int
    {
        return $this->companyUserId;
    }

    /**
     * @param int $companyUserId
     */
    public function setCompanyUserId(int $companyUserId): void
    {
        $this->companyUserId = $companyUserId;
    }

}
