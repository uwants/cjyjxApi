<?php

namespace App\Services\Company\Models;

class UpdateOrCreateCompanyResponse
{
    private int $id = 0;

    private int $userId = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UpdateOrCreateCompanyResponse
     */
    public function setId(int $id): UpdateOrCreateCompanyResponse
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return UpdateOrCreateCompanyResponse
     */
    public function setUserId(int $userId): UpdateOrCreateCompanyResponse
    {
        $this->userId = $userId;
        return $this;
    }
}
