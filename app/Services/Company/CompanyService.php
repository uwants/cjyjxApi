<?php

namespace App\Services\Company;

use App\Events\MessageEvent;
use App\Exceptions\MessageException;
use App\Facades\DingTalkBizData;
use App\Facades\DingTalkCompanyCorp;
use App\Facades\PlatformCode;
use App\Facades\PlatformLog;
use App\Models\Company;
use App\Services\Company\Models\CreateCompanyByUserRequest;
use App\Services\Company\Models\CreateCompanyByUserResponse;
use App\Services\Company\Models\UpdateCompanyByUserRequest;
use App\Services\Company\Models\UpdateCompanyByUserResponse;
use App\Services\Company\Models\UpdateOrCreateCompanyRequest;
use App\Services\Company\Models\UpdateOrCreateCompanyResponse;
use App\Services\DingTalkBizData\Models\CreateCompanyByCorpIdRequest;
use App\Services\DingTalkCompanyCorp\Models\FirstOrCreateCompanyCorpRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use App\Services\PlatformLog\Models\CreateLogRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class CompanyService
{
    /**
     * @param UpdateOrCreateCompanyRequest $request
     * @return UpdateOrCreateCompanyResponse
     */
    public function updateOrCreateCompany(UpdateOrCreateCompanyRequest $request): UpdateOrCreateCompanyResponse
    {
        Log::info('updateOrCreateCompany开始');
        $response = new UpdateOrCreateCompanyResponse();
        if ($request->getDingTalkCorpId()) {
            Log::info('request->getDingTalkCorpId()');
            $getCompanyIdByCorpIdRequest = new GetCompanyIdByCorpIdRequest();
            $getCompanyIdByCorpIdRequest->setCorpId($request->getDingTalkCorpId());
            $getCompanyIdByCorpIdResponse = DingTalkCompanyCorp::getCompanyIdByCorpId($getCompanyIdByCorpIdRequest);
            $companyId = $getCompanyIdByCorpIdResponse->getCompanyId();
            Log::info('companyId = $getCompanyIdByCorpIdResponse->getCompanyId()', compact('companyId'));
            if (!empty($companyId)) {
                $response->setId($companyId);
                return $response;
            }
            Log::info('CreateCompanyByCorpIdRequest()');
            $createCompanyByCorpIdRequest = new CreateCompanyByCorpIdRequest();
            Log::info('CreateCompanyByCorpIdRequest');
            $createCompanyByCorpIdRequest->setUserId($request->getUserId());
            $createCompanyByCorpIdRequest->setCorpId($request->getDingTalkCorpId());
            Log::info('UserId', [$request->getUserId()]);
            Log::info('CorpId(', [$request->getDingTalkCorpId()]);
            $createCompanyByCorpIdResponse = DingTalkBizData::createCompanyByCorpId($createCompanyByCorpIdRequest);
            $companyId = $createCompanyByCorpIdResponse->getCompanyId();
            Log::info('createCompanyByCorpIdResponse companyId', [$companyId]);
            if (empty($companyId)) {
                return $response;
            }
            Log::info('empty($companyId)', [$companyId]);
            $firstOrCreateCompanyCorpRequest = new FirstOrCreateCompanyCorpRequest();
            $firstOrCreateCompanyCorpRequest->setCorpId($request->getDingTalkCorpId());
            $firstOrCreateCompanyCorpRequest->setCompanyId($companyId);
            Log::info('$firstOrCreateCompanyCorpRequest', [$firstOrCreateCompanyCorpRequest]);
            DingTalkCompanyCorp::firstOrCreateCompanyCorp($firstOrCreateCompanyCorpRequest);
            $response->setId($companyId);
            $response->setUserId($request->getUserId());
            return $response;
        }
        $status = $request->getStatus();
        Log::info('企业状态：', [$status]);
        if ($status == config('common.company.status.pass')) {
            $model = Company::withTrashed()
                ->where('name', $request->getName())
                ->where('status', $status)
                ->first();
            Log::info('企业信息：', [$model]);
            if (!empty($model)) {
                $updateData = [];
                if (empty($model->getOriginal('user_id'))) {
                    Arr::set($updateData, 'user_id', $request->getUserId());
                }
                if (empty($model->getOriginal('logo'))) {
                    Arr::set($updateData, 'logo', $request->getLogo());
                }
                $model->fill($updateData);
                $model->save();
                $response->setId($model->getKey());
                $response->setUserId($model->getOriginal('user_id'));
                return $response;
            }
        }
        $model = new Company();
        $model->fill([
            'name' => $request->getName(),
            'status' => $request->getStatus(),
            'user_id' => $request->getUserId(),
            'logo' => empty($request->getLogo()) ? config('common.default_company_logo') : $request->getLogo(),
            'profile' => $request->getProfile(),
        ]);
        $model->save();

        //创建企业平台记录
        $createLogRequest = new CreateLogRequest();
        $createLogRequest->setCompanyId($model->getKey());
        $createLogRequest->setPlatform(PlatformCode::getPlatformCode());
        PlatformLog::createCompanyPlatformLog($createLogRequest);

        $response->setId($model->getKey());
        $response->setUserId($request->getUserId());
        return $response;
    }

    public function createCompany(CreateCompanyByUserRequest $request): CreateCompanyByUserResponse
    {
        $response = new CreateCompanyByUserResponse();
        $status = $request->getStatus();
        if ($status == Company::STATUS_CERTIFIED_PASS) {
            $model = Company::withTrashed()
                ->where('name', $request->getName())
                ->where('status', Company::STATUS_CERTIFIED_PASS)
                ->first();
            if (!empty($model)) {
                if ($model->trashed()) {
                    $model->restore();
                }
                $response->setId($model->getKey());
                return $response;
            }
        }
        $model = new  Company();
        $model->fill([
            'name' => $request->getName(),
            'status' => $status,
            'logo' => $request->getLogo(),
            'poster' => !empty($request->getPoster()) ? $request->getPoster() : config('common.default_company_poster'),
            'user_id' => $request->getUserId(),
            'profile' => !empty($request->getProfile()) ? $request->getProfile() : '完善简介让更多人认识您的组织',
            'industry' => !empty($request->getIndustry()) ? $request->getIndustry() : '',
        ]);
        $model->save();
        $response->setId($model->getKey());
        //创建企业平台记录
        $createLogRequest = new CreateLogRequest();
        $createLogRequest->setCompanyId($model->getKey());
        $createLogRequest->setPlatform(PlatformCode::getPlatformCode());
        PlatformLog::createCompanyPlatformLog($createLogRequest);
        return $response;
    }

    /**
     * @param UpdateCompanyByUserRequest $request
     * @return UpdateCompanyByUserResponse
     * @throws MessageException
     */
    public function updateCompany(UpdateCompanyByUserRequest $request): UpdateCompanyByUserResponse
    {
        $model = Company::query()
            ->findOrFail($request->getCompanyId());
        $oldName = $model->getOriginal('name');
        if ($request->getCompanyPoster()) {
            $model->fill([
                'poster' => $request->getCompanyPoster()
            ]);
        }
        if ($request->getCompanyProfile()) {
            $model->fill([
                'profile' => $request->getCompanyProfile()
            ]);
        }
        if ($request->getCompanyLogo()) {
            $model->fill([
                'logo' => $request->getCompanyLogo()
            ]);
        }
        if ($request->getCompanyUserId()) {
            $model->fill([
                'user_id' => $request->getCompanyUserId()
            ]);
        }
        if ($request->getCompanyName()) {
            $model->fill([
                'name' => $request->getCompanyName()
            ]);
        }
        if ($request->getCompanyIndustry()) {
            $model->fill([
                'industry' => $request->getCompanyIndustry()
            ]);
        }
        if ($model->getAttribute('status') == config('common.company.status.pass')) {
            if ($model->isDirty('name')) {
                throw new MessageException('企业已认证不能修改名称');
            }
        }
        $model->save();
        log::info('企业名字', [$oldName, $request->getCompanyName(), $model->getAttribute('status'), ($request->getCompanyName() != $oldName)]);
        if ($request->getCompanyName() != $oldName && !empty($request->getCompanyName()) && $model->getAttribute('status') == config('common.company.status.pass')) {
            event(new MessageEvent('COMPANY-NAME-CHANGE', $request->getCompanyId(), 0, Auth::id(), $oldName));
        }
        $updateCompanyResponse = new UpdateCompanyByUserResponse();
        $updateCompanyResponse->setCompanyId($model->getKey());
        return $updateCompanyResponse;
    }

}
