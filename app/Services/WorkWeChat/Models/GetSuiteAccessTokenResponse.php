<?php


namespace App\Services\WorkWeChat\Models;


class GetSuiteAccessTokenResponse
{
    private string $suiteAccessToken = '';

    /**
     * @return string
     */
    public function getSuiteAccessToken(): string
    {
        return $this->suiteAccessToken;
    }

    /**
     * @param string $suiteAccessToken
     */
    public function setSuiteAccessToken(string $suiteAccessToken): void
    {
        $this->suiteAccessToken = $suiteAccessToken;
    }

}
