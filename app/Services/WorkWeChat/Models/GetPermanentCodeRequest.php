<?php


namespace App\Services\WorkWeChat\Models;


class GetPermanentCodeRequest
{
    private string $authCode = '';
    private string $suiteAccessToken = '';

    /**
     * @return string
     */
    public function getAuthCode(): string
    {
        return $this->authCode;
    }

    /**
     * @param string $authCode
     */
    public function setAuthCode(string $authCode): void
    {
        $this->authCode = $authCode;
    }

    /**
     * @return string
     */
    public function getSuiteAccessToken(): string
    {
        return $this->suiteAccessToken;
    }

    /**
     * @param string $suiteAccessToken
     */
    public function setSuiteAccessToken(string $suiteAccessToken): void
    {
        $this->suiteAccessToken = $suiteAccessToken;
    }

}
