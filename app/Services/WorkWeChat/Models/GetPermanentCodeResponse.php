<?php


namespace App\Services\WorkWeChat\Models;


class GetPermanentCodeResponse
{
    private array $data = [];

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
