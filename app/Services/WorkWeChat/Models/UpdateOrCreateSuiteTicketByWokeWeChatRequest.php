<?php


namespace App\Services\WorkWeChat\Models;


class UpdateOrCreateSuiteTicketByWokeWeChatRequest
{
    private string $corpId = '';

    private string $suiteTicket = '';

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }

    /**
     * @return string
     */
    public function getSuiteTicket(): string
    {
        return $this->suiteTicket;
    }

    /**
     * @param string $suiteTicket
     */
    public function setSuiteTicket(string $suiteTicket): void
    {
        $this->suiteTicket = $suiteTicket;
    }

}
