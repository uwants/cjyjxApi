<?php

namespace App\Services\WorkWeChat\Models;

class GetSuiteTicketByCorpIdResponse
{
    private string $suiteTicket = '';

    /**
     * @return string
     */
    public function getSuiteTicket(): string
    {
        return $this->suiteTicket;
    }

    /**
     * @param string $suiteTicket
     */
    public function setSuiteTicket(string $suiteTicket): void
    {
        $this->suiteTicket = $suiteTicket;
    }
}
