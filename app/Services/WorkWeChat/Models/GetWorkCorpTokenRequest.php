<?php


namespace App\Services\WorkWeChat\Models;


class GetWorkCorpTokenRequest
{
    private string $corpId = '';
    private string $suiteAccessToken = '';

    /**
     * @return string
     */
    public function getSuiteAccessToken(): string
    {
        return $this->suiteAccessToken;
    }

    /**
     * @param string $suiteAccessToken
     */
    public function setSuiteAccessToken(string $suiteAccessToken): void
    {
        $this->suiteAccessToken = $suiteAccessToken;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }

}
