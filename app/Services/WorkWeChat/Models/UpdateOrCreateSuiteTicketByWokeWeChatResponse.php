<?php


namespace App\Services\WorkWeChat\Models;


class UpdateOrCreateSuiteTicketByWokeWeChatResponse
{
    private string $corpId = '';

    private string $suiteTicket = '';

    private int $id = 0;

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }

    /**
     * @return string
     */
    public function getSuiteTicket(): string
    {
        return $this->suiteTicket;
    }

    /**
     * @param string $suiteTicket
     */
    public function setSuiteTicket(string $suiteTicket): void
    {
        $this->suiteTicket = $suiteTicket;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

}
