<?php


namespace App\Services\WorkWeChat\Models;


class WorkMessageSendRequest
{
    private string $toUser = '';
    private string $appId = '';
    private string $page = '';
    private string $title = '';
    private string $accessToken = '';
    private array $contentItem = [];

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getToUser(): string
    {
        return $this->toUser;
    }

    /**
     * @param string $toUser
     */
    public function setToUser(string $toUser): void
    {
        $this->toUser = $toUser;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId): void
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * @param string $page
     */
    public function setPage(string $page): void
    {
        $this->page = $page;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getContentItem(): array
    {
        return $this->contentItem;
    }

    /**
     * @param array $contentItem
     */
    public function setContentItem(array $contentItem): void
    {
        $this->contentItem = $contentItem;
    }


}
