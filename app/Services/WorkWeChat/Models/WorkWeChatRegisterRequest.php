<?php


namespace App\Services\WorkWeChat\Models;


class WorkWeChatRegisterRequest
{
    private string $code = '';
    private string $iv = '';
    private string $encryptedData = '';
    private string $permanentCode = '';
    private string $corpId = '';
    private int $userId = 0;
    private string $suiteAccessToken = '';
    private string $accessToken = '';

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getSuiteAccessToken(): string
    {
        return $this->suiteAccessToken;
    }

    /**
     * @param string $suiteAccessToken
     */
    public function setSuiteAccessToken(string $suiteAccessToken): void
    {
        $this->suiteAccessToken = $suiteAccessToken;
    }

    /**
     * @return string
     */
    public function getPermanentCode(): string
    {
        return $this->permanentCode;
    }

    /**
     * @param string $permanentCode
     */
    public function setPermanentCode(string $permanentCode): void
    {
        $this->permanentCode = $permanentCode;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getIv(): string
    {
        return $this->iv;
    }

    /**
     * @param string $iv
     */
    public function setIv(string $iv): void
    {
        $this->iv = $iv;
    }

    /**
     * @return string
     */
    public function getEncryptedData(): string
    {
        return $this->encryptedData;
    }

    /**
     * @param string $encryptedData
     */
    public function setEncryptedData(string $encryptedData): void
    {
        $this->encryptedData = $encryptedData;
    }

}
