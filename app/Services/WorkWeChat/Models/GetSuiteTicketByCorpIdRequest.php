<?php

namespace App\Services\WorkWeChat\Models;

class GetSuiteTicketByCorpIdRequest
{
    private string $corpId = '';

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->corpId;
    }

    /**
     * @param string $corpId
     */
    public function setCorpId(string $corpId): void
    {
        $this->corpId = $corpId;
    }
}
