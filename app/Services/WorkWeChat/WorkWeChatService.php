<?php

namespace App\Services\WorkWeChat;

use App\Exceptions\MessageException;
use App\Models\Platform;
use App\Models\WorkWeiXinCompanyCorp;
use App\Models\SuiteTickets;
use App\Models\User;
use App\Models\WeiXinUser;
use App\Models\WorkWeiXinUsers;
use App\Services\WorkWeChat\Models\GetSuiteTicketByCorpIdRequest;
use App\Services\WorkWeChat\Models\GetSuiteTicketByCorpIdResponse;
use App\Services\WorkWeChat\Models\UpdateOrCreateSuiteTicketByWokeWeChatRequest;
use App\Services\WorkWeChat\Models\UpdateOrCreateSuiteTicketByWokeWeChatResponse;
use App\Services\User\Models\CreateTokenByUserIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdResponse;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenResponse;
use App\Services\WorkWeChat\Models\GetPermanentCodeRequest;
use App\Services\WorkWeChat\Models\GetPermanentCodeResponse;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenResponse;
use App\Services\WorkWeChat\Models\GetWorkUserIdByCodeRequest;
use App\Services\WorkWeChat\Models\GetWorkUserIdByCodeResponse;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendResponse;
use App\Services\WorkWeChat\Models\WorkWeChatRegisterRequest;
use App\Services\WorkWeChat\Models\WorkWeChatRegisterResponse;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WorkWeChatService
{
    /**
     * @return GetSuiteAccessTokenResponse
     * @throws Exception
     */
    public function getSuiteAccessToken(): GetSuiteAccessTokenResponse
    {
        $response = new GetSuiteAccessTokenResponse();
        $model = SuiteTickets::query()
            ->where('corp_id', config('common.mpWorkWeChat.suiteId'))
            ->first();
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token';
        $data = [
            'suite_id' => config('common.mpWorkWeChat.suiteId'),
            'suite_secret' => config('common.mpWorkWeChat.suiteSecret'),
            'suite_ticket' => $model->suite_ticket,
        ];
        $list = Http::post($url, $data);
        $suite_access_token = Arr::get($list, 'suite_access_token');
        Log::info('$list：', [$list]);
        if (empty($suite_access_token)) {
            throw new Exception('suite_access_token生成失败');
        }
        $response->setSuiteAccessToken($suite_access_token);
        return $response;
    }

    public function getPermanentCode(GetPermanentCodeRequest $request): GetPermanentCodeResponse
    {
        $response = new GetPermanentCodeResponse();
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token=' . $request->getSuiteAccessToken();
        $data = [
            'auth_code' => $request->getAuthCode(),
        ];
        $list = Http::post($url, $data);
        $response->setData(json_decode($list, true));
        return $response;
    }

    public function getUserIdByCode(GetWorkUserIdByCodeRequest $request): GetWorkUserIdByCodeResponse
    {
        $response = new GetWorkUserIdByCodeResponse();
        $path = 'https://api.weixin.qq.com/sns/jscode2session';
        $param = [
            'appid' => config('common.weChat.appId'),
            'secret' => config('common.weChat.appSecret'),
            'js_code' => $request->getCode(),
            'grant_type' => 'authorization_code',
        ];
        $list = Http::post($path, $param);
        $sessionKey = Arr::get($list, 'session_key');
        $openId = Arr::get($list, 'openid');
        $unionId = Arr::get($list, 'unionid');
        Log::info('微信授权企业微信记录：', [$list, config('common.weChat.appId'), config('common.weChat.appSecret'), $request->getCode()]);
        if (empty($unionId)) {
            $response->setUserId(0);
            return $response;
        }
        $model = WeiXinUser::query()
            ->with('user')
            ->where('unionid', $unionId)
            ->first();
        if (!$model) {
            $model = new WeiXinUser();
            $model->fill([
                'unionid' => $unionId,
                'openid' => $openId,
                'session_key' => $sessionKey,
            ]);
            $model->save();
        }
        $model->fill([
            'openid' => $openId,
            'session_key' => $sessionKey,
        ]);
        $model->save();
        $userId = $model->user_id;
        $mobile = Arr::get($model, 'user.mobile');
        //企业微信小程序记录
        $accessToken = $request->getWorkAccessToken();
        $urlAcc = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token=$accessToken";
        $dataMobile = [
            "mobile" => $mobile,
        ];
        $workMsg = Http::post($urlAcc, $dataMobile);
        Log::info('企业微信记录：', [$workMsg, $mobile]);
        $errCode = Arr::get($workMsg, 'errcode');
        if ($errCode != 0) {
            $response->setUserId(0);
            return $response;
        }
        $workModel = WorkWeiXinUsers::query()
            ->where('user_id', $userId)
            ->where('work_user_id', Arr::get($workMsg, 'userid'))
            ->where('corp_id', $request->getCorpId())
            ->first();
        if (!$workModel) {
            $workModel = new WorkWeiXinUsers();
            $workModel->fill([
                'user_id' => $userId,
                'work_user_id' => Arr::get($workMsg, 'userid'),
                'corp_id' => $request->getCorpId(),
            ]);
            $workModel->save();
        }
        $workModel->fill([
            'user_id' => $userId,
            'work_user_id' => Arr::get($workMsg, 'userid'),
            'corp_id' => $request->getCorpId(),
        ]);
        $workModel->save();
        $response->setUnionId($unionId);
        $response->setUserId($userId);
        return $response;
    }

    public function getCorpIdByCompanyId(GetWorkCorpIdByCompanyIdRequest $request): GetWorkCorpIdByCompanyIdResponse
    {
        $response = new GetWorkCorpIdByCompanyIdResponse();
        $plat = WorkWeiXinCompanyCorp::query()
            ->where('company_id', $request->getCompanyId())
            ->first();
        $corp_id = Arr::get($plat, 'corp_id');
        if(!empty($corp_id)){
            $response->setCorpId($corp_id);
        }
        return $response;
    }


    /**
     * @param GetWorkCorpTokenRequest $request
     * @return GetWorkCorpTokenResponse
     * @throws Exception
     */
    public function getCorpToken(GetWorkCorpTokenRequest $request): GetWorkCorpTokenResponse
    {
        $response = new GetWorkCorpTokenResponse();
        $suiteAccessToken = $request->getSuiteAccessToken();
        $urlAcc = "https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=$suiteAccessToken";
        $platForm = WorkWeiXinCompanyCorp::query()
            ->where('corp_id', (string)$request->getCorpId())
            ->first();
        $dataAcc = [
            "auth_corpid" => $request->getCorpId(),
            "permanent_code" => $platForm->permanent_code
        ];
        $workMsg = Http::post($urlAcc, $dataAcc);
        $workMsg = $workMsg->json();
        $access_token = Arr::get($workMsg, 'access_token');
        Log::info('$access_token：', [$workMsg]);
        if (empty($access_token)) {
            throw new Exception('accessToken获取失败');
        }
        $response->setAccessToken($access_token);
        return $response;
    }


    /**
     * @param WorkWeChatRegisterRequest $request
     * @return WorkWeChatRegisterResponse
     * @throws MessageException
     * @throws Exception
     */
    public function register(WorkWeChatRegisterRequest $request): WorkWeChatRegisterResponse
    {
        $response = new WorkWeChatRegisterResponse();
        $userModel = User::query()
            ->where('id', $request->getUserId())
            ->first();
        $mobile = $userModel->mobile;
        $accessToken = $request->getAccessToken();
        $urlAcc = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token=$accessToken";
        $dataMobile = [
            "mobile" => $mobile,
        ];
        $workMsg = Http::post($urlAcc, $dataMobile);
        $workMsg = $workMsg->json();
        Log::info('注册返回信息：', [$workMsg]);
        $errCode = Arr::get($workMsg, 'errcode');
        $workUserId = Arr::get($workMsg, 'userid');
        if ($errCode != 0) {
            throw new MessageException(Arr::get($workMsg, 'errmsg'));
        }
        $WorkWechatUsers = WorkWeiXinUsers::withTrashed()
            ->updateOrCreate([
                'work_user_id' => $workUserId,
                'corp_id' => $request->getCorpId()
            ], [
                'user_id' => $request->getUserId(),
            ]);
        if ($WorkWechatUsers->trashed()) {
            $WorkWechatUsers->restore();
        }
        return $response;
    }

    public function getSuiteTicketByCorpId(GetSuiteTicketByCorpIdRequest $request): GetSuiteTicketByCorpIdResponse
    {
        $model = SuiteTickets::query()
            ->where('corp_id', $request->getCorpId() ?: config('common.mpDingTalk.corpId'))
            ->first();
        $response = new GetSuiteTicketByCorpIdResponse();
        if (!empty($model)) {
            $response->setSuiteTicket($model->getOriginal('suite_ticket'));
        }
        return $response;
    }

    public function updateOrCreateSuiteTicketByWokeWeChat(UpdateOrCreateSuiteTicketByWokeWeChatRequest $request): UpdateOrCreateSuiteTicketByWokeWeChatResponse
    {
        $model = SuiteTickets::withTrashed()
            ->updateOrCreate([
                'corp_id' => $request->getCorpId(),
                'biz_id' => 0
            ], [
                'suite_ticket' => $request->getSuiteTicket(),
            ]);
        $response = new UpdateOrCreateSuiteTicketByWokeWeChatResponse();
        if (!empty($model)) {
            $response->setId($model->getKey());
        }
        return $response;
    }


    public function sendMsg(WorkMessageSendRequest $request): WorkMessageSendResponse
    {
        $response = new WorkMessageSendResponse();
        $url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" . $request->getAccessToken();
        $param = [
            'touser' => $request->getToUser(),
            'msgtype' => 'miniprogram_notice',
            'miniprogram_notice' => [
                'appid' => $request->getAppId(),
                'page' => $request->getPage(),
                'title' => $request->getTitle(),
                'description' => date('Y-m-d H:i:s', time()),
                'emphasis_first_item' => false,//是否放大第一个content_item
                'content_item' => $request->getContentItem(),
            ],
        ];
        $data = Http::post($url, $param);
        Log::info('企业微信发送消息：', [$data, $param]);
        return $response;
    }

}
