<?php


namespace App\Services\Message\Models;


class CreateMessageResponse
{
    private int $messageId = 0;

    /**
     * @return int
     */
    public function getMessageId(): int
    {
        return $this->messageId;
    }

    /**
     * @param int $messageId
     */
    public function setMessageId(int $messageId): void
    {
        $this->messageId = $messageId;
    }

}
