<?php

namespace App\Services\Message;

use App\Services\Message\Models\CreateMessageRequest;
use App\Services\Message\Models\CreateMessageResponse;
use App\Models\SystemMessage;
use App\Models\BoxMessage;

class MessageService
{
    public function createBoxMessage(CreateMessageRequest $request): CreateMessageResponse
    {
        $model = BoxMessage::query()
            ->insertGetId([
                'user_id' => $request->getUserId(),
                'template_id' => $request->getTemplateId(),
                'box_id' => $request->getBoxId(),
                'company_id' => $request->getCompanyId(),
                'title' => $request->getTitle(),
                'content' => $request->getInfo(),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ]);
        $createLogResponse = new CreateMessageResponse();
        $createLogResponse->setMessageId($model);
        return $createLogResponse;
    }

    public function createSystemMessage(CreateMessageRequest $request): CreateMessageResponse
    {
        $model = SystemMessage::query()
            ->insertGetId([
                'user_id' => $request->getUserId(),
                'template_id' => $request->getTemplateId(),
                'title' => $request->getTitle(),
                'content' => $request->getInfo(),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ]);
        $createLogResponse = new CreateMessageResponse();
        $createLogResponse->setMessageId($model);
        return $createLogResponse;
    }
}
