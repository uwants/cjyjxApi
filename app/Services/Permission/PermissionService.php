<?php

namespace App\Services\Permission;

use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Permission\Models\GetPermissionResponse;
use App\Models\AdminRolePermission;
use App\Models\AdminRoleRelationship;
use Illuminate\Support\Facades\Auth;

class PermissionService
{

    public function checkPower(GetPermissionRequest $request): GetPermissionResponse
    {
        $powerList = [];
        $getPermissionResponse = new GetPermissionResponse();
        if (Auth::id() == 1) {
            foreach ($request->getPowerName() as $key => $val) {
                $powerList[$val] = true;
            }
            $getPermissionResponse->setPowerList($powerList);
            return $getPermissionResponse;
        }
        $role = AdminRoleRelationship::query()
            ->where('admin_id', Auth::id())
            ->first();
        $list = AdminRolePermission::query()
            ->with('menu')
            ->where('role_id', @$role->role_id)
            ->get();
        //Log::info('信息',[$list]);
        foreach ($request->getPowerName() as $key => $val) {
            $powerList[$val] = false;
        }
        foreach ($list as $key => $val) {
            if (in_array($val->menu->power_name, $request->getPowerName())) {
                $powerList[$val->menu->power_name] = true;
            }
        }
        $getPermissionResponse->setPowerList($powerList);
        return $getPermissionResponse;
    }
}
