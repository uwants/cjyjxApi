<?php


namespace App\Services\Permission\Models;


class GetPermissionRequest
{
    private int $adminId = 0;
    private array  $powerName = [];

    /**
     * @return int
     */
    public function getAdminId(): int
    {
        return $this->adminId;
    }

    /**
     * @param int $adminId
     */
    public function setAdminId(int $adminId): void
    {
        $this->adminId = $adminId;
    }

    /**
     * @return array
     */
    public function getPowerName(): array
    {
        return $this->powerName;
    }

    /**
     * @param array $powerName
     */
    public function setPowerName(array $powerName): void
    {
        $this->powerName = $powerName;
    }





}
