<?php


namespace App\Services\Permission\Models;


class GetPermissionResponse
{
    private array $powerList = [];
    private bool $power = true;

    /**
     * @return array
     */
    public function getPowerList(): array
    {
        return $this->powerList;
    }

    /**
     * @param array $powerList
     */
    public function setPowerList(array $powerList): void
    {
        $this->powerList = $powerList;
    }



    /**
     * @return bool
     */
    public function isPower(): bool
    {
        return $this->power;
    }

    /**
     * @param bool $power
     */
    public function setPower(bool $power): void
    {
        $this->power = $power;
    }


}
