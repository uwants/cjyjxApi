<?php

namespace App\Facades;

use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenResponse;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendResponse;
use App\Services\WeiXinApi\Models\Code2SessionRequest;
use App\Services\WeiXinApi\Models\Code2SessionResponse;
use App\Services\WeiXinApi\Models\GetPhoneNumberRequest;
use App\Services\WeiXinApi\Models\GetPhoneNumberResponse;
use App\Services\WeiXinApi\WeiXinApiService;
use Illuminate\Support\Facades\Facade;

/**
 * Class WeiXinApi
 * @package App\Facades
 * @method static Code2SessionResponse code2Session(Code2SessionRequest $request)
 * @method static GetPhoneNumberResponse getPhoneNumber(GetPhoneNumberRequest $request)
 * @method static UniformMessageSendResponse sendMsg(UniformMessageSendRequest $request)
 * @method static UniformMessageSendResponse sendMsgService(UniformMessageSendRequest $request)
 * @method static GetAccessTokenResponse getAccessToken(GetAccessTokenRequest $request)
 */
class WeiXinApi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WeiXinApiService::class;
    }
}
