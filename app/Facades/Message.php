<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\Message\MessageService;

/**
 * Class Message
 * @package App\Facades
 * @method static CreateMessageResponse createBoxMessage(CreateMessageRequest $request)
 * @method static CreateMessageResponse createSystemMessage(CreateMessageRequest $request)
 */
class Message extends Facade
{
    protected static function getFacadeAccessor()
    {
        return MessageService::class;
    }
}
