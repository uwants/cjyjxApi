<?php

namespace App\Facades;

use App\Services\PlatformLog\Models\CreateLogRequest;
use App\Services\PlatformLog\Models\CreateLogResponse;
use App\Services\PlatformLog\PlatformLogService;
use Illuminate\Support\Facades\Facade;

/**
 * Class PlatformLog
 * @package App\Facades
 * @method static CreateLogResponse createUserPlatformLog(CreateLogRequest $request)
 * @method static CreateLogResponse createCompanyPlatformLog(CreateLogRequest $request)
 */
class PlatformLog extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PlatformLogService::class;
    }
}
