<?php

namespace App\Facades;

use App\Services\Permission\PermissionService;
use App\Services\Permission\Models\GetPermissionResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class Permission
 * @package App\Facades
 * @method static GetPermissionResponse checkPower(GetPermissionRequest $request)
 */
class Permission extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PermissionService::class;
    }
}
