<?php


namespace App\Facades;


use App\Services\Sms\Models\IsValidMobileCodeRequest;
use App\Services\Sms\Models\IsValidMobileCodeResponse;
use App\Services\Sms\Models\SendCodeByMobileRequest;
use App\Services\Sms\Models\SendCodeByMobileResponse;
use App\Services\Sms\SmsService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Sms
 * @package App\Facades
 * @method static SendCodeByMobileResponse sendCodeByMobile(SendCodeByMobileRequest $request)
 * @method static IsValidMobileCodeResponse isValidMobileCode(IsValidMobileCodeRequest $request)
 */
class Sms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return SmsService::class;
    }
}