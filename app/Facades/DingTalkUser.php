<?php

namespace App\Facades;

use App\Services\DingTalkUser\DingTalkUserService;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserRequest;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class DingTalkUser
 * @package App\Facades
 * @method static UpdateOrCreateDingTalkUserResponse updateOrCreateDingTalkUser(UpdateOrCreateDingTalkUserRequest $request)
 */
class DingTalkUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DingTalkUserService::class;
    }
}
