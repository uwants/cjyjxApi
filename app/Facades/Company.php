<?php

namespace App\Facades;

use App\Services\Company\CompanyService;
use App\Services\Company\Models\UpdateOrCreateCompanyRequest;
use App\Services\Company\Models\UpdateOrCreateCompanyResponse;
use App\Services\Company\Models\CreateCompanyByUserRequest;
use App\Services\Company\Models\CreateCompanyByUserResponse;
use App\Services\Company\Models\UpdateCompanyByUserRequest;
use App\Services\Company\Models\UpdateCompanyByUserResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class Company
 * @package App\Facades
 * @method static UpdateOrCreateCompanyResponse updateOrCreateCompany(UpdateOrCreateCompanyRequest $request)
 * @method static CreateCompanyByUserResponse createCompany(CreateCompanyByUserRequest $request)
 * @method static UpdateCompanyByUserResponse updateCompany(UpdateCompanyByUserRequest $request)
 */
class Company extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CompanyService::class;
    }
}
