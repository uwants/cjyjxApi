<?php


namespace App\Facades;


use App\Services\AliasUser\AliasUserService;
use App\Services\AliasUser\Models\UpdateOrCreateAliasUserRandomByUserIdRequest;
use App\Services\AliasUser\Models\UpdateOrCreateAliasUserRandomByUserIdResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class AliasUser
 * @package App\Facades
 * @method static UpdateOrCreateAliasUserRandomByUserIdResponse  UpdateOrCreateAliasUserRandomByUserId(UpdateOrCreateAliasUserRandomByUserIdRequest $request)
 */
class AliasUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AliasUserService::class;
    }
}