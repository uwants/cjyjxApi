<?php

namespace App\Facades;

use App\Services\Response\Models\ErrorRequest;
use App\Services\Response\Models\JsonRequest;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use App\Services\Response\ResponseService;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Facade;

/**
 * Class Response
 * @package App\Facades
 * @method static HttpResponseException success(SuccessRequest $request)
 * @method static HttpResponseException error(ErrorRequest $request)
 * @method static HttpResponseException json(JsonRequest $request)
 * @method static HttpResponseException page(PageRequest $request)
 */
class Response extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ResponseService::class;
    }
}
