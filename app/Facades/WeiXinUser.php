<?php

namespace App\Facades;

use App\Services\WeiXinUser\Models\UpdateOrCreateWeiXinUserRequest;
use App\Services\WeiXinUser\Models\UpdateOrCreateWeiXinUserResponse;
use App\Services\WeiXinUser\WeiXinUserService;
use Illuminate\Support\Facades\Facade;

/**
 * Class WeiXinUser
 * @package App\Facades
 * @method static UpdateOrCreateWeiXinUserResponse updateOrCreateWeiXinUser(UpdateOrCreateWeiXinUserRequest $request)
 */
class WeiXinUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WeiXinUserService::class;
    }
}
