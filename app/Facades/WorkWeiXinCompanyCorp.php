<?php

namespace App\Facades;

use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdResponse;
use App\Services\WorkWeiXinCompanyCorp\WorkWeiXinCompanyCorpService;
use Illuminate\Support\Facades\Facade;

/**
 * Class User
 * @package App\Facades
 * @method static GetCompanyIdByCorpIdResponse getCompanyIdByCorpId(GetCompanyIdByCorpIdRequest $request)
 * @method static CreateCompanyPlatformResponse createWeiXinCompany(CreateCompanyPlatformRequest $request)
 */
class WorkWeiXinCompanyCorp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WorkWeiXinCompanyCorpService::class;
    }
}
