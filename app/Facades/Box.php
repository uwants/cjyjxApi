<?php


namespace App\Facades;


use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\Box\Models\GetByBoxIdResponse;
use App\Services\Box\BoxService;
use App\Services\Box\Models\CreateBoxRequest;
use App\Services\Box\Models\CreateBoxResponse;
use App\Services\Box\Models\MoveByBoxIdsRequest;
use App\Services\Box\Models\MoveByBoxIdsResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class Box
 * @package App\Facades
 * @method static CreateBoxResponse createBox(CreateBoxRequest $request)
 * @method static GetByBoxIdResponse getByBoxId(GetByBoxIdRequest $request)
 * @method static MoveByBoxIdsResponse moveByBoxIds(MoveByBoxIdsRequest $request)
 */
class Box extends Facade
{
    protected static function getFacadeAccessor()
    {
        return BoxService::class;
    }
}
