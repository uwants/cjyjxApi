<?php

namespace App\Facades;

use App\Services\DingTalkApi\DingTalkApiService;
use App\Services\DingTalkApi\Models\GetContactUserRequest;
use App\Services\DingTalkApi\Models\GetContactUserResponse;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenResponse;
use App\Services\DingTalkApi\Models\GetUserAccessTokenRequest;
use App\Services\DingTalkApi\Models\GetUserAccessTokenResponse;
use App\Services\DingTalkApi\Models\GetUserByUserIdRequest;
use App\Services\DingTalkApi\Models\GetUserByUserIdResponse;
use App\Services\DingTalkApi\Models\GetUserInfoRequest;
use App\Services\DingTalkApi\Models\GetUserInfoResponse;
use App\Services\DingTalkApi\Models\GetUserRequest;
use App\Services\DingTalkApi\Models\GetUserResponse;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkApi\Models\SendByTemplateResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class DingTalkApi
 * @package App\Facades
 * @method static GetCorpAccessTokenResponse getCorpAccessToken(GetCorpAccessTokenRequest $request)
 * @method static GetContactUserResponse getContactUser(GetContactUserRequest $request)
 * @method static GetUserAccessTokenResponse getUserAccessToken(GetUserAccessTokenRequest $request)
 * @method static GetUserResponse getUser(GetUserRequest $request)
 * @method static GetUserInfoResponse getUserInfo(GetUserInfoRequest $request)
 * @method static GetUserByUserIdResponse getUserByUserId(GetUserByUserIdRequest $request)
 * @method static SendByTemplateResponse sendByTemplate(SendByTemplateRequest $request)
 */
class DingTalkApi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DingTalkApiService::class;
    }
}
