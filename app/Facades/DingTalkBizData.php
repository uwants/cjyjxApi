<?php

namespace App\Facades;

use App\Services\DingTalkBizData\DingTalkBizDataService;
use App\Services\DingTalkBizData\Models\CreateCompanyByCorpIdRequest;
use App\Services\DingTalkBizData\Models\CreateCompanyByCorpIdResponse;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdResponse;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class DingTalkBizData
 * @package App\Facades
 * @method static CreateCompanyByCorpIdResponse createCompanyByCorpId(CreateCompanyByCorpIdRequest $request)
 * @method static GetSuiteTicketResponse getSuiteTicket(GetSuiteTicketRequest $request)
 * @method static GetAgentIdByCorpIdResponse getAgentIdByCorpId(GetAgentIdByCorpIdRequest $request)
 */
class DingTalkBizData extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DingTalkBizDataService::class;
    }
}
