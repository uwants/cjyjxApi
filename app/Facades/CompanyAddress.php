<?php


namespace App\Facades;


use App\Services\CompanyAddress\CompanyAddressService;
use App\Services\CompanyAddress\Models\UpdateOrCreateCompanyAddressRequest;
use App\Services\CompanyAddress\Models\UpdateOrCreateCompanyAddressResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class CompanyAddress
 * @package App\Facades
 * @method static UpdateOrCreateCompanyAddressResponse updateOrCreateCompanyAddress(UpdateOrCreateCompanyAddressRequest $request)
 */
class CompanyAddress extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CompanyAddressService::class;
    }
}