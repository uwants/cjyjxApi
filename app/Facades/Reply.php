<?php

namespace App\Facades;

use App\Services\Reply\Models\CreateReplyRequest;
use App\Services\Reply\Models\CreateReplyResponse;
use App\Services\Reply\ReplyService;
use Illuminate\Support\Facades\Facade;

/**
 * Class User
 * @package App\Facade
 * @method static CreateReplyResponse createReply(CreateReplyRequest $request)
 */
class Reply extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ReplyService::class;
    }
}
