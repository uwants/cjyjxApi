<?php

namespace App\Facades;

use App\Services\CompanyMain\CompanyMainService;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainResponse;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\CompanyMain\Models\GetCompanyIdResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class CompanyMain
 * @package App\Facades
 * @method static UpdateOrCreateCompanyMainResponse updateOrCreateCompanyMain(UpdateOrCreateCompanyMainRequest $request)
 * @method static GetCompanyIdResponse getCompanyId(GetCompanyIdRequest $request)
 */
class CompanyMain extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CompanyMainService::class;
    }
}
