<?php

namespace App\Facades;

use App\Services\CompanyUser\CompanyUserService;
use App\Services\CompanyUser\Models\FirstOrCreateCompanyUserRequest;
use App\Services\CompanyUser\Models\FirstOrCreateCompanyUserResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class CompanyUser
 * @package App\Facades
 * @method static FirstOrCreateCompanyUserResponse firstOrCreateCompanyUser(FirstOrCreateCompanyUserRequest $request)
 */
class CompanyUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CompanyUserService::class;
    }
}
