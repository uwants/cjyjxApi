<?php

namespace App\Facades;

use App\Services\DingTalkAuthCorp\DingTalkAuthCorpService;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdRequest;
use App\Services\DingTalkAuthCorp\Models\GetAccessTokenByCorpIdResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class DingTalkAuthCorp
 * @package App\Facades
 * @method static GetAccessTokenByCorpIdResponse getAccessTokenByCorpId(GetAccessTokenByCorpIdRequest $request)
 */
class DingTalkAuthCorp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DingTalkAuthCorpService::class;
    }
}
