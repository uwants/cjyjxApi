<?php

namespace App\Facades;

use App\Services\User\Models\GetAccessTokenByUserIdRequest;
use App\Services\User\Models\GetAccessTokenByUserIdResponse;
use App\Services\User\Models\GetUserInfoByCompanyIdRequest;
use App\Services\User\Models\GetUserInfoByCompanyIdResponse;
use App\Services\User\Models\FirstOrCreateUserRequest;
use App\Services\User\Models\FirstOrCreateUserResponse;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Facade;

/**
 * Class User
 * @package App\Facades
 * @method static FirstOrCreateUserResponse firstOrCreateUser(FirstOrCreateUserRequest $request)
 * @method static GetAccessTokenByUserIdResponse getAccessTokenByUserId(GetAccessTokenByUserIdRequest $request)
 * @method static GetUserInfoByCompanyIdResponse getUserInfoByCompanyId(GetUserInfoByCompanyIdRequest $request)
 */
class User extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UserService::class;
    }
}
