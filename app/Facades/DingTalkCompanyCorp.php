<?php

namespace App\Facades;

use App\Services\DingTalkCompanyCorp\DingTalkCompanyCorpService;
use App\Services\DingTalkCompanyCorp\Models\FirstOrCreateCompanyCorpRequest;
use App\Services\DingTalkCompanyCorp\Models\FirstOrCreateCompanyCorpResponse;
use App\Services\DingTalkCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCompanyIdByCorpIdResponse;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyResponse;
use Illuminate\Support\Facades\Facade;

/**
 * Class User
 * @package App\Facades
 * @method static GetCompanyIdByCorpIdResponse getCompanyIdByCorpId(GetCompanyIdByCorpIdRequest $request)
 * @method static FirstOrCreateCompanyCorpResponse firstOrCreateCompanyCorp(FirstOrCreateCompanyCorpRequest $request)
 * @method static GetCorpIdByCompanyResponse getCorpIdByCompanyId(GetCorpIdByCompanyIdRequest $request)
 */
class DingTalkCompanyCorp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DingTalkCompanyCorpService::class;
    }
}
