<?php

namespace App\Facades;


use App\Services\WorkWeChat\Models\GetWorkCorpTokenResponse;
use App\Services\WorkWeChat\Models\GetWorkUserIdByCodeRequest;
use App\Services\WorkWeChat\Models\GetWorkUserIdByCodeResponse;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdResponse;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\GetPermanentCodeRequest;
use App\Services\WorkWeChat\Models\GetPermanentCodeResponse;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenResponse;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendResponse;
use App\Services\WorkWeChat\Models\WorkWeChatRegisterRequest;
use App\Services\WorkWeChat\Models\WorkWeChatRegisterResponse;
use App\Services\WorkWeChat\WorkWeChatService;
use Illuminate\Support\Facades\Facade;

/**
 * Class WorkWeChat
 * @package App\Facade
 * @method static GetSuiteAccessTokenResponse getSuiteAccessToken(GetSuiteAccessTokenRequest $request)
 * @method static GetPermanentCodeResponse getPermanentCode(GetPermanentCodeRequest $request)
 * @method static GetWorkCorpTokenResponse getCorpToken(GetWorkCorpTokenRequest $request)
 * @method static WorkWeChatRegisterResponse register(WorkWeChatRegisterRequest $request)
 * @method static GetWorkCorpIdByCompanyIdResponse getCorpIdByCompanyId(GetWorkCorpIdByCompanyIdRequest $request)
 * @method static WorkMessageSendResponse sendMsg(WorkMessageSendRequest $request)
 * @method static GetWorkUserIdByCodeResponse getUserIdByCode(GetWorkUserIdByCodeRequest $request)
 * @method static updateOrCreateSuiteTicketByWokeWeChatResponse updateOrCreateSuiteTicketByWokeWeChat(UpdateOrCreateSuiteTicketByWokeWeChatRequest $request)
 */
class WorkWeChat extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WorkWeChatService::class;
    }
}
