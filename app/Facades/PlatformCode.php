<?php

namespace App\Facades;

use App\Services\Platform\PlatformCodeService;
use Illuminate\Support\Facades\Facade;

/**
 * Class User
 * @package App\Facades
 * @method static void setPlatformCode(string $platformCode)
 * @method static string getPlatformCode()
 * @method static bool isPc()
 * @method static bool isH5()
 * @method static bool isMpDingTalk()
 * @method static bool isMpWeiXin()
 * @method static bool isMpWorkWeiXin()
 */
class PlatformCode extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PlatformCodeService::class;
    }
}
