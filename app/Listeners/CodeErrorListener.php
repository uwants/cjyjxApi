<?php

namespace App\Listeners;

use App\Events\CodeErrorEvent;
use App\Facades\PlatformCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;
use Throwable;

class CodeErrorListener
{


    public function handle(CodeErrorEvent $event)
    {
        $model = $event->e;
        Log::info('$event：', [$model]);
        $url = 'https://oapi.dingtalk.com/robot/send?access_token=' . config('common.robot.access_token.code_error');
        $userId = empty(Auth::id()) ? 0 : Auth::id();
        $message = [
            'at' => [
                'isAtAll' => false
            ],
            'text' => [
                "content" => "错误\n" .
                    "群关键词：uat" . "\n" .
                    "错误消息环境：" . config('app.env') . "\n" .
                    "请求参数：" . json_encode(Request::all()) . "\n" .
                    "错误详情：" . $model->getMessage() . "\n" .
                    "平台：" . PlatformCode::getPlatformCode() . "\n" .
                    "用户：" . $userId . "\n" .
                    "追踪:" . substr($model->getTraceAsString(), 0, 1500)
            ],
            'msgtype' => 'text'
        ];
        if (config('app.env') == 'production' || config('app.env') == 'uat') {
            $run = Http::post($url, $message);
            $runMsg = $run->json();
            Log::info('$runMsg：', [$runMsg]);
        }
        return;
    }
}
