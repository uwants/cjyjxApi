<?php


namespace App\Listeners;


use App\Events\BoxCreateEvent;
use App\Facades\DingTalkApi;
use App\Facades\DingTalkBizData;
use App\Facades\DingTalkCompanyCorp;
use App\Facades\PlatformCode;
use App\Facades\WeiXinApi;
use App\Facades\WorkWeChat;
use App\Models\Box;
use App\Models\DingTalkUser;
use App\Models\WeiXinUser;
use App\Models\WeiXinUserSubscribe;
use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BoxCreateListener
{
    /**
     * @param BoxCreateEvent $event
     */
    public function handle(BoxCreateEvent $event)
    {
        $model = $event->model;
        if (empty($model)) {
            return;
        }
        if (PlatformCode::isMpDingTalk()) {//钉钉
            $this->dingTalk($model);
            return;
        }
        if (PlatformCode::isMpWeiXin()) {//小程序
            //$this->weChat($model);
            return;
        }
        if (PlatformCode::isMpWorkWeiXin()) {//企业微信
            $this->work($model);
            return;
        }
    }

    public function work(Box $model)
    {
        $boxCommentUserIds = $model
            ->box_comment_users()
            ->pluck('user_id')
            ->toArray();
        $workUsers = WorkWeiXinUsers::query()
            ->whereIn('user_id', $boxCommentUserIds)
            ->pluck('work_user_id')
            ->toArray();
        if (empty($workUsers)) {
            return;
        }
        // 根据意见箱id获取企业id,
        $getByBoxIdRequest = new GetByBoxIdRequest();
        $getByBoxIdRequest->setBoxId($model->getAttribute('id'));
        $getByBoxIdResponse = \App\Facades\Box::getByBoxId($getByBoxIdRequest);
        $companyId = $getByBoxIdResponse->getCompanyId();
        $workCorp = new GetWorkCorpIdByCompanyIdRequest();
        $workCorp->setCompanyId($companyId);
        $corpIdMsg = WorkWeChat::getCorpIdByCompanyId($workCorp);
        $work = new WorkMessageSendRequest();
        $work->setAppId(config('common.weChat.appId'));
        $work->setTitle('亲，你的意见被回复了哟');
        //获取suiteAccessToken
        $suiteAccessToken = new GetSuiteAccessTokenRequest();
        $suiteAccessToken->setCorpId($corpIdMsg->getCorpId());
        $suiteAccessToken = WorkWeChat::getSuiteAccessToken($suiteAccessToken);
        Log::info('$suiteAccessToken：', [$suiteAccessToken->getSuiteAccessToken()]);
        //获取企业微信AccessToken
        $workAccessToken = new GetWorkCorpTokenRequest();
        $workAccessToken->setCorpId($corpIdMsg->getCorpId());
        $workAccessToken->setSuiteAccessToken($suiteAccessToken->getSuiteAccessToken());
        $accessToken = WorkWeChat::getCorpToken($workAccessToken);
        $work->setAccessToken($accessToken->getAccessToken());
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        if (config('app.env') == 'production') {
            $work->setPage('pages/boxes/detail?boxId=' . Arr::get($model, 'id'));

        } else {
            $work->setPage('pages/index/index');
        }
        $work->setContentItem([
            ['key' => '你好，邀请你来提意见了', 'value' => Arr::get($model, 'name')],
            ['key' => '日期', 'value' => Carbon::parse(Arr::get($model, 'created_at'))->format('Y 年m 月d 日')],
            ['key' => '内容', 'value' => Str::limit(Arr::get($model, 'content.content'), 30)],
            ['key' => '备注', 'value' => '谢谢您的宝贵意见和建议'],
        ]);
        foreach ($workUsers as $workUserId) {
            $work->setToUser($workUserId);
            Log::info('$work：', [$work]);
            WorkWeChat::sendMsg($work);
        }
    }

    public function dingTalk(Box $box)
    {
        //消息推送//查询指定人员，拥有的钉钉的人的资料
        //需要的信息
        $boxName = $box->getAttribute('name');
        $boxId = $box->getKey();
        $boxContent = $box->getAttribute('content.content');
        $companyId = $box->getAttribute('company_id');
        $boxCommentUsersIds = $box
            ->box_comment_users()
            ->pluck('user_id')
            ->toArray();

        if (empty($boxCommentUsersIds)) {
            return;
        }

        //获取corpId
        $getCorpIdByCompanyIdRequest = new GetCorpIdByCompanyIdRequest();
        $getCorpIdByCompanyIdRequest->setCompanyId($companyId);
        $getCorpIdByCompanyIdResponse = DingTalkCompanyCorp::getCorpIdByCompanyId($getCorpIdByCompanyIdRequest);
        $corpId = $getCorpIdByCompanyIdResponse->getCorpId();
        if (empty($corpId)) {
            return;
        }

        $dingTalkUserIds = DingTalkUser::query()
            ->whereIn('user_id', $boxCommentUsersIds)
            ->where('corp_id', $corpId)
            ->pluck('ding_talk_user_id')
            ->toArray();
        if (empty($dingTalkUserIds)) {
            return;
        }
        $dingTalkUserList = implode(',', $dingTalkUserIds);

        //获取agentId
        $getAgentIdByCorpIdRequest = new GetAgentIdByCorpIdRequest();
        $getAgentIdByCorpIdRequest->setCorpId($corpId);
        $getAgentIdByCorpIdResponse = DingTalkBizData::getAgentIdByCorpId($getAgentIdByCorpIdRequest);
        $agentId = $getAgentIdByCorpIdResponse->getAgentId();
        if (empty($agentId)) {
            return;
        }
        //获取suiteTicket
        $getSuiteTicketRequest = new GetSuiteTicketRequest();
        $getSuiteTicketResponse = DingTalkBizData::getSuiteTicket($getSuiteTicketRequest);
        $suiteTicket = $getSuiteTicketResponse->getSuiteTicket();
        if (empty($suiteTicket)) {
            return;
        }
        //获取CorpAccessToken
        $getCorpAccessTokenRequest = new GetCorpAccessTokenRequest();
        $getCorpAccessTokenRequest->setSuiteTicket($suiteTicket);
        $getCorpAccessTokenRequest->setAuthCorpId($corpId);
        $getCorpAccessTokenResponse = DingTalkApi::getCorpAccessToken($getCorpAccessTokenRequest);
        $accessToken = $getCorpAccessTokenResponse->getAccessToken();
        if (empty($accessToken)) {
            return;
        }
        Log::error('信息:', compact('dingTalkUserList', 'corpId', 'agentId', 'suiteTicket', 'accessToken'));
        //发送模板消息
        $sendByTemplateRequest = new SendByTemplateRequest();
        $sendByTemplateRequest->setData([
            'box' => (string)$boxName,
            'content' => (string)$boxContent,
            'boxId' => (string)$boxId
        ]);
        $sendByTemplateRequest->setAccessToken($accessToken);
        $sendByTemplateRequest->setAgentId($agentId);
        $sendByTemplateRequest->setTemplateId(config('common.mpDingTalk.templateId.boxCreate'));
        $sendByTemplateRequest->setUserList($dingTalkUserList);
        DingTalkApi::sendByTemplate($sendByTemplateRequest);
    }

    public function weChat(Box $model)
    {
        $boxCommentUserIds = $model
            ->box_comment_users()
            ->pluck('user_id')
            ->toArray();
        $wechatUserUnionIds = WeiXinUser::query()
            ->whereIn('user_id', $boxCommentUserIds)
            ->pluck('union_id')
            ->toArray();
        $wechatUserSubscribeOpenIds = WeiXinUserSubscribe::query()
            ->whereIn('unionid', $wechatUserUnionIds)
            ->pluck('openid')
            ->toArray();
        if (empty($wechatUserSubscribeOpenIds)) {
            return;
        }
        $send = new UniformMessageSendRequest();
        $send->setAppId(config('common.weChatService.appId'));
        $send->setTemplateId(config('common.weChatService.templateId.boxCreate'));
        if (config('app.env') == 'production') {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'page' => 'pages/boxes/detail?boxId=' . $model->getAttribute('id'),
            ]);
        } else {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'page' => 'pages/index/index',
            ]);
        }
        Log::info('setMiniProgram：', [$send->getMiniProgram()]);
        $send->setData([
            'first' => [
                'value' => '你好，邀请你来提意见了'
            ],
            'keyword1' => [
                'value' => $model->getAttribute('name')
            ],
            'keyword2' => [
                'value' => Carbon::parse(Arr::get($model, 'created_at'))->format('Y 年m 月d 日')
            ],
            'keyword3' => [
                'value' => Str::limit(Arr::get($model, 'content.content'), 30)
            ],
            'remark' => [
                'value' => '提意见'
            ],
        ]);
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChatService.appId'));
        $accessTokenSend->setSecret(config('common.weChatService.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        foreach ($wechatUserSubscribeOpenIds as $openId) {
            $send->setOpenId($openId);
            Log::info('$accessToken：', [$accessToken->getAccessToken()]);
            $send->setAccessToken($accessToken->getAccessToken());
            WeiXinApi::sendMsgService($send);
        }
    }
}
