<?php

namespace App\Listeners;

use App\Events\CommentCreateEvent;
use App\Facades\WeiXinApi;
use App\Facades\WorkWeChat;
use App\Facades\DingTalkApi;
use App\Facades\DingTalkBizData;
use App\Facades\DingTalkCompanyCorp;
use App\Facades\PlatformCode;
use App\Models\Box;
use App\Models\BoxUser;
use App\Models\WeiXinUser;
use App\Models\WeiXinUserSubscribe;
use App\Models\WorkWeiXinUsers;
use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\Comment;
use App\Models\DingTalkUser;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;

class CommentCreateListener
{

    /**
     * @param CommentCreateEvent $event
     */
    public function handle(CommentCreateEvent $event)
    {
        $model = $event->model;
        $boxId = $model->getAttribute('box_id');
        if (empty($boxId)) {
            return;
        }
        Log::info('$model：', [$model]);
        $box = Box::query()
            ->find($boxId);
        if (empty($box)) {
            return;
        }
        Log::info('$box：', [$box]);
        /*if (PlatformCode::isMpDingTalk()) {
            $this->dingTalk($model);
            return;
        }
        if (PlatformCode::isMpWeiXin()) {//小程序
            $this->weChat($box, $model);
            return;
        }*/
        $this->dingTalk($model);
        Log::info('one：', ['one']);
        $this->weChat($box, $model);
        Log::info('two：', ['two']);
        if (PlatformCode::isMpWorkWeiXin()) {//企业微信
            $this->work($box, $model);
            return;
        }
    }

    private function dingTalk(Comment $comment)
    {
        Log::info('开始钉钉的收到意见箱推送');
        //消息推送//查询指定人员，拥有的钉钉的人的资料
        //需要的信息
        $boxId = $comment->getAttribute('box_id');
        $box = Box::query()
            ->find($boxId);

        Log::info('查找推送意见箱', [$box]);
        if (empty($box)) {
            return;
        }
        $boxName = $box->getAttribute('name');
        $companyId = $box->getAttribute('company_id');
        $boxUserIds = BoxUser::query()
            ->where('box_id', $boxId)
            ->pluck('user_id')
            ->toArray();
        $userId = $box->getAttribute('user_id');
        $boxUserIds = Arr::prepend($boxUserIds, $userId);

        Log::info('推送的人群', [$boxUserIds]);
        if (empty($boxUserIds) || empty($companyId) || empty($boxId)) {
            Log::info('empty($boxUserIds) || $companyId || $boxId', [$boxUserIds]);
            return;
        }

        //获取corpId
        $getCorpIdByCompanyIdRequest = new GetCorpIdByCompanyIdRequest();
        $getCorpIdByCompanyIdRequest->setCompanyId($companyId);
        $getCorpIdByCompanyIdResponse = DingTalkCompanyCorp::getCorpIdByCompanyId($getCorpIdByCompanyIdRequest);
        $corpId = $getCorpIdByCompanyIdResponse->getCorpId();
        Log::info('$corpId', [$corpId]);
        if (empty($corpId)) {
            return;
        }

        $dingTalkUserIds = DingTalkUser::query()
            ->whereIn('user_id', $boxUserIds)
            ->where('corp_id', $corpId)
            ->pluck('ding_talk_user_id')
            ->toArray();

        Log::info('推送的钉钉人群', [$dingTalkUserIds]);
        if (empty($dingTalkUserIds)) {
            return;
        }
        $dingTalkUserList = implode(',', $dingTalkUserIds);

        //获取agentId
        $getAgentIdByCorpIdRequest = new GetAgentIdByCorpIdRequest();
        $getAgentIdByCorpIdRequest->setCorpId($corpId);
        $getAgentIdByCorpIdResponse = DingTalkBizData::getAgentIdByCorpId($getAgentIdByCorpIdRequest);
        $agentId = $getAgentIdByCorpIdResponse->getAgentId();
        Log::info('$corpId', [$agentId]);
        if (empty($agentId)) {
            return;
        }
        //获取suiteTicket
        $getSuiteTicketRequest = new GetSuiteTicketRequest();
        $getSuiteTicketResponse = DingTalkBizData::getSuiteTicket($getSuiteTicketRequest);
        $suiteTicket = $getSuiteTicketResponse->getSuiteTicket();

        Log::info('$suiteTicket', [$suiteTicket]);
        if (empty($suiteTicket)) {
            return;
        }
        //获取CorpAccessToken
        $getCorpAccessTokenRequest = new GetCorpAccessTokenRequest();
        $getCorpAccessTokenRequest->setSuiteTicket($suiteTicket);
        $getCorpAccessTokenRequest->setAuthCorpId($corpId);
        $getCorpAccessTokenResponse = DingTalkApi::getCorpAccessToken($getCorpAccessTokenRequest);
        $accessToken = $getCorpAccessTokenResponse->getAccessToken();
        Log::info('$accessToken', [$accessToken]);
        if (empty($accessToken)) {
            return;
        }
        //发送模板消息
        $sendByTemplateRequest = new SendByTemplateRequest();
        $sendByTemplateRequest->setData([
            'box' => (string)$boxName . "在 " . date('Y-m-d H:i', time()),
            'boxId' => (string)$boxId,
        ]);
        $sendByTemplateRequest->setAccessToken($accessToken);
        $sendByTemplateRequest->setAgentId($agentId);
        $sendByTemplateRequest->setTemplateId(config('common.mpDingTalk.templateId.receiveComment'));
        $sendByTemplateRequest->setUserList($dingTalkUserList);
        DingTalkApi::sendByTemplate($sendByTemplateRequest);
    }

    public function WeChat($box, $model)
    {
        $userIds[] = $box->getAttribute('user_id');
        $boxUsers = BoxUser::query()
            ->where('box_id', $box->getAttribute('id'))
            ->get();
        foreach ($boxUsers as $boxUser) {
            $userIds[] = $boxUser->user_id;
        }
        $userIds = array_unique($userIds);
        if (empty($userIds)) {
            return;
        }
        Log::info('$userIds：', [$userIds]);
        $unionIds = WeiXinUser::query()
            ->whereIn('user_id', $userIds)
            ->where('user_id', '!=', '0')
            ->pluck('union_id')
            ->toArray();
        Log::info('$unionIds：', [$unionIds]);
        if (empty($unionIds)) {
            return;
        }
        $openIds = WeiXinUserSubscribe::query()
            ->whereIn('unionid', $unionIds)
            ->pluck('openid')
            ->toArray();
        $openIds = array_filter(array_unique($openIds));
        Log::info('$openIds：', [$openIds]);
        if (empty($openIds)) {
            return;
        }
        $send = new UniformMessageSendRequest();
        $send->setAppId(config('common.weChat.appId'));
        if (config('app.env') == 'production') {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/boxes/detail?boxId=' . $box->getAttribute('id')
            ]);
        } else {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/index/index'
            ]);
        }
        Log::info('setMiniProgram：', [$send->getMiniProgram()]);
        $send->setTemplateId(config('common.weChatService.templateId.commentCreate'));
        $send->setData([
            'first' => [
                'value' => '【' . Arr::get($box, 'name') . '】收到了新意见'
            ],
            'keyword1' => [
                'value' => Str::limit((string)Arr::get($model, 'content.content'), 30),
            ],
            'keyword2' => [
                'value' => Carbon::parse(Arr::get($model, 'created_at'))->format('Y 年m 月d 日'),
            ],
            'remark' => [
                'value' => '点击查看详情'
            ],
        ]);
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChatService.appId'));
        $accessTokenSend->setSecret(config('common.weChatService.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        $send->setAccessToken($accessToken->getAccessToken());
        foreach ($openIds as $openId) {
            $send->setOpenId($openId);
            WeiXinApi::sendMsgService($send);
        }
    }

    public function work($box, $model)
    {
        Log::info('$box333：', [$box]);
        // 根据意见箱id获取企业id,
        $getByBoxIdRequest = new GetByBoxIdRequest();
        $getByBoxIdRequest->setBoxId($box->getAttribute('id'));
        $getByBoxIdResponse = \App\Facades\Box::getByBoxId($getByBoxIdRequest);
        $companyId = $getByBoxIdResponse->getCompanyId();
        Log::info('$companyId：', [$companyId]);
        $workCorp = new GetWorkCorpIdByCompanyIdRequest();
        $workCorp->setCompanyId($companyId);
        $corpIdMsg = WorkWeChat::getCorpIdByCompanyId($workCorp);
        Log::info('$corpIdMsg：', [$corpIdMsg->getCorpId()]);
        if (empty($corpIdMsg->getCorpId())) {
            return;
        }
        Log::info('$userID：', [$box->getAttribute('user_id')]);
        $workUser = WorkWeiXinUsers::query()
            ->where('corp_id', $corpIdMsg->getCorpId())
            ->where('user_id', $box->getAttribute('user_id'))
            ->first();
        Log::info('$workUser：', [$workUser]);
        if (empty($workUser)) {
            return;
        }
        $work = new WorkMessageSendRequest();
        $work->setToUser($workUser->work_user_id);
        $work->setAppId(config('common.weChat.appId'));
        $work->setTitle('亲，你收到新的意见建议哟');
        //获取suiteAccessToken
        $suiteAccessToken = new GetSuiteAccessTokenRequest();
        $suiteAccessToken->setCorpId($workUser->corp_id);
        $suiteAccessToken = WorkWeChat::getSuiteAccessToken($suiteAccessToken);
        Log::info('$suiteAccessToken：', [$suiteAccessToken->getSuiteAccessToken()]);
        //获取企业微信AccessToken
        $workAccessToken = new GetWorkCorpTokenRequest();
        $workAccessToken->setCorpId($workUser->corp_id);
        $workAccessToken->setSuiteAccessToken($suiteAccessToken->getSuiteAccessToken());
        $accessToken = WorkWeChat::getCorpToken($workAccessToken);
        $work->setAccessToken($accessToken->getAccessToken());
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        if (config('app.env') == 'production') {
            $work->setPage('pages/boxes/detail?boxId=' . $model->getKey());
        } else {
            $work->setPage('pages/index/index');
        }
        $work->setContentItem([
            ['key' => '意见箱', 'value' => Arr::get($box, 'box.name')],
            ['key' => '提意见日期', 'value' => Carbon::parse(Arr::get($model, 'created_at'))->format('Y 年m 月d 日')],
            ['key' => '意见内容', 'value' => Str::limit(Arr::get($model, 'content.content'), 30)],
            ['key' => '备注', 'value' => '请您立即查看吧'],
        ]);
        WorkWeChat::sendMsg($work);
    }
}
