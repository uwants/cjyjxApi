<?php


namespace App\Listeners;

use App\Events\ReplyCreateEvent;
use App\Facades\DingTalkApi;
use App\Facades\DingTalkBizData;
use App\Facades\DingTalkCompanyCorp;
use App\Facades\PlatformCode;
use App\Models\DingTalkUser;
use App\Models\Reply;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;
use Illuminate\Support\Facades\Log;
use App\Facades\Box;
use App\Facades\CompanyPlatform;
use App\Facades\SuiteTicket;
use App\Facades\WeiXinApi;
use App\Facades\WorkWeChat;
use App\Models\WeiXinUser;
use App\Models\WeiXinUserSubscribe;
use App\Models\WorkWeiXinUsers;
use App\Models\Comment;
use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class ReplyCreateListener
{

    /**
     * @param ReplyCreateEvent $event
     */
    public function handle(ReplyCreateEvent $event)
    {
        $model = $event->model;
        if (empty($model)) {
            return;
        }
        Log::info('$model：', [$model]);
        $box = Comment::query()
            ->with('box')
            ->find($model->getAttribute('comment_id'));
        Log::info('$box：', [$box]);
        Log::info('$boxTT：', [$box->getAttribute('box_id')]);
        if (empty($box->getAttribute('box_id'))) {
            return;
        }
       /* if (PlatformCode::isMpDingTalk()) {//钉钉
            $this->dingTalk($model);
            return;
        }
        if (PlatformCode::isMpWeiXin()) {//小程序
            $this->weChat($box, $model);
            return;
        }*/
        $this->dingTalk($model);
        $this->weChat($box, $model);
        if (PlatformCode::isMpWorkWeiXin()) {//企业微信
            $this->work($box, $model);
            return;
        }
    }

    private function dingTalk(Reply $reply)
    {
        //消息推送//查询指定人员，拥有的钉钉的人的资料
        //需要的信息

        Log::info('钉钉回复推送');
        $comment = $reply
            ->comment()
            ->first();
        Log::info('$comment', [$comment]);
        if (empty($comment->box_id)) {
            return;
        }
        $boxId = $comment->box_id;
        $userId = $comment->getAttribute('user_id');
        $box = \App\Models\Box::query()
            ->find($boxId);

        Log::info('$box', [$box]);
        if (empty($box)) {
            return;
        }

        Log::info('$comment', [$comment]);
        if (empty($comment)) {
            Log::error("comment empty", compact('comment'));
            return;
        }
        $boxId = $box->getKey();
        if (empty($box)) {
            Log::error("box empty", compact('box'));
            return;
        }
        $boxName = $box->getAttribute('name');
        $companyId = $box->getAttribute('company_id');

        Log::info('$companyId', [$companyId]);
        Log::info('$userId', [$userId]);
        if (empty($companyId) || empty($userId)) {
            return;
        }

        //获取corpId
        $getCorpIdByCompanyIdRequest = new GetCorpIdByCompanyIdRequest();
        $getCorpIdByCompanyIdRequest->setCompanyId($companyId);
        $getCorpIdByCompanyIdResponse = DingTalkCompanyCorp::getCorpIdByCompanyId($getCorpIdByCompanyIdRequest);
        $corpId = $getCorpIdByCompanyIdResponse->getCorpId();

        Log::info('$corpId', [$corpId]);
        if (empty($corpId)) {
            return;
        }

        $dingTalkUserModel = DingTalkUser::query()
            ->where('corp_id', $corpId)
            ->where('user_id', $userId)
            ->first();

        Log::info('$dingTalkUserModel', [$dingTalkUserModel]);
        if (empty($dingTalkUserModel)) {
            return;
        }
        $dingTalkUserId = $dingTalkUserModel
            ->getAttribute('ding_talk_user_id');

        Log::info('getAttribute', [$dingTalkUserId]);
        if (empty($dingTalkUserId)) {
            return;
        }

        //获取agentId
        $getAgentIdByCorpIdRequest = new GetAgentIdByCorpIdRequest();
        $getAgentIdByCorpIdRequest->setCorpId($corpId);
        $getAgentIdByCorpIdResponse = DingTalkBizData::getAgentIdByCorpId($getAgentIdByCorpIdRequest);
        $agentId = $getAgentIdByCorpIdResponse->getAgentId();

        Log::info('$agentId', [$agentId]);
        if (empty($agentId)) {
            return;
        }
        //获取suiteTicket
        $getSuiteTicketRequest = new GetSuiteTicketRequest();
        $getSuiteTicketResponse = DingTalkBizData::getSuiteTicket($getSuiteTicketRequest);
        $suiteTicket = $getSuiteTicketResponse->getSuiteTicket();

        Log::info('$suiteTicket', [$suiteTicket]);
        if (empty($suiteTicket)) {
            return;
        }
        //获取CorpAccessToken
        $getCorpAccessTokenRequest = new GetCorpAccessTokenRequest();
        $getCorpAccessTokenRequest->setSuiteTicket($suiteTicket);
        $getCorpAccessTokenRequest->setAuthCorpId($corpId);
        $getCorpAccessTokenResponse = DingTalkApi::getCorpAccessToken($getCorpAccessTokenRequest);
        $accessToken = $getCorpAccessTokenResponse->getAccessToken();
        Log::info('$accessToken', [$accessToken]);

        if (empty($accessToken)) {
            return;
        }
        //发送模板消息
        $sendByTemplateRequest = new SendByTemplateRequest();
        $sendByTemplateRequest->setData([
            'box' => (string)$boxName,
            'boxId' => (string)$boxId,
            'time' => (string)date('Y-m-d H:i', time()),
        ]);
        $sendByTemplateRequest->setAccessToken($accessToken);
        $sendByTemplateRequest->setAgentId($agentId);
        $sendByTemplateRequest->setTemplateId(config('common.mpDingTalk.templateId.receiveReplyNew'));
        $sendByTemplateRequest->setUserList($dingTalkUserId);
        DingTalkApi::sendByTemplate($sendByTemplateRequest);
    }

    public function weChat($comment, $model)
    {
        Log::info('$comment：', [$comment]);
        Log::info('$model：', [$model]);
        $wechatUser = WeiXinUser::query()
            ->where('user_id', $comment->getAttribute('user_id'))
            ->first();
        Log::info('$wechatUser：', [$wechatUser]);
        if (empty($wechatUser)) {
            return;
        }
        $wechatUserSubscribe = WeiXinUserSubscribe::query()
            ->where('unionid', $wechatUser->getAttribute('union_id'))
            ->first();
        Log::info('$wechatUserSubscribe：', [$wechatUserSubscribe]);
        if (empty($wechatUserSubscribe)) {
            return;
        }
        $openId = $wechatUserSubscribe->getAttribute('openid');
        Log::info('$openId：', [$openId]);
        if (empty($openId)) {
            return;
        }
        $send = new UniformMessageSendRequest();
        $send->setAppId(config('common.weChat.appId'));
        $send->setTemplateId(config('common.weChatService.templateId.replyCreate'));
        if (config('app.env') == 'production') {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/boxes/detail?boxId=' . Arr::get($comment, 'box_id'),
            ]);
        } else {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/index/index',
            ]);
        }
        Log::info('setMiniProgram：', [$send->getMiniProgram()]);
        $send->setData([
            'first' => [
                'value' => '你好，你提出的意见建议已收到反馈'
            ],
            'keyword1' => [
                'value' => Arr::get($comment, 'box.name')
            ],
            'keyword2' => [
                'value' => Carbon::parse(Arr::get($comment, 'created_at'))->format('Y 年m 月d 日')
            ],
            'keyword3' => [
                'value' => Str::limit(Arr::get($model, 'content.content'), 30)
            ],
            'remark' => [
                'value' => '谢谢您的宝贵意见和建议'
            ],
        ]);
        $send->setOpenId($openId);
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChatService.appId'));
        $accessTokenSend->setSecret(config('common.weChatService.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        $send->setAccessToken($accessToken->getAccessToken());
        WeiXinApi::sendMsgService($send);
    }

    public function work($comment, $model)
    {
        // 根据意见箱id获取企业id,
        $getByBoxIdRequest = new GetByBoxIdRequest();
        $getByBoxIdRequest->setBoxId($comment->getAttribute('box_id'));
        $getByBoxIdResponse = Box::getByBoxId($getByBoxIdRequest);
        $companyId = $getByBoxIdResponse->getCompanyId();
        Log::info('$companyId：', [$companyId]);
        $workCorp = new GetWorkCorpIdByCompanyIdRequest();
        $workCorp->setCompanyId($companyId);
        $corpIdMsg = WorkWeChat::getCorpIdByCompanyId($workCorp);
        Log::info('$corpIdMsg：', [$corpIdMsg->getCorpId()]);
        $workUser = WorkWeiXinUsers::query()
            ->where('corp_id', $corpIdMsg->getCorpId())
            ->where('user_id', $comment->getAttribute('user_id'))
            ->first();
        Log::info('$workUser：', [$workUser]);
        if (empty($workUser)) {
            return;
        }
        $work = new WorkMessageSendRequest();
        $work->setToUser($workUser->work_user_id);
        $work->setAppId(config('common.weChat.appId'));
        $work->setTitle('亲，你的意见被回复了哟');
        //获取suiteAccessToken
        $suiteAccessToken = new GetSuiteAccessTokenRequest();
        $suiteAccessToken->setCorpId($workUser->corp_id);
        $suiteAccessToken = WorkWeChat::getSuiteAccessToken($suiteAccessToken);
        Log::info('$suiteAccessToken：', [$suiteAccessToken->getSuiteAccessToken()]);
        //获取企业微信AccessToken
        $workAccessToken = new GetWorkCorpTokenRequest();
        $workAccessToken->setCorpId($workUser->corp_id);
        $workAccessToken->setSuiteAccessToken($suiteAccessToken->getSuiteAccessToken());
        $accessToken = WorkWeChat::getCorpToken($workAccessToken);
        $work->setAccessToken($accessToken->getAccessToken());
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        if (config('app.env') == 'production') {
            $work->setPage('pages/boxes/detail?boxId=' . Arr::get($comment, 'box_id'));
        } else {
            $work->setPage('pages/index/index');
        }
        $work->setContentItem([
            ['key' => '提意见意见箱', 'value' => Arr::get($comment, 'box.name')],
            ['key' => '提意见日期', 'value' => Carbon::parse(Arr::get($comment, 'created_at'))->format('Y 年m 月d 日')],
            ['key' => '回复内容', 'value' => Str::limit(Arr::get($model, 'content.content'), 30)],
            ['key' => '备注', 'value' => '谢谢您的宝贵意见和建议'],
        ]);
        WorkWeChat::sendMsg($work);
    }
}
