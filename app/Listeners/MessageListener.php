<?php

namespace App\Listeners;


use App\Events\MessageEvent;
use App\Models\Box;
use App\Models\BoxUser;
use App\Models\Company;
use App\Models\MessageTemplate;
use App\Models\CompanyUser;
use App\Services\Message\Models\CreateMessageRequest;
use App\Facades\Message;
use Illuminate\Support\Facades\Log;

class MessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param MessageEvent $event
     */
    public function handle(MessageEvent $event)
    {
        $info = $event->info;
        $name = $event->name;
        $boxModel = Box::withTrashed()->where('id', $event->boxId)->first();
        $companyModel = Company::withTrashed()->where('id', $event->companyId)->first();
        if (empty($companyModel->id)) {
            $companyModel = Company::query()->where('id', @$boxModel->company_id)->first();
        }
        log::info('$info', [$info, $companyModel]);
        switch ($info) {
            case'USER-COMPANY-BOX'://用户向企业意见箱提意见
                $userIds = [$boxModel->user_id];
                $companyUser = companyUser::query()
                    ->where(['company_id' => $boxModel->company_id, 'user_id' => $boxModel->user_id])
                    ->first();
                $this->sendMessage('box', $boxModel, $companyModel, '2', $userIds, @$companyUser->name, '');
                break;
            case'USER-BOX'://用户向意见箱提意见
                $userIds = [$boxModel->user_id];
                $list = BoxUser::query()->where('box_id', $boxModel->id)->pluck('user_id')->toArray();
                if (!empty($list)) {
                    $userIds = array_merge($list,$userIds);
                }
                $this->sendMessage('box', $boxModel, $companyModel, '2', $userIds, '', '');
                break;
            case'COMMENT-REPLY'://提意见后收到回复
                $userIds = [$event->userId];
                $this->sendMessage('box', $boxModel, $companyModel, '3', $userIds, '', '');
                break;
            case'COMMENT-ADOPT'://提意见后意见被采纳
                $userIds = [$event->userId];
                $this->sendMessage('box', $boxModel, $companyModel, '4', $userIds, '', '');
                break;
            case'COMPANY-CER-SUC'://企业认证结成功
                $userIds = [$event->userId];
                //$this->sendMessage('system', $boxModel, $companyModel, '5', $userIds, '', '');
                break;
            case'COMPANY-CER-FAIL'://企业认证结果失败
                $userIds = [$event->userId];
                //$this->sendMessage('system', $boxModel, $companyModel, '6', $userIds, '', '');
                break;
            /*case'BOX-HELP'://意见箱创建者把成员设为助手
                $list = trim($name, ',');
                $userIds = explode(',', $list);
                log::info('助手', [$userIds, $list]);
                $companyUser = companyUser::query()
                    ->where(['company_id' => $boxModel->company_id, 'user_id' => $boxModel->user_id])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '16', $userIds, @$companyUser->name, '');
                break;
            case'BOX-DELETE'://意见箱创建者删除意见箱
                $list = BoxUser::query()->where('box_id', $boxModel->id)->pluck('user_id')->toArray();
                if (!empty($list)) {
                    $userIds = $list;
                } else {
                    $userIds = [];
                }
                $companyUser = companyUser::query()
                    ->where(['company_id' => $boxModel->company_id, 'user_id' => $boxModel->user_id])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '7', $userIds, @$companyUser->name, '');
                break;
            case'POWER-MOVE'://超管把权限转移到其他成员
                $userIds = [$event->userId];
                $this->sendMessage('system', $boxModel, $companyModel, '8', $userIds, $name, '');
                break;
            case'POWER-SET'://超管把普通成语设为管理员
                $userIds = [$event->userId];
                $companyUser = companyUser::query()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $name])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '9', $userIds, @$companyUser->name, '');
                break;
            case'POWER-DOWN'://超管把管理员移除为普通成语
                $userIds = [$event->userId];
                $companyUser = companyUser::query()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $name])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '10', $userIds, @$companyUser->name, '');
                break;
            case'POWER-DOWN-ALL'://管理员主动卸任成为普通成员
                $userIds = [$event->userId];
                $companyUser = companyUser::query()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $name])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '11', $userIds, @$companyUser->name, '');
                break;
            case'USER-REMOVE'://超管把普通成员/管理员移出企业管理员把普通成员移出企业
                $userIds = [$event->userId];
                $companyUser = companyUser::query()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $name])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '12', $userIds, @$companyUser->name, '');
                break;
            case'USER-OUT'://普通成员/管理员自行退出企业
                $userIds = [$event->userId];
                $companyUser = companyUser::withTrashed()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $name])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '13', $userIds, @$companyUser->name, '');
                break;
            case'COMPANY-NAME-CHANGE'://企业超管、管理员更改企业名称
                $companyUserIds = companyUser::query()
                    ->where(['company_id' => $companyModel->id])
                    ->pluck('user_id')
                    ->toArray();
                $companyUser = companyUser::query()
                    ->where(['company_id' => $companyModel->id, 'user_id' => $event->userId])
                    ->first();
                $this->sendMessage('system', $boxModel, $companyModel, '14', $companyUserIds, @$companyUser->name, $name);
                break;
            case'USER-FIRST'://新用户第一次登陆超级意见箱
                $userIds = [$event->userId];
                $this->sendMessage('system', $boxModel, $companyModel, '15', $userIds, $name, $name);
                break;*/

        }
        return;
    }

    /**
     * @param $type
     * @param $box
     * @param $company
     * @param $templateId
     * @param $userIds
     * @param $userName
     * @param $nameMessage
     * $userName 用户名字，$nameMessage 活动名字
     */
    public function sendMessage($type, $box, $company, $templateId, $userIds, $userName, $nameMessage)
    {
        $messageTpl = MessageTemplate::query()
            ->where('id', $templateId)
            ->first();
        if (empty($messageTpl->id)) {
            return;
        }
        if (empty($userIds)) {
            return;
        }
        log::info('用户IDS', [$userIds]);
        $messageTpl->content = str_replace('{{$BOX}}', @$box->name, $messageTpl->content);
        $messageTpl->content = str_replace('{{$COMPANY}}', @$company->name, $messageTpl->content);
        $messageTpl->content = str_replace('{{$USER}}', @$userName, $messageTpl->content);
        $messageTpl->content = str_replace('{{$NAME}}', @$nameMessage, $messageTpl->content);
        $companyId = empty($company->id) ? 0 : $company->id;
        $boxId = empty($box->id) ? 0 : $box->id;
        foreach ($userIds as $key => $val) {
            $CreateMessageRequest = new CreateMessageRequest();
            $CreateMessageRequest->setUserId($val);
            $CreateMessageRequest->setCompanyId($companyId);
            $CreateMessageRequest->setBoxId($boxId);
            $CreateMessageRequest->setTemplateId($templateId);
            $CreateMessageRequest->setTitle($messageTpl->title);
            $CreateMessageRequest->setInfo($messageTpl->content);
            if ($type == 'system') {
                Message::CreateSystemMessage($CreateMessageRequest);
            } else {
                Message::CreateBoxMessage($CreateMessageRequest);
            }
        }
    }
}
