<?php

namespace App\Listeners;

use App\Events\CompanyApplyCreateEvent;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CompanyApplyCreateListener
{

    public function handle(CompanyApplyCreateEvent $event)
    {
        $model = $event->model;
        $model->load(['box', 'company']);
        $url = 'https://oapi.dingtalk.com/robot/send?access_token=' . config('common.robot.access_token.company_apply');
        $message = [
            'at' => [
                'isAtAll' => true
            ],
            'text' => [
                "content" => "升级意见箱审核\n" .
                    "环境：prod" . "\n" .
                    "意见箱ID：" . $model->getOriginal('box_id') . "\n" .
                    "意见箱名称：" . Arr::get($model, 'box.name') . "\n" .
                    "公司ID：" . $model->getOriginal('company_id') . "\n" .
                    "公司：" . Arr::get($model, 'company.name')
            ],
            'msgtype' => 'text'
        ];
        if (config('app.env') == 'production') {
            $run = Http::post($url, $message);
            $runMsg = $run->json();
            Log::info('$runMsg：', [$runMsg]);
        }
        return;
    }
}
