<?php


namespace App\Listeners;


use App\Events\CommentAdoptEvent;
use App\Facades\DingTalkApi;
use App\Facades\DingTalkBizData;
use App\Facades\DingTalkCompanyCorp;
use App\Facades\PlatformCode;
use App\Models\Comment;
use App\Models\DingTalkUser;
use App\Services\DingTalkApi\Models\GetCorpAccessTokenRequest;
use App\Services\DingTalkApi\Models\SendByTemplateRequest;
use App\Services\DingTalkBizData\Models\GetAgentIdByCorpIdRequest;
use App\Services\DingTalkBizData\Models\GetSuiteTicketRequest;
use App\Services\DingTalkCompanyCorp\Models\GetCorpIdByCompanyIdRequest;
use App\Facades\Box;
use App\Facades\CompanyPlatform;
use App\Facades\SuiteTicket;
use App\Facades\WeiXinApi;
use App\Facades\WorkWeChat;
use App\Models\WeiXinUser;
use App\Models\WeiXinUserSubscribe;
use App\Models\WorkWeiXinUsers;
use App\Services\Box\Models\GetByBoxIdRequest;
use App\Services\DingTalkApi\Models\GetCorpTokenRequest;
use App\Services\DingTalkUser\Models\GetDingTalkUserIdByUserIdAndCorpIdRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use App\Services\WeiXinApi\Models\UniformMessageSendRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpIdByCompanyIdRequest;
use App\Services\WorkWeChat\Models\GetWorkCorpTokenRequest;
use App\Services\WorkWeChat\Models\WorkMessageSendRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CommentAdoptListener
{
    /**
     * @param CommentAdoptEvent $event
     */
    public function handle(CommentAdoptEvent $event)
    {
        $model = $event->model;
        Log::info('$model333：', [$model]);
        if (empty($model)) {
            return;
        }
       /* if (PlatformCode::isMpDingTalk()) {//钉钉
            $this->dingTalk($model);
            return;
        }
        if (PlatformCode::isMpWeiXin()) {//小程序
            $this->weChat($model);
            return;
        }*/
        $this->dingTalk($model);
        $this->weChat($model);
        if (PlatformCode::isMpWorkWeiXin()) {//企业微信
            $this->work($model);
            return;
        }
    }

    private function dingTalk(Comment $comment)
    {
        //消息推送//查询指定人员，拥有的钉钉的人的资料
        //需要的信息

        Log::info('钉钉采纳推送');
        $userId = $comment->getAttribute('user_id');
        $boxId = $comment->getAttribute('box_id');
        $box = \App\Models\Box::query()
            ->find($boxId);

        Log::info('$box', [$box]);
        if (empty($box)) {
            return;
        }
        $boxName = $box->getAttribute('name');
        $companyId = $box->getAttribute('company_id');

        Log::info('$companyId', [$companyId]);

        Log::info('$userId', [$userId]);
        if (empty($companyId) || empty($userId)) {
            return;
        }

        //获取corpId
        $getCorpIdByCompanyIdRequest = new GetCorpIdByCompanyIdRequest();
        $getCorpIdByCompanyIdRequest->setCompanyId($companyId);
        $getCorpIdByCompanyIdResponse = DingTalkCompanyCorp::getCorpIdByCompanyId($getCorpIdByCompanyIdRequest);
        $corpId = $getCorpIdByCompanyIdResponse->getCorpId();

        Log::info('$corpId', [$corpId]);
        if (empty($corpId)) {
            return;
        }
        $dingTalkUserModel = DingTalkUser::query()
            ->where('user_id', $userId)
            ->where('corp_id', $corpId)
            ->first();

        Log::info('$dingTalkUserModel', [$dingTalkUserModel]);
        if (empty($dingTalkUserModel)) {
            return;
        }
        $dingTalkUserId = $dingTalkUserModel
            ->getAttribute('ding_talk_user_id');

        Log::info('$dingTalkUserId', [$dingTalkUserId]);
        if (empty($dingTalkUserId)) {
            return;
        }
        //获取agentId
        $getAgentIdByCorpIdRequest = new GetAgentIdByCorpIdRequest();
        $getAgentIdByCorpIdRequest->setCorpId($corpId);
        $getAgentIdByCorpIdResponse = DingTalkBizData::getAgentIdByCorpId($getAgentIdByCorpIdRequest);
        $agentId = $getAgentIdByCorpIdResponse->getAgentId();

        Log::info('$agentId', [$agentId]);
        if (empty($agentId)) {
            return;
        }
        //获取suiteTicket
        $getSuiteTicketRequest = new GetSuiteTicketRequest();
        $getSuiteTicketResponse = DingTalkBizData::getSuiteTicket($getSuiteTicketRequest);
        $suiteTicket = $getSuiteTicketResponse->getSuiteTicket();

        Log::info('$suiteTicket', [$suiteTicket]);
        if (empty($suiteTicket)) {
            return;
        }
        //获取CorpAccessToken
        $getCorpAccessTokenRequest = new GetCorpAccessTokenRequest();
        $getCorpAccessTokenRequest->setSuiteTicket($suiteTicket);
        $getCorpAccessTokenRequest->setAuthCorpId($corpId);
        $getCorpAccessTokenResponse = DingTalkApi::getCorpAccessToken($getCorpAccessTokenRequest);
        $accessToken = $getCorpAccessTokenResponse->getAccessToken();

        Log::info('$accessToken', [$accessToken]);
        if (empty($accessToken)) {
            return;
        }
        //发送模板消息
        $sendByTemplateRequest = new SendByTemplateRequest();
        $sendByTemplateRequest->setData([
            'box' => (string)$boxName,
            'boxId' => (string)$boxId
        ]);
        $sendByTemplateRequest->setAccessToken($accessToken);
        $sendByTemplateRequest->setAgentId($agentId);
        $sendByTemplateRequest->setTemplateId(config('common.mpDingTalk.templateId.adoptComment'));
        $sendByTemplateRequest->setUserList($dingTalkUserId);
        DingTalkApi::sendByTemplate($sendByTemplateRequest);
    }


    public function WeChat($model)
    {
        Log::info('$model：', [$model]);
        $wechatUser = WeiXinUser::query()
            ->where('user_id', $model->getAttribute('user_id'))
            ->first();
        Log::info('$wechatUser：', [$wechatUser]);
        if (empty($wechatUser)) {
            return;
        }
        $wechatUserSubscribe = WeiXinUserSubscribe::query()
            ->where('unionid', $wechatUser->getAttribute('union_id'))
            ->first();
        Log::info('$wechatUserSubscribe：', [$wechatUserSubscribe]);
        if (empty($wechatUserSubscribe)) {
            return;
        }
        $openId = $wechatUserSubscribe->getAttribute('openid');
        Log::info('$openId：', [$openId]);
        if (empty($openId)) {
            return;
        }
        $send = new UniformMessageSendRequest();
        $send->setTemplateId(config('common.weChat.templateId.commentAdopt'));
        $send->setAppId(config('common.weChat.appId'));
        if (config('app.env') == 'production') {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/boxes/detail?boxId=' . Arr::get($model, 'box_id'),
            ]);
        } else {
            $send->setMiniProgram([
                'appid' => config('common.weChat.appId'),
                'pagepath' => 'pages/index/index',
            ]);
        }
        Log::info('setMiniProgram：', [$send->getMiniProgram()]);
        $send->setData([
            'first' => [
                'value' => '你好，你提出的意见建议已被采纳'
            ],
            'keyword1' => [
                'value' => Arr::get($model, 'box.name')
            ],
            'keyword2' => [
                'value' => Str::limit(Arr::get($model, 'content.content'), 30)
            ],
            'keyword3' => [
                'value' => Carbon::parse(Arr::get($model, 'updated_at'))->format('Y 年m 月d 日')
            ],
            'remark' => [
                'value' => '点击查看详情'
            ],
        ]);
        $send->setOpenId($openId);
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChatService.appId'));
        $accessTokenSend->setSecret(config('common.weChatService.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        $send->setAccessToken($accessToken->getAccessToken());
        WeiXinApi::sendMsgService($send);
    }

    public function work($model)
    {
        Log::info('$model：', [$model]);
        // 根据意见箱id获取企业id,
        $getByBoxIdRequest = new GetByBoxIdRequest();
        $getByBoxIdRequest->setBoxId(Arr::get($model, 'box.id'));
        $getByBoxIdResponse = Box::getByBoxId($getByBoxIdRequest);
        $companyId = $getByBoxIdResponse->getCompanyId();
        $workCorp = new GetWorkCorpIdByCompanyIdRequest();
        Log::info('$companyId：', [$companyId]);
        $workCorp->setCompanyId($companyId);
        $corpIdMsg = WorkWeChat::getCorpIdByCompanyId($workCorp);
        Log::info('$corpIdMsg：', [$corpIdMsg]);
        if (empty($corpIdMsg->getCorpId())) {
            return;
        }
        $workUser = WorkWeiXinUsers::query()
            ->where('corp_id', $corpIdMsg->getCorpId())
            ->where('user_id', $model->getAttribute('user_id'))
            ->first();
        Log::info('$workUser：', [$workUser]);
        if (empty($workUser)) {
            return;
        }
        $work = new WorkMessageSendRequest();
        $work->setToUser($workUser->work_user_id);
        $work->setAppId(config('common.weChat.appId'));
        $work->setTitle('亲，你提的意见被采纳了');
        //获取suiteAccessToken
        $suiteAccessToken = new GetSuiteAccessTokenRequest();
        $suiteAccessToken->setCorpId($workUser->corp_id);
        $suiteAccessToken = WorkWeChat::getSuiteAccessToken($suiteAccessToken);
        Log::info('$suiteAccessToken：', [$suiteAccessToken->getSuiteAccessToken()]);
        //获取企业微信AccessToken
        $workAccessToken = new GetWorkCorpTokenRequest();
        $workAccessToken->setCorpId($workUser->corp_id);
        $workAccessToken->setSuiteAccessToken($suiteAccessToken->getSuiteAccessToken());
        $accessToken = WorkWeChat::getCorpToken($workAccessToken);
        $work->setAccessToken($accessToken->getAccessToken());
        Log::info('$accessToken：', [$accessToken->getAccessToken()]);
        if (config('app.env') == 'production') {
            $work->setPage('pages/boxes/detail?boxId=' . $model->getKey());
        } else {
            $work->setPage('pages/index/index');
        }
        $work->setContentItem([
            ['key' => '意见箱', 'value' => Arr::get($model, 'box.name')],
            ['key' => '意见内容', 'value' => Str::limit(Arr::get($model, 'content.content'), 30)],
            ['key' => '采纳日期', 'value' => Carbon::parse(Arr::get($model, 'created_at'))->format('Y 年m 月d 日')],
            ['key' => '备注', 'value' => '请您立即查看吧'],
        ]);
        WorkWeChat::sendMsg($work);
    }
}
