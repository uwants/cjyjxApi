<?php


namespace App\Http\Controllers\Api;

use App\Logic\Api\WeChatLogic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WeChatController extends Controller
{
    private WeChatLogic $logic;

    public function __construct(WeChatLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     */
    public function qrCode()
    {
        $this->validate($this->request, [
            'page' => ['required', 'string'],
            'query' => ['sometimes', 'array'],
            'width' => ['sometimes', 'integer'],
        ]);
        $this->logic->qrCode($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function inviteQrCode()
    {
        $this->validate($this->request, [
            'page' => ['required', 'string'],
            'query' => ['sometimes', 'array'],
            'width' => ['sometimes', 'integer'],
        ]);
        $this->logic->inviteQrCode($this->request);
    }

    public function workSuiteToken()
    {
        $this->logic->workSuiteToken($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function getSkin()
    {
        $this->validate($this->request, [
            'appId' => ['sometimes', 'string'],
        ]);
        $this->logic->getSkin($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function companyBoxCode()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'page' => ['required', 'string'],
        ]);
        $this->logic->companyBoxCode($this->request);
    }
}
