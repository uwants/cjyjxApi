<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\SmsLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SmsController extends Controller
{
    private SmsLogic $logic;

    public function __construct(SmsLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     */
    public function sendCodeByMobile()
    {
        $this->validate($this->request, [
            'mobile' => 'string',
        ]);
        $this->logic->sendCodeByMobile($this->request);
    }
}