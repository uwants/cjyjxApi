<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Logic\Api\WechatUserSubscribeLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WechatUserSubscribesController extends Controller
{
    private WechatUserSubscribeLogic $logic;

    public function __construct(WechatUserSubscribeLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function check()
    {
        $this->logic->check($this->request);
    }
}