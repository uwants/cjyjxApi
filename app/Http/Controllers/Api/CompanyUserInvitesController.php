<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\CompanyUserInviteLogic;
use Illuminate\Http\Request;

class CompanyUserInvitesController extends Controller
{
    private CompanyUserInviteLogic $logic;

    public function __construct(Request $request, CompanyUserInviteLogic $logic)
    {
        $this->logic = $logic;
        parent::__construct($request);
    }

    public function store()
    {
        $this->logic->store();
    }

    public function show()
    {
        $this->logic->show($this->request);
    }
}