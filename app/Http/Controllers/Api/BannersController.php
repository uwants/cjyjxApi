<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\BannerLogic;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    private BannerLogic $logic;

    public function __construct(Request $request, BannerLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index();
    }
}