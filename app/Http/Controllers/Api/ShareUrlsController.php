<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\ShareUrlLogic;
use Illuminate\Http\Request;

class ShareUrlsController extends Controller
{
    private ShareUrlLogic $logic;

    /**
     * ShareUrlsController constructor.
     * @param ShareUrlLogic $logic
     */
    public function __construct(Request $request, ShareUrlLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function show()
    {
        $this->logic->show($this->request);
    }
}