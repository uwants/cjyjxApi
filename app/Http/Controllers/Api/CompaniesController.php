<?php


namespace App\Http\Controllers\Api;


use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use App\Logic\Api\CompanyLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CompaniesController extends Controller
{
    private CompanyLogic $logic;

    public function __construct(Request $request, CompanyLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * 企业详情
     */
    public function show()
    {
        $this->logic->show();
    }

    /**
     * 企业列表
     */
    public function index()
    {
        $this->logic->index();
    }

    /**
     * @throws ValidationException
     * 更新企业
     */
    public function update()
    {
        $this->validate($this->request, [
            'companyName' => ['sometimes', 'string'],
            'companyLogo' => ['sometimes', 'string'],
            'companyPoster' => ['sometimes', 'string'],
            'companyProfile' => ['sometimes', 'string'],
            'companyIndustry' => ['sometimes', 'string'],
            'companyAddress' => ['sometimes'],
            'companyAddress.province' => ['required_with:companyAddress'],
            'companyAddress.city' => ['required_with:companyAddress'],
            'companyAddress.area' => ['required_with:companyAddress'],
        ]);
        $this->logic->update($this->request);
    }

    /**
     * @throws ValidationException
     * 创建企业
     */
    public function store()
    {
        $this->validate($this->request, [
            'companyName' => ['required', 'string'],
            'companyLogo' => ['required', 'string'],
            'companyPoster' => ['required', 'string'],
            'companyProfile' => ['required', 'string'],
            'companyIndustry' => ['required', 'string'],
            'companyAddress.province' => ['required', 'string'],
            'companyAddress.city' => ['required', 'string'],
            'companyAddress.area' => ['required', 'string'],
        ]);
        $this->logic->create($this->request);
    }

    /**
     * @throws ValidationException
     * 企业申请认证
     */
    public function apply()
    {
        $this->validate($this->request, [
            'companyApplyIdCardFace' => ['required', 'string'],
            'companyApplyIdCardPositive' => ['required', 'string'],
            'companyApplyBusiness' => ['required', 'string'],
            'companyId' => ['sometimes', 'int']
        ]);
        $this->logic->apply($this->request);
    }

    /**
     * 企业认证状态
     */
    public function applyStatus()
    {
        $this->logic->applyStatus();
    }

    /**
     * 认证失败原因
     * @throws MessageException
     */
    public function applyReason()
    {
        $this->logic->applyReason();
    }

    /**
     * 获取默认主企业
     */
    public function mainShow()
    {
        $this->logic->mainCompany();
    }

    /**
     * @throws ValidationException
     * 默认主企业更新
     */
    public function mainUpdate()
    {
        $this->validate($this->request, [
            'mainCompanyId' => ['required', 'int']
        ]);
        $this->logic->mainCompanyUpdate($this->request);
    }

    public function exist()
    {
        $this->logic->exist();
    }

    public function commentRank()
    {
        $this->logic->commentRank();
    }

    /**
     * @throws ValidationException
     * 企业搜索
     */
    public function searchCompany()
    {
        $this->validate($this->request, [
            'keyword' => ['required', 'string']
        ]);
        $this->logic->searchList($this->request);
    }

    /**
     * @throws ValidationException
     * 天眼查搜索
     */
    public function getSearchCompany()
    {
        $this->validate($this->request, [
            'keyword' => ['required', 'string']
        ]);
        $this->logic->getSearchCompany($this->request);
    }

    /**
     * @throws ValidationException
     * 搜索企业意见箱
     */
    public function getSearchCompanyBox()
    {
        $this->validate($this->request, [
            'keyword' => ['required', 'string']
        ]);
        $this->logic->getSearchCompanyBox($this->request);
    }

    /**
     * 天眼查获取企业信息
     */
    public function createCompany()
    {
        $this->logic->createCompany($this->request);
    }

    public function hotIndex()
    {
        $this->logic->hotIndex();
    }

    public function infosComplete()
    {
        $this->logic->infosComplete();
    }

    /**
     * @throws ValidationException
     */
    public function marketStore()
    {
        $this->validate($this->request, [
            'companyName' => ['required', 'string'],
            'companyAddress.province' => ['required', 'string'],
            'companyAddress.city' => ['required', 'string'],
            'companyAddress.area' => ['required', 'string'],
            'boxName' => ['required', 'string'],
        ]);
        $this->logic->marketCreate($this->request);
    }
}
