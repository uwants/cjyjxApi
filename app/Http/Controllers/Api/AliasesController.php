<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\AliasLogic;
use Exception;
use Illuminate\Http\Request;

class AliasesController extends Controller
{
    protected AliasLogic $logic;

    /**
     * AliasController constructor.
     * @param AliasLogic $logic
     */
    public function __construct(Request $request, AliasLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index();
    }

    /**
     * @throws Exception
     */
    public function show()
    {
        $this->logic->show();
    }
}