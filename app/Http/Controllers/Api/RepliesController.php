<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Logic\Api\ReplyLogic;

class RepliesController extends Controller
{
    private ReplyLogic $logic;

    public function __construct(ReplyLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function store()
    {
        $this->logic->store($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function update()
    {
        $this->logic->update($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }
}
