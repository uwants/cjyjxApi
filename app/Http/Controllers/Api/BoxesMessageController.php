<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\BoxMessageLogic;
use Illuminate\Http\Request;

class BoxesMessageController extends Controller
{

    private BoxMessageLogic $logic;

    public function __construct(BoxMessageLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function read()
    {
        $this->logic->read($this->request);
    }

    public function receiveComment()
    {
        $this->logic->receiveComment();
    }

    public function unReadTotal()
    {
        $this->logic->unReadTotal();
    }

}