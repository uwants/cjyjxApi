<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\QiNiuLogic;
use Illuminate\Http\Request;

class QiNiuController extends Controller
{
    private QiNiuLogic $logic;

    public function __construct(QiNiuLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function token()
    {
        $this->logic->token();
    }

}