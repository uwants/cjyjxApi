<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\BoxPosterLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BoxesPosterController extends Controller
{
    private BoxPosterLogic $logic;

    /**
     * BoxesPosterController constructor.
     * @param BoxPosterLogic $logic
     */
    public function __construct(Request $request, BoxPosterLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     */
    public function show()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int']
        ]);
        $this->logic->show($this->request);
    }
    
    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'boxPoster' => ['required', 'string']
        ]);
        $this->logic->update($this->request);
    }
}