<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\CustomerCompanyLogoLogic;
use Illuminate\Http\Request;

class CustomerCompanyLogosController extends Controller
{
    private CustomerCompanyLogoLogic $logic;

    /**
     * CustomerCompanyLogosController constructor.
     * @param CustomerCompanyLogoLogic $logic
     */
    public function __construct(CustomerCompanyLogoLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function index()
    {
        $this->logic->index();
    }
}