<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Logic\Api\WeChatCallbackLogic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WeChatCallbackController extends Controller
{
    private WeChatCallbackLogic $logic;

    public function __construct(WeChatCallbackLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index();
    }

    public function callbackTest()
    {
       $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $token = 'cjyjx';
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        Log::info('企业微信发送消息：', [$tmpStr, $signature]);
        if ($tmpStr == $signature) {
            echo $_GET['echostr'];
            return true;
        } else {
            echo $_GET['echostr'];
            return true;
        }
        //$this->logic->index();
    }
}
