<?php

namespace App\Http\Controllers\Api;

use App\Logic\Api\ThirdAuthLogic;
use App\Http\Controllers\Controller;
use App\Logic\Api\WorkWechat\WXBizMsgCryptLogic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ThirdAuthController extends Controller
{
    private ThirdAuthLogic $logic;

    public function __construct(ThirdAuthLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
        /*Log::info('微信测试1：', $this->request->all());
        $encodingAesKey = "QkGHxMX6fNlJxzSzVznAGevhugDhFh6sAVBEs93wWdK";
        $token = "KLgfwHi4uVS4cf2WHrD8xrmg1";
        $receiveid = "ww41f0d96e84c59766";
        $wxcpt = new WXBizMsgCryptLogic($token, $encodingAesKey, $receiveid);
        var_dump($wxcpt);
        $sVerifyMsgSig = $this->request->input('msg_signature');
        $sVerifyTimeStamp = $this->request->input('timestamp');
        $sVerifyNonce = $this->request->input('nonce');
        $sVerifyEchoStr=$this->request->input('echostr');
        //$sVerifyEchoStr = $request->getContent();
        Log::info('微信测试2：', [$this->request->getContent()]);
        $sEchoStr = '';
        $errCode = $wxcpt->VerifyURL($sVerifyMsgSig, $sVerifyTimeStamp, $sVerifyNonce, $sVerifyEchoStr, $sEchoStr);
        Log::info('w微信测试3：', [$sEchoStr, $errCode]);
        return $sEchoStr;*/
    }
}
