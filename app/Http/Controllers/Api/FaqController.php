<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\FaqLogic;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    private FaqLogic $logic;

    public function __construct(Request $request, FaqLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }
    public function index()
    {
        $this->logic->index($this->request);
    }
}