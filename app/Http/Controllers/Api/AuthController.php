<?php

namespace App\Http\Controllers\Api;

use App\Facades\PlatformCode;
use App\Http\Controllers\Controller;
use App\Logic\Api\AuthLogic;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    private AuthLogic $logic;

    public function __construct(AuthLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function login()
    {
        $this->validate($this->request, [
            'dingTalkAuthCode' => [Rule::requiredIf(PlatformCode::isMpDingTalk()), 'string'],
            'dingTalkCorpId' => [Rule::requiredIf(PlatformCode::isMpDingTalk()), 'string'],
            'authCode' => [Rule::requiredIf(PlatformCode::isMpWeiXin()), 'string'],
            'corpId' => [Rule::requiredIf(PlatformCode::isMpWorkWeiXin())],
            'mobile' => [Rule::requiredIf(PlatformCode::isPc() || PlatformCode::isH5()), 'string'],
            'code' => [Rule::requiredIf(PlatformCode::isPc() || PlatformCode::isH5()), 'string'],
        ]);
        $this->logic->login($this->request);
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function register()
    {
        $this->validate($this->request, [
            'dingTalkAuthCode' => [Rule::requiredIf(PlatformCode::isMpDingTalk()), 'string'],
            'dingTalkCorpId' => [Rule::requiredIf(PlatformCode::isMpDingTalk()), 'string'],
            'authCode' => [Rule::requiredIf(PlatformCode::isMpWeiXin()), 'string'],
            'corpId' => [Rule::requiredIf(PlatformCode::isMpWorkWeiXin())],
            'iv' => [Rule::requiredIf(PlatformCode::isMpWeiXin() || PlatformCode::isMpWorkWeiXin()), 'string'],
            'encryptedData' => [Rule::requiredIf(PlatformCode::isMpWorkWeiXin()), 'string'],
            'mobile' => [Rule::requiredIf(PlatformCode::isPc() || PlatformCode::isH5()), 'string'],
            'code' => [Rule::requiredIf(PlatformCode::isPc() || PlatformCode::isH5()), 'string'],
        ]);
        $this->logic->register($this->request);
    }
}
