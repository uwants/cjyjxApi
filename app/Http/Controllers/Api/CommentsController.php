<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\MessageException;
use App\Exports\MessageExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Logic\Api\CommentLogic;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;

class CommentsController extends Controller
{
    private CommentLogic $logic;

    public function __construct(CommentLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    /**
     * @throws ValidationException
     * @throws MessageException
     */
    public function store()
    {
        $this->validate($this->request, [
            'commentBoxId' => ['required', 'integer'],
            'commentContent.content' => ['required', 'string', 'max:500'],
            'commentContent.images' => ['sometimes', 'array', 'max:5'],
            'commentContent.videos' => ['sometimes', 'array', 'max:2'],
        ]);
        $this->logic->store($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function adopt()
    {
        $this->validate($this->request, [
            'commentId' => ['required', 'integer'],
        ]);
        $this->logic->adopt($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function cancelAdopt()
    {
        $this->validate($this->request, [
            'commentId' => ['required', 'integer'],
        ]);
        $this->logic->cancelAdopt($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function next()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'commentId' => ['required', 'int']
        ]);
        $this->logic->next($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function update()
    {
        $this->logic->update($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    public function total()
    {
        $this->logic->total();
    }

    /**
     * @throws ValidationException
     */
    public function export()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
        ]);
        return Excel::download(new MessageExport($this->request->input('boxId')), time() . ".xlsx");
    }

}
