<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\NewsLogic;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    private NewsLogic $logic;

    public function __construct(Request $request, NewsLogic $logic)
    {
        $this->logic = $logic;
        parent::__construct($request);
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function guide()
    {
        $this->logic->guide();
    }

    public function show()
    {
        $this->logic->show($this->request);
    }
}
