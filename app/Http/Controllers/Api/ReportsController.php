<?php


namespace App\Http\Controllers\Api;

use App\Events\CodeErrorEvent;
use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use Exception;

class ReportsController extends Controller
{
    /**
     * @throws Exception
     */
    public function index()
    {
        $e = new MessageException('前端错误信息！,' . '信息:' . $this->request->input('title') . ',内容:' . json_encode($this->request->input('msg'), JSON_UNESCAPED_UNICODE));
        event(new CodeErrorEvent($e));
        return ['code' => 0];
    }
}