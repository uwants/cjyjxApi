<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use App\Logic\Api\CompanyUserLogic;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CompanyUsersController extends Controller
{
    private CompanyUserLogic $logic;

    public function __construct(Request $request, CompanyUserLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * 公司成员列表
     */
    public function index()
    {
        $this->logic->index($this->request);
    }

    /**
     * @throws ValidationException
     * @throws MessageException
     * 企业用户退出或删除
     */
    public function destroy()
    {
        $this->validate($this->request, [
            'companyUserId' => ['required', 'int'],
        ]);
        $this->logic->destroy($this->request);
    }

    /**
     * @throws ValidationException|MessageException
     * 企业添加管理员
     */
    public function storeAdmin()
    {
        $this->validate($this->request, [
            'companyUserId' => ['required', 'int'],
        ]);
        $this->logic->storeAdmin($this->request);
    }

    /**
     * @throws ValidationException|MessageException
     * 企业删除管理员
     */
    public function destroyAdmin()
    {
        $this->validate($this->request, [
            'companyUserId' => ['required', 'int'],
        ]);
        $this->logic->destroyAdmin($this->request);
    }

    /**
     * @throws ValidationException
     * 企业成员名称修改
     */
    public function updateName()
    {
        $this->validate($this->request, [
            'companyUserId' => ['required', 'int'],
            'companyUserName' => ['required', 'string'],
        ]);
        $this->logic->updateName($this->request);
    }


    /**
     * @throws MessageException|ValidationException
     */
    public function store()
    {
        $this->validate($this->request, [
            'companyUserInviteCompanyId' => ['required', 'int'],
            'companyUserInviteToken' => ['required', 'string'],
            'companyUserName' => ['required', 'string']
        ]);
        $this->logic->store($this->request);
    }

    /**
     * @throws Exception
     */
    public function supperAdministratorExchange()
    {
        $this->logic->supperAdministratorExchange($this->request);
    }
}
