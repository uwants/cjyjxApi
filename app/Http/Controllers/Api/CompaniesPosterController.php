<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\CompanyPosterLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CompaniesPosterController extends Controller
{
    private CompanyPosterLogic $logic;

    public function __construct(Request $request, CompanyPosterLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     */
    public function show()
    {
        $this->validate($this->request, [
            'companyId' => ['required', 'int']
        ]);
        $this->logic->show($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->validate($this->request, [
            'companyId' => ['required', 'int'],
            'companyPoster' => ['required', 'string']
        ]);
        $this->logic->update($this->request);
    }
}