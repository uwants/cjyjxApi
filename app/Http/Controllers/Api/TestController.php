<?php


namespace App\Http\Controllers\Api;

use App\Events\BoxCreateEvent;
use App\Events\CommentCreateEvent;
use App\Facades\Response;
use App\Facades\User;
use App\Http\Controllers\Controller;
use App\Models\Box;
use App\Services\Response\Models\SuccessRequest;
use App\Services\User\Models\GetAccessTokenByUserIdRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class TestController extends Controller
{
    public function index()
    {
    }

    public function checkMessage()
    {
    }

}
