<?php


namespace App\Http\Controllers\Api;


use App\Facades\Response;
use App\Http\Controllers\Controller;
use App\Services\Response\Models\SuccessRequest;
use Firebase\JWT\JWT;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GoTokenController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function show(): HttpResponseException
    {
        $key = "ILYHermes9";
        $issuedAt = date_format(now(), "Y-m-d") . "T" . date_format(now(), "H:i:s") . "Z";
        $userId = Auth::id();
        $payload = array(
            "userId" => (string)$userId,
            "authenticated" => false,
            "weChatId" => "",
            "issuedAt" => $issuedAt
        );
        $data = [
            'goToken' => JWT::encode($payload, $key, 'HS256')
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}