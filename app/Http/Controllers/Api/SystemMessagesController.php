<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\SystemMessageLogic;
use Illuminate\Http\Request;

class SystemMessagesController extends Controller
{
    private SystemMessageLogic $logic;

    /**
     * SystemMessagesController constructor.
     * @param SystemMessageLogic $logic
     */
    public function __construct(SystemMessageLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function read()
    {
        $this->logic->read($this->request);
    }
}