<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use App\Logic\Api\BoxLogic;
use Illuminate\Validation\ValidationException;

class BoxesController extends Controller
{
    private BoxLogic $logic;

    public function __construct(BoxLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function index()
    {
        $this->logic->index($this->request);
    }

    public function feedback()
    {
        $this->logic->feedback();
    }

    /**
     * @throws ValidationException
     * @throws MessageException
     */
    public function store()
    {
        $this->validate($this->request, [
            'boxName' => ['required', 'string', 'max:20'],
            'boxIsOnlyOnce' => ['required', 'bool'],
            'boxContent.content' => ['required', 'string'],
            'boxType' => ['required', 'int'],
            'boxNotify' => ['sometimes', 'bool'],
            'boxShow' => ['sometimes', 'bool'],
            'boxUserIds' => ['sometimes', 'array'],
            'boxCommentUserIds' => ['sometimes', 'array']
        ]);
        $this->logic->store($this->request);
    }

    /**
     * @throws ValidationException
     * @throws MessageException
     */
    public function show()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
        ]);
        $this->logic->show($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'boxName' => ['required', 'string', 'max:20'],
            'boxContent.content' => ['required', 'string'],
            'boxIsOnlyOnce' => ['required', 'bool'],
        ]);
        $this->logic->update($this->request);
    }

    /**
     * @throws ValidationException
     */
    /**
     * @throws ValidationException
     */
    public function suspend()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
        ]);
        $this->logic->suspend($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function open()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
        ]);
        $this->logic->open($this->request);
    }


    /**
     * @throws ValidationException
     */
    public function destroy()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int']
        ]);
        $this->logic->destroy($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function collectionStore()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int']
        ]);
        $this->logic->collectionStore($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function collectionDestroy()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int']
        ]);
        $this->logic->collectionDestroy($this->request);
    }

    public function collectionIndex()
    {
        $this->logic->collectionIndex($this->request);
    }

    public function total()
    {
        $this->logic->total();
    }

    public function hotIndex()
    {
        $this->logic->hotIndex();
    }

    /**
     * @throws Exception
     */
    public function editShow()
    {
        $this->logic->editShow($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function old()
    {
        $this->validate($this->request, [
            'uuid' => ['required', 'string'],
        ]);
        $this->logic->oldBox($this->request);
    }

    public function getCompany()
    {
        $this->logic->getCompany($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function getComment()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'userId' => ['required', 'int'],
        ]);
        $this->logic->getCommentByUser($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function marketBoxMessage()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
            'time' => ['required', 'int'],
        ]);
        $this->logic->marketBoxMessage($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function moveBox()
    {
        $this->validate($this->request, [
            'boxId' => ['required', 'int'],
        ]);
        $this->logic->moveBox($this->request);
    }

}
