<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Logic\Api\TabCardLogic;
use Illuminate\Http\Request;

class TabCardController extends Controller
{
    private TabCardLogic $logic;

    public function __construct(Request $request, TabCardLogic $logic)
    {
        $this->logic = $logic;
        parent::__construct($request);
    }

    public function index()
    {
        $this->logic->index();
    }
}