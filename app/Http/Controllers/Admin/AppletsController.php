<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Logic\Admin\AppletLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AppletsController extends Controller
{
    private AppletLogic $logic;

    public function __construct(AppletLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index();
    }

    public function update()
    {
        $this->logic->update($this->request);
    }




/**
     * @throws ValidationException
     */
    public function skin()
    {
        $this->validate($this->request, [
            'appId' => ['required', 'string'],
        ]);
        $this->logic->getSkin($this->request);
    }

    public function setSkin()
    {
        $this->logic->setSkin($this->request);
    }



}



