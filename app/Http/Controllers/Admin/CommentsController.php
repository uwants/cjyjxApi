<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Logic\Admin\CommentLogic;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    private CommentLogic $logic;

    public function __construct(CommentLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function explode()
    {
        $this->logic->explode($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }
}
