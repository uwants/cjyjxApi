<?php

namespace App\Http\Controllers\Admin;

use App\Logic\Admin\ArticleLogic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    private ArticleLogic $logic;

    public function __construct(ArticleLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function store()
    {
        $this->logic->store($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    public function all()
    {
        $this->logic->all($this->request);
    }

    public function switch()
    {
        $this->logic->switch($this->request);
    }

}
