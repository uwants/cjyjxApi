<?php

namespace App\Http\Controllers\Admin;

use App\Logic\Admin\PayLogic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PayController extends Controller
{
    private PayLogic $logic;

    public function __construct(PayLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function weChatPay()
    {
        $this->logic->weChatPay($this->request);
    }

    public function weChatNotify()
    {
        $this->logic->weChatNotify($this->request);
    }

    public function WeChatCarry()
    {
        $this->logic->WeChatCarry($this->request);
    }

    public function aliPayCarry()
    {
        $this->logic->aliPayCarry($this->request);
    }

    public function aliPayReturn()
    {
        $this->logic->aliPayReturn();
    }

    public function aliPayNotify()
    {
        $this->logic->aliPayNotify();
    }

}
