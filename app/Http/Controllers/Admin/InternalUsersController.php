<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\InternalUserLogic;
use Illuminate\Http\Request;

class InternalUsersController extends Controller
{
    private InternalUserLogic $logic;

    /**
     * InternalUsersController constructor.
     * @param InternalUserLogic $logic
     */
    public function __construct(Request $request,InternalUserLogic $logic)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }
    public function index()
    {
        $this->logic->index($this->request);
    }
    public function store()
    {
        $this->logic->store($this->request);
    }
    public function destroy()
    {
        $this->logic->destroy($this->request);
    }


}