<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Logic\Admin\MessageLogic;

class MessageController extends Controller
{
    private MessageLogic $logic;

    public function __construct(MessageLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function template()
    {
        $this->logic->template($this->request);
    }

    public function getTemplate()
    {
        $this->logic->getTemplate($this->request);
    }

    public function templateStore()
    {
        $this->logic->templateStore($this->request);
    }

    public function messageLog()
    {
        $this->logic->messageLog($this->request);
    }

    public function systemLog()
    {
        $this->logic->systemLog($this->request);
    }


}
