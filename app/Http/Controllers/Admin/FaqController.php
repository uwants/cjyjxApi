<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\FaqLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FaqController extends Controller
{
    private FaqLogic $logic;

    /**
     * FaqController constructor.
     * @param FaqLogic $logic
     * @param Request $request
     */
    public function __construct(FaqLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function index()
    {
        $this->logic->index($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->logic->update($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    public function store()
    {
        $this->logic->store($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function switchStatus()
    {
        $this->logic->switchStatus($this->request);
    }
}