<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\BannerLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BannersController extends Controller
{
    private BannerLogic $logic;

    /**
     * BannersController constructor.
     * @param BannerLogic $logic
     */
    public function __construct(BannerLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function index()
    {
        $this->logic->index($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->validate($this->request, [
            'image' => ['required', 'url']
        ]);
        $this->logic->update($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function store()
    {
        $this->validate($this->request, [
            'image' => ['required', 'url'],
        ]);
        $this->logic->store($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function switchStatus()
    {
        $this->logic->switchStatus($this->request);
    }
}