<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\BoxHotLogic;
use Illuminate\Http\Request;

class BoxesHotController extends Controller
{
    private BoxHotLogic $logic;

    /**
     * BoxesHotController constructor.
     * @param BoxHotLogic $logic
     */
    public function __construct(BoxHotLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function store()
    {
        $this->logic->store($this->request);
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    public function search()
    {
        $this->logic->search($this->request);
    }

    public function switchStatus()
    {
        $this->logic->switchStatus($this->request);
    }

}