<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Logic\Admin\ReplyLogic;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    private ReplyLogic $logic;

    public function __construct(ReplyLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function explode()
    {
        $this->logic->explode($this->request);
    }

    public function get()
    {
        $this->logic->get($this->request);
    }
}
