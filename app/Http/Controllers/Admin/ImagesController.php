<?php

namespace App\Http\Controllers\Admin;

use App\Facades\Response;
use App\Http\Controllers\Controller;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ImagesController extends Controller
{
    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws Exception
     */
    public function create(Request $request)
    {
        $config_url = config('common.qiniu.QN_CDN_URL');
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filePath = 'image/'.gmdate("Y")."/".gmdate("m")."/".gmdate("d")."/".uniqid(str::random(8)).'.'.$extension;
        $mess=Storage::disk('qiniu')->put($filePath,File::get($file));
        if($mess){
            $url = $config_url.'/'.$filePath;
            $value = $filePath;
            $successRequest = new SuccessRequest();
            $successRequest->setData([
                'url' => $url,
                'value' => $value,
            ]);
            return Response::success($successRequest);
        }else{
            throw new Exception($mess);
        }

    }
}
