<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Logic\Admin\UserLogic;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserLogic $logic;

    public function __construct(UserLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function box()
    {
        $this->logic->box($this->request);
    }

    public function opinion()
    {
        $this->logic->opinion($this->request);
    }

    public function getUserTransaction(){
        $this->logic->getUserTransaction($this->request);
    }
}
