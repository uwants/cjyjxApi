<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\TransactionLogic;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private TransactionLogic $logic;

    public function __construct(TransactionLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function refuse()
    {
        $this->logic->refuse($this->request);
    }

    public function orderList()
    {
        $this->logic->orderList($this->request);
    }

    public function orderMessage()
    {
        $this->logic->orderMessage($this->request);
    }

    public function dataIndex()
    {
        $this->logic->dataIndex($this->request);
    }

    public function countNum()
    {
        $this->logic->countNum();
    }
}
