<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\GamLogic;
use Illuminate\Http\Request;

class GamController extends Controller
{
    private GamLogic $logic;

    public function __construct(GamLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function snake()
    {
        $this->logic->snake($this->request);
    }

    public function snakeStart()
    {
        $this->logic->snakeStart($this->request);
    }
}
