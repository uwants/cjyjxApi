<?php

namespace App\Http\Controllers\Admin;


use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use App\Logic\Admin\AdminLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AdminsController extends Controller
{
    private AdminLogic $logic;

    public function __construct(AdminLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    /**
     * @throws ValidationException
     * @throws MessageException
     * 登录
     */
    public function login()
    {
        $this->validate($this->request, [
            'name' => ['required', 'string'],
            'password' => ['required', 'string'],
        ]);
        $this->logic->login($this->request);
    }

    /**
     * 列表
     */
    public function index()
    {
        $this->logic->index($this->request);
    }

    public function show()
    {
        $this->logic->show($this->request);
    }

    public function update()
    {
        $this->logic->update($this->request);
    }

    /**
     * @throws MessageException
     */
    public function create()
    {
        $this->logic->create($this->request);
    }

    public function delete()
    {
        $this->logic->delete($this->request);
    }

    public function menu()
    {
        $this->logic->menu($this->request);
    }


    public function createMenu()
    {
        $this->logic->createMenu($this->request);
    }

    public function listMenu()
    {
        $this->logic->listMenu($this->request);
    }

    public function deleteMenu()
    {
        $this->logic->deleteMenu($this->request);
    }

    public function role()
    {
        $this->logic->role($this->request);
    }

    /**
     * @throws MessageException
     */
    public function roleCreate()
    {
        $this->logic->roleCreate($this->request);
    }

    public function roleMenu()
    {
        $this->logic->roleMenu($this->request);
    }

    public function updateRole()
    {
        $this->logic->updateRole($this->request);
    }

    /**
     * @throws MessageException
     */
    public function deleteRole()
    {
        $this->logic->deleteRole($this->request);
    }


}
