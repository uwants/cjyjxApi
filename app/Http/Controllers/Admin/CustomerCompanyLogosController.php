<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Logic\Admin\CustomerCompanyLogoLogic;
use Illuminate\Http\Request;

class CustomerCompanyLogosController extends Controller
{
    private CustomerCompanyLogoLogic $logic;

    /**
     * CustomerCompanyLogosController constructor.
     * @param CustomerCompanyLogoLogic $logic
     */
    public function __construct(CustomerCompanyLogoLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index($this->request);
    }

    public function update()
    {
        $this->logic->update($this->request);
    }

    public function store()
    {
        $this->logic->store($this->request);
    }

    public function destroy()
    {
        $this->logic->destroy($this->request);
    }

    public function switchStatus()
    {
        $this->logic->switchStatus($this->request);
    }

}