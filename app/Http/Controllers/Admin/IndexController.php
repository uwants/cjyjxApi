<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Logic\Admin\IndexLogic;
use Illuminate\Http\Request;


class IndexController extends Controller
{
    private IndexLogic $logic;

    public function __construct(IndexLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }

    public function index()
    {
        $this->logic->index();
    }

    public function box()
    {
        $this->logic->thirtyBox();
    }

    public function opinion()
    {
        $this->logic->thirtyOp();
    }

    public function userThirty()
    {
        $this->logic->userThirty();
    }


}
