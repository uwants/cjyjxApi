<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use App\Logic\Admin\CompanyLogic;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CompanyController extends Controller
{

    private CompanyLogic $logic;

    public function __construct(CompanyLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }


    public function apply()
    {
        $this->logic->apply($this->request);
    }

    /**
     * @throws MessageException
     * @throws ValidationException
     */
    public function check()
    {
        $this->validate($this->request, [
            'name' => ['required', 'string'],
            'oldName' => ['required', 'string'],
            'status' => ['required', 'string'],
            'id' => ['required', 'int'],
        ]);
        $this->logic->check($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function index()
    {
        $this->validate($this->request, [
            'filter' => ['required', 'bool'],
            'companyStatus' => ['required', 'string'],
            'status' => ['required', 'string'],
        ]);
        $this->logic->index($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function update()
    {
        $this->validate($this->request, [
            'id' => ['required', 'int'],
            'data' => ['required', 'array'],
        ]);
        $this->logic->update($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function message()
    {
        $this->validate($this->request, [
            'id' => ['required', 'int'],
        ]);
        $this->logic->message($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function companyBox()
    {
        $this->validate($this->request, [
            'companyId' => ['required', 'int'],
        ]);
        $this->logic->companyBox($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function opinion()
    {
        $this->validate($this->request, [
            'companyId' => ['required', 'int'],
        ]);
        $this->logic->opinion($this->request);
    }

    /**
     * @throws ValidationException
     */
    public function member()
    {
        $this->validate($this->request, [
            'companyId' => ['required', 'int'],
        ]);
        $this->logic->member($this->request);
    }

    public function hot()
    {
        $this->logic->hot($this->request);
    }

    /**
     * @throws MessageException
     */
    public function updateHot()
    {
        $this->logic->updateHot($this->request);
    }

    public function deleteHot()
    {
        $this->logic->deleteHot($this->request);
    }

    public function switchHot()
    {
        $this->logic->switchHot($this->request);
    }

    public function select()
    {
        $this->logic->select();
    }

    public function tycSelect()
    {
        $this->logic->tycSelect($this->request);
    }

    public function getTycMessage()
    {
        $this->logic->getTycMessage($this->request);
    }

    public function automatic()
    {
        $this->logic->automatic();
    }



}
