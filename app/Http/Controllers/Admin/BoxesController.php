<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Logic\Admin\BoxLogic;

class BoxesController extends Controller
{
    private BoxLogic $logic;

    public function __construct(BoxLogic $logic, Request $request)
    {
        parent::__construct($request);
        $this->logic = $logic;
    }
    public function index()
    {
        $this->logic->index($this->request);
    }


    public function show()
    {
        $this->logic->show($this->request);
    }

    public function opinion()
    {
        $this->logic->opinion($this->request);
    }

}
