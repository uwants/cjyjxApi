<?php

namespace App\Http\Middleware;

use App\Facades\PlatformCode;
use Closure;
use Illuminate\Http\Request;

class PlatformMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $platformCode = $request->header('Platform-Code', $request->input('platformCode'));
        if ($platformCode) {
            PlatformCode::setPlatformCode($platformCode);
        }
        return $next($request);
    }
}
