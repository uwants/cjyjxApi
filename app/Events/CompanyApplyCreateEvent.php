<?php

namespace App\Events;

use App\Models\CompanyApply;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompanyApplyCreateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public CompanyApply $model;

    /**
     * CompanyApplyCreateEvent constructor.
     * @param CompanyApply $model
     */
    public function __construct(CompanyApply $model)
    {
        $this->model = $model;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
