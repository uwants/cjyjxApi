<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


class MessageEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $info = '';
    public int $companyId = 0;
    public int $boxId = 0;
    public int $userId = 0;
    public string $name = '';

    /**
     * MessageEvent constructor.
     * @param $info
     * @param $companyId
     * @param $boxId
     * @param $userId
     * @param $name
     * $name 活动名字，也可以用于用户名字
     */
    public function __construct($info, $companyId, $boxId, $userId, $name)
    {
        $this->info = $info;
        $this->companyId = $companyId;
        $this->boxId = $boxId;
        $this->userId = $userId;
        $this->name = $name;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
