<?php


namespace App\Events;


use App\Models\Box;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BoxCreateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Box $model;

    /**
     * BoxCreateEvent constructor.
     * @param Box $box
     */
    public function __construct(Box $box)
    {
        $this->model = $box;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}