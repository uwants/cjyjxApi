<?php


namespace App\Events;


use App\Models\Reply;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReplyCreateEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Reply $model;

    /**
     * ReplyCreateEvent constructor.
     * @param Reply $reply
     */
    public function __construct(Reply $reply)
    {
        $this->model = $reply;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}

