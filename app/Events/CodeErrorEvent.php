<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Throwable;

class CodeErrorEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Throwable $e;

    /**
     * CodeErrorEvent constructor.
     * @param Throwable $e
     */
    public function __construct(Throwable $e)
    {
        $this->e = $e;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
