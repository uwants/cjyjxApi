<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FeatureOldBox extends Command
{
    protected $signature = 'feature:oldBox';
    protected $description = 'Command description other';

    /**
     * FeatureOtherData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //$this->getOldBox();
        return 0;
    }

    public function getOldBox()
    {
        $search = DB::connection('mysql_change')->query()
            ->from('boxes')
            ->where('old_box_id', '!=', '')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('old_boxes')
            ->delete();
        $insertList = [];
        foreach ($search as $key => $val) {
            $insertList[$key]['uuid_id'] = $val->old_box_id;
            $insertList[$key]['box_id'] = $val->id;
            $insertList[$key]['company_id'] = $val->company_id;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
        }
        Log::info('xinxix', $insertList);
        DB::connection('mysql_T')->query()
            ->from('old_boxes')
            ->insert($insertList);
    }
}
