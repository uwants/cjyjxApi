<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyHot;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class LogInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lesson:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->automatic();
        //https://app81592.eapps.dingtalkcloud.com
        //https://uat-api2.cjyjx.cn
        Http::post('https://app81592.eapps.dingtalkcloud.com/admin/company/automatic-out', []);
        //Http::post('https://uat-api2.cjyjx.cn/admin/company/automatic-out',[]);
    }

    public function automatic()
    {
        $nowTime = time();
        $model = CompanyHot::withTrashed()->pluck('company_id')->toArray();
        $modelIn = CompanyHot::query()->pluck('company_id')->toArray();
        $number = count($model);
        if ($number <= 10) {//少于十个企业全部开启
            $this->sort($model, $nowTime);
        }
        if ($number <= 20 && $number > 10) {//10个到20个之间
            $list = array_rand($model, 10);
            $newArray = [];
            foreach ($list as $key => $va) {
                $newArray[] = $model[$va];
            }
            $this->sort($newArray, $nowTime);
        }
        if ($number > 20) {
            $listNew = CompanyHot::withTrashed()->whereNotIn('company_id', $modelIn)
                ->pluck('company_id')->toArray();
            $listNum = count($listNew);
            $newArray = [];
            if ($listNum < 10) {
                $list = array_rand($model, 10);
                foreach ($list as $key => $va) {
                    $newArray[] = $model[$va];
                }
            } else {
                $list = array_rand($listNew, 10);
                foreach ($list as $key => $va) {
                    $newArray[] = $listNew[$va];
                }
            }
            $this->sort($newArray, $nowTime);
        }

    }

    public function sort($ids, $time)
    {
        $model = Company::withTrashed()
            ->select('*',
                DB::raw("(select count(id) from opinions where box_id in (select id from boxes where boxes.company_id=companies.id and deleted_at is null) and deleted_at is null) as companyOp"),
            )
            ->whereIn('id', $ids)
            ->orderBy('companyOp', 'asc')
            ->get();
        CompanyHot::withTrashed()->delete();
        foreach ($model as $key => $val) {
            CompanyHot::withTrashed()
                ->where('company_id', $val->id)
                ->update([
                    'created_at' => date('Y-m-d H:i:s', $time + $key),
                    'deleted_at' => null
                ]);
        }
    }
}
