<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\UserPlatform;

class FeatureChange extends Command
{
    protected $signature = 'feature:changeMessage';
    protected $description = 'Command description other';

    /**
     * FeatureOtherData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->userChange();
        return 0;
    }

    public function userChange()
    {
        $list = UserPlatform::query()->get();
        foreach ($list as $key => $val) {
            User::query()
                ->where('id',$val->user_id)
                ->update([
                    'platform'=>$val->platform
                ]);
        }
    }
}
