<?php

namespace App\Console\Commands;

use App\Models\CompanyFollow;
use App\Models\WechatUser;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FeatureData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    const IMAGE_HOST = 'https://cjyjxqny.missapp.com/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     * @throws Exception
     */
    public function handle()
    {
        //$this->featureUser();
        //$this->featureWeiXinUser();
        //$this->featureWeiXinUserSubscribes();
        //$this->featureCompany();
        //$this->featureCompanyUser();
        //$this->featureCompanyUserInvites();
        //$this->featureCompanyApplies();
        //$this->featureBoxUser();
        //$this->featureBoxCommentUsers();
        //$this->featureComments();
        //$this->featureReplies();
        return 0;
    }

    /**
     * 用户表
     */
    private function featureUser()
    {
        $users = DB::connection('mysql_change')->query()
            ->from('users')
            ->get();
        $insertList = [];
        foreach ($users as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['mobile'] = empty($val->mobile) ? '' : $val->mobile;
            $insertList[$key]['name'] = empty($val->name) ? '超级意见箱用户' : $val->name;
            $insertList[$key]['avatar'] = empty($val->avatar) ? config('common.default_avatar') : $val->avatar;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
        }
        DB::connection('mysql_T')->query()
            ->from('users')
            ->delete();
        DB::connection('mysql_T')->query()
            ->from('users')
            ->insert($insertList);

    }

    /**
     * 微信你小程序
     */
    public function featureWeiXinUser()
    {
        $users = DB::connection('mysql_change')->query()
            ->from('wechat_users')
            ->select('id', 'user_id', 'openid', 'unionid', 'session_key', 'created_at', 'updated_at')
            ->get();
        $insertList = [];
        foreach ($users as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['open_id'] = empty($val->openid) ? '' : $val->openid;
            $insertList[$key]['union_id'] = empty($val->unionid) ? '' : $val->unionid;
            $insertList[$key]['session_key'] = empty($val->session_key) ? '' : $val->session_key;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
        }
        DB::connection('mysql_T')->query()
            ->from('wei_xin_users')
            ->delete();
        DB::connection('mysql_T')->query()
            ->from('wei_xin_users')
            ->insert($insertList);
    }

    /**
     * 关注服务号表
     */
    public function featureWeiXinUserSubscribes()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('wechat_user_subscribes')
            ->get();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['openid'] = $val->openid;
            $insertList[$key]['unionid'] = $val->unionid;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('wei_xin_user_subscribes')
            ->delete();
        DB::connection('mysql_T')->query()
            ->from('wei_xin_user_subscribes')
            ->insert($insertList);
    }

    /**
     * 企业表
     */
    public function featureCompany()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('companies')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('companies')
            ->delete();
        $insertList = [];
        $number = count($model);
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['name'] = $val->name;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['status'] = empty($val->certified_status) ? 'STATUS_CERTIFIED_PRE_COMMIT' : $val->certified_status;
            $insertList[$key]['logo'] = empty($val->logo) ? config('common.default_company_logo') : $val->logo;
            $insertList[$key]['profile'] = empty($val->profile) ? '完善简介让更多人认识您的组织' : $val->profile;
            $insertList[$key]['poster'] = empty($val->poster) ? config('common.default_company_poster') : $val->poster;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
            if ($key % 1000 == 0 || ($key + 1) == $number) {
                DB::connection('mysql_T')->query()
                    ->from('companies')
                    ->insert($insertList);
                $insertList = [];
            }
        }
    }

    /**
     * 企业成员
     */
    public function featureCompanyUser()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('company_members')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('company_members')
            ->delete();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['company_id'] = $val->company_id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['name'] = $val->name;
            $insertList[$key]['role'] = $val->role;
            $insertList[$key]['avatar'] = empty($val->avatar) ? config('common.default_avatar') : $val->avatar;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('company_members')
            ->insert($insertList);
    }

    /**
     * 邀请
     */
    public function featureCompanyUserInvites()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('company_user_invites')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('company_user_invites')
            ->delete();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['company_id'] = $val->company_id;
            $insertList[$key]['company_user_id'] = $val->company_user_id;
            $insertList[$key]['token'] = $val->token;
            $insertList[$key]['token_expired_at'] = $val->token_expired_at;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('company_user_invites')
            ->insert($insertList);
    }

    /**
     * 企业申请
     */
    public function featureCompanyApplies()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('company_applies')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('company_applies')
            ->delete();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['box_id'] = $val->box_id;
            $insertList[$key]['company_id'] = $val->company_id;
            $insertList[$key]['id_card_face'] = $val->id_card_face;
            $insertList[$key]['id_card_positive'] = $val->id_card_overleaf;
            $insertList[$key]['business'] = $val->business;
            $insertList[$key]['reject_reason'] = $val->reject_reason;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('company_applies')
            ->insert($insertList);
    }

    /**
     * 意见箱管理员
     */
    public function featureBoxUser()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('box_users')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('box_users')
            ->delete();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['box_id'] = $val->box_id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('box_users')
            ->insert($insertList);
    }

    /**
     * 可提意见
     */
    public function featureBoxCommentUsers()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('box_comment_users')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('box_comment_users')
            ->delete();
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['box_id'] = $val->box_id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('box_comment_users')
            ->insert($insertList);
    }

    /**
     * 意见列表
     */
    public function featureComments()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('opinions')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('opinions')
            ->delete();
        $number = count($model);
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['box_id'] = $val->box_id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['content'] = $val->content;
            $insertList[$key]['adopt'] = $val->adopt;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
            if ($key % 1000 == 0 || ($key + 1) == $number) {
                DB::connection('mysql_T')->query()
                    ->from('opinions')
                    ->insert($insertList);
                $insertList = [];
            }
        }
    }

    public function featureReplies()
    {
        $model = DB::connection('mysql_change')->query()
            ->from('replies')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('replies')
            ->delete();
        $number = count($model);
        $insertList = [];
        foreach ($model as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['comment_id'] = $val->comment_id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['content'] = $val->content;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
            if ($key % 1000 == 0 || ($key + 1) == $number) {
                DB::connection('mysql_T')->query()
                    ->from('replies')
                    ->insert($insertList);
                $insertList = [];
            }
        }
    }

    /**
     * @param $url
     * @return string
     */
    protected function getImageUrl($url): string
    {
        if (!$url) {
            return '';
        }
        if (strpos($url, 'https://') === false) {
            return self::IMAGE_HOST . $url;
        }
        return $url;
    }
}
