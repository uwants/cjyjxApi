<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FeatureOtherData extends Command
{
    protected $signature = 'feature:otherData';
    protected $description = 'Command description other';

    /**
     * FeatureOtherData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //$this->featureBox();
        return 0;
    }

    public function featureBox()
    {
        $search = DB::connection('mysql_change')->query()
            ->from('boxes')
            ->get();
        DB::connection('mysql_T')->query()
            ->from('boxes')
            ->delete();
        $insertList = [];
        $number = count($search);
        foreach ($search as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['user_id'] = $val->user_id;
            $insertList[$key]['name'] = $val->name;
            $insertList[$key]['suspend'] = $val->status == 'STATUS_DISABLE';
            $insertList[$key]['once'] = $val->is_only_once;
            $insertList[$key]['open'] = $val->open;
            $insertList[$key]['all'] = $val->all;
            $insertList[$key]['system'] = $val->system;
            $insertList[$key]['show'] = $val->show;
            $insertList[$key]['content'] = $val->content;
            $insertList[$key]['created_at'] = $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
            if ((empty($val->company_id) || $val->company_id == 0) && (!empty($val->user_id))) {
                $company = DB::connection('mysql_T')->query()
                    ->from('companies')
                    ->where('user_id', $val->user_id)
                    ->where('name', '个人团体')
                    ->first();
                if (empty($company->id)) {//创建个人团体
                    $companyId = $this->createCompany($val->user_id);
                    if (empty($companyId)) {
                        unset($insertList[$key]);
                        continue;
                    } else {
                        $insertList[$key]['company_id'] = $companyId;
                    }
                } else {
                    $insertList[$key]['company_id'] = $company->id;
                }
            } else {
                $insertList[$key]['company_id'] = $val->company_id;
            }
            Log::info($key,$insertList[$key]);
            if ($key % 1000 == 0 || ($key + 1) == $number) {
                DB::connection('mysql_T')->query()
                    ->from('boxes')
                    ->insert($insertList);
                $insertList = [];
            }

        }

    }

    public function createCompany($userId)
    {
        $user = DB::connection('mysql_T')->query()
            ->from('users')
            ->where('id', $userId)
            ->first();
        if (empty($user->id)) {
            return 0;
        }
        $company = DB::connection('mysql_T')->query()
            ->from('companies')
            ->insertGetId(
                [
                    'name' => '个人团体',
                    'status' => 'STATUS_CERTIFIED_PRE_COMMIT',
                    'logo' => config('common.default_company_logo'),
                    'poster' => config('common.default_company_poster'),
                    'user_id' => $userId,
                    'profile' => '完善简介让更多人认识您的组织',
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ]
            );
        //创建超级管理员
        DB::connection('mysql_T')->query()
            ->from('company_members')
            ->insertGetId(
                [
                    'company_id' => $company,
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'avatar' => $user->avatar,
                    'role' => 'ROLE_SUPPER_ADMINISTRATOR',
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ]
            );
        return $company;
    }


}
