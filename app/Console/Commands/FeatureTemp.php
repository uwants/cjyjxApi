<?php

namespace App\Console\Commands;

use App\Facades\CompanyMain;
use App\Models\Alias;
use App\Models\Banner;
use App\Models\Box;
use App\Models\BoxHot;
use App\Models\CompanyHot;
use App\Models\TabCard;
use App\Models\User;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class FeatureTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->featureMobileCode();
//        $this->featureCompanyMain();
        $this->banner();
        $this->companyHot();
        $this->hotBox();
        $this->tabCard();
        return Command::SUCCESS;
    }


    public function featureMobileCode()
    {
        $mobileCodes = DB::connection('mysql_change')
            ->query()
            ->from('mobile_codes')
            ->select('id', 'mobile', 'code', 'expired_at', 'created_at', 'updated_at', 'deleted_at')
            ->get();
        $insertList = [];
        foreach ($mobileCodes as $key => $val) {
            $insertList[$key]['id'] = $val->id;
            $insertList[$key]['mobile'] = $val->mobile;
            $insertList[$key]['code'] = empty($val->code) ? '' : $val->code;
            $insertList[$key]['expired_at'] = empty($val->expired_at) ? '' : $val->expired_at;
            $insertList[$key]['created_at'] = empty($val->created_at) ? '' : $val->created_at;
            $insertList[$key]['updated_at'] = $val->updated_at;
            $insertList[$key]['deleted_at'] = $val->deleted_at;
        }
        DB::connection('mysql_T')->query()
            ->from('mobile_codes')
            ->insert($insertList);
    }

    public function featureCompanyMain()
    {
        $userIds = User::query()
            ->select('id')
            ->pluck('id')
            ->toArray();
        $getCompanyIdRequest = new GetCompanyIdRequest();
        foreach ($userIds as $userId) {
            $getCompanyIdRequest->setUserId($userId);
            CompanyMain::getCompanyId($getCompanyIdRequest);
        }
    }

    public function hotBox()
    {
        $boxHot = BoxHot::withTrashed()
            ->firstOrCreate(['box_id' => 3273]);
        if ($boxHot->trashed()) {
            $boxHot->restore();
        }
        $boxHot = BoxHot::withTrashed()
            ->firstOrCreate(['box_id' => 5113]);
        if ($boxHot->trashed()) {
            $boxHot->restore();
        }
        $boxHot = BoxHot::withTrashed()
            ->firstOrCreate(['box_id' => 5112]);
        if ($boxHot->trashed()) {
            $boxHot->restore();
        }
    }

    public function companyHot()
    {
        $companyHot = CompanyHot::withTrashed()
            ->firstOrCreate(['company_id' => 226]);
        if ($companyHot->trashed()) {
            $companyHot->restore();
        }
        $companyHot = CompanyHot::withTrashed()
            ->firstOrCreate(['company_id' => 34]);
        if ($companyHot->trashed()) {
            $companyHot->restore();
        }
        $companyHot = CompanyHot::withTrashed()
            ->firstOrCreate(['company_id' => 274]);
        if ($companyHot->trashed()) {
            $companyHot->restore();
        }
        $companyHot = CompanyHot::withTrashed()
            ->firstOrCreate(['company_id' => 377]);
        if ($companyHot->trashed()) {
            $companyHot->restore();
        }
        $companyHot = CompanyHot::withTrashed()
            ->firstOrCreate(['company_id' => 681]);
        if ($companyHot->trashed()) {
            $companyHot->restore();
        }

    }

    public function banner()
    {
        $banner = Banner::withTrashed()
            ->firstOrCreate([
                'type' => 1,
                'image' => 'https://cjyjxqny.missapp.com/banne2r.png',
                'suspend' => 0,
                'sort' => 0
            ]);
        if ($banner->trashed()) {
            $banner->restore();
        }
    }

    public function tabCard()
    {
        $tabCard = TabCard::withTrashed()
            ->firstOrCreate([
                'title' => '提意见',
                'image' => '',
                'path' => '',
                'suspend' => 0,
                'sort' => 0
            ]);
        if ($tabCard->trashed()) {
            $tabCard->restore();
        }
        $tabCard = TabCard::withTrashed()
            ->firstOrCreate([
                'title' => '提意见',
                'image' => '',
                'path' => '',
                'suspend' => 0,
                'sort' => 0
            ]);
        if ($tabCard->trashed()) {
            $tabCard->restore();
        }
        $tabCard = TabCard::withTrashed()
            ->firstOrCreate([
                'title' => '企业组织',
                'image' => '',
                'path' => '',
                'suspend' => 0,
                'sort' => 0
            ]);
        if ($tabCard->trashed()) {
            $tabCard->restore();
        }
        $tabCard = TabCard::withTrashed()
            ->firstOrCreate([
                'title' => '操作指引',
                'image' => '',
                'path' => '',
                'suspend' => 0,
                'sort' => 0
            ]);
        if ($tabCard->trashed()) {
            $tabCard->restore();
        }
    }
}
