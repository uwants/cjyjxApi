<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use EasyWeChat\Factory;
use App\Models\WithdrawOrder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function EasyWeChat\Kernel\Support\generate_sign;
use Yansongda\Pay\Pay;

class PayLogic
{

    //调取支付
    public function weChatPay(Request $request): HttpResponseException
    {
        $orderNumber = $request->input('orderNumber');
        $totalFee = $request->input('totalFee');
        $app = Factory::payment(config('wechat.payment.default'));
        $openId = '';
        $result = $app->order->unify([
            'body' => '支付测试',
            'out_trade_no' => $orderNumber,
            'total_fee' => $totalFee * 100,
            'notify_url' => config('wechat.payment.default.notify_url'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            'trade_type' => 'JSAPI', // 请对应换成你的支付方式对应的值类型
            'openid' => $openId,
        ]);
        if ($result['return_code'] === 'SUCCESS') {
            // 二次签名的参数必须与下面相同
            $timeStamp = time();
            $params = [
                'appId' => config('wechat.payment.default.app_id'),
                'timeStamp' => "$timeStamp",
                'nonceStr' => $result['nonce_str'],
                'package' => 'prepay_id=' . $result['prepay_id'],
                'signType' => 'MD5'
            ];
            $params['paySign'] = generate_sign($params, config('wechat.payment.default.key'));
            $params['return'] = $result;
            Log::error('支付', $params);
        } else {
            $params['return'] = $result;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('params'));
        return Response::success($successRequest);
    }

    //回调
    public function weChatNotify(Request $request)
    {
        $postStr = file_get_contents("php://input", "r");
        $notify = (array)simplexml_load_string($postStr, "SimpleXMLElement", LIBXML_NOCDATA);
        $outTradeNo = empty($notify['out_trade_no']) ? '' : $notify['out_trade_no']; // 商户订单号
        $tradeNo = empty($notify['transaction_id']) ? '' : $notify['transaction_id'];  // 微信支付订单号
        $totalFee = empty($notify['total_fee']) ? '' : $notify['total_fee'];//支付总金额
        $orderList = explode('-', $outTradeNo);
        $order = DB::table('orders')
            ->where('order_number', $orderList[0])
            ->first();
        if (empty($order->id)) {
            Log::error('支付回调订单不存在', $notify);
            $str = /** @lang text */
                "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[修改订单状态失败]]></return_msg></xml>";
            echo $str;
            exit('回调失败');
        }
        if ($notify['return_code'] === 'SUCCESS') {
            if ($notify['result_code'] === 'SUCCESS') {
                $pay = DB::table('orders')
                    ->where('id', $order->id)
                    ->update([
                        'status' => 'PAID_DEPOSIT',
                        'deposit_paid_sn' => $tradeNo,
                        'paid_amount' => ($totalFee / 100) + $order->paid_amount,
                    ]);
                if ($pay) {
                    Log::error('支付回调订单支付成功', $notify);
                    $str = /** @lang text */
                        "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
                    echo $str;
                    exit('回调成功');
                } else {
                    Log::error('支付回调订单状态修改失败', $notify);
                    $str = /** @lang text */
                        "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[修改订单状态失败]]></return_msg></xml>";
                    echo $str;
                    exit('回调失败');
                }
            } else {
                Log::error('支付回调业务结果失败', $notify);
                $str = /** @lang text */
                    "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[修改订单状态失败]]></return_msg></xml>";
                echo $str;
                exit('回调失败');
            }
        } else {
            $str = /** @lang text */
                "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[修改订单状态失败]]></return_msg></xml>";
            echo $str;
            exit('回调失败');
        }
    }

    public function WeChatCarry(Request $request): HttpResponseException
    {
        $orderNumber = '';//提取单号，自动生成
        $totalFee = $request->input('totalFee');
        $app = Factory::payment(config('wechat.payment.default'));
        $result = $app->transfer->toBalance([
            'partner_trade_no' => $orderNumber, //特别注意这里，参数跟用户支付给企业out_trade_no区分开来,这里可以使用随机字符串作为订单号，跟红包和支付一个概念。
            'openid' => 'XXXXX', //收款人的openid
            'check_name' => 'NO_CHECK',  //文档中有三种校验实名的方法 NO_CHECK不校验 OPTION_CHECK参数校验 FORCE_CHECK强制校验
            're_user_name' => '',     //OPTION_CHECK FORCE_CHECK 校验实名的时候必须提交
            'amount' => $totalFee * 100,  //单位为分
            'desc' => '钱包提现',
            'spbill_create_ip' => '39.108.XXX.VVV',  //发起交易的服务器IP地址
        ]);
        if ($result['return_code'] === 'SUCCESS') {
            $params['one'] = '';
        } else {
            $params['one'] = '';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('params'));
        return Response::success($successRequest);
    }

    public function aliPayCarry(Request $request): HttpResponseException
    {
        $order = WithdrawOrder::query()
            ->where('id', $request->input('id'))
            ->first();
        $result = pay::alipay(config('aliPay.pay'))->transfer([
            'out_biz_no' => date('Ymd', time()) . time(),
            'trans_amount' => number_format($order->amount,2,'.',''),
            'product_code' => 'TRANS_ACCOUNT_NO_PWD',
            'biz_scene' => 'DIRECT_TRANSFER',
            'payee_info' => [
                'identity' => 'aulapx1333@sandbox.com',//用户唯一标识
                'identity_type' => 'ALIPAY_LOGON_ID',//1、ALIPAY_USER_ID 支付宝的会员ID 2、ALIPAY_LOGON_ID：支付宝登录号，支持邮箱和手机号格式
                'name' => 'aulapx1333'//用户姓名
            ],
        ]);
        if ($result['msg'] === 'Success') {
            $params['order_id'] = $result['order_id'];
        } else {
            $params['one'] = '111111';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('result','params'));
        return Response::success($successRequest);
    }

    //响应支付宝回调
    public function aliPayReturn()
    {
        return pay::alipay(config('aliPay.pay'))->success();
    }

    //支付宝回调处理
    public function aliPayNotify()
    {
        $result = pay::alipay(config('aliPay.pay'))->callback();
    }


}
