<?php

namespace App\Logic\Admin;

use App\Facades\Permission;
use App\Facades\Response;
use App\Models\Box;
use App\Models\Comment;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class BoxLogic
{
    public function index(Request $request): HttpResponseException
    {
        if ($request->input('exportData') == 1) {
            ini_set('memory_limit', '1000M');
            set_time_limit(0);
            $columns = [
                ['title' => '意见箱名字', 'key' => 'name',],
                ['title' => '创建者', 'key' => 'opinionPeople',],
                ['title' => '创建者手机', 'key' => 'opinionPeopleMobile',],
                ['title' => '意见箱类型', 'key' => 'boxStatus'],
                ['title' => '参与人数', 'key' => 'people'],
                ['title' => '收到意见', 'key' => 'number'],
            ];
        } else {
            $columns = [
                ['title' => '信息', 'key' => 'id', 'minWidth' => 280, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left', 'sortable' => 'custom'],
                ['title' => '意见箱内容', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '参与人数', 'key' => 'people', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '收到意见', 'key' => 'number', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '打赏', 'key' => 'money', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '是否公开', 'key' => 'open', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '仅限一次', 'key' => 'onlyOnce', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '是否删除', 'key' => 'isDelete', 'minWidth' => 100, 'align' => 'center', 'slot' => 'delete'],
                ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ];
        }

        $model = Box::withTrashed()
            ->select('boxes.*',
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id and deleted_at is null) as number"),
                DB::raw("(select count(DISTINCT(user_id)) from opinions where opinions.box_id=boxes.id and deleted_at is null) as people"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.box_id=boxes.id and deleted_at is null)) as money"),
            )
            ->with('company')
            ->with('user');
        //内部人员意见箱过滤、同时意见总数不过滤内部人员的意见数。
        if ($request->input('filter')) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找
        if ($request->input('keyword')) {
            switch ($request->input('status')) {
                case'name':
                    $model->where('name', 'like', '%' . $request->input('keyword') . '%');
                    break;
                case'user':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    })
                        ->orWhereIn('company_id', function ($query) use ($request) {
                            $query->select('id')
                                ->from('companies')
                                ->where('name', 'like', '%' . $request->input('keyword') . '%');
                        });
                    break;
            }
        }
        switch ($request->input('boxStatus')) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $model->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $model->orderBy('id', 'desc');
        }
        if ($request->input('exportData') == 1) {
            $item = $model->get();
            $items = [];
            foreach ($item as $key => $val) {
                $items[$key]['name'] = $val->name;
                $items[$key]['opinionPeople'] = empty($val->user->name) ? '' : $val->user->name;
                $items[$key]['opinionPeopleMobile'] = empty($val->user->mobile) ? '' : $val->user->mobile;
                $items[$key]['boxStatus'] = $val->company_id > 0 ? '企业意见箱' : '个人意见箱';
                $items[$key]['people'] = $val->people;
                $items[$key]['number'] = $val->number;
                $items[$key]['money'] = number_format($val->money, 2, '.', '');
            }
            $successRequest = new SuccessRequest();
            $successRequest->setData([
                'items' => $items,
                'columns' => $columns,
            ]);
            return Response::success($successRequest);
        } else {
            $box = $model->paginate($request->input('pageSize'));
            $item = $box->items();
            $total = $box->total();
        }
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['open'] = $val->open ? '是' : '否';
            $items[$key]['onlyOnce'] = $val->once ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxStatus'] = $val->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $items[$key]['number'] = $val->number;
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['people'] = $val->people;
            $items[$key]['opinionPeopleMobile'] = empty($val->user->mobile) ? '' : $val->user->mobile;
            $company = empty($val->company->name) ? '' : $val->company->name;
            $userPeople = empty($val->user->name) ? '' : $val->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
            if ($val->company_id > 0) {
                $items[$key]['avatar'] = empty($val->company->logo) ? '' : $val->company->logo;
            } else {
                $items[$key]['avatar'] = empty($val->user->avatar) ? '' : $val->user->avatar;
            }
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['exportBox', 'boxUserMobile']);//导出意见箱
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }


    public function show(Request $request): HttpResponseException
    {
        $model = Box::withTrashed()
            ->select('boxes.*',
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id) as number"),
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id and opinions.adopt=1 and opinions.deleted_at is null ) as adoptNumber"),
                DB::raw("(select count(id) from replies where replies.comment_id in( select id from opinions where opinions.box_id=boxes.id) and replies.deleted_at is null ) as replyNumber"),
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id and opinions.deleted_at is not null) as delNumber"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.box_id=boxes.id and deleted_at is null)) as money"),
            //DB::raw("(select count(id) from box_visits where box_visits.box_id=boxes.id and box_visits.type='share') as share"),
            //DB::raw("(select count(id) from box_visits where box_visits.box_id=boxes.id and box_visits.type='qr_code') as code")
            )
            ->with('company')
            ->with('user')
            ->where('id', $request->input('id'))
            ->first();
        $list = [];
        $list['name'] = $model->name;
        $list['id'] = $model->id;
        $list['content'] = Arr::get($model->content, 'content');
        $list['anonymousType'] = $model->is_allow_anonymous ? true : false;
        $list['onlyOnce'] = $model->is_only_once ? true : false;
        $list['isDelete'] = $model->deleted_at ? true : false;
        $list['deleteTime'] = empty($model->deleted_at) ? '' : date($model->deleted_at);
        $list['createTime'] = date($model->created_at);
        $list['boxStatus'] = $model->company_id > 0 ? '企业意见箱' : '个人意见箱';
        $list['number'] = $model->number;
        //$items['money'] = number_format($model->money, 2, '.', '');
        $list['status'] = $model->status == 'STATUS_ENABLE';
        $company = empty($model->company->name) ? '' : $model->company->name;
        $userPeople = empty($model->user->name) ? '' : $model->user->name;
        if ($company && empty($userPeople)) {
            $list['opinionPeople'] = $company;
        } elseif (empty($company) && $userPeople) {
            $list['opinionPeople'] = $userPeople;
        } elseif (!empty($company) && !empty($userPeople)) {
            $list['opinionPeople'] = $company . '/' . $userPeople;
        } else {
            $list['opinionPeople'] = '';
        }
        if ($model->company_id > 0) {
            $list['avatar'] = empty($model->company->logo) ? '' : $model->company->logo;
        } else {
            $list['avatar'] = empty($model->user->avatar) ? '' : $model->user->avatar;
        }
        $list['numOne'] = array(
            ['label' => '收到意见数量', 'num' => $model->number],
            ['label' => '正常意见总数', 'num' => $model->number - $model->delNumber],
            ['label' => '已删除意见总数', 'num' => $model->delNumber],
            ['label' => '采纳意见总数', 'num' => $model->adoptNumber],
            ['label' => '回复意见总数', 'num' => $model->replyNumber],
            ['label' => '打赏', 'num' => (float)number_format($model->money, 2, '.', '')],
            //['label' => '访问次数', 'num' => $model->share + $model->code],
            //['label' => '扫码访问', 'num' => $model->share],
            //['label' => '微信分享访问', 'num' => $model->code]
        );
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['boxCommentList']);//查看意见内容
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $list['power'] = $getPermissionResponse->getPowerList();
        $successRequest = new SuccessRequest();
        $successRequest->setData($list);
        return Response::success($successRequest);
    }

    public function opinion(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '用户信息', 'key' => 'userId', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'sortable' => 'custom'],
            ['title' => '意见内容', 'key' => 'contentMsg', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '是否已回复', 'key' => 'replayType', 'minWidth' => 110, 'align' => 'center', 'slot' => 'replay'],
            ['title' => '打赏', 'key' => 'money', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '是否采纳', 'key' => 'adoptType', 'minWidth' => 100, 'align' => 'center', 'slot' => 'adopt'],
            //['title' => '是否已读', 'key' => 'readType', 'minWidth' => 100, 'align' => 'center', 'slot' => 'read'],
            ['title' => '是否删除', 'key' => 'isDelete', 'minWidth' => 100, 'align' => 'center', 'slot' => 'delete'],
            ['title' => '提出时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit'],
        ];
        $boxMsg = Box::withTrashed()
            ->where('id', $request->input('boxId'))
            ->first();

        $companyId = $boxMsg->company_id;
        $model = Comment::withTrashed()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with('user')
            ->with(['replies' => function ($query) use ($companyId) {
                $query->with(['companyUser' => function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                }]);
            }])
            ->where('box_id', $request->input('boxId'));
        if ($request->input('keyword')) {
            switch ($request->input('keywordState')) {
                case'mobile':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')->from('users')->where('mobile', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'nickName':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')->from('users')->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
            }
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $model->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $model->orderBy('id', 'desc');
        }
        $box = $model->paginate($request->input('pageSize'));
        $item = $box->items();
        $total = $box->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['userId'] = $val->user_id;
            $items[$key]['content'] = $val->content;
            $items[$key]['contentMsg'] = Arr::get($val->content, 'content');
            $items[$key]['anonymous'] = $val->anonymous;
            $items[$key]['anonymousType'] = $val->anonymous ? '是' : '否';
            $items[$key]['adopt'] = $val->adopt;
            $items[$key]['adoptType'] = $val->adopt ? '是' : '否';
            $items[$key]['read'] = $val->read;
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['readType'] = $val->read ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['userMsg'] = $val->user;
            if (!empty($val->replies)) {
                foreach ($val->replies as $ke => $va) {
                    $items[$key]['reply'][$ke]['replyName'] = empty($va->companyUser[0]) ? @$va->user->name : $va->companyUser[0]->name;
                    $items[$key]['reply'][$ke]['avatar'] = empty($va->companyUser[0]) ? @$va->user->avatar : $va->companyUser[0]->avatar;
                    $items[$key]['reply'][$ke]['replyTime'] = date($va->created_at);
                    $items[$key]['reply'][$ke]['readType'] = $va->read == 1 ? '已读' : '未读';
                    $items[$key]['reply'][$ke]['replyContent'] = Arr::get($va->content, 'content');
                }
            } else {
                $items[$key]['reply'] = [];
            }
            $items[$key]['replayType'] = empty($items[$key]['reply']) ? '0' : '1';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

}
