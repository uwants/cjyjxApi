<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\MessageTemplate;
use App\Models\BoxMessage;
use App\Models\SystemLog;

class MessageLogic
{

    public function template(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '标题', 'key' => 'title', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '内容', 'key' => 'content', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '备注', 'key' => 'remark', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '类型', 'key' => 'type', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 210, 'align' => 'center', 'slot' => 'edit'],
        ];
        $model = MessageTemplate::query();
        if ($request->input('keyword')) {
            $model->where('title', 'like', '%' . $request->input('keyword') . '%');
        }
        $model->orderBy('id', 'desc');
        $template = $model->paginate($request->input('pageSize'));
        $item = $template->items();
        $total = $template->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['title'] = $val->title;
            $items[$key]['content'] = $val->content;
            $items[$key]['remark'] = $val->remark;
            $items[$key]['type'] = $val->type;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function getTemplate(Request $request): HttpResponseException
    {
        $model = MessageTemplate::query()
            ->where('id', $request->input('id'))
            ->first();
        if (empty($model->id)) {
            $arrayList = ['id' => 0, 'title' => '', 'type' => 'BOX-MESSAGE', 'content' => '', 'remark' => ''];
        } else {
            $arrayList = $model->toArray();
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($arrayList);
        return Response::success($successRequest);
    }

    public function templateStore(Request $request): HttpResponseException
    {
        $data = (array)$request->input('data');
        if (!empty($data['id'])) {
            MessageTemplate::query()
                ->where('id', $data['id'])
                ->update([
                    'title' => $data['title'],
                    'content' => $data['content'],
                    'type' => $data['type'],
                    'remark' => $data['remark'],
                ]);
            $id = $data['id'];
        } else {
            $model = MessageTemplate::query()
                ->insertGetId([
                    'title' => $data['title'],
                    'content' => $data['content'],
                    'type' => $data['type'],
                    'remark' => $data['remark'],
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ]);
            $id = $model;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(['id' => $id]);
        return Response::success($successRequest);
    }

    public function messageLog(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '接受者', 'key' => 'user', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header',],
            ['title' => '详情', 'key' => 'message', 'minWidth' => 180, 'align' => 'left', 'slot' => 'some', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '标题', 'key' => 'title', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '内容', 'key' => 'content', 'minWidth' => 250, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '发送时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
        ];
        $model = BoxMessage::query()
            ->with('user')
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }]);
        //订单关键字查找
        if ($request->input('keyword')) {
            switch ($request->input('searchType')) {
                case'mobile'://订单号
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('mobile', 'like', "%" . $request->input('keyword') . "%");
                    });
                    break;
                case
                'alias'://匿名分省
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('alias_name', 'like', "%" . $request->input('keyword') . "%");
                    });
                    break;
            }
        }
        $model->orderBy('id', 'desc');
        $template = $model->paginate($request->input('pageSize'));
        $item = $template->items();
        $total = $template->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['title'] = $val->title;
            $items[$key]['userId'] = $val->user_id;
            $items[$key]['content'] = $val->content;
            $items[$key]['aliasName'] = @$val->user->alias_name;
            $items[$key]['avatar'] = empty($val->user->alias_avatar) ? config('common.default_avatar') : $val->user->alias_avatar;
            $items[$key]['mobile'] = @$val->user->mobile;
            $items[$key]['read'] = $val->read;
            $items[$key]['companyName'] = @$val->company->name;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function systemLog(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => 'ID', 'key' => 'id', 'width' => 80, 'align' => 'center',],
            ['title' => '标题', 'key' => 'typeName', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '内容', 'key' => 'message', 'minWidth' => 500, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'createTime', 'width' => 100, 'align' => 'center', 'slot' => 'edit'],
        ];
        $model = SystemLog::query();
        if ($request->input('searchType') && $request->input('searchType') != 'all') {
            $model->where('type', $request->input('searchType'));
        }
        $model->orderBy('id', 'desc');
        $template = $model->paginate($request->input('pageSize'));
        $item = $template->items();
        $total = $template->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['typeName'] = $val->TypeDesc;
            $items[$key]['message'] = $val->message;
            $items[$key]['messageArray'] = json_decode($val->message);
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }
}
