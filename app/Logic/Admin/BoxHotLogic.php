<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Http\Controllers\Controller;
use App\Models\Box;
use App\Models\BoxHot;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BoxHotLogic extends Controller
{
    public function index(Request $request): HttpResponseException
    {
        $pageSize = $request->input('pageSize');
        $page = $request->input('page');
        $model = BoxHot::withTrashed()
            ->whereHas('box')
            ->with('box')
            ->latest()
            ->paginate($pageSize, ['*'], 'page', $page);
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = $val->box->id;
            $items[$key]['name'] = $val->box->name;
            $items[$key]['content'] = empty(Arr::get($val->box->content, 'content')) ? '无' : Arr::get($val->box->content, 'content');
            $items[$key]['open'] = (bool)$val->box->open;
            if (empty($val->deleted_at)) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
        }

        $columns = [
            ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '意见箱名字', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '意见箱内容', 'key' => 'content', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
        ];
        $total = $model->total();
        $pageTotal = $model->lastPage();
        $data = (compact('items', 'total', 'page', 'pageSize', 'columns', 'pageTotal'));
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        $boxId = $request->input('boxId');
        $model = BoxHot::withTrashed()
            ->updateOrCreate([
                'box_id' => $boxId
            ]);
        if ($model->trashed()) {
            $model->restore();
        }
        $boxId = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxId'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        BoxHot::query()
            ->where('box_id', $request->input('boxId'))
            ->forceDelete();
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function search(Request $request): HttpResponseException
    {
        $model = Box::query()
            ->where('name', 'like', '%' . $request->input('keyword') . '%')
            ->where('suspend', false)
            ->where('open', true)
            ->latest()
            ->limit(20)
            ->get();

        $data = [];
        foreach ($model as $item) {
            $data[] = [
                'boxId' => $item->id,
                'boxName' => $item->name,
                'boxContent' => $item->content
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }


    public function switchStatus(Request $request): HttpResponseException
    {
        $model = BoxHot::withTrashed()
            ->where('box_id', $request->input('boxId'))
            ->first();
        if ($request->input('status')) {
            $model->restore();
        } else {
            $model->delete();
        }
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }
}