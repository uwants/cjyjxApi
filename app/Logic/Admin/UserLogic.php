<?php

namespace App\Logic\Admin;

use App\Facades\Response;
use App\Facades\Permission;
use App\Models\Comment;
use App\Models\User;
use App\Models\Box;
use App\Models\WeiXinUser;
use App\Models\Company;
use App\Models\Order;
use App\Models\WithdrawOrder;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class UserLogic
{
    public function index(Request $request): HttpResponseException
    {
        if (@$request->input('exportData') == 1) {
            $columns = [
                ['title' => '用户ID', 'key' => 'id'],
                ['title' => '用户昵称', 'key' => 'nickName'],
                ['title' => '用户手机', 'key' => 'mobile'],
                ['title' => '用户来源', 'key' => 'platform'],
                ['title' => '是否企业超级管理员、管理员', 'key' => 'admin'],
                ['title' => '创建意见箱数量', 'key' => 'boxNum'],
                ['title' => '发表意见数量', 'key' => 'opinionNum'],
                ['title' => '打赏', 'key' => 'activeMoney'],
                ['title' => '被打赏', 'key' => 'passiveMoney'],
            ];
        } elseif (@$request->input('exportData') == 2) {
            $columns = [
                ['type' => 'selection', 'width' => 50, 'align' => 'center'],
                ['title' => '用户信息', 'key' => 'id', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header'],
                ['title' => '所属企业', 'key' => 'company', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '意见箱数', 'key' => 'boxNum', 'minWidth' => 85, 'align' => 'center'],
                ['title' => '提意见数', 'key' => 'opinionNum', 'minWidth' => 85, 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ];
        } else {
            $columns = [
                ['title' => '用户信息', 'key' => 'id', 'minWidth' => 250, 'sortable' => 'custom', 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '所属企业', 'key' => 'company', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '意见箱数', 'key' => 'boxNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '提意见数', 'key' => 'opinionNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '打赏', 'key' => 'activeMoney', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '被打赏', 'key' => 'passiveMoney', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '平台', 'key' => 'platform', 'minWidth' => 120, 'slot' => 'platform', 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center', 'slot' => 'follow'],
                ['title' => '注册时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ];
        }
        $userModel = User::query()
            ->select('mobile', 'id', 'created_at', 'name', 'avatar', 'platform', 'alias_name',
                DB::raw("(select count(id) from boxes where boxes.user_id = users.id and deleted_at is null) as boxNum"),
                DB::raw("(select count(id) from opinions where opinions.user_id = users.id and deleted_at is null) as opinionNum"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and from_user_id=users.id) as activeMoney"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and to_user_id=users.id) as passiveMoney"),
            )
            ->with(['userUnionid' => function ($query) {
                $query->with('wapOpenId');
            }])
            ->with('company')
            ->with(['companyUser' => function ($query) {
                $query->where('company_id', '!=', 0)
                    ->with(['company' => function ($query) {
                        return call_user_func([$query, 'withTrashed']);
                    }]);
            }]);
        if (@$request->input('exportData') == 2) {
            $userModel->whereIn('id', function ($query) {
                $query->select('user_id')->from('wei_xin_users')
                    ->whereIn('union_id', function ($wechat) {
                        $wechat->select('unionid')->from('wei_xin_user_subscribes')
                            ->where('deleted_at', null);
                    });
            });
        }
//        过滤代码
        if ($request->input('filter')) {
            $userModel->whereNotIn('id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //订单关键字查找
        if ($request->input('keyword')) {
            switch ($request->input('searchType')) {
                case'mobile'://订单号
                    $userModel->where('mobile', 'like', "%" . $request->input('keyword') . "%");
                    break;
                case'ID':
                    $userModel->where('id', $request->input('keyword'));
                    break;
                case'nickName':
                    $userModel->where('name', 'like', "%" . $request->input('keyword') . "%");
                    break;
            }
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $userModel->whereBetween('created_at', [$start, $end]);
        }
        if (!empty($request->input('platform')) && $request->input('platform') != 'all') {
            if ($request->input('platform') == 'PC' || $request->input('platform') == 'BUSINESS-WEB') {
                $userModel->where('platform', 'PC')
                    ->orWhere('platform', 'BUSINESS-WEB');
            } else {
                $userModel->where('platform', $request->input('platform'));
            }
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $userModel->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $userModel->orderBy('id', 'desc');
        }

        if ($request->input('exportData') == 1) {
            $item = $userModel->get();
            $total = count($item);
        } else {
            $user = $userModel->paginate($request->input('pageSize'));
            $item = $user->items();
            $total = $user->total();
        }
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['mobile'] = $val->mobile;
            $items[$key]['nickName'] = $val->name;
            $items[$key]['aliasName'] = empty($val->alias_name) ? '' : $val->alias_name;
            $items[$key]['headImg'] = empty($val->avatar) ? '' : $val->avatar;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxNum'] = $val->boxNum;
            $items[$key]['opinionNum'] = $val->opinionNum;
            $items[$key]['activeMoney'] = number_format($val->activeMoney, 2, '.', '');
            $items[$key]['passiveMoney'] = number_format($val->passiveMoney, 2, '.', '');
            $items[$key]['follow'] = empty($val->userUnionid->wapOpenId->id) ? '否' : '是';
            $items[$key]['wapOpenId'] = empty($val->userUnionid->wapOpenId->openid) ? '' : $val->userUnionid->wapOpenId->openid;
            $items[$key]['company'] = '';
            $items[$key]['admin'] = '否';
            $items[$key]['platform'] = empty($val->platform) ? 'MP-WEIXIN' : $val->platform;
            $items[$key]['platformDesc'] = $val->PlatformDesc;
            $items[$key]['companyUser'] = $val->companyUser;
            foreach ($val->companyUser as $k => $user) {
                if ($user->role == 'ROLE_SUPPER_ADMINISTRATOR') {
                    $items[$key]['admin'] = '是';
                    $items[$key]['company'] .= @$user->company->name . '【超级管理员】、';
                } elseif ($user->role == 'ROLE_ADMINISTRATOR') {
                    $items[$key]['admin'] = '是';
                    $items[$key]['company'] .= @$user->company->name . '【管理员】、';
                } else {
                    $items[$key]['admin'] = '否';
                    $items[$key]['company'] .= @$user->company->name . '【普通成员】、';
                }
            }
            $items[$key]['company'] = empty($items[$key]['company']) ? '--' : $items[$key]['company'];
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['userExport', 'userMobile']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => (int)$request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);

    }

    public function show(Request $request): HttpResponseException
    {
        $user = User::query()
            ->with('wallet')
            ->where('id', $request->input('id'))
            ->first();
        //return call_user_func([$query, 'withTrashed']);
        $company = Company::query()
            ->where('user_id', $request->input('id'))
            ->orWhereIn('id', function ($query) use ($request) {
                $query->from('company_members')
                    ->select('company_id')
                    ->where('user_id', $request->input('id'))
                    ->where('deleted_at', null);
            })
            ->with(['company_users' => function ($query) use ($request) {
                $query->where('user_id', $request->input('id'));
            }])
            ->get();
        if (!empty($user->id)) {
            $returnList['id'] = $user->id;
            $returnList['name'] = $user->name;
            $returnList['mobile'] = $user->mobile;
            $returnList['balance'] = number_format(empty($user->wallet->balance) ? 0.00 : $user->wallet->balance, 2, '.', '');
            $returnList['total_amount'] = number_format(empty($user->wallet->total_amount) ? 0.00 : $user->wallet->total_amount, 2, '.', '');
            $returnList['frozen_amount'] = number_format(empty($user->wallet->frozen_amount) ? 0.00 : $user->wallet->frozen_amount, 2, '.', '');
            $returnList['history_amount'] = number_format(empty($user->wallet->history_amount) ? 0.00 : $user->wallet->history_amount, 2, '.', '');
            $returnList['avatar'] = $user->avatar;
            $returnList['platform'] = $user->PlatformDesc;
            $returnList['alias'] = empty($user->alias_name) ? '无' : $user->alias_name;
            $returnList['alias_avatar'] = empty($user->alias_avatar) ? '' : $user->alias_avatar;
            $returnList['createTime'] = date($user->created_at);
            $returnList['updateTime'] = date($user->updated_at);
            $returnList['applet'] = [];
            $appletList = WeiXinUser::query()
                ->where('user_id', $user->id)
                ->get();
            foreach ($appletList as $key => $val) {
                $returnList['applet'][$key] = $val->AppletDesc;
            }
            $companyList = [];
            $companyRole = array(
                'ROLE_SUPPER_ADMINISTRATOR' => '超级管理员',
                'ROLE_ADMINISTRATOR' => '管理员',
                'ROLE_NORMAL' => '普通成员',
            );
            foreach ($company as $ke => $va) {
                $companyList[$ke]['companyName'] = $va->name;
                $companyList[$ke]['companyUser'] = empty($va->company_users[0]->name) ? '' : $va->company_users[0]->name;
                $companyList[$ke]['companyRole'] = empty($va->company_users[0]->role) ? '' : $companyRole[$va->company_users[0]->role];
            }
            $returnList['company'] = empty($companyList) ? '--' : $companyList;
            $returnList['moneyMess'] = [
                ['num' => (float)$returnList['history_amount'], 'label' => '累计收入'],
                ['num' => (float)$returnList['total_amount'], 'label' => '总金额(元)'],
                ['num' => (float)$returnList['balance'], 'label' => '账户余额'],
                ['num' => (float)$returnList['frozen_amount'], 'label' => '冻结金额'],
            ];
            $returnList['reward'] = $this->userMoney($request->input('id'));
        } else {
            $returnList = [];
        }

        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['userExport', 'userMobile', 'userBoxPower', 'userAmount']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $returnList['power'] = $getPermissionResponse->getPowerList();
        $successRequest = new SuccessRequest();
        $successRequest->setData($returnList);
        return Response::success($successRequest);
    }

    public function box(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '意见箱状态', 'key' => 'status', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '收到意见', 'key' => 'number', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '打赏', 'key' => 'money', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '是否仅限提一次', 'key' => 'only', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '简介', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit'],
        ];
        $box = Box::query()
            ->select('boxes.*',
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id and deleted_at is null) as number"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.box_id=boxes.id and deleted_at is null)) as money"),
            )
            ->where('user_id', $request->input('userId'));
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $box->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $box->orderBy('id', 'desc');
        }
        $boxList = $box->paginate($request->input('pageSize'));
        $item = $boxList->items();
        $total = $boxList->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['number'] = $val->number;
            $items[$key]['status'] = empty($val->suspend) ? '开启' : '关闭';
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            // $items[$key]['allow'] = $val->is_allow_anonymous == 1 ? '是' : '否';
            $items[$key]['only'] = $val->once == 1 ? '是' : '否';
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function opinion(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '所属企业', 'key' => 'companyName', 'minWidth' => 110, 'align' => 'center'],
            ['title' => '意见内容', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '打赏', 'key' => 'money', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
            //['title' => '是否匿名', 'key' => 'anonymous', 'minWidth' => 80, 'align' => 'center'],
            ['title' => '提出时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit'],
        ];
        $opinion = Comment::query()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with(['box' => function ($query) {
                $query->with('company');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user')
            ->where('user_id', $request->input('userId'));
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $opinion->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $opinion->orderBy('id', 'desc');
        }
        $opList = $opinion->paginate($request->input('pageSize'));
        $item = $opList->items();
        $total = $opList->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = empty($val->box->name) ? '' : $val->box->name;
            $items[$key]['companyName'] = empty($val->box->company->name) ? '' : $val->box->company->name;
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            //$items[$key]['anonymous'] = $val->anonymous == 1 ? '是' : '否';
            $items[$key]['adopt'] = $val->adopt == 1 ? '是' : '否';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function getUserTransaction(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '交易类型', 'key' => 'id', 'minWidth' => 100, 'center' => 'center', 'slot' => 'message'],
            ['title' => '交易金额', 'key' => 'amount', 'minWidth' => 100, 'align' => 'center', 'slot' => 'money'],
            ['title' => '状态', 'key' => 'state', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '交易时间', 'key' => 'createdTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '交易详情', 'key' => 'edit', 'minWidth' => 180, 'align' => 'left', 'slot' => 'edit'],
        ];
        $order = Order::query()
            ->select('id', 'order_id', 'opinion_id', 'amount', 'order_state', 'order_state as alipay_order_state', 'to_user_id', 'from_user_id', 'from_user as review_user', 'to_user as reject_reason', 'created_at')
            ->with(['opinion' => function ($query) {
                $query->with('box');
            }])
            ->whereIn('id', function ($query) use ($request) {
                $query->select('id')
                    ->where('from_user_id', $request->input('id'))
                    ->orWhere('to_user_id', $request->input('id'));
            });
        $withdrawOrder = WithdrawOrder::query()
            ->select('id', 'order_id', 'id as opinion_id', 'amount', 'state as order_state', 'alipay_order_state', 'to_user_id', 'id as from_user_id', 'review_user', 'reject_reason', 'created_at')
            ->where('to_user_id', $request->input('id'));
        //类型
        switch ($request->input('type')) {
            case'accept'://收到打赏
                $order->where('to_user_id', $request->input('id'));
                $withdrawOrder->where('id', 0);
                break;
            case'active'://打赏
                $order->where('from_user_id', $request->input('id'));
                $withdrawOrder->where('id', 0);
                break;
            case'withdrawal'://提现
                $withdrawOrder->where('to_user_id', $request->input('id'));
                $order->where('id', 0);
                break;
        }
        //状态
        switch ($request->input('state')) {
            case'success':
                $order->where('order_state', 'SUCCESS');
                $withdrawOrder->where('order_state', 'success');
                break;
            case'fail':
                $order->where('order_state', 'PAYERROR');
                $withdrawOrder->where('order_state', 'fail');
                break;
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $order->whereBetween('created_at', [$start, $end]);
            $withdrawOrder->whereBetween('created_at', [$start, $end]);
        }
        $order->unionAll($withdrawOrder);
        $order->orderBy('created_at', 'desc');
        $userOrder = $order->paginate($request->input('pageSize'));
        $item = $userOrder->items();
        $total = $userOrder->total();
        $items = [];
        $withdrawOrderStatusDesc = [
            'SUCCESS' => '成功',
            'wait' => '等待审核',
            'WAIT' => '等待审核',
            'REJECT' => '拒绝',
        ];
        $withdrawOrderAliPayStatusDesc = [
            'SUCCESS' => '提现成功',
            'PAYEE_NOT_EXIST' => '提现失败',
        ];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['amount'] = number_format($val->amount, 2, ' . ', '');
            $items[$key]['createdTime'] = date($val->created_at);
            $items[$key]['toUserId'] = $val->to_user_id;
            $items[$key]['boxName'] = empty($val->opinion->box->name) ? '' : $val->opinion->box->name;
            if (strstr($val->order_id, 'order') || empty($val->order_id)) {
                $items[$key]['orderType'] = 'order';
                if ($val->from_user_id == $request->input('id')) {
                    $items[$key]['TransactionType'] = 'orderReduce';
                } elseif ($val->to_user_id == $request->input('id')) {
                    $items[$key]['TransactionType'] = 'orderAdd';//被打赏
                } else {
                    $items[$key]['TransactionType'] = '';
                }
                $items[$key]['state'] = $val->OrderStates;
                $items[$key]['reviewUser'] = '';
                $items[$key]['rejectReason'] = '';
            } else {
                $items[$key]['orderType'] = 'draw';
                $items[$key]['TransactionType'] = 'draw';
                $orderState = empty(@$withdrawOrderAliPayStatusDesc[$val->alipay_order_state]) ? '提现失败' : @$withdrawOrderAliPayStatusDesc[$val->alipay_order_state];
                $items[$key]['state'] = $withdrawOrderStatusDesc[$val->order_state] . "(" . $orderState . ")";
                $items[$key]['reviewUser'] = $val->review_user;
                $items[$key]['rejectReason'] = $val->reject_reason;
            }
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => (int)$request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function userMoney($userId)
    {
        //总打赏次数
        $orderAll = Order::query()
            ->where('from_user_id', $userId)
            ->count();
        //实际打赏次数
        $orderReally = Order::query()
            ->where('from_user_id', $userId)
            ->where('order_state', 'SUCCESS')
            ->count();
        //打赏总金额
        $orderAllMoney = Order::query()
            ->where('from_user_id', $userId)
            ->sum('amount');
        //实际打赏金额
        $orderReallyMoney = Order::query()
            ->where('from_user_id', $userId)
            ->where('order_state', 'SUCCESS')
            ->sum('amount');

        //被打赏总次数
        $orderAllOther = Order::query()
            ->where('to_user_id', $userId)
            ->count();
        //实际被打赏次数
        $orderReallyOther = Order::query()
            ->where('to_user_id', $userId)
            ->where('order_state', 'SUCCESS')
            ->count();
        //被打赏总金额
        $orderAllMoneyOther = Order::query()
            ->where('to_user_id', $userId)
            ->sum('amount');
        //实际被打赏金额
        $orderReallyMoneyOther = Order::query()
            ->where('to_user_id', $userId)
            ->where('order_state', 'SUCCESS')
            ->sum('amount');

        //提现总次数
        $withAll = WithdrawOrder::query()
            ->where('to_user_id', $userId)
            ->count();
        //成功提现次数
        $withAllReally = WithdrawOrder::query()
            ->where('to_user_id', $userId)
            ->where('state', 'SUCCESS')
            ->count();
        //提现总金额
        $withAllMoney = WithdrawOrder::query()
            ->where('to_user_id', $userId)
            ->sum('amount');
        //实际提现金额
        $withAllMoneyReally = WithdrawOrder::query()
            ->where('to_user_id', $userId)
            ->where('alipay_order_state', 'SUCCESS')
            ->sum('amount');
        return array(
            'orderAll' => [
                'orderAll' => (int)$orderAll,
                'orderAllReally' => (int)$orderReally,
                'orderPer' => $orderAll > 0 ? $orderReally / $orderAll * 100 : 0,
                'orderAllMoney' => (float)$orderAllMoney,
                'orderReallyMoney' => (float)$orderReallyMoney,
                'orderMoneyPer' => $orderAllMoney > 0 ? $orderReallyMoney / $orderAllMoney * 100 : 0
            ],
            'orderMoney' => [
                'orderAllOther' => $orderAllOther,
                'orderReallyOther' => $orderReallyOther,
                'orderOtherPer' => $orderAllOther > 0 ? $orderReallyOther / $orderAllOther * 100 : 0,
                'orderAllMoneyOther' => (float)$orderAllMoneyOther,
                'orderReallyMoneyOther' => (float)$orderReallyMoneyOther,
                'orderMoneyOtherPer' => $orderAllMoneyOther > 0 ? $orderReallyMoneyOther / $orderAllMoneyOther * 100 : 0
            ],
            'withOther' => [
                'withAll' => (int)$withAll,
                'withAllReally' => (int)$withAllReally,
                'withOtherPer' => $withAll > 0 ? $withAllReally / $withAll * 100 : 0,
                'withAllMoney' => (float)$withAllMoney,
                'withAllMoneyReally' => (float)$withAllMoneyReally,
                'withAllMoneyPer' => $withAllMoney > 0 ? $withAllMoneyReally / $withAllMoney * 100 : 0
            ]
        );
    }

}
