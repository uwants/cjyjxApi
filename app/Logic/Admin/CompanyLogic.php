<?php

namespace App\Logic\Admin;

use App\Events\MessageEvent;
use App\Exceptions\MessageException;
use App\Facades\Permission;
use App\Facades\PlatformLog;
use App\Facades\Response;
use App\Models\Box;
use App\Models\User;
use App\Models\Company;
use App\Models\Company as CompanyModel;
use App\Models\CompanyApply;
use App\Models\CompanyUser;
use App\Models\CompanyHot;
use App\Models\Comment;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\PlatformLog\Models\CreateLogRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class CompanyLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     * 企业列表
     */
    public function index(Request $request): HttpResponseException
    {
        if ($request->input('exportData') == 1) {
            set_time_limit(0);
            $columns = [
                ['title' => '企业ID', 'key' => 'id'],
                ['title' => '企业名称', 'key' => 'companyName'],
                ['title' => '组织人数', 'key' => 'companyUser'],
                ['title' => '企业管理员', 'key' => 'admin'],
                ['title' => '管理员电话', 'key' => 'adminMobile'],
                ['title' => '累计创建意见箱条数', 'key' => 'companyBox'],
                ['title' => '累计意见数', 'key' => 'companyOp'],
                ['title' => '累计打赏', 'key' => 'money'],
            ];
        } else {
            $columns = [
                ['title' => '公司信息', 'key' => 'id', 'minWidth' => 280, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'sortable' => 'custom'],
                ['title' => '平台', 'key' => 'platform', 'minWidth' => 100, 'slot' => 'platform', 'align' => 'center'],
                ['title' => '组织人数', 'key' => 'companyUser', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '意见箱', 'key' => 'companyBox', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '意见数', 'key' => 'companyOp', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '打赏', 'key' => 'money', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '企业状态', 'key' => 'companyStatus', 'minWidth' => 100, 'align' => 'center', 'slot' => 'status'],
                ['title' => '行业', 'key' => 'companyIndustry', 'width' => 100, 'align' => 'center'],
                ['title' => '地址', 'key' => 'companyAddressStr', 'width' => 100, 'align' => 'center'],
                ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'width' => 210, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ];
        }
        $model = Company::query()
            ->select('*',
                DB::raw("(select count(id) from company_members where company_members.company_id=companies.id and deleted_at is null) as companyUser"),
                DB::raw("(select count(id) from boxes where boxes.company_id=companies.id and deleted_at is null) as companyBox"),
                DB::raw("(select count(id) from opinions where box_id in (select id from boxes where boxes.company_id=companies.id and deleted_at is null) and deleted_at is null) as companyOp"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.company_id=companies.id and deleted_at is null)) as money"),
            )
            ->with('user');
        //过滤代码
        if ($request->input('filter') == 'true') {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找企业
        if ($request->input('keyword')) {
            $keyword = $request->input('keyword');
            switch ($request->input('status')) {
                case'companyName':
                    $model->where('name', 'like', "%$keyword%");
                    break;
                case'industry':
                    $model->where('industry', 'like', "%$keyword%");
                    break;
            }
        }
        switch ($request->input('companyStatus')) {
            case'certified':
                $model->where('status', 'STATUS_CERTIFIED_PASS');
                break;
            case'noCertified':
                $model->where('status', '!=', 'STATUS_CERTIFIED_PASS');
                break;
        }
        switch ($request->input('peopleStatus')) {
            case'claim':
                $model->where('user_id', '!=', 0);
                break;
            case'noClaim':
                $model->where('user_id', 0);
                break;
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $model->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $model->orderBy('id', 'desc');
        }
        if (!empty($request->input('platform') && $request->input('platform') != 'all')) {
            if ($request->input('platform') == 'PC' || $request->input('platform') == 'BUSINESS-WEB') {
                $model->where('platform', 'PC')
                    ->orWhere('platform', 'BUSINESS-WEB');
            } else {
                $model->where('platform', $request->input('platform'));
            }

        }
        if ($request->input('exportData') == 1) {
            $item = $model->get();
            $total = count($item);
        } else {
            $apply = $model->paginate($request->input('pageSize'));
            $item = $apply->items();
            $total = $apply->total();
        }
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['companyName'] = $val->name;
            $items[$key]['companyUser'] = $val->companyUser;
            $items[$key]['companyBox'] = $val->companyBox;
            $items[$key]['companyOp'] = $val->companyOp;
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['logo'] = empty($val->logo) ? config('common.default_company_logo') : $val->logo;
            $items[$key]['poster'] = empty($val->poster) ? config('common.default_company_poster') : $val->poster;
            $items[$key]['profile'] = $val->profile;
            $items[$key]['alias'] = $val->alias;
            $items[$key]['industry'] = empty($val->industry) ? '--' : $val->industry;
            $items[$key]['platform'] = empty($val->platform) ? 'MP-WEIXIN' : $val->platform;
            $items[$key]['admin'] = empty($val->user->name) ? '--' : $val->user->name;
            $items[$key]['adminMobile'] = empty($val->user->mobile) ? '--' : $val->user->mobile;
            $items[$key]['companyStatus'] = $val->status == 'STATUS_CERTIFIED_PASS' ? '1' : '0';
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['companyIndustry'] = empty($val->industry) ? '未填行业' : $val->getAttribute('industry');
            $items[$key]['companyAddressStr'] = empty($val->addressStr) ? '未填地址' : $val->addressStr;
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['exportCompany', 'editCompany', 'companyComment', 'companyMobile']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 企业审核
     */
    public function check(Request $request): HttpResponseException
    {
        DB::beginTransaction();
        $apply = CompanyApply::query()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }])
            ->where('id', $request->input('id'))
            ->first();
        if (empty($apply->id)) {
            throw new MessageException('申请信息异常！');
        }
        if ($apply->company->status == 'STATUS_CERTIFIED_PASS') {
            throw new MessageException('该组织已经被认证！');
        }
        if (!empty($apply->company->name)) {
            $companyHas = Company::query()
                ->where('name', $apply->company->name)
                ->where('status', 'STATUS_CERTIFIED_PASS')
                ->where('id', '!=', $apply->company_id)
                ->get();
        }
        if ($request->input('status') == 'DELETE') {//不通过
            CompanyApply::query()
                ->where('id', $request->input('id'))
                ->update(['reject_reason' => $request->input('reason'), 'deleted_at' => date('Y-m-d H:i:s', time())]);
            Company::query()
                ->where('id', $apply->company_id)
                ->update(['status' => 'STATUS_CERTIFIED_FAILED']);
            //发送消息
            event(new MessageEvent('COMPANY-CER-FAIL', $apply->company_id, 0, $apply->company->user_id, ''));
        } else {//
            $company = Company::query()
                ->where('id', $apply->company_id)
                ->update([
                    'user_id' => $apply->user_id,
                    'status' => 'STATUS_CERTIFIED_PASS',
                    'name' => $request->input('name')
                ]);
            //天眼查企业数据修改删除
            /* Company::query()
                 ->where('user_id', 0)
                 ->where('name', $request->input('oldName'))
                 ->delete();*/
            $box = Box::query()
                ->where(['user_id' => 0, 'name' => $request->input('oldName'), 'system' => 1])
                ->orWhereRaw("(user_id = " . $apply->company->user_id . " and `name` = '" . $request->input('oldName') . "' and `system`= 1)")
                ->first();
            if (!$company) {
                DB::rollBack();
                throw new MessageException('企业信息不存在审核失败！');
            }
            if ($request->input('merge') == 'yes') {
                $reject_reason = '合并认证';
            } else {
                $reject_reason = '';
                //增加超级管理员
                $this->superAdmin($apply->user_id, $apply->company_id);
            }
            if (empty($box->id)) {
                //创建意见箱
                $model = Box::query()->firstOrCreate([
                    'user_id' => $apply->user_id,
                    'name' => $request->input('name'),
                    'suspend' => 0,
                    'once' => 0,
                    'content' => '[]',
                    'company_id' => $apply->company_id,
                    'system' => 1,
                    'open' => 1,
                    'show' => 1,
                    'all' => 0,
                ]);
                CompanyApply::query()
                    ->where('id', $request->input('id'))
                    ->update(['box_id' => $model->getOriginal('id'), 'reject_reason' => $reject_reason]);
                CompanyApply::query()
                    ->where('id', '!=', $request->input('id'))
                    ->where('company_id', $apply->company_id)
                    ->update(['reject_reason' => '已经被认证', 'deleted_at' => date('Y-m-d H:i:s', time())]);
            } else {
                Box::query()
                    ->where('id', $box->id)
                    ->update([
                        'user_id' => $apply->company->user_id,
                        'name' => $request->input('name'),
                        'company_id' => $apply->company_id,
                        'once' => 0,
                        'open' => 1,
                        'show' => 1,
                    ]);
                CompanyApply::query()
                    ->where('id', $request->input('id'))
                    ->update(['box_id' => $box->id, 'reject_reason' => $reject_reason]);
                CompanyApply::query()
                    ->where('id', '!=', $request->input('id'))
                    ->where('company_id', $apply->company_id)
                    ->update(['reject_reason' => '已经被认证', 'deleted_at' => date('Y-m-d H:i:s', time())]);
            }
            //合并处理
            if (count($companyHas) > 0 && $request->input('merge') == 'yes') {
                $this->mergeMessage($companyHas, $apply);
                DB::table('system_logs')
                    ->insert([
                        'type' => 'merge_company',
                        'message' => json_encode(['$companyHas' => $companyHas, '$apply' => $apply]),
                        'created_at' => date('Y-m-d H:i:s', time()),
                        'updated_at' => date('Y-m-d H:i:s', time())
                    ]);
            }
            event(new MessageEvent('COMPANY-CER-SUC', $apply->company_id, 0, $apply->company->user_id, ''));
        }
        DB::commit();
        $successRequest = new SuccessRequest();
        $successRequest->setData([]);
        return Response::success($successRequest);
    }

    public function superAdmin($userId, $companyId)
    {
        $userMsg = User::query()->where('id', $userId)->first();
        $companyUser = CompanyUser::withTrashed()
            ->where('company_id', $companyId)
            ->where('role', 'ROLE_SUPPER_ADMINISTRATOR')
            ->first();
        if (empty($companyUser->id)) {
            CompanyUser::query()
                ->insertGetId([
                    'company_id' => $companyId,
                    'user_id' => $userId,
                    'name' => empty($userMsg->name) ? '' : $userMsg->name,//
                    'role' => 'ROLE_SUPPER_ADMINISTRATOR',
                    'avatar' => config('common.default_avatar'),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ]);
        }
    }

    /**
     * @param $hasCompany
     * @param $applyCompany
     * @throws MessageException
     * $hasCompany 存在的企业，$applyCompany 认证的企业
     * 已存在的企业信息合并到认证的企业中
     */
    public function mergeMessage($hasCompany, $applyCompany)
    {
        foreach ($hasCompany as $key => $value) {
            //合并意见箱
            //合并企业用户
            if ($value->user_id != $applyCompany->company->user_id) {
                throw new MessageException('合并企业的超级管理员跟认证人不匹配，合并失败！请确认身份后再操作。');
            }
            company::query()
                ->where('id', $value->id)
                ->delete();
            box::query()
                ->where('company_id', $value->id)
                ->update(['company_id' => $applyCompany->company_id]);
            $mergeCompanyUser = CompanyUser::query()//被转移的企业用户
            ->where('company_id', $value->id)
                ->get();
            $mainCompanyUserModel = CompanyUser::query()//接受转移的企业用户
            ->where('company_id', $applyCompany->company_id)
                ->pluck('user_id')
                ->toArray();
            foreach ($mergeCompanyUser as $ke => $val) {
                if (!in_array($val->user_id, $mainCompanyUserModel)) {
                    CompanyUser::query()
                        ->where('id', $val->id)
                        ->update([
                            'company_id' => $applyCompany->company_id,
                            'role' => $val->role
                        ]);
                } else {
                    //存在企业中，如果是管理员需要保留管理
                    if ($val->role == 'ROLE_ADMINISTRATOR') {
                        CompanyUser::query()
                            ->where('id', $val->id)
                            ->update([
                                'role' => $val->role
                            ]);
                    } else {
                        CompanyUser::query()
                            ->where('id', $val->id)
                            ->delete();
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 审核列表
     */
    public function apply(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '公司信息', 'key' => 'id', 'minWidth' => 250, 'align' => 'left', 'slot' => 'company', 'ellipsis' => true],
            ['title' => '认证人', 'key' => 'title', 'minWidth' => 230, 'align' => 'left', 'slot' => 'user'],
            ['title' => '审核状态', 'key' => 'status', 'width' => 100, 'align' => 'center', 'slot' => 'status'],
            ['title' => '身份证正面', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'idCard'],
            ['title' => '身份证反面', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'idCardBack'],
            ['title' => '证件', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'buss'],
            ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ];
        $model = CompanyApply::withTrashed()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with(['box' => function ($query) {
                $query->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user');
        //关键字查找企业
        if ($request->input('keyword')) {
            $keyword = $request->input('keyword');
            $model->whereIn('company_id', function ($query) use ($keyword) {
                $query->select('id')
                    ->from('companies')
                    ->where('name', 'like', "%$keyword%");
            });
        }
        if ($request->input('status') == 'pass') {//已审核状态
            $model->Where('box_id', '>', 0)
                ->where('deleted_at', '=', null);
        }
        if ($request->input('status') == 'wait') {//待审核状态
            $model->where('box_id', 0)
                ->where('deleted_at', '=', null);
        }
        if ($request->input('status') == 'noPass') {
            $model->where('deleted_at', '!=', null);
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($time[0]));
            $end = date('Y-m-d H:i:S', strtotime($time[0]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        $model->orderBy('id', 'desc');
        $model->orderBy('box_id', 'desc');
        $company = $model->paginate($request->input('pageSize'));
        $item = $company->items();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['companySource'] = empty($val->company->user_id) ? '天眼查' : '自建组织';
            $items[$key]['companyName'] = @$val->company->name;
            $items[$key]['companyLogo'] = empty($val->company->logo) ? config('common.default_company_logo') : $val->company->logo;
            $items[$key]['industry'] = @$val->company->industry;
            $items[$key]['userMobile'] = empty($val->user->mobile) ? @$val->box->user->mobile : $val->user->mobile;
            $items[$key]['userName'] = empty($val->user->name) ? @$val->box->user->name : $val->user->name;
            $items[$key]['userAvatar'] = empty($val->user->avatar) ? config('common.default_company_logo') : $val->user->avatar;
            $items[$key]['image'] = empty($val->image) ? config('common.default_company_logo') : $val->image;
            $items[$key]['idCard'] = empty($val->id_card_face) ? config('common.default_company_logo') : $val->id_card_face;
            $items[$key]['idCardBack'] = empty($val->id_card_positive) ? config('common.default_company_logo') : $val->id_card_positive;
            $items[$key]['buss'] = empty($val->business) ? config('common.default_company_logo') : $val->business;
            $items[$key]['reason'] = $val->reject_reason;
            $items[$key]['companyHas'] = $val->CompanyHas;
            if (!empty($val->deleted_at)) {
                $items[$key]['status'] = 2;
                $items[$key]['statusValue'] = 1;//审核按钮屏蔽
            } elseif (!empty($val->box_id)) {
                $items[$key]['status'] = 1;
                $items[$key]['statusValue'] = 1;
            } else {
                $items[$key]['status'] = 0;
                $items[$key]['statusValue'] = 0;
            }
            $items[$key]['createTime'] = date($val->created_at);
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['examinePower']);//
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $powerList = $getPermissionResponse->getPowerList();
        if (!$powerList['examinePower']) {
            unset($columns[7]);//意见内容
            $columns = array_values($columns);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $company->total(),
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }

    public function message(Request $request): HttpResponseException
    {
        $company = Company::query()
            ->select('*',
                DB::raw("(select count(id) from company_members where company_members.company_id=companies.id and deleted_at is null) as companyUser"),
                DB::raw("(select count(id) from boxes where boxes.company_id=companies.id and deleted_at is null) as companyBox"),
                DB::raw("(select count(id) from opinions where box_id in (select id from boxes where boxes.company_id=companies.id and deleted_at is null) and deleted_at is null) as companyOp"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.company_id=companies.id and deleted_at is null)) as money"),
            )
            ->with('user')
            ->where('id', $request->input('id'))
            ->first();

        if (!empty($company->id)) {
            $returnList['id'] = $company->id;
            $returnList['name'] = $company->name;
            $returnList['alias'] = $company->alias;
            $returnList['logo'] = empty($company->logo) ? '' : $company->logo;
            $returnList['industry'] = empty($company->industry) ? '' : $company->industry;
            $returnList['address'] = @$company->province . @$company->city . @$company->area . ' ' . @$company->address;
            $returnList['profile'] = $company->profile;
            $returnList['companyUser'] = $company->companyUser;
            $returnList['companyBox'] = $company->companyBox;
            $returnList['companyOp'] = $company->companyOp;
            $returnList['money'] = number_format($company->money, '2', '.', '');
            $returnList['updateTime'] = date($company->updated_at);
            $returnList['platform']=$company->platform;
            $returnList['auth'] = $company->status == 'STATUS_CERTIFIED_PASS' ? '是' : '否';
            if ($company->status == 'STATUS_CERTIFIED_PASS') {
                $returnList['authName'] = empty($company->user->name) ? '' : $company->user->name;
                $returnList['mobile'] = empty($company->user->mobile) ? '' : $company->user->mobile;
            } else {
                $returnList['authName'] = '';
                $returnList['mobile'] = '';
            }
        } else {
            $returnList = [];
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['companyComment', 'companyMobile']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $returnList['power'] = $getPermissionResponse->getPowerList();
        $successRequest = new SuccessRequest();
        $successRequest->setData($returnList);
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $data = $request->input('data');
        Company::query()
            ->where('id', '=', $request->input('id'))
            ->update([
                'name' => $data['companyName'],
                'logo' => empty($data['logo']) ? config('common.default_company_logo') : $data['logo'],
                'profile' => $data['profile'],
                'poster' => empty($data['poster']) ? config('common.default_company_poster') : $data['poster']
            ]);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $request->input('id')
        ]);
        return Response::success($successRequest);
    }

    public function companyBox(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '意见箱状态', 'key' => 'status', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '收到意见', 'key' => 'number', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '意见打赏', 'key' => 'money', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '仅限提一次', 'key' => 'only', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '简介', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
        ];
        $box = Box::query()
            ->select('boxes.*',
                DB::raw("(select count(id) from opinions where opinions.box_id=boxes.id and deleted_at is null) as number"),
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id in (select id from opinions where opinions.box_id=boxes.id and deleted_at is null)) as money"),
            )
            ->where('company_id', $request->input('companyId'));
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $box->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $box->orderBy('id', 'desc');
        }
        $boxList = $box->paginate($request->input('pageSize'));
        $item = $boxList->items();
        $total = $boxList->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['number'] = $val->number;
            $items[$key]['status'] = empty($val->suspend) ? '开启' : '关闭';
            //$items[$key]['allow'] = $val->is_allow_anonymous == 1 ? '是' : '否';
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['only'] = $val->once == 1 ? '是' : '否';
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function opinion(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '提出人', 'key' => 'userName', 'minWidth' => 110, 'align' => 'center'],
            ['title' => '意见内容', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '打赏', 'key' => 'money', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '是否采纳', 'key' => 'adopt', 'minWidth' => 85, 'align' => 'center'],
            ['title' => '提出时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
        ];
        $opinion = Comment::query()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with('box')
            ->with('user')
            ->whereIn('box_id', function ($query) use ($request) {
                $query->select('id')->from('boxes')->where('company_id', $request->input('companyId'));
            });
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $opinion->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $opinion->orderBy('id', 'desc');
        }
        $opList = $opinion->paginate($request->input('pageSize'));
        $item = $opList->items();
        $total = $opList->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = empty($val->box->name) ? '' : $val->box->name;
            $items[$key]['userName'] = empty($val->user->name) ? '' : $val->user->name;
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['adopt'] = $val->adopt == 1 ? '是' : '否';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function member(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '信息', 'key' => 'user', 'minWidth' => 120, 'align' => 'center', 'slot' => 'avatar'],
            ['title' => '名称', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '角色', 'key' => 'role', 'minWidth' => 110, 'align' => 'center', 'slot' => 'message'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 85, 'align' => 'center'],
        ];
        $model = CompanyUser::query()
            ->with('user')
            ->where('company_id', $request->input('companyId'));
        $model->orderByRaw("FIND_IN_SET(role,'" . implode(',', config('common.companyUser.role')) . "')");
        $apply = $model->paginate($request->input('pageSize'));
        $item = $apply->items();
        $total = $apply->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['nickName'] = @$val->user->name;
            $items[$key]['avatar'] = empty($val->avatar) ? config('common.default_company_logo') : $val->avatar;
            $items[$key]['role'] = $val->role_desc;
            $items[$key]['roleValue'] = $val->role_value;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 热门企业列表
     */
    public function hot(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '企业ID', 'key' => 'companyId', 'width' => 80, 'align' => 'center'],
            ['title' => '企业logo', 'key' => 'logo', 'minWidth' => 120, 'align' => 'center', 'slot' => 'logo'],
            ['title' => '企业名字', 'key' => 'name', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '状态', 'key' => 'statusType', 'minWidth' => 150, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 220, 'align' => 'center', 'slot' => 'edit'],
        ];
        $model = CompanyHot::withTrashed()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }]);
        //关键字查找企业
        if ($request->input('keyword')) {
            //$model->where('name', 'like', "%" . $this->getKeyword() . "%");
            $model->whereIn('company_id', function ($query) use ($request) {
                $query->from('companies')->select('id')
                    ->where('name', 'like', '%' . $request->input('keyword') . '%');
            });
        }
        // 排序
        $model->orderBy('deleted_at', 'asc');
        $model->orderBy('created_at', 'desc');
        $hot = $model->paginate($request->input('pageSize'));
        $item = $hot->items();
        $total = $hot->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = @$val->company->name;
            $items[$key]['statusType'] = empty($val->deleted_at);
            $items[$key]['logo'] = empty($val->company->logo) ? '' : $val->company->logo;
            $items[$key]['poster'] = empty($val->company->poster) ? '' : $val->company->poster;
            $items[$key]['profile'] = $val->company->profile;
            $items[$key]['companyId'] = $val->company_id;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 热门企业增加、更新
     */
    public function updateHot(Request $request): HttpResponseException
    {
        $haveHot = CompanyHot::withTrashed()->where('company_id', $request->input('companyId'))->first();
        if (!empty($haveHot->id) && $request->input('id') == 0) {
            throw new MessageException('热门企业已经存在！');
        }
        if ($request->input('id') > 0) {
            $hotMsg = CompanyHot::withTrashed()->where('id', $request->input('id'))->first();
            if (empty($hotMsg->id)) {
                throw new MessageException('热门企业不存在！');
            }
            Company::withTrashed()
                ->where('id', $hotMsg->company_id)
                ->update([
                    'name' => $request->input('name'),
                    'logo' => $request->input('logo'),
                    'profile' => $request->input('profile'),
                    'poster' => $request->input('poster'),
                    'deleted_at' => null
                ]);
            //状态显示
            if ($request->input('status')) {
                CompanyHot::withTrashed()
                    ->where('id', $request->input('id'))
                    ->update(['deleted_at' => null]);
            } else {
                CompanyHot::withTrashed()
                    ->where('id', $request->input('id'))
                    ->update(['deleted_at' => date('Y-m-d H:i:s', time())]);
            }
            $id = $request->input('id');
        } else {
            if ($request->input('companyId')) {//自己环境数据
                Company::withTrashed()
                    ->where('id', $request->input('companyId'))
                    ->update([
                        'name' => $request->input('name'),
                        'logo' => $request->input('logo'),
                        'profile' => $request->input('profile'),
                        'poster' => $request->input('poster'),
                        'deleted_at' => $request->input('status') ? null : date('Y-m-d H:i:s', time()),
                    ]);
                $companyId = $request->input('companyId');
            } else {//天眼查数据（创建企业，创建默认意见箱），如果存在不再创建
                $model = Company::withTrashed()
                    ->UpdateOrCreate([
                        'name' => $request->input('name'),
                        'user_id' => 0,
                        'status' => 'STATUS_CERTIFIED_PRE_COMMIT',
                    ], [
                        'logo' => $request->input('logo'),
                        'profile' => $request->input('profile'),
                        'poster' => $request->input('poster'),
                        'deleted_at' => null
                    ]);
                $companyId = $model->getKey();
                $box = Box::withTrashed()
                    ->updateOrCreate([
                        'company_id' => $companyId,
                        'name' => $request->input('name'),
                        'system' => true,
                    ], [
                        'open' => true,
                        'show' => true,
                        'content' => []
                    ]);
                if ($box->trashed()) {
                    $box->restore();
                }
                //创建企业平台记录
                $createLogRequest = new CreateLogRequest();
                $createLogRequest->setCompanyId($companyId);
                $createLogRequest->setPlatform('ADMIN');
                PlatformLog::createCompanyPlatformLog($createLogRequest);
            }
            //增加热门企业记录
            $hotModel = CompanyHot::query()
                ->insertGetId([
                    'company_id' => $companyId,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time()),
                    'deleted_at' => $request->input('status') ? null : date('Y-m-d H:i:s', time()),
                ]);
            $id = $hotModel;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(['id' => $id]);
        return Response::success($successRequest);
    }

    public function deleteHot(Request $request): HttpResponseException
    {
        CompanyHot::withTrashed()
            ->where('id', $request->input('id'))
            ->forceDelete();
        $successRequest = new SuccessRequest();
        $successRequest->setData(['id' => $request->input('id')]);
        return Response::success($successRequest);
    }

    public function switchHot(Request $request): HttpResponseException
    {
        //状态显示
        if ($request->input('status')) {
            CompanyHot::withTrashed()
                ->where('id', $request->input('id'))
                ->update(['deleted_at' => null]);
        } else {
            CompanyHot::withTrashed()
                ->where('id', $request->input('id'))
                ->update(['deleted_at' => date('Y-m-d H:i:s', time())]);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(['id' => $request->input('id')]);
        return Response::success($successRequest);
    }

    public function select(): HttpResponseException
    {
        $model = Company::query()
            ->select('name', 'logo', 'poster', 'profile', 'id', 'status')
            ->where('deleted_at', null)
            ->where('status', 'STATUS_CERTIFIED_PASS')
            ->get();
        $items = [];
        foreach ($model as $key => $val) {
            $items[$key]['value'] = empty($val->logo) ? '' : $val->logo;
            $items[$key]['label'] = $val->name;
            $items[$key]['poster'] = $val->poster;
            $items[$key]['profile'] = $val->profile;
            $items[$key]['companyId'] = $val->id;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'items' => $items,
        ]);
        return Response::success($successRequest);
    }


    public function tycSelect(Request $request): HttpResponseException
    {
        $list = [];
        $tycUrl = config('common.tianYanCha.hostUrl');
        $headerA = config('common.tianYanCha.token');
        $http = Http::withHeaders([
            'authorization' => $headerA,
        ])
            ->get($tycUrl, [
                'word' => $request->input('keyword'),
                'pageSize' => $request->input('pageSize'),
                'pageNum' => $request->input('page')
            ]);
        $response = $http->json();
        $returnData = empty($response['result']['items']) ? [] : $response['result']['items'];
        $response['result']['total'] = empty($response['result']['total']) ? 0 : $response['result']['total'];
        foreach ($returnData as $company) {
            $list[] = [
                'companyName' => strip_tags(Arr::get($company, 'name')),
                'tianYanChaId' => Arr::get($company, 'id')
            ];
        }
        $names = Arr::pluck($list, 'companyName');
        $company = CompanyModel::withTrashed()
            ->select('id', 'name', 'user_id', 'logo', 'poster', 'profile')
            ->with('company_apply')
            ->where('status', 'STATUS_CERTIFIED_PASS')
            ->whereIn('name', $names)
            ->get();
        $companyIds = [];
        $companyLogos = [];
        $companyPoster = [];
        $companyProfile = [];
        $companyUserIds = [];
        $companyApply = [];
        foreach ($company as $item) {
            $name = $item->name;
            $companyIds[$name] = $item->id;
            $companyLogos[$name] = $item->logo;
            $companyPoster[$name] = $item->poster;
            $companyProfile[$name] = $item->profile;
            $companyUserIds[$name] = $item->user_id;
            $companyApply[$name] = $item->company_apply;
        }
        foreach ($list as &$value) {
            $certifiedStatus = 0;
            $name = Arr::get($value, 'companyName');
            if ((bool)Arr::get($companyUserIds, $name)) {
                $certifiedStatus = 2;
            } elseif ((bool)Arr::get($companyApply, $name)) {
                $certifiedStatus = 1;
            }
            $value['id'] = (int)Arr::get($companyIds, $name);
            $value['companyLogo'] = Arr::get($companyLogos, $name) ?: config('common.default_company_logo');
            $value['companyPoster'] = Arr::get($companyPoster, $name) ?: config('common.default_company_poster');
            $value['companyProfile'] = Arr::get($companyProfile, $name) ?: '';
            $value['isCertified'] = (bool)Arr::get($companyUserIds, $name);
            $value['isApply'] = (bool)Arr::get($companyApply, $name);
            $value['certifiedStatus'] = $certifiedStatus;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $response['result']['total'],
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $list,
        ]);
        return Response::success($successRequest);
    }

    public function getTycMessage(Request $request): HttpResponseException
    {
        $tycUrl = config('common.tianYanCha.hostMsgUrl');
        $http = Http::withHeaders([
            'authorization' => config('common.tianYanCha.token'),
        ])
            ->get($tycUrl, [
                'keyword' => $request->input('companyName'),
            ]);
        $response = $http->json();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'companyProfile' => $response['result']['businessScope'],
            'companyName' => $response['result']['name'],
            'companyAlias' => $response['result']['alias'],
            'companyLogo' => config('common.default_company_logo'),
            'companyPoster' => config('common.default_company_poster'),
        ]);
        return Response::success($successRequest);
    }

    public function automatic(): HttpResponseException
    {
        $nowTime = time();
        $model = CompanyHot::withTrashed()->pluck('company_id')->toArray();
        $modelIn = CompanyHot::query()->pluck('company_id')->toArray();
        $number = count($model);
        if ($number <= 10) {//少于十个企业全部开启
            $this->sort($model, $nowTime);
        }
        if ($number <= 20 && $number > 10) {//10个到20个之间
            $list = array_rand($model, 10);
            $newArray = [];
            foreach ($list as $key => $va) {
                $newArray[] = $model[$va];
            }
            $this->sort($newArray, $nowTime);
        }
        if ($number > 20) {
            $listNew = CompanyHot::withTrashed()->whereNotIn('company_id', $modelIn)
                ->pluck('company_id')->toArray();
            $listNum = count($listNew);
            $newArray = [];
            if ($listNum < 10) {
                $list = array_rand($model, 10);
                foreach ($list as $key => $va) {
                    $newArray[] = $model[$va];
                }
            } else {
                $list = array_rand($listNew, 10);
                foreach ($list as $key => $va) {
                    $newArray[] = $listNew[$va];
                }
            }
            $this->sort($newArray, $nowTime);
        }
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function sort($ids, $time)
    {
        $model = Company::withTrashed()
            ->select('*',
                DB::raw("(select count(id) from opinions where box_id in (select id from boxes where boxes.company_id=companies.id and deleted_at is null) and deleted_at is null) as companyOp"),
            )
            ->whereIn('id', $ids)
            ->orderBy('companyOp', 'asc')
            ->get();
        CompanyHot::withTrashed()->delete();
        foreach ($model as $key => $val) {
            CompanyHot::withTrashed()
                ->where('company_id', $val->id)
                ->update([
                    'created_at' => date('Y-m-d H:i:s', $time + $key),
                    'deleted_at' => null
                ]);
        }
    }

}
