<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\CustomerCompanyLogo;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class CustomerCompanyLogoLogic
{
    public function index(Request $request): HttpResponseException
    {

        $model = CustomerCompanyLogo::query()
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($request->input('pageSize'), ['*'], 'page', $request->input('page'));
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = empty($val->id) ? '' : $val->id;
            $items[$key]['logo'] = empty($val->logo) ? '' : $val->logo;
            $items[$key]['sort'] = $val->sort;
            if ($val->suspend == 0) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
        }
        $columns = ([
            ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '图片', 'key' => 'logo', 'minWidth' => 200, 'align' => 'center', 'slot' => 'image'],
            ['title' => '排序', 'key' => 'sort', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
        ]
        );
        $total = $model->total();
        $page = $model->lastPage();
        $pageSize = $request->input('pageSize');
        $data = (compact('items', 'total', 'page', 'pageSize', 'columns'));
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        $model = new CustomerCompanyLogo();
        $model->fill([
            'logo' => $request->input('logo'),
            'sort' => $request->input('sort'),
            'suspend' => $request->input('suspend'),
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        $model = CustomerCompanyLogo::query()
            ->findOrFail($request->input('id'));
        $model->delete();
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $model = CustomerCompanyLogo::query()
            ->where('id', $request->input('id'))
            ->first();
        $model->fill([
            'logo' => $request->input('logo'),
            'sort' => $request->input('sort'),
            'suspend' => $request->input('suspend'),
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function switchStatus(Request $request): HttpResponseException
    {
        $model = CustomerCompanyLogo::query()
            ->findOrFail($request->input('id'));
        $model->fill([
            'suspend' => $request->input('status') == true ? 0 : 1,
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

}