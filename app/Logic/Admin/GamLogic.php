<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class GamLogic
{
    public function snake(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        //初始化运动方向
        if (!$request->input("direction")) {
            $this->set("direction", "left");
        } else {
            $this->set("direction", $request->input("direction"));
        }
        $snake = $request->input("snake");
        $this->set('snake',$snake);
        $food = $request->input("food");
        //计算蛇头坐标
        switch ($this->get("direction")) {
            case "up":
            {
                $snakeHead = [
                    $snake[0][0],
                    $snake[0][1] - 1
                ];
                break;
            }
            case "down":
            {
                $snakeHead = [
                    $snake[0][0],
                    $snake[0][1] + 1
                ];
                break;
            }
            case "left":
            {
                $snakeHead = [
                    $snake[0][0] - 1,
                    $snake[0][1]
                ];
                break;
            }
            case "right":
            {
                $snakeHead = [
                    $snake[0][0] + 1,
                    $snake[0][1]
                ];
                break;
            }
        }
        //咬到自己，游戏结束
        if (in_array($snakeHead, $snake)) {
            $this->gameOver();
        }
        //添加蛇头坐标
        array_unshift($snake, $snakeHead);
        //撞墙，游戏结束,长度(60,30)
        if ($snake[0][0] < 0 || $snake[0][1] < 0 || $snake[0][0] > 59 || $snake[0][1] > 29) {
            $this->gameOver();
        }
        //咬到食物得一分
        if (in_array($request->input("food"), $snake)) {
            $this->getFood();
            $this->set("score", $request->input("score") + 1);
        } else {
            $this->set("food", $food);
            $this->set("score", $request->input("score"));
            unset($snake[count($snake) - 1]);//恢复原来的蛇
        }
        $this->set("snake", $snake);
        $successRequest->setData([
            'food' => $this->get('food'),
            'snake' => $this->get('snake'),
            'score' => $this->get('score'),
            'header'=>$snakeHead,
            'die' => 0
        ]);
        return Response::success($successRequest);
    }

    public function snakeStart(): HttpResponseException
    {
        session_start();
        $successRequest = new SuccessRequest();
        $this->set('snake', [[mt_rand(1, 59), mt_rand(1, 29)]]);
        $this->getFood();
        $this->set("score", 0);
        $snake = $this->get('snake');
        $snakeHead = [
            $snake[0][0] + 1,
            $snake[0][1]
        ];
        array_unshift($snake, $snakeHead);//数组开头插入一个元素
        $successRequest->setData([
            'food' => $this->get('food'),
            'snake' => $snake,
            'score' => 0,
            'header'=>$snakeHead,
            'die' => 0
        ]);
        return Response::success($successRequest);
    }

    public function gameOver()
    {
        session_unset();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'food' => $this->get('food'),
            'snake' => $this->get('snake'),
            'score' => $this->get('score'),
            'header'=>[],
            'die' => 1
        ]);
        return Response::success($successRequest);
    }

    public function getFood()
    {
        $food = [mt_rand(1, 59), mt_rand(1, 29)];
        $this->set("food", $food);
        if (in_array($food, $this->get("snake"))) {
            $this->getFood();
        }
    }

    /**
     * 设置虚拟显存session中的数据
     * @param [type] $k [description]
     * @param [type] $v [description]
     */
    public function set($k, $v)
    {
        $_SESSION[$k] = $v;
    }

    /**
     * 读取虚拟缓存session中的数据
     * @param  [type] $k [description]
     * @return false|mixed [type]    [description]
     */
    public function get($k)
    {
        return $_SESSION[$k] ?? false;
    }

}
