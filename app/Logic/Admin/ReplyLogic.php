<?php

namespace App\Logic\Admin;

use App\Facades\Permission;
use App\Facades\Response;
use App\Models\Reply;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ReplyLogic
{
    public function index(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '回复人员信息', 'key' => 'id', 'width' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left'],
            ['title' => '意见反馈', 'key' => 'content', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '意见内容', 'key' => 'opinionContent', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '采纳', 'key' => 'adopt', 'width' => 80, 'align' => 'center'],
            ['title' => '回复时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ];
        $model = Reply::withTrashed()->with(['user', 'comment']);
        //        过滤代码
        if ($request->input('filter')) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users');
            });
        }
        //关键字查询
        if ($request->input('keyword')) {
            switch ($request->input('status')) {
                case 'nickeName':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case 'commentId':
                    $model->where('comment_id', $request->input('keyword'));
                    break;
                case 'replyContent':
                    $model->where('content', 'like', '%' . $request->input('keyword') . '%');
                    break;
                case 'userId':
                    $model->where('user_id', $request->input('keyword'));
                    break;
                case 'commentContent':
                    $model->whereIn('comment_id', function ($query) use ($request) {
                        $query->select('id')->from('comments')
                            ->where('content', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
            }
        }
        //筛选删除
        switch ($request->input('opinionStatus')) {
            case 'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case 'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        //时间筛选
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        //排序
        switch ($request->input('sortType')) {
            case 'timeDESC':
                $model->orderBy('created_at', 'desc');
                break;
            case 'timeASC':
                $model->orderBy('created_at', 'ASC');
                break;
            case 'commentIdASC':
                $model->orderBy('comment_id', 'ASC');
                break;
            case 'commentIdDESC':
                $model->orderBy('comment_id', 'desc');
                break;
        }
        $apply = $model->paginate($request->input('pageSize'));
        $items = $apply->items();
        $total = $apply->total();
        $list = [];
        foreach ($items as $item) {
            $content = Arr::get($item->content, 'content');
            $opinionContent = Arr::get($item->comment, 'content.content');
            $list[] = [
                'id' => $item->id,
                'commentId' => empty($item->comment->id) ? '' : $item->comment->id,
                'userId' => empty($item->user->id) ? '' : $item->user->id,
                'mobile' => empty($item->user->mobile) ? '' : $item->user->mobile,
                'nickName' => empty($item->user->name) ? '' : $item->user->name,
                'avatar' => empty($item->user->avatar) ? '' : $item->user->avatar,
                'content' => empty($content) ? '' : $content,
                'opinionContent' => empty($opinionContent) ? '' : $opinionContent,
                'adopt' => empty($item->comment->adopt) ? '否' : '是',
                'createTime' => date($item->created_at),
            ];
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['replyMobile','replyExport']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'list' => $list,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }

    public function explode(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '回复人员ID', 'key' => 'userId'],
            ['title' => '用户昵称', 'key' => 'nickName'],
            ['title' => '手机号码', 'key' => 'mobile'],
            ['title' => '意见反馈', 'key' => 'content'],
            ['title' => '意见内容', 'key' => 'opinionContent'],
            ['title' => '采纳', 'key' => 'adopt', 'align' => 'center'],
            ['title' => '回复时间', 'key' => 'createTime', 'align' => 'center'],
        ];
        $model = Reply::withTrashed()->with(['user', 'comment']);
        //        过滤代码
        if ($request->input('filter')) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users');
            });
        }
        //关键字查询
        if ($request->input('keyword')) {
            switch ($request->input('status')) {
                case 'nickeName':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case 'replyContent':
                    $model->where('content', 'like', '%' . $request->input('keyword') . '%');
                    break;
                case 'userId':
                    $model->where('user_id', $request->input('keyword'));
                    break;
            }
        }
        //筛选删除
        switch ($request->input('opinionStatus')) {
            case 'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case 'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        //时间筛选
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        //排序
        $model->orderBy('created_at', 'desc');
        $items = $model->get();
        $list = [];
        foreach ($items as $item) {
            $content = Arr::get($item->content, 'content');
            $opinionContent = Arr::get($item->comment, 'content.content');
            $list[] = [
                'id' => $item->id,
                'userId' => empty($item->user->id) ? '' : $item->user->id,
                'mobile' => empty($item->user->mobile) ? '' : $item->user->mobile,
                'nickName' => empty($item->user->name) ? '' : $item->user->name,
                'avatar' => empty($item->user->avatar) ? '' : $item->user->avatar,
                'content' => empty($content) ? '' : $content,
                'opinionContent' => empty($opinionContent) ? '' : $opinionContent,
                'adopt' => empty($item->comment->adopt) ? '否' : '是',
                'createTime' => date($item->created_at),
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'list' => $list,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function get(Request $request): HttpResponseException
    {
        $model = Reply::query()
            ->select('id', 'comment_id', 'content', 'user_id', 'read', 'created_at')
            ->findOrFail($request->input('id'))
            ->with('comment');
        $successRequest = new SuccessRequest();
        $successRequest->setData($model->toArray());
        return Response::success($successRequest);
    }
}
