<?php

namespace App\Logic\Admin;

use App\Facades\Permission;
use App\Facades\Response;
use App\Models\Comment;
use App\Models\Order;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CommentLogic
{

    public function index(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '用户信息', 'key' => 'id', 'width' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left'],
            ['title' => '意见箱', 'key' => 'boxName', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '意见箱来源', 'key' => 'opinionStatus', 'minWidth' => 120, 'align' => 'center', 'slot' => 'people'],
            ['title' => '意见内容', 'key' => 'content', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '是否采纳', 'key' => 'adopt', 'width' => 85, 'align' => 'center'],
            ['title' => '打赏', 'key' => 'money', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
            ['title' => '是否删除', 'key' => 'isDelete', 'width' => 85, 'align' => 'center', 'slot' => 'delete'],
            ['title' => '提出时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ];
        $model = Comment::withTrashed()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with(['box' => function ($query) {
                $query->with(['company' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }])
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user');
        //        过滤代码
        if ($request->input('filter')) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找
        if ($request->input('keyword')) {
            switch ($request->input('status')) {
                case'nickName':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'boxName':
                    $model->whereIn('box_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'userId':
                    $model->where('user_id', $request->input('keyword'));
                    break;
            }
        }
        switch ($request->input('opinionStatus')) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $model->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $model->orderBy('id', 'desc');
        }
        $apply = $model->paginate($request->input('pageSize'));
        $item = $apply->items();
        $total = $apply->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['nickName'] = empty($val->user->name) ? '' : $val->user->name;
            $items[$key]['mobile'] = empty($val->user->mobile) ? '' : $val->user->mobile;
            $items[$key]['avatar'] = empty($val->user->avatar) ? '' : $val->user->avatar;
            $items[$key]['userId'] = empty($val->user->id) ? '' : $val->user->id;
            $items[$key]['boxName'] = empty($val->box->name) ? '' : $val->box->name;
            $items[$key]['content'] = Arr::get($val->content, 'content');
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['adopt'] = $val->adopt ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['opinionStatus'] = @$val->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $company = empty($val->box->company->name) ? '' : $val->box->company->name;
            $userPeople = empty($val->box->user->name) ? '' : $val->box->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
        }
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['commentContent', 'opinionMobile']);//查看意见内容
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $powerList = $getPermissionResponse->getPowerList();
        if (!$powerList['commentContent']) {
            unset($columns[3], $columns[7]);//意见内容
            $columns = array_values($columns);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }

    public function explode(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '用户ID', 'key' => 'userId'],
            ['title' => '用户昵称', 'key' => 'nickName'],
            ['title' => '意见箱', 'key' => 'boxName', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '意见像所属', 'key' => 'opinionStatus'],
            ['title' => '意见所属', 'key' => 'opinionPeople'],
            ['title' => '意见内容', 'key' => 'content'],
            ['title' => '是否采纳', 'key' => 'adopt'],
            ['title' => '打赏', 'key' => 'money'],
            ['title' => '是否删除', 'key' => 'isDelete'],
            ['title' => '提出时间', 'key' => 'createTime'],
        ];
        $model = Comment::withTrashed()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with(['box' => function ($query) {
                $query->with(['company' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }])
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user');
        //        过滤代码
        if ($request->input('filter')) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找
        if ($request->input('keyword')) {
            switch ($request->input('status')) {
                case'nickName':
                    $model->whereIn('user_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'boxName':
                    $model->whereIn('box_id', function ($query) use ($request) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('name', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'userId':
                    $model->where('user_id', $request->input('keyword'));
                    break;
            }
        }
        switch ($request->input('opinionStatus')) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($request->input('sortColumn')) && !empty($request->input('sortVal'))) {
            $model->orderBy($request->input('sortColumn'), $request->input('sortVal'));
        } else {
            $model->orderBy('id', 'desc');
        }
        $item = $model->get();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['nickName'] = empty($val->user->name) ? '' : $val->user->name;
            $items[$key]['avatar'] = empty($val->user->avatar) ? '' : $val->user->avatar;
            $items[$key]['userId'] = empty($val->user->id) ? '' : $val->user->id;
            $items[$key]['boxName'] = empty($val->box->name) ? '' : $val->box->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : str_replace([',', '\n', '\r\n'], '', $content->content);
            $items[$key]['adopt'] = $val->adopt ? '是' : '否';
            $items[$key]['money'] = number_format($val->money, 2, '.', '');
            $items[$key]['isDelete'] = $val->deleted_at ? '是' : '否';
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['opinionStatus'] = @$val->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $company = empty($val->box->company->name) ? '' : $val->box->company->name;
            $userPeople = empty($val->box->user->name) ? '' : $val->box->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $model = Comment::withTrashed()
            ->select('*',
                DB::raw("(select sum(amount) from orders where order_state='SUCCESS' and opinion_id=opinions.id ) as money"),
            )
            ->with(['box' => function ($query) {
                $query->with(['company' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }])
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user')
            ->with(['replies' => function ($query) {
                $query->with('user');
            }])
            ->where('id', $request->input('id'))
            ->first();
        $list = [];
        $list['content'] = Arr::get($model->content, 'content');
        $list['images'] = Arr::get($model->content, 'images');
        $list['title'] = Arr::get($model->content, 'title');
        $list['video'] = Arr::get($model->content, 'videos');
        //$list['money'] = number_format($model->money, 2, '.', '');
        $company = empty($model->box->company->name) ? '' : $model->box->company->name;
        $userPeople = empty($model->box->user->name) ? '' : $model->box->user->name;
        if ($company && empty($userPeople)) {
            $list['opinionPeople'] = $company;
        } elseif (empty($company) && $userPeople) {
            $list['opinionPeople'] = $userPeople;
        } elseif (!empty($company) && !empty($userPeople)) {
            $list['opinionPeople'] = $company . '/' . $userPeople;
        } else {
            $list['opinionPeople'] = '';
        }
        $list['opinionStatus'] = @$model->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
        $list['createTime'] = date($model->created_at);
        $list['mobile'] = empty($model->user->mobile) ? '' : $model->user->mobile;
        $list['nickName'] = empty($model->user->name) ? '' : $model->user->name;
        $list['boxName'] = empty($model->box->name) ? '' : $model->box->name;
        $list['aliasName'] = empty($model->user->alias_name) ? '' : $model->user->alias_name;
        $list['adopt'] = $model->adopt == 1;
        $list['anonymous'] = $model->anonymous == 1;
        $list['delete'] = !empty($model->deleted_at);
        if (!empty($model->replies)) {
            foreach ($model->replies as $key => $val) {
                $list['reply'][$key]['replyName'] = empty($val->user->name) ? '' : $val->user->name;
                $list['reply'][$key]['replyTime'] = date($val->created_at);
                $list['reply'][$key]['readType'] = '';
                $list['reply'][$key]['replyContent'] = Arr::get($val->content, 'content');
            }
        } else {
            $list['reply'] = [];
        }
        //打赏信息
        $list['columns'] = [
            ['title' => '打赏人手机', 'key' => 'fromUserMobile', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '打赏金额', 'key' => 'amount', 'minWidth' => 110, 'align' => 'center'],
            ['title' => '状态', 'key' => 'orderState', 'minWidth' => 200, 'align' => 'center'],
            ['title' => '支付方式', 'key' => 'payState', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '交易类型', 'key' => 'tradeType', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '预支付订单号', 'key' => 'prepayId', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '支付订单号', 'key' => 'wechatTransactionId', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '打赏时间', 'key' => 'createdTime', 'width' => 100, 'align' => 'center'],
        ];
        $order = Order::query()
            ->with(['opinion' => function ($query) {
                $query->with(['box' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }]);
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('fromUser')
            ->where('opinion_id', $request->input('id'))
            ->orderBy('created_at', 'desc')
            ->get();
        $items = [];
        foreach ($order as $key => $val) {
            $items[$key]['fromUser'] = $val->from_user;
            $items[$key]['fromUserMobile'] = @$val->fromUser->mobile;
            $items[$key]['fromUserId'] = $val->from_user_id;
            $items[$key]['amount'] = number_format($val->amount, 2, '.', '');
            $items[$key]['prepayId'] = empty($val->prepay_id) ? '--' : (string)$val->prepay_id;
            $items[$key]['payState'] = $val->pay_type;
            $items[$key]['orderState'] = $val->OrderStates;
            $items[$key]['wechatTransactionId'] = empty($val->wechat_transaction_id) ? '--' : (string)$val->wechat_transaction_id;
            $items[$key]['tradeType'] = empty($val->trade_type) ? '--' : $val->trade_type;
            $items[$key]['createdTime'] = date($val->created_at);
            $items[$key]['id'] = $val->id;
        }
        $list['items'] = $items;
        $orderAll = Order::query()
            ->where('opinion_id', $request->input('id'))
            ->sum('amount');
        $orderHaveMoney = Order::query()
            ->where('opinion_id', $request->input('id'))
            ->where('order_state', 'SUCCESS')
            ->count();
        $list['dataShow'] = array(
            ['name' => '打赏总金额', 'number' => (float)number_format($orderAll, 2, '.', '')],
            ['name' => '实际被打赏总金额', 'number' => (float)number_format($model->money, 2, '.', '')],
            ['name'=>'打赏记录','number'=>count($items)],
            ['name'=>'有效记录','number'=>$orderHaveMoney],
        );
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['opinionMobile']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $list['power'] = $getPermissionResponse->getPowerList();
        $successRequest = new SuccessRequest();
        $successRequest->setData($list);
        return Response::success($successRequest);
    }

}
