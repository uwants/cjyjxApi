<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\Faq;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class FaqLogic
{
    public function index(Request $request): HttpResponseException
    {
        $model = Faq::query()
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($request->input('pageSize'));
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = empty($val->id) ? '' : $val->id;
            $items[$key]['question'] = empty($val->question) ? '' : $val->question;
            $items[$key]['answer'] = empty($val->answer) ? '' : $val->answer;
            if ($val->suspend == 0) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
            $items[$key]['sort'] = $val->sort;
        }
        $columns = [
            ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '问题', 'key' => 'question', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '回答', 'key' => 'answer', 'minWidth' => 180, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '排序', 'key' => 'sort', 'minWidth' => 50, 'align' => 'center'],
            ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
        ];
        $total = $model->total();
        $page = $request->input('page');
        $pageSize = $request->input('pageSize');
        $data = (compact('items', 'total', 'page', 'pageSize', 'columns'));
        $pageRequest = new PageRequest();
        $pageRequest->setData($data);
        return Response::page($pageRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        $model = new Faq();
        $model->fill([
            'question' => $request->input('question'),
            'answer' => $request->input('answer'),
            'sort' => $request->input('sort'),
            'suspend' => $request->input('suspend'),
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $bannerId = $request->input('id');
        $model = Faq::query()
            ->firstOrFail($bannerId);
        $successRequest = new SuccessRequest();
        $successRequest->setData($model->toArray());
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $model = Faq::query()
            ->where('id', $request->input('id'))
            ->first();
        $model->fill([
            'question' => $request->input('question'),
            'answer' => $request->input('answer'),
            'sort' => $request->input('sort'),
            'suspend' => $request->input('suspend'),
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        $model = Faq::query()
            ->findOrFail($request->input('id'));
        $model->delete();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function switchStatus(Request $request): HttpResponseException
    {
        $model = Faq::query()
            ->findOrFail($request->input('id'));
        $model->fill([
            'suspend' => !$request->input('status')
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }
}