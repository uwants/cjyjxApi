<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\Order;
use App\Models\WithdrawOrder;
use App\Models\Wallet;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class TransactionLogic
{
    public function index(Request $request): HttpResponseException
    {
        if (@$request->input('exportData') == 1) {
            $columns = [
                ['title' => '用户手机', 'key' => 'userMobile'],
                ['title' => '支付宝账户', 'key' => 'alipayUser'],
                ['title' => '支付宝名字', 'key' => 'alipayNmae'],
                ['title' => '提现金额', 'key' => 'amount'],
                ['title' => '状态', 'key' => 'stateName'],
                ['title' => '发起时间', 'key' => 'createdTime'],
                ['title' => '处理时间', 'key' => 'updatedTime',],
            ];
        } else {
            $columns = [
                ['title' => '用户信息', 'key' => 'id', 'minWidth' => 180, 'align' => 'left', 'slot' => 'message'],
                ['title' => '支付宝账户', 'key' => 'alipayUser', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '支付宝名字', 'key' => 'alipayName', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '提现金额', 'key' => 'amount', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '审核状态', 'key' => 'stateName', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '提现状态', 'key' => 'alipayOrderStateName', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '发起时间', 'key' => 'createdTime', 'width' => 100, 'align' => 'center'],
                ['title' => '处理时间', 'key' => 'updatedTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 300, 'align' => 'center', 'slot' => 'edit'],
            ];
        }

        $withdrawOrder = WithdrawOrder::query()
            ->with('user');
        if ($request->input('state') && $request->input('state') != 'all') {
            $withdrawOrder->where('state', $request->input('state'));
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $withdrawOrder->whereBetween('created_at', [$start, $end]);
        }
        if ($request->input('keyword')) {
            $withdrawOrder->whereIn('to_user_id', function ($query) use ($request) {
                $query->select('id')->from('users')
                    ->where('mobile', 'like', '%' . $request->input('keyword') . '%');
            });
        }
        $withdrawOrder->orderBy('created_at', 'desc');
        if ($request->input('exportData') == 1) {
            $item = $withdrawOrder->get();
            $total = count($item);
        } else {
            $userOrder = $withdrawOrder->paginate($request->input('pageSize'));
            $item = $userOrder->items();
            $total = $userOrder->total();
        }
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['orderId'] = $val->order_id;
            $items[$key]['userId'] = @$val->user->id;
            $items[$key]['userMobile'] = @$val->user->mobile;
            $items[$key]['alipayUser'] = $val->alipay_user_id;
            $items[$key]['alipayName'] = $val->alipay_user_name;
            $items[$key]['amount'] = number_format($val->amount, 2, '.', '');
            $items[$key]['state'] = $val->state;
            $items[$key]['stateName'] = $val->WithdrawOrder;
            $items[$key]['createdTime'] = date($val->created_at);
            $items[$key]['updatedTime'] = $val->state == 'WAIT' ? '--' : date($val->updated_at);
            $items[$key]['reviewUser'] = $val->review_user;
            $items[$key]['rejectReason'] = $val->reject_reason;
            $items[$key]['alipayOrderState'] = $val->alipay_order_state;
            $items[$key]['alipayOrderStateName'] = $val->WithdrawOrderUser;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => (int)$request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $withdrawOrder = WithdrawOrder::query()
            ->with('user')
            ->where('id', $request->input('id'))
            ->first();
        $withdrawOrder->amount = number_format($withdrawOrder->amount, 2, '.', '');
        $withdrawOrder->updatedTime = $withdrawOrder->state == 'WAIT' ? '--' : date('Y-m-d H:i:s', strtotime($withdrawOrder->updated_at));
        $withdrawOrder->createdTime = date('Y-m-d H:i:s', strtotime($withdrawOrder->created_at));;
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('withdrawOrder'));
        return Response::success($successRequest);
    }

    public function refuse(Request $request): HttpResponseException
    {
        $withdrawOrderMsg = WithdrawOrder::query()
            ->where('id', $request->input('id'))
            ->first();
        $wallet = Wallet::query()
            ->where('user_id', $withdrawOrderMsg->to_user_id)
            ->first();
        $adminUser = Auth::user();
        $withdrawOrder = WithdrawOrder::query()
            ->where('id', $request->input('id'))
            ->update([
                'review_user' => $adminUser->name,
                'review_user_id' => Auth::id(),
                'state' => 'reject',
                'reject_reason' => $request->input('reason')
            ]);
        //修改冻结余额
        Wallet::query()
            ->where('user_id', $withdrawOrderMsg->to_user_id)
            ->update([
                'frozen_amount' => $wallet->frozen_amount - $withdrawOrderMsg->amount,
                'balance' => $wallet->balance + $withdrawOrderMsg->amount
            ]);
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('withdrawOrder'));
        return Response::success($successRequest);
    }

    public function orderList(Request $request): HttpResponseException
    {
        if (@$request->input('exportData') == 1) {
            $columns = [
                ['title' => '打赏人手机', 'key' => 'fromUserMobile'],
                ['title' => '被打赏人手机', 'key' => 'toUserMobile'],
                ['title' => '打赏金额', 'key' => 'amount'],
                ['title' => '订单状态', 'key' => 'orderState'],
                ['title' => '支付方式', 'key' => 'payState'],
                ['title' => '交易类型', 'key' => 'tradeType',],
                ['title' => '预支付订单号', 'key' => 'prepayId',],
                ['title' => '支付订单号', 'key' => 'wechatTransactionId',],
                ['title' => '发起时间', 'key' => 'createdTime'],
            ];
        } else {
            $columns = [
                ['title' => '打赏人信息', 'key' => 'fromUser', 'minWidth' => 180, 'align' => 'left', 'slot' => 'fromMessage'],
                ['title' => '被打赏人信息', 'key' => 'toUser', 'minWidth' => 180, 'align' => 'left', 'slot' => 'toMessage'],
                ['title' => '打赏', 'key' => 'amount', 'minWidth' => 240, 'align' => 'left', 'slot' => 'countMessage'],
                ['title' => '支付方式', 'key' => 'payState', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '交易类型', 'key' => 'tradeType', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '预支付订单号', 'key' => 'prepayId', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '支付订单号', 'key' => 'wechatTransactionId', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '发起时间', 'key' => 'createdTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 100, 'align' => 'center', 'slot' => 'edit'],
            ];
        }
        $order = Order::query()
            ->with(['opinion' => function ($query) {
                $query->with(['box' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }]);
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('fromUser')
            ->with('toUser');
        if ($request->input('keyword')) {
            switch ($request->input('searchType')) {
                case'fromMobile':
                    $order->whereIn('from_user_id', function ($query) use ($request) {
                        $query->from('users')
                            ->select('id')
                            ->where('mobile', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'toMobile':
                    $order->whereIn('to_user_id', function ($query) use ($request) {
                        $query->from('users')
                            ->select('id')
                            ->where('mobile', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'nickName':
                    $order->whereIn('id', function ($query) use ($request) {
                        $query->from('order')
                            ->select('id')
                            ->where('to_user', 'like', '%' . $request->input('keyword') . '%')
                            ->orWhere('from_user', 'like', '%' . $request->input('keyword') . '%');
                    });
                    break;
                case'prepayId':
                    $order->where('prepay_id', 'like', '%' . $request->input('keyword') . '%');
                    break;
                case'wechatTransactionId':
                    $order->where('wechat_transaction_id', 'like', '%' . $request->input('keyword') . '%');
                    break;
            }
        }
        $time = $request->input('time');
        if (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $order->whereBetween('created_at', [$start, $end]);
        }
        if ($request->input('state') && $request->input('state') != 'all') {
            $order->where('order_state', $request->input('state'));
        }
        if (!empty($request->input('state')) && $request->input('state') != 'all') {
            $order->where('order_state', $request->input('state'));
        }
        $order->orderBy('created_at', 'desc');
        if ($request->input('exportData') == 1) {
            $item = $order->get();
            $total = count($item);
        } else {
            $user = $order->paginate($request->input('pageSize'));
            $item = $user->items();
            $total = $user->total();
        }
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['fromUser'] = $val->from_user;
            $items[$key]['fromUserMobile'] = @$val->fromUser->mobile;
            $items[$key]['fromUserId'] = $val->from_user_id;
            $items[$key]['toUser'] = $val->to_user;
            $items[$key]['toUserMobile'] = @$val->toUser->mobile;
            $items[$key]['toUserId'] = $val->to_user_id;
            $items[$key]['amount'] = number_format($val->amount, 2, '.', '');
            $items[$key]['prepayId'] = empty($val->prepay_id) ? '--' : (string)$val->prepay_id;
            $items[$key]['opinion'] = empty($val->opinion->content) ? '' : Arr::get($val->opinion->content, 'content');
            $items[$key]['boxName'] = empty($val->opinion->box->name) ? '' : $val->opinion->box->name;
            $items[$key]['payState'] = $val->pay_type;
            $items[$key]['orderState'] = $val->OrderStates;
            $items[$key]['wechatTransactionId'] = empty($val->wechat_transaction_id) ? '--' : (string)$val->wechat_transaction_id;
            $items[$key]['tradeType'] = empty($val->trade_type) ? '--' : $val->trade_type;
            $items[$key]['createdTime'] = date($val->created_at);
            $items[$key]['id'] = $val->id;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => (int)$request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function orderMessage(Request $request): HttpResponseException
    {
        $order = Order::query()
            ->with(['opinion' => function ($query) {
                $query->with(['box' => function ($query) {
                    return call_user_func([$query, 'withTrashed']);
                }]);
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('fromUser')
            ->with('toUser')
            ->where('id', $request->input('id'))
            ->first();
        $order->amount = number_format($order->amount, 2, '.', '');
        $order->createdTime = date('Y-m-d H:i:s', strtotime($order->created_at));
        $order->updateTime = date('Y-m-d H:i:s', strtotime($order->updated_at));
        $order->paySuccessTime = date('Y-m-d H:i:s', strtotime($order->pay_success_time));
        $order->orderState = $order->OrderStates;
        $order->opinionContent = empty($order->opinion->content) ? '' : Arr::get($order->opinion->content, 'content');
        $order->boxname = empty($order->opinion->box->name) ? '' : $order->opinion->box->name;
        $order->toUserMobile = empty($order->toUser->mobile) ? '' : $order->toUser->mobile;
        $order->toUserName = empty($order->toUser->name) ? '' : $order->toUser->name;
        $order->fromUserMobile = empty($order->fromUser->mobile) ? '' : $order->fromUser->mobile;
        $order->fromUserName = empty($order->fromUser->name) ? '' : $order->fromUser->name;
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('order'));
        return Response::success($successRequest);
    }

    public function dataIndex(Request $request): HttpResponseException
    {
        $start = '';
        $end = '';
        switch ($request->input('timeState')) {
            case'yesterday':
                $start = date('Y-m-d', strtotime("-1 day"));
                $end = date('Y-m-d', strtotime("-1 day"));
                break;
            case'lastSevenDays':
                $start = date('Y-m-d', strtotime("-7 day"));
                $end = date('Y-m-d', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d', strtotime("-30 day"));
                $end = date('Y-m-d', strtotime("-1 day"));
                break;
            case'lastWeek':
                $start = date('Y-m-d', strtotime("last week"));
                $end = date('Y-m-d', strtotime('this week -1 day'));
                break;
            case'lastMonth':
                $start = date('Y-m-01', strtotime("-1 month"));
                $end = date('Y-m-d', strtotime(date('Y-m-01', time())) - 1);
                break;
        }
        $timeData = 0;
        $time = $request->input('time');
        if ($start && $end) {
            $timeData = (strtotime($end) - strtotime($start)) / 86400 + 1;
        } elseif (!empty($time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($time[0]));
            $end = date('Y-m-d H:i:s', strtotime($time[1]));
            $timeData = (strtotime($time[1]) - strtotime($time[0])) / 86400 + 1;
        }

        //$withdrawOrder->whereBetween('created_at', [$start, $end]);

        $items = [];
        for ($i = 0; $i < $timeData; $i++) {
            $show = date('Y-m-d', strtotime($start) + (86400 * $i));
            $items[$i]['time'] = $show;
            $order = Order::query();
            $order->whereBetween('created_at', [$show . ' 00:00:00', $show . ' 23:59:59']);
            $allMoney = $order->sum('amount');
            $items[$i]['allNumber'] = $order->count();
            $items[$i]['allMoney'] = number_format($allMoney, 2, '.', '');

            $money = $order->where('order_state', 'SUCCESS')->sum('amount');
            $items[$i]['money'] = number_format($money, 2, '.', '');
            $items[$i]['perMoney'] = empty($allMoney) ? 0 : (float)number_format($money / $allMoney * 100, 3, '.', '');
            $items[$i]['number'] = $order->count();
            $items[$i]['perNumber'] = empty($items[$i]['allNumber']) ? 0 : (float)number_format($items[$i]['number'] / $items[$i]['allNumber'] * 100, 3, '.', '');
        }
        $order = Order::query();
        $order->whereBetween('created_at', [$start . ' 00:00:00', $end . ' 23:59:59']);
        $allMoney = $order->sum('amount');
        $allNumber = $order->count();
        $successMoney = $order->where('order_state', 'SUCCESS')->sum('amount');
        $perMoney = empty($allMoney) ? 0 : (float)$successMoney / $allMoney * 100;
        $successNumber = $order->count();
        $perNumber = empty($allNumber) ? 0 : (float)$successNumber / $allNumber;
        if ($request->input('sortVal') == 'desc') {
            $items = array_reverse($items);
        }
        $columns = [
            ['title' => '日期', 'key' => 'time', 'minWidth' => 100, 'sortable' => 'custom', 'align' => 'center'],
            ['title' => '打赏总金额', 'key' => 'allMoney', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '打赏成功金额', 'key' => 'money', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '成功占比 %', 'key' => 'perMoney', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '订单总数', 'key' => 'allNumber', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '支付成功数', 'key' => 'number', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '支付成功占比 %', 'key' => 'perNumber', 'minWidth' => 120, 'align' => 'center'],
        ];
        $headerData = [
            ['number' => (float)$allMoney, 'name' => '打赏总金额'],
            ['number' => (float)$successMoney, 'name' => '打赏成功金额'],
            ['number' => $perMoney, 'name' => '成功占比'],
            ['number' => $allNumber, 'name' => '订单总数'],
            ['number' => $successNumber, 'name' => '支付成功数'],
            ['number' => $perNumber, 'name' => '支付成功占比'],
        ];
        $timeSer = [$start, $end];
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'time' => $timeSer,
            'items' => $items,
            'columns' => $columns,
            'headerData' => $headerData
        ]);
        return Response::success($successRequest);
    }

    public function countNum()
    {
        $withdrawOrder = WithdrawOrder::query()
            ->where('state', 'WAIT')
            ->count();
        echo $withdrawOrder;
    }
}
