<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\InternalUser;
use App\Models\User;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class InternalUserLogic
{
    public function index(Request $request): HttpResponseException
    {
        $internalUserIds = InternalUser::query()
            ->select('user_id')
            ->pluck('user_id')
            ->toArray();
        $model = User::query()->whereIn('id', $internalUserIds)->orderBy('id', 'asc');
        $user = $model->paginate($request->input('pageSize'), ['*'], 'page', $request->input('page'));
        $item = $user->items();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['mobile'] = $val->mobile;
            $items[$key]['nickName'] = $val->name;
            $items[$key]['createTime'] = date($val->created_at);
        }

        $columns = [
            ['title' => '用户ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '名称', 'key' => 'nickName', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '手机', 'key' => 'mobile', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 100, 'align' => 'center', 'slot' => 'edit']
        ];
        $total = $user->total();
        $page = $user->lastPage();
        $pageSize = $request->input('pageSize');
        $data = (compact('total', 'pageSize', 'columns', 'page', 'items'));
        $pageRequest = new PageRequest();
        $pageRequest->setData($data);
        return Response::page($pageRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        User::query()
            ->findOrFail($request->input('user_id'));
        $model = InternalUser::withTrashed()
            ->firstOrCreate(
                [
                    'user_id' => $request->input('user_id')
                ]
            );
        if ($model->trashed()) {
            $model->restore();
        }
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        InternalUser::query()
            ->where('user_id', $request->input('id'))
            ->delete();
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }
}