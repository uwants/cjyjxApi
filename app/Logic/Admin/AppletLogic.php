<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\AdminAppletSetting;
use Illuminate\Http\Request;
use App\Facades\WeiXinApi;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;

class AppletLogic
{
    public function index()
    {
        $getAccessTokenRequest = new GetAccessTokenRequest();
        $getAccessTokenRequest->setAppId(config('common.weChatService.appId'));
        $getAccessTokenRequest->setSecret(config('common.weChatService.appSecret'));
        $returnMsg = WeiXinApi::getAccessToken($getAccessTokenRequest);
        $access_token = $returnMsg->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=$access_token";
        $code_json_list = Http::get($url);
        $code_arr_tpList = $code_json_list->json();
        $rs = [];
        $select = ['menu1', 'menu2', 'menu3'];
        $viewList = $this->getSome();
        foreach ($select as $key => $val) {
            if (!empty($code_arr_tpList['selfmenu_info']['button'][$key])) {
                $rs[$val] = $this->createShow($code_arr_tpList['selfmenu_info']['button'][$key], $key);
            } else {
                $tp = array('messageShow' => false, 'message' => array(
                    'name' => '+',
                    'urlType' => 'url',
                    'url' => '',
                    'applets' => '',
                    'onlyOne' => true,
                    'msgType' => false,
                    'main' => $key + 1,
                    'mainSon' => 0
                ));
                $rs[$val]['main'] = $tp;
                $rs[$val]['son'] = [];
            }
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('rs', 'viewList'));
        return Response::success($successRequest);
    }

    public function createShow($data, $kee)
    {
        $rs = [];
        $urlType = ['view' => 'url', 'miniprogram' => 'applets', 'view_limited' => 'view'];
        if (!empty($data['sub_button'])) {
            foreach ($data['sub_button']['list'] as $key => $val) {
                $tp = array(
                    'name' => $val['name'],
                    'urlType' => $urlType[$val['type']],
                    'url' => @$val['url'],
                    'applets' => empty($val['pagepath']) ? '' : $val['pagepath'],
                    'view' => empty($val['media_id']) ? '' : $val['media_id'],
                    'onlyOne' => true,
                    'msgType' => true,
                    'main' => $kee + 1,
                    'mainSon' => $key
                );
                $rs['son'][$key] = $tp;
            }
            $tp = array('messageShow' => false, 'message' => array(
                'name' => $data['name'],
                'urlType' => 'url',
                'url' => '',
                'applets' => '',
                'view' => '',
                'onlyOne' => false,
                'msgType' => false,
                'main' => $kee + 1,
                'mainSon' => 0
            ));
            $rs['main'] = $tp;
        } else {
            $tp = array('messageShow' => false, 'message' => array(
                'name' => $data['name'],
                'urlType' => $data['type'] == 'view' ? 'url' : 'applets',
                'url' => $data['url'],
                'applets' => empty($data['pagepath']) ? '' : $data['pagepath'],
                'view' => empty($val['media_id']) ? '' : $val['media_id'],
                'onlyOne' => true,
                'msgType' => false,
                'main' => $kee + 1,
                'mainSon' => 0
            ));
            $rs['main'] = $tp;
            $rs['son'] = [];
        }
        return $rs;
    }

    public function getSome()
    {
        $getAccessTokenRequest = new GetAccessTokenRequest();
        $getAccessTokenRequest->setAppId(config('common.weChatService.appId'));
        $getAccessTokenRequest->setSecret(config('common.weChatService.appSecret'));
        $returnMsg = WeiXinApi::getAccessToken($getAccessTokenRequest);
        $access_token = $returnMsg->getAccessToken();
        $postOne = array('type' => 'news', 'offset' => 0, 'count' => 100);
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=$access_token";
        $code_json_list = Http::post($url, $postOne);
        $code_arr_tpList = $code_json_list->json();
        $list = [];
        foreach ($code_arr_tpList['item'] as $key => $val) {
            $list[$key]['value'] = $val['media_id'];
            $list[$key]['name'] = $val['content']['news_item'][0]['title'];
        }
        return $list;
    }

    public function update(Request $request): HttpResponseException
    {
        $newRs = $this->createMenu($request->input('menuMessage'));
        $postOne = array('button' => $newRs);
        $getAccessTokenRequest = new GetAccessTokenRequest();
        $getAccessTokenRequest->setAppId(config('common.weChatService.appId'));
        $getAccessTokenRequest->setSecret(config('common.weChatService.appSecret'));
        $returnMsg = WeiXinApi::getAccessToken($getAccessTokenRequest);
        $access_token = $returnMsg->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$access_token";
        //$res_str = Http::post($url,$postOne);
        //$res = $res_str->json();
        $req_str = json_encode($postOne, JSON_UNESCAPED_UNICODE);
        $res_str = $this->curl_post($url, $req_str);
        $res = json_decode($res_str, true);
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('res', 'url'));
        return Response::success($successRequest);
    }

    public function curl_post($url, $curlPost, $headerOpt = [])
    {
        if (empty($headerOpt)) {
            $headerOpt = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
        }
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerOpt);//修改header信息
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $out_put = curl_exec($ch);//运行curl
        curl_close($ch);
        return $out_put;

    }

    public function createMenu($menu)
    {
        $rs = array();
        $i = 0;
        foreach ($menu as $key => $val) {
            if ($val['main']['message']['name'] != '+' && empty($val['son'])) {
                //小程序
                if ($val['main']['message']['urlType'] == 'applets') {
                    $rs[$i] = array(
                        "type" => 'miniprogram',
                        "name" => $val['main']['message']['name'],
                        "url" => $val['main']['message']['applets'],
                        "appid" => config('common.mpWeiXin.appId'),
                        "pagepath" => $val['main']['message']['applets'],
                    );
                } elseif ($val['main']['message']['urlType'] == 'url') {
                    $rs[$i] = array(
                        "type" => 'view',
                        "name" => $val['main']['message']['name'],
                        "url" => empty($val['main']['message']['url']) ? '' : $val['main']['message']['url'],
                    );
                } else {
                    $rs[$i] = array(
                        "type" => 'view_limited',
                        "name" => $val['main']['message']['name'],
                        "url" => empty($val['main']['message']['view']) ? '' : $val['main']['message']['view'],
                    );
                }
                $i++;
            } elseif (!empty($val['son'])) {
                $son = array();
                foreach ($val['son'] as $ke => $va) {
                    //小程
                    if ($va['urlType'] == 'applets') {
                        $son[$ke] = array(
                            "type" => 'miniprogram',
                            "name" => $va['name'],
                            "url" => empty($va['applets']) ? '' : $va['applets'],
                            "appid" => config('common.mpWeiXin.appId'),
                            "pagepath" => empty($va['applets']) ? '' : $va['applets'],
                        );
                    } elseif ($va['urlType'] == 'url') {//网页
                        $son[$ke] = array(
                            "type" => 'view',
                            "name" => $va['name'],
                            "url" => empty($va['url']) ? '' : $va['url'],
                        );
                    } else {
                        $son[$ke] = array(
                            "type" => 'view_limited',
                            "name" => $va['name'],
                            "media_id" => empty($va['view']) ? '' : $va['view'],
                        );
                    }
                }//foreach(2)
                $rs[$i] = array(
                    "name" => $val['main']['message']['name'],
                    "sub_button" => $son,
                );
                $i++;
            }
        }

        return $rs;
    }

    public function getSkin(Request $request): HttpResponseException
    {
        $model = AdminAppletSetting::query()
            ->where('type', 'SKIN')
            ->where('app_id', $request->input('appId'))
            ->first();
        if (empty($model->id)) {
            $skin = 'SCHEME_ONE';
        } else {
            $skin = Arr::get($model->setting, 'skinValue');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'skin' => $skin,
        ]);
        return Response::success($successRequest);
    }

    public function setSkin(Request $request): HttpResponseException
    {
        $content = [];
        Arr::set($content, 'skinValue', (string)$request->input('skin'));
        $model = AdminAppletSetting::withTrashed()
            ->where('type', 'SKIN')
            ->where('app_id', $request->input('appId'))
            ->first();
        if (!empty($model->id)) {
            AdminAppletSetting::withTrashed()
                ->where('id', $model->id)
                ->update([
                    'setting' => $content,
                    'deleted_at' => null
                ]);
        } else {
            $modelIns = new AdminAppletSetting();
            $modelIns->fill([
                'type' => 'SKIN',
                'app_id' => $request->input('appId'),
                'state' => true,
                'setting' => $content,
            ]);
            $modelIns->save();
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'skin' => $request->input('skin'),
        ]);
        return Response::success($successRequest);
    }
}
