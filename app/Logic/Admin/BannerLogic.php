<?php


namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\Banner;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class BannerLogic
{
    public function index(Request $request): HttpResponseException
    {
        $pageSize = $request->input('pageSize');
        $model = Banner::query()
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize, ['*'], 'page', $request->input('page'));
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['image'] = $val->image;
            $items[$key]['url'] = $val->url;
            $items[$key]['sort'] = $val->sort;
            $items[$key]['type'] = $val->type;
            if ($val->suspend == 0) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
        }
        $columns = [
            ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '图片', 'key' => 'image', 'minWidth' => 200, 'align' => 'center', 'slot' => 'image'],
            ['title' => '链接', 'key' => 'url', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '排序', 'key' => 'sort', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '类型', 'key' => 'type', 'minWidth' => 100, 'align' => 'center','slot' => 'type'],
            ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
        ];
        $total = $model->total();
        $page = $model->lastPage();
        $data = (compact('items', 'total', 'page', 'pageSize', 'columns'));
        $pageRequest = new PageRequest();
        $pageRequest->setData($data);
        return Response::page($pageRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        $model = new Banner();
        if ($request->input('url')) {
            $model->fill([
                'url' => $request->input('url'),
            ]);
        }
        $model->fill([
            'image' => $request->input('image'),
            'sort' => $request->input('sort'),
            'suspend' => $request->input('suspend'),
            'type' => $request->input('type')
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $bannerId = $request->input('id');
        $model = Banner::query()
            ->firstOrFail($bannerId);
        $successRequest = new SuccessRequest();
        $successRequest->setData($model->toArray());
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $model = Banner::query()
            ->where('id', $request->input('id'))
            ->first();

        if ($request->input('url')) {
            $model->fill([
                'url' => $request->input('url'),
            ]);
        }
        $model->fill([
            'image' => $request->input('image'),
            'sort' => $request->input('sort'),
            'type' => $request->input('type'),
            'suspend' => $request->input('suspend'),
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        $model = Banner::query()
            ->findOrFail($request->input('id'));
        $model->delete();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function switchStatus(Request $request): HttpResponseException
    {
        $model = Banner::query()
            ->findOrFail($request->input('id'));
        $model->fill([
            'suspend' => !$request->input('status')
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }
}
