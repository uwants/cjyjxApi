<?php

namespace App\Logic\Admin;


use App\Exceptions\MessageException;
use App\Facades\Permission;
use App\Facades\Response;
use App\Models\AdminMenu;
use App\Models\AdminMenus;
use App\Models\AdminRole;
use App\Models\AdminUser;
use App\Models\AdminRolePermission;
use App\Models\AdminRoleRelationship;
use App\Services\Permission\Models\GetPermissionRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AdminLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 登录
     */
    public function login(Request $request): HttpResponseException
    {
        $model = AdminUser::query()
            ->with('relationship')
            ->where('name', '=', $request->input('name'))
            ->where('password', '=', md5(md5($request->input('password'))))
            ->first();
        if (empty($model->id)) {
            throw new MessageException('用户名字或者密码错误！');
        } else {
            $accessToken = $model->createToken(config('common.adminTokenName'));
        }
        Auth::loginUsingId($model->id);
        //权限
        $getPermissionRequest = new GetPermissionRequest();
        $getPermissionRequest->setPowerName(['withdrawalReview']);
        $getPermissionResponse = Permission::checkPower($getPermissionRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $model->id,
            'accessToken' => $accessToken->plainTextToken,
            'name' => $model->name,
            'roleId' => empty($model->relationship->role_id) ? 0 : $model->relationship->role_id,
            'nickName' => $model->nick_name,
            'avatar' => $model->avatar,
            'power' => $getPermissionResponse->getPowerList()
        ]);
        return Response::success($successRequest);
    }

    public function index(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '管理员ID', 'key' => 'id', 'minWidth' => 50, 'align' => 'center'],
            ['title' => '名字', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '角色', 'key' => 'roleName', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 150, 'align' => 'center', 'slot' => 'edit'],
        ];
        $model = AdminUser::query()
            ->with(['relationship' => function ($query) {
                $query->with('role');
            }])
            ->select('name', 'id', 'avatar', 'nick_name', 'created_at');
        if (Auth::id() != 1) {
            $model->whereIn('id', function ($query) {
                $query->from('admin_role_relationships')
                    ->select('admin_id')
                    ->where('created_admin_id', Auth::id());
            });
        }
        if ($request->input('keyWord')) {
            $model->where('name', 'like', "%" . $request->input('keyWord') . "%");
        }
        $model->orderByDesc('id');
        $admin = $model->paginate($request->input('pageSize'));
        $item = $admin->items();
        $total = $admin->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['roleName'] = empty($val->relationship->role->name) ? '保留账号' : $val->relationship->role->name;
            $items[$key]['roleId'] = empty($val->relationship->role_id) ? 0 : $val->relationship->role_id;
            $items[$key]['avatar'] = $val->avatar;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
            'roleList' => $this->roleList()
        ]);
        return Response::success($successRequest);
    }

    public function roleList()
    {
        $model = AdminRole::query();
        if (Auth::id() != 1) {
            $model->where('created_admin_id', Auth::id());
        }
        $item = $model->get();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['roleId'] = $val->id;
            $items[$key]['name'] = $val->name;
        }
        return $items;
    }

    public function show(Request $request): HttpResponseException
    {
        $model = AdminUser::query()
            ->where('id', $request->input('id'))
            ->first();
        $successRequest = new SuccessRequest();
        $successRequest->setData($model->toArray());
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $message = $request->input('data');
        $admin = AdminUser::query()
            ->where('id', $message['id'])
            ->first();
        $oldPass = md5(md5($message['oldPass']));
        $newPass = md5(md5($message['newPass']));
        if ($oldPass != $admin->password) {
            throw new Exception('原始密码不正确！');
        }
        if (!empty($message['newName'])) {
            $oldMessage = AdminUser::query()
                ->where('name', $message['newName'])
                ->where('id', '!=', $message['id'])
                ->first();
            if (!empty($oldMessage->id)) {
                throw new Exception('账号名字已经存在，修改失败！');
            }
        }
        $model = AdminUser::query()
            ->where('id', $message['id'])
            ->findOrFail($message['id']);
        $model->fill(Arr::where([
            'name' => $message['newName'],
            'avatar' => $message['headerUrl'],
            'password' => $newPass,
        ], function ($item) {
            return !is_null($item);
        }));
        $model->save();
        $successRequest = new SuccessRequest();
        $successRequest->setData(['id' => $message['id']]);
        return Response::success($successRequest);
    }

    public function listMenu(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'menuList' => $this->allMenu(0),
            'powerList' => $this->allMenuSelect(0)
        ]);
        return Response::success($successRequest);
    }

    public function allMenu($pid = 0)
    {
        $query = AdminMenu::query()
            ->where('parent_id', $pid)
            ->orderBy('sort', 'desc')
            ->get();
        $arr = array();
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $give['title'] = $datum->title;
                $give['expand'] = true;
                $give['children'] = static::allMenu($datum->id);
                $arr[] = $give;
            }
        }
        return $arr;
    }

    public function allMenuSelect($pid = 0)
    {
        $query = AdminMenu::query()
            ->where('parent_id', $pid)
            ->orderBy('sort', 'desc')
            ->get();
        static $messageShow = array();
        $stringList = [1 => '', '2' => '--', '3' => '----'];
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $datum['is_ban'] = !empty($datum['ban']);
                $datum['stringList'] = $stringList[$datum->level];
                $messageShow[] = $datum;
                static::allMenuSelect($datum->id);
            }
        }
        return $messageShow;
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function createMenu(Request $request): HttpResponseException
    {
        $menu = $request->input('menu');
        if ($menu['id'] > 0) {
            $message = AdminMenu::query()
                ->where('power_name', $menu['power_name'])
                ->where('id', '!=', $menu['id'])
                ->first();
            if (!empty($message->id)) {
                throw new MessageException('操作失败，权限名字已经存在！');
            }
            $message = AdminMenu::query()
                ->where('id', $menu['id'])
                ->update([
                    'name' => $menu['name'],
                    'path' => $menu['path'],
                    'title' => $menu['title'],
                    'icon' => empty($menu['icon']) ? '' : $menu['icon'],
                    'power_name' => $menu['power_name'],
                    'parent_id' => $menu['parent_id'],
                    'level' => $menu['level'],
                    'ban' => $menu['is_ban'],
                    'sort' => $menu['sort'],
                ]);
        } else {
            $message = AdminMenu::query()->where('power_name', $menu['power_name'])
                ->first();
            if (!empty($message->id)) {
                throw new MessageException('操作失败，权限名字已经存在！');
            }
            $message = AdminMenu::query()
                ->insert([
                    'name' => $menu['name'],
                    'path' => $menu['path'],
                    'title' => $menu['title'],
                    'icon' => empty($menu['icon']) ? '' : $menu['icon'],
                    'power_name' => $menu['power_name'],
                    'parent_id' => $menu['parent_id'],
                    'level' => $menu['level'],
                    'ban' => $menu['is_ban'],
                    'sort' => $menu['sort'],
                ]);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'message' => $message,
        ]);
        return Response::success($successRequest);
    }

    public function deleteMenu(Request $request): HttpResponseException
    {
        AdminMenu::query()
            ->where('id', $request->input('id'))
            ->forceDelete();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $request->input('id'),
        ]);
        return Response::success($successRequest);
    }

    public function role(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '角色ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '角色名称', 'key' => 'name', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '备注', 'key' => 'remark', 'minWidth' => 100, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '创建者', 'key' => 'createUser', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '添加时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '状态', 'key' => 'stateType', 'width' => 100, 'align' => 'center'],
            ['title' => '编辑', 'key' => 'edit', 'minWidth' => 250, 'align' => 'center', 'slot' => 'edit',],
        ];

        $model = AdminRole::withTrashed()
            ->with('adminUser');
        if (Auth::id() != 1) {
            $model->where('created_admin_id', Auth::id());
        }
        if ($request->input('keyword')) {
            $model->where('name', 'like', "%" . $request->input('keyword') . "%");
        }
        switch ($request->input('state')) {
            case'delete':
                $model->where('deleted_at', 'is not null');
                break;
            case'stay':
                $model->where('deleted_at', null);
                break;
        }
        $model->orderBy('id', 'desc');
        $admin = $model->paginate($request->input('pageSize'));
        $item = $admin->items();
        $total = $admin->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['remark'] = $val->remark;
            $items[$key]['createUser'] = @$val->adminUser->name;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['stateType'] = empty($val->deleted_at) ? '正常' : '已删除';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => $request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function roleCreate(Request $request): HttpResponseException
    {
        $role = $request->input('role');
        if ($role['id'] > 0) {
            $roleMsg = AdminRole::query()
                ->where('id', $role['id'])
                ->first();
            if ($role['id'] > 0 && Auth::id() != $roleMsg->created_admin_id && Auth::id() != 1) {
                throw new MessageException('操作失败，参数错误！你没有修改该角色的权限');
            }
            $model = AdminRole::query()
                ->where('id', $role['id'])
                ->update([
                    'name' => $role['name'],
                    'remark' => empty($role['remark']) ? '' : $role['remark'],
                ]);
        } else {
            $model = AdminRole::query()
                ->insert([
                    'name' => $role['name'],
                    'remark' => empty($role['remark']) ? '' : $role['remark'],
                    'created_admin_id' => Auth::id(),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time())
                ]);
        }
        if (!$model) {
            throw new MessageException('操作失败，数据修改失败！');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'model' => $model,

        ]);
        return Response::success($successRequest);
    }

    public function roleMenu(Request $request): HttpResponseException
    {
        if (Auth::id() == 1) {
            $master = AdminMenu::query()
                ->where(['ban' => 0])
                ->pluck('id')
                ->toArray();
        } else {
            $adminRole = AdminRoleRelationship::query()
                ->where('admin_id', Auth::id())
                ->first();
            $master = AdminRolePermission::query()
                ->where(['role_id' => @$adminRole->role_id])
                ->whereIn('menu_id', function ($query) {
                    $query->from('admin_menus')->select('id')
                        ->where('ban', 0);
                })
                ->pluck('menu_id')
                ->toArray();
        }
        $role = AdminRolePermission::query()
            ->where(['role_id' => $request->input('roleId')])
            ->pluck('menu_id')
            ->toArray();
        $arr = static::getAllPower(0, $master, $role);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'arr' => $arr,

        ]);
        return Response::success($successRequest);
    }

    public function getAllPower($pid, $master, $role_list)
    {
        $list = AdminMenu::query()
            ->where(['parent_id' => $pid, 'ban' => 0])
            ->get();
        $arr = array();
        //以超级管理员权限作为后台主要权限
        if (sizeof($list) != 0) {
            foreach ($list as $k => $datum) {
                if (in_array($datum->id, $master)) {
                    $give['title'] = $datum->title;
                    $give['expand'] = true;
                    $give['value'] = $datum->id;
                    $give['pId'] = $datum->parent_id;
                    $give['level'] = $datum->level;
                    if (in_array($datum->id, $role_list)) {
                        //$give['selected']=true;
                        $give['checked'] = true;
                    } else {
                        $give['checked'] = false;
                    }
                    $give['children'] = static::getAllPower($datum->id, $master, $role_list);
                    $arr[] = $give;
                }
            }
        }
        return $arr;
    }

    public function updateRole(Request $request): HttpResponseException
    {
        $insert_arr = array();
        $time_set = date('Y-m-d H:i:s', time());
        foreach ($request->input('roleList') as $key => $val) {
            $insert_arr[$key]['role_id'] = $request->input('roleId');
            $insert_arr[$key]['menu_id'] = $val['menuId'];
            $insert_arr[$key]['level'] = $val['level'];
            $insert_arr[$key]['parent_id'] = $val['pId'];
            $insert_arr[$key]['created_at'] = $time_set;
            $insert_arr[$key]['updated_at'] = $time_set;
        }
        DB::beginTransaction();
        AdminRolePermission::query()
            ->where(['role_id' => $request->input('roleId')])
            ->delete();
        $insertMes = AdminRolePermission::query()->insert($insert_arr);
        if ($insertMes) {
            DB::commit();
        } else {
            throw new Exception('操作失败，数据修改失败！');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'insertMes' => $insertMes,
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function deleteRole(Request $request): HttpResponseException
    {
        $adminUser = AdminRoleRelationship::query()->where('role_id', $request->input('roleId'))->first();
        if (!empty($adminUser->id)) {
            throw new Exception('操作失败，该角色存在管理员，请移除后再删除！');
        }
        $roleMsg = AdminRole::query()
            ->where('id', $request->input('roleId'))
            ->first();
        if (Auth::id() != $roleMsg->created_admin_id && Auth::id() != 1) {
            throw new MessageException('操作失败，参数错误！你没有修改该角色的权限');
        }
        DB::beginTransaction();
        $deleteOne = AdminRolePermission::query()
            ->where(['role_id' => $request->input('roleId')])
            ->forceDelete();
        $deleteTwo = AdminRole::query()
            ->where('id', $request->input('roleId'))
            ->forceDelete();
        AdminRoleRelationship::query()
            ->where('role_id', $request->input('roleId'))
            ->forceDelete();
        if ($deleteOne || $deleteTwo) {
            DB::commit();
        } else {
            throw new MessageException('操作失败，数据删除失败！');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'deleteOne' => $deleteOne,
            'deleteTwo' => $deleteTwo,
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function create(Request $request): HttpResponseException
    {
        $oldMessage = AdminUser::withTrashed()
            ->where('name', $request->input('adminName'))
            ->first();
        if (!empty($oldMessage->id)) {
            throw new MessageException('账号名字已经被占用，操作失败！');
        }
        if ($request->input('id') > 0) {
            AdminUser::query()
                ->where('id', $request->input('id'))
                ->update([
                    'name' => $request->input('adminName'),
                ]);
            AdminRoleRelationship::query()
                ->firstOrCreate(
                    [
                        'admin_id' => $request->input('id')
                    ],
                    [
                        'created_admin_id' => Auth::id(),
                        'role_id' => $request->input('roleId')
                    ]
                );
            $id = $request->input('id');
        } else {
            $model = AdminUser::query()->insertGetId([
                'name' => $request->input('adminName'),
                'password' => md5(md5($request->input('password'))),
                'avatar' => config('common.default_avatar'),
                'nick_name' => $request->input('adminName'),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
            $id = $model;
            AdminRoleRelationship::query()
                ->firstOrCreate(
                    [
                        'admin_id' => $id
                    ],
                    [
                        'created_admin_id' => Auth::id(),
                        'role_id' => $request->input('roleId')
                    ]
                );
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $id,
        ]);
        return Response::success($successRequest);
    }

    public function delete(Request $request): HttpResponseException
    {
        AdminUser::query()
            ->where('id', $request->input('id'))
            ->delete();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $request->input('id'),
        ]);
        return Response::success($successRequest);
    }

    public function menu(Request $request): HttpResponseException
    {
        $query = Cache::get('menu_list_' . $request->input('roleId'));
        if (empty($query)) {
            //记录角色权限
            if ($request->input('roleId') == 0) {
                $weight = DB::table('admin_menus')->where(['ban' => 0])->pluck('id')->toArray();
            } else {
                $weight = DB::table('admin_role_permissions')
                    ->where(['role_id' => $request->input('roleId')])
                    ->pluck('menu_id')->toArray();
            }
            $query = static::getAllMenu('0', $weight);
            Cache::put('menu_list_' . $request->input('roleId'), $query, '3600');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($query);
        return Response::success($successRequest);
    }

    public function getAllMenu($pid, $role_list, $type = 1)
    {
        $query = DB::table('admin_menus')
            ->where(['parent_id' => $pid, 'ban' => 0])
            ->orderBy('sort', 'desc')
            ->get();
        $arr = array();
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $give['name'] = $datum->name;
                $give['title'] = $datum->title;
                $give['icon'] = empty($datum->icon) ? '' : $datum->icon;
                $give['meta'] = [
                    'icon' => empty($datum->icon) ? '' : $datum->icon,
                    'title' => $datum->title
                ];
                if ($type == 1) {
                    if ($datum->level < 2) {//过滤菜单等级
                        $give['children'] = static::getAllMenu($datum->id, $role_list);
                    }
                } else {
                    $give['children'] = static::getAllMenu($datum->id, $role_list);
                }
                if (in_array($datum->id, $role_list)) {
                    $arr[] = $give;
                }
            }
        }
        return $arr;
    }
}
