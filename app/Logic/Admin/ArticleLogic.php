<?php

namespace App\Logic\Admin;


use App\Facades\Response;
use App\Models\AdminArticle;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ArticleLogic
{
    public function index(Request $request): HttpResponseException
    {
        $columns = [
            ['title' => '文章ID', 'key' => 'id', 'minWidth' => 50, 'align' => 'center'],
            ['title' => '文章标题', 'key' => 'title', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 100, 'align' => 'center', 'slot' => 'open'],
            ['title' => '排序', 'key' => 'sort', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '平台', 'key' => 'platform', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 220, 'align' => 'center', 'slot' => 'edit'],
        ];
        $model = AdminArticle::query()
            ->select('id', 'title', 'content', 'suspend', 'sort', 'created_at', 'type');
        //订单关键字查找
        if ($request->input('keyword')) {
            $model->where('title', 'like', "%" . $request->input('keyword') . "%");
        }
        $model->orderBy('sort', 'desc');
        $article = $model->paginate($request->input('pageSize'));
        $item = $article->items();
        $total = $article->total();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['sort'] = $val->sort;
            $items[$key]['title'] = $val->title;
            $items[$key]['platform'] = $val->ArticleType;
            $items[$key]['suspend'] = $val->suspend == 0;
            $items[$key]['content'] = $val->content;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'total' => $total,
            'pageSize' => (int)$request->input('pageSize'),
            'page' => empty($request->input('page')) ? 1 : $request->input('page'),
            'items' => $items,
            'columns' => $columns,
        ]);
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        if ($request->input('id') > 0) {
            $data = AdminArticle::query()
                ->where('id', $request->input('id'))
                ->first()->toArray();
            $returnList['content'] = $data['content'];
            $returnList['id'] = $data['id'];
            $returnList['sort'] = $data['sort'];
            $returnList['title'] = $data['title'];
            $returnList['type'] = explode(',', $data['type']);
            $returnList['suspend'] = $data['suspend'] ? false : true;
            $returnList['createTime'] = date($data['created_at']);
        } else {
            $returnList['content'] = '';
            $returnList['id'] = 0;
            $returnList['sort'] = 0;
            $returnList['title'] = '';
            $returnList['type'] = ['INSTRUCTION_WEIXIN'];
            $returnList['suspend'] = true;
            $returnList['createTime'] = '';
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($returnList);
        return Response::success($successRequest);
    }

    public function store(Request $request): HttpResponseException
    {
        if ($request->input('id') > 0) {
            AdminArticle::query()
                ->where('id', $request->input('id'))
                ->update([
                    'title' => $request->input('title'),
                    'type' => implode(',', $request->input('type')),
                    'sort' => $request->input('sort'),
                    'suspend' => $request->input('suspend') == 'true' ? 0 : 1,
                    'content' => $request->input('content')
                ]);
            $id = $request->input('id');
        } else {
            $model = AdminArticle::query()
                ->firstOrCreate([
                    'title' => $request->input('title'),
                    'type' => implode(',', $request->input('type')),
                    'sort' => $request->input('sort'),
                    'suspend' => $request->input('suspend') == 'true' ? 0 : 1,
                    'content' => $request->input('content')
                ]);
            $id = $model->getOriginal('id');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        AdminArticle::query()
            ->where('id', '=', $request->input('id'))
            ->delete();
        $id = $request->input('id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function all(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }

    public function switch(Request $request): HttpResponseException
    {
        AdminArticle::query()
            ->where('id', '=', $request->input('id'))
            ->update([
                'suspend' => $request->input('suspend') == true ? 0 : 1,
            ]);
        $id = $request->input('id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }
}
