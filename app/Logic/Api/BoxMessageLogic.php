<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\Box;
use App\Models\BoxMessage;
use App\Models\Comment;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BoxMessageLogic
{
    public function index(Request $request): HttpResponseException
    {
        $page = $request->input('page');
        $pageSize = $request->input('pageSize');
        $messageModel = BoxMessage::withTrashed()
            ->where('user_id', Auth::id())
            ->where('template_id', '!=', 2)
            ->with([
                'box' => function ($query) {
                    call_user_func([$query, 'withTrashed']);
                }
            ])
            ->latest()
            ->paginate($pageSize, ['*'], 'page', $page);
        $items = $messageModel->items();
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'boxMessageId' => $item->id,
                'boxId' => $item->box->id,
                'boxMessageContent' => $item->content,
                'boxMessageTitle' => $item->title,
                'boxMessageRead' => (bool)$item->read,
                'boxMessageCreatedTime' => Carbon::make($item->created_at)->format('m月d日 H:i')
            ];
        }
        $pageRequest = new PageRequest();
        $pageRequest->setList($data);
        $pageRequest->setPage($page);
        $pageRequest->setPageSize($pageSize);
        $pageRequest->setTotalPage($messageModel->lastPage());
        $pageRequest->setTotalNumber($messageModel->total());
        $pageRequest->execute();
        return Response::page($pageRequest);
    }

    public function receiveComment(): HttpResponseException
    {
        $model = BoxMessage::query()
            ->select('box_id', DB::raw('count(*) as total'))
            ->groupBy('box_id')
            ->where('user_id', Auth::id())
            ->where('template_id', 2)
            ->where('read', false)
            ->with([
                'box' => function ($query) {
                    call_user_func([$query, 'withTrashed']);
                }
            ])
            ->orderByDesc(
                Comment::query()
                    ->select('created_at')
                    ->whereColumn('box_id', 'box_messages.box_id')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
            )
            ->get();
        $data = [];
        foreach ($model as $item) {
            $data[] = [
                'boxId' => $item->box_id,
                'boxMessageContent' => $item->box->name,
                'boxMessageTitle' => '收到新意见',
                'boxMessageNum' => $item->total,
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function read(Request $request): HttpResponseException
    {
        if ($request->input('boxId')) {
            BoxMessage::query()
                ->where('user_id', Auth::id())
                ->where('template_id', 2)
                ->where('box_id', $request->input('boxId'))
                ->update([
                    'read' => true
                ]);
        }
        if ($request->input('boxMessageId')) {
            BoxMessage::query()
                ->where('id', $request->input('boxMessageId'))
                ->update([
                    'read' => true
                ]);
        }
        $data = [];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function unReadTotal(): HttpResponseException
    {
        $boxMessageNum = BoxMessage::query()
            ->where('user_id', Auth::id())
            ->where('template_id', '!=', 2)
            ->where('read', false)
            ->count();
        $receiveBox = BoxMessage::query()
            ->where('user_id', Auth::id())
            ->where('template_id', 2)
            ->where('read', false)
            ->pluck('box_id')
            ->unique()
            ->count();
        $successRequest = new SuccessRequest();
        $data = [
            'unReadNum' => $boxMessageNum + $receiveBox
        ];
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}