<?php


namespace App\Logic\Api;


use App\Exceptions\MessageException;
use App\Facades\CompanyMain;
use App\Facades\Response;
use App\Models\CompanyUser;
use App\Models\CompanyUserInvite;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\Response\Models\SuccessRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CompanyUserInviteLogic
{
    /**
     * @return HttpResponseException
     * @throws MessageException
     */
    public function store(): HttpResponseException
    {
        $userId = Auth::id();
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdRequest->setUserId($userId);
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        if(empty($companyId)){
            throw new MessageException('无企业组织');
        }
        $companyUser = CompanyUser::query()
            ->select('id')
            ->where('user_id', $userId)
            ->where('company_id', $companyId)
            ->firstOrFail();
        $model = new CompanyUserInvite();
        $model->fill([
            'company_id' => $companyId,
            'company_user_id' => $companyUser->getKey(),
            'token' => Str::random(16),
            'token_expired_at' => Carbon::now()->addDay()->toDateString(),
        ]);
        $model->save();
        $companyUserInviteToken = $model->getAttribute('token');
        $companyUserInviteCompanyId = $model->getAttribute('company_id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyUserInviteToken', 'companyUserInviteCompanyId'));
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $companyUserInviteCompanyId = $request->input('companyUserInviteCompanyId');
        $companyUserInviteToken = $request->input('companyUserInviteToken');
        $model = CompanyUserInvite::withTrashed()
            ->where('company_id', $companyUserInviteCompanyId)
            ->where('token', $companyUserInviteToken)
            ->firstOrFail();
        $model->load([
            'company_user' => function (BelongsTo $query) {
                call_user_func([$query, 'withTrashed']);
            },
            'company'
        ]);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'companyUserName' => Arr::get($model, 'company_user.name'),
            'companyUserAvatar' => Arr::get($model, 'company_user.avatar'),
            'companyName' => Arr::get($model, 'company.name'),
            'companyLogo' => Arr::get($model, 'company.logo', config('common.default_company_logo')),
        ]);
        return Response::success($successRequest);
    }
}
