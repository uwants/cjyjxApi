<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Storage;

class QiNiuLogic
{
    /**
     * @return HttpResponseException
     */
    public function token(): HttpResponseException
    {
        $driver = Storage::disk('qiniu')
            ->getDriver();
        $token = call_user_func([$driver, 'uploadToken']);
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('token'));
        return Response::success($successRequest);
    }

}