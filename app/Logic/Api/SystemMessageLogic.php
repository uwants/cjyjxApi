<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\SystemMessage;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SystemMessageLogic
{
    public function index(Request $request): HttpResponseException
    {
        $page = $request->input('page');
        $pageSize = $request->input('pageSize');
        $model = SystemMessage::query()
            ->where('user_id', Auth::id())
            ->latest()
            ->paginate($pageSize, ['*'], 'page', $page);
        $items = $model->items();
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'systemMessageId' => empty($item->id) ? 0 : $item->id,
                'systemMessageTitle' => empty($item->title) ? '' : $item->title,
                'systemMessageContent' => empty($item->content) ? '' : $item->content,
            ];
        }
        $pageRequest = new PageRequest();
        $pageRequest->setPageSize($pageSize);
        $pageRequest->setTotalPage($model->lastPage());
        $pageRequest->setPage($page);
        $pageRequest->setTotalNumber($model->total());
        $pageRequest->setList($data);
        $pageRequest->execute();
        Response::page($pageRequest);
    }

    public function read(Request $request): HttpResponseException
    {
        $systemMessageId = $request->input('systemMessageId');
        $model = SystemMessage::query()
            ->where('id', $systemMessageId)
            ->first();
        $model->fill([
            'read' => true
        ]);
        $model->save();
        $id = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('id'));
        return Response::success($successRequest);
    }
}