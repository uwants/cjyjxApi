<?php


namespace App\Logic\Api;


use App\Facades\PlatformCode;
use App\Facades\Response;
use App\Models\AdminArticle;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class NewsLogic
{
    /**
     * @throws Exception
     */
    public function index(Request $request): HttpResponseException
    {
        $page = $request->input('page');
        $pageSize = $request->input('pageSize');
        $pageRequest = new PageRequest();
        $pageRequest->setPage($page);
        $pageRequest->setPageSize($pageSize);
        $offset = $pageSize * ($page - 1);
        $accessToken = $this->getAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=$accessToken";
        $http = Http::post($url, [
            'type' => 'news',
            'offset' => $offset,
            'count' => $pageSize,
        ]);
        $result = $http->json();
        $errCode = Arr::get($result, 'errcode');
        if (!empty($errCode)) {
            throw new Exception(Arr::get($result, 'errmsg'));
        }
        $list = [];
        $pageRequest->setTotalNumber(Arr::get($result, 'total_count'));
        $pageRequest->setPageSize(Arr::get($result, 'item_count'));
        $totalPage = ceil($pageRequest->getTotalNumber() / $pageRequest->getPageSize());
        $pageRequest->setTotalPage($totalPage);
        $items = Arr::get($result, 'item');
        foreach ($items as $key => $val) {
            foreach ($val['content']['news_item'] as $ke => $va) {
                $list[$key]['createdTime'] = date('Y-m-d', $val['content']['create_time']);
                $list[$key]['title'] = $va['title'];
                $list[$key]['author'] = $va['author'];
                $list[$key]['digest'] = $va['digest'];
                $list[$key]['content'] = str_replace("data-src", "src", $va['content']);
                $list[$key]['mediaId'] = $val['media_id'];
                $list[$key]['thumbUrl'] = $va['thumb_url'];
                $list[$key]['url'] = $va['url'];
                break;
            }
        }
        $pageRequest->setList($list);
        $pageRequest->execute();
        return Response::page($pageRequest);
    }

    /**
     * @return string
     * @throws Exception
     */
    private function getAccessToken(): string
    {
        $appId = config('common.weChatSubscribe.appId');
        $secret = config('common.weChatSubscribe.appSecret');
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$secret";
        $accessToken = Cache::get('app_access_token_' . $appId);
        if (!empty($accessToken)) {
            $access_token = $accessToken;
        } else {
            $http = Http::get($url);
            $result = $http->json();
            $errCode = Arr::get($result, 'errcode');
            if (!empty($errCode)) {
                throw new Exception(Arr::get($result, 'errmsg'));
            }
            Cache::put('app_access_token_' . $appId, Arr::get($result, 'access_token'), '100');
            $access_token = Arr::get($result, 'access_token');
        }
        return $access_token;
    }

    public function guide(): HttpResponseException
    {
        $model = AdminArticle::query()
            ->select('id', 'title', 'content', 'suspend', 'sort', 'created_at', 'type')
            ->where('suspend', 0);
        switch (PlatformCode::getPlatformCode()) {
            case'MP-DINGTALK':
                $model->where('type', 'like', '%INSTRUCTION_DING%');
                break;
            case'MP-WEIXIN':
                $model->where('type','like' ,'%INSTRUCTION_WEIXIN%');
                break;
            case'H5':
                $model->where('type', 'like','%H5%');
                break;
        }
        $modelMess = $model->orderBy('sort', 'desc')
            ->get();
        $returnList = [];
        foreach ($modelMess as $key => $val) {
            $returnList[$key]['id'] = $val->id;
            $returnList[$key]['title'] = $val->title;
            $returnList[$key]['content'] = $val->content;
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($returnList);
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        $model = AdminArticle::query()
            ->select('id', 'title', 'content', 'suspend', 'sort', 'created_at')
            ->where('id', $request->input('id'))
            ->first();
        $returnList['id'] = $model->id;
        $returnList['title'] = $model->title;
        $returnList['content'] = $model->content;
        $successRequest = new SuccessRequest();
        $successRequest->setData($returnList);
        return Response::success($successRequest);
    }
}
