<?php

namespace App\Logic\Api;

use App\Facades\Reply;
use App\Facades\Response;
use App\Services\Reply\Models\CreateReplyRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ReplyLogic
{
    public function index(Request $request): HttpResponseException
    {
        //
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     */
    public function store(Request $request): HttpResponseException
    {
        $createReplyRequest = new CreateReplyRequest();
        $createReplyRequest->setUserId(Auth::id());
        $createReplyRequest->setCommentId($request->input('commentId'));
        $createReplyRequest->setContent((array) $request->input('content'));
        $createReplyResponse = Reply::createReply($createReplyRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'replyId' => $createReplyResponse->getId(),
            'replyContent' => $createReplyResponse->getContent(),
            'CreatedTime' =>  Carbon::parse($createReplyResponse->getCreatedAt())->format('Y-m-d'),
        ]);
        return Response::success($successRequest);
    }

    public function show(Request $request): HttpResponseException
    {
        //
    }

    public function update(Request $request): HttpResponseException
    {
        //
    }

    public function destroy(Request $request): HttpResponseException
    {
        //
    }
}
