<?php


namespace App\Logic\Api;


use App\Facades\Company;
use App\Facades\CompanyPlatform;
use App\Facades\PlatformCode;
use App\Facades\Response;
use App\Facades\SuiteTicket;
use App\Facades\WorkWeChat;
use App\Facades\WorkWeiXinCompanyCorp;
use App\Logic\Api\WorkWechat\Prpcrypt;
use App\Logic\Api\WorkWechat\WXBizMsgCryptLogic;
use App\Services\Company\Models\CreateCompanyByUserRequest;
use App\Services\Response\Models\SuccessRequest;
use App\Services\Company\Models\CreateCompanyRequest;
use App\Services\Response\Models\JsonRequest;
use App\Services\WorkWeChat\Models\UpdateOrCreateSuiteTicketByWokeWeChatRequest;
use App\Services\WorkWeChat\Models\GetPermanentCodeRequest;
use App\Services\WorkWeChat\Models\GetSuiteAccessTokenRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\CreateCompanyPlatformRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ThirdAuthLogic
{
    public function index(Request $request)
    {
        $platformCode = $request->input('platformCode');
        if ($platformCode == config('common.platformCode.mpWorkWeiXin') || PlatformCode::isMpWorkWeiXin()) {
            return $this->workWeChat($request);
        }
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function workWeChat(Request $request)
    {
        $postStr = file_get_contents("php://input", "r");
        $notify = (array)simplexml_load_string($postStr, "SimpleXMLElement", LIBXML_NOCDATA);
        Log::info('微信测试ticket：', $notify);
        $encodingAesKey = config('common.mpWorkWeChat.encodingAesKey');
        $receiveId = empty($notify['ToUserName']) ? '0' : $notify['ToUserName'];
        $sEchoStr = empty($notify['Encrypt']) ? '0' : $notify['Encrypt'];
        $pc = new Prpcrypt($encodingAesKey);
        $message = $pc->decrypt($sEchoStr, $receiveId);
        $message[1] = empty($message[1]) ? '' : $message[1];
        $sendMsg = (array)simplexml_load_string($message[1], "SimpleXMLElement", LIBXML_NOCDATA);
        $sendMsg['InfoType'] = empty($sendMsg['InfoType']) ? '' : $sendMsg['InfoType'];
        if (trim($sendMsg['InfoType']) == 'suite_ticket') {//推送suite_ticket
            $update = new UpdateOrCreateSuiteTicketByWokeWeChatRequest();
            $update->setCorpId($sendMsg['SuiteId']);
            $update->setSuiteTicket($sendMsg['SuiteTicket']);
            WorkWeChat::updateOrCreateSuiteTicketByWokeWeChat($update);
        } elseif ($sendMsg['InfoType'] == 'create_auth') {//授权成功通知
            Log::info('微信授权：', $sendMsg);
            //suite_access_token获取第三方应用凭证
            $suite_token = new GetSuiteAccessTokenRequest();
            $suite_access_token = WorkWeChat::getSuiteAccessToken($suite_token);
            //获取企业的永久授权码
            Log::info('suite_access_token：', [$suite_access_token->getSuiteAccessToken()]);
            $permanent = new GetPermanentCodeRequest();
            $permanent->setSuiteAccessToken($suite_access_token->getSuiteAccessToken());
            $permanent->setAuthCode($sendMsg['AuthCode']);
            $permanent = WorkWeChat::getPermanentCode($permanent);
            $permanentMsg = $permanent->getData();
            Log::info('微信授权返回信息：', $permanentMsg);
            // 判断是否授权
            $getCompanyIdByWorkCorpIdRequest = new GetCompanyIdByCorpIdRequest();
            $getCompanyIdByWorkCorpIdRequest->setCorpId($permanentMsg['auth_corp_info']['corpid']);
            $getCompanyIdByDingTalkCorpIdResponse = WorkWeiXinCompanyCorp::getCompanyIdByCorpId($getCompanyIdByWorkCorpIdRequest);
            $companyId = $getCompanyIdByDingTalkCorpIdResponse->getCompanyId();
            if (!$companyId) {
                $createCompanyRequest = new CreateCompanyByUserRequest();
                $createCompanyRequest->setStatus(config('common.company.status.pass'));
                $corpName = empty($permanentMsg['auth_corp_info']['corp_full_name']) ? $permanentMsg['auth_corp_info']['corp_name'] : $permanentMsg['auth_corp_info']['corp_full_name'];
                $createCompanyRequest->setName($corpName);
                $createCompanyRequest->setLogo($permanentMsg['auth_corp_info']['corp_square_logo_url']);
                $createCompanyRequest->setPoster('');
                $createCompanyResponse = Company::createCompany($createCompanyRequest);
                $companyId = $createCompanyResponse->getId();
            }
            $createCompanyPlatformRequest = new CreateCompanyPlatformRequest();
            $createCompanyPlatformRequest->setCompanyId($companyId);
            $createCompanyPlatformRequest->setCorpId($permanentMsg['auth_corp_info']['corpid']);
            $createCompanyPlatformRequest->setPermanentCode($permanentMsg['permanent_code']);
            $createCompanyPlatformRequest->setAgentId($permanentMsg['auth_info']['agent'][0]['agentid']);
            WorkWeiXinCompanyCorp::createWeiXinCompany($createCompanyPlatformRequest);
        } else {
            $wxCpt = new WXBizMsgCryptLogic(config('common.mpWorkWeChat.token'), $encodingAesKey, config('common.mpWorkWeChat.corpId'));
            $sVerifyMsgSig = $request->input('msg_signature');
            $sVerifyTimeStamp = $request->input('timestamp');
            $sVerifyNonce = $request->input('nonce');
            $sVerifyEchoStr = $request->input('echostr');
            Log::info('微信测试2：', [@$request->getContent()]);
            $sEchoStr = '';
            $errCode = $wxCpt->VerifyURL($sVerifyMsgSig, $sVerifyTimeStamp, $sVerifyNonce, $sVerifyEchoStr, $sEchoStr);
            Log::info('w微信测试3：', [$sEchoStr, $errCode]);
            return $sEchoStr;
        }
        $jsonRequest = new JsonRequest();
        $jsonRequest->setData(['success']);
        return Response::json($jsonRequest);

    }
}
