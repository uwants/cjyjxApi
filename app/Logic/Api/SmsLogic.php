<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Facades\Sms;
use App\Services\Response\Models\SuccessRequest;
use App\Services\Sms\Models\SendCodeByMobileRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class SmsLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     */
    public function sendCodeByMobile(Request $request): HttpResponseException
    {
        $sendCodeByMobileRequest = new SendCodeByMobileRequest();
        $sendCodeByMobileRequest->setMobile($request->input('mobile'));
        $sendCodeByMobileResponse = Sms::sendCodeByMobile($sendCodeByMobileRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'id' => $sendCodeByMobileResponse->getId()
        ]);
        return Response::success($successRequest);
    }
}