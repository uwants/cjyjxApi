<?php


namespace App\Logic\Api;


use App\Events\CompanyApplyCreateEvent;
use App\Exceptions\MessageException;
use App\Facades\Company;
use App\Facades\CompanyAddress;
use App\Facades\CompanyMain;
use App\Facades\PlatformCode;
use App\Facades\PlatformLog;
use App\Facades\Box as boxCreate;
use App\Facades\Response;
use App\Models\Company as CompanyModel;
use App\Models\CompanyApply;
use App\Models\CompanyHot;
use App\Models\CompanySearch;
use App\Models\InternalUser;
use App\Models\User;
use App\Models\CompanyUser;
use App\Models\Box;
use App\Services\Box\Models\CreateBoxRequest;
use App\Services\Company\Models\CreateCompanyByUserRequest;
use App\Services\Company\Models\UpdateCompanyByUserRequest;
use App\Services\CompanyAddress\Models\UpdateOrCreateCompanyAddressRequest;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use App\Services\PlatformLog\Models\CreateLogRequest;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class CompanyLogic
{
    /**
     * @return HttpResponseException
     * 企业详情
     */
    public function show(): HttpResponseException
    {
        //主体企业
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $model = [];
        if (!empty($companyId)) {
            $model = CompanyModel::query()
                ->findOrFail($companyId);
        }
        $data = [
            'companyName' => empty($model->name) ? '无企业组织' : $model->getAttribute('name'),
            'companyLogo' => empty($model->logo) ? config('common.default_company_logo') : $model->getAttribute('logo'),
            'companyProfile' => empty($model->profile) ? config('common.default_company_profile') : $model->getAttribute('profile'),
            'companyPoster' => empty($model->poster) ? config('common.default_company_poster') : $model->getAttribute('poster'),
            'companyStatus' => empty($model->status) ? config('common.company.status.default') : $model->getAttribute('status'),
            'companyId' => empty($model->id) ? 0 : $model->getAttribute('id'),
            'companyIndustry' => empty($model->industry) ? '' : $model->getAttribute('industry'),
            'companyAddressStr' => empty($model->addressStr) ? '' : $model->addressStr,
            'companyAddress' => [
                "province" => empty($model->province) ? '' : $model->province,
                "city" => empty($model->city) ? '' : $model->city,
                "area" => empty($model->area) ? '' : $model->area,
                "info" => empty($model->address) ? '' : $model->address,
            ]
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function infosComplete(): HttpResponseException
    {
        //主体企业
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $model = CompanyModel::query()
            ->where('id', $companyId)
            ->first();
        $isComplete = true;
        if (empty($model->getAttribute('industry'))) {
            $isComplete = false;
        }
        if (empty($model->getAttribute('province')) || empty($model->getAttribute('city')) || empty($model->getAttribute('area'))) {
            $isComplete = false;
        }
        $data = [
            'isComplete' => $isComplete
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);

    }

    public function hotIndex(): HttpResponseException
    {
        $CompanyHotIds = CompanyHot::query()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }])
            ->orderBy('created_at', 'desc')
            ->get();
        $data = [];
        foreach ($CompanyHotIds as $item) {
            $data [] = [
                'companyId' => $item->company_id,
                'companyName' => empty($item->company->name) ? '' : $item->company->name,
                'companyLogo' => empty($item->company->logo) ? config('common.default_company_logo') : $item->company->logo,
                'companyStatus' => empty($item->company->status) ? config('common.company.status.default') : $item->company->status,
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function commentRank(): HttpResponseException
    {

        $seven = date("Y-m-d 00:00:00", strtotime("-7 day"));
        $today = date("Y-m-d 23:59:59", strtotime("-1 day"));
        $showTime = [date("m/d", strtotime("-7 day")), date("m/d", strtotime("-1 day"))];
        //企业收到的意见
        $testUserIds = InternalUser::query()
            ->pluck('user_id')
            ->toArray();
        $testCompanyIds = CompanyUser::query()
            ->whereIn('user_id', $testUserIds)
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->pluck('company_id')
            ->toArray();
        $model = CompanyModel::query()
            ->select('id', 'name',
                DB::raw("(select count(id) from `opinions` where `box_id` in (select `id` from `boxes` where boxes.company_id = companies.id) and opinions.created_at between '" . $seven . "' and '" . $today . "' and `opinions`.`deleted_at` is null ) as number"))
            ->whereNotIn('id', $testCompanyIds)
            ->orderBy('number', 'desc')
            ->limit(10)
            ->get();
        $list = [];
        foreach ($model as $item) {
            $list[] = [
                'companyId' => $item->id,
                'companyName' => $item->name,
                'companyCommentNumber' => $item->number
            ];
        }
        $data = compact('list', 'showTime');
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    /**
     * @return HttpResponseException
     * 企业列表
     */
    public function index(): HttpResponseException
    {
        $model = CompanyModel::query()
            ->select('id', 'name', 'logo', 'status')
            ->where('user_id', Auth::id())
            ->orWhereHas('company_users', function (Builder $query) {
                $query->where('user_id', Auth::id());
            })
            ->orderBy('id', 'desc')
            ->get();
        $list = [];
        foreach ($model as $item) {
            $list[] = [
                'companyId' => !empty($item->id) ? $item->id : 0,
                'companyName' => !empty($item->name) ? $item->name : "",
                'companyLogo' => !empty($item->logo) ? $item->logo : config('common.default_company_logo'),
                'companyStatus' => !empty($item->status) ? $item->status : config('common.company.status.default'),
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($list);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 创建企业
     */
    public function create(Request $request): HttpResponseException
    {
        $createCompanyRequest = new CreateCompanyByUserRequest();
        $createCompanyRequest->setUserId(Auth::id());
        $createCompanyRequest->setName($request->input('companyName'));
        $createCompanyRequest->setLogo($request->input('companyLogo'));
        $createCompanyRequest->setPoster($request->input('companyPoster'));
        $createCompanyRequest->setStatus(CompanyModel::STATUS_CERTIFIED_DEFAULT);
        $createCompanyRequest->setProfile($request->input('companyProfile'));
        $createCompanyRequest->setIndustry($request->input('companyIndustry'));
        $createCompanyResponse = Company::createCompany($createCompanyRequest);
        $companyId = $createCompanyResponse->getId();

        $user = User::withTrashed()
            ->findOrFail(Auth::id());
        $companyUser = CompanyUser::withTrashed()
            ->firstOrCreate([
                'user_id' => Auth::id(),
                'company_id' => $companyId
            ], [
                'name' => $user->getOriginal('name') ?: substr($user->getAttribute('mobile'), -4, 4),
                'avatar' => $user->getOriginal('avatar') ?: config('common.default_avatar'),
                'role' => config('common.companyUser.role.supperAdministrator'),
            ]);
        if ($companyUser->trashed()) {
            $companyUser->restore();
        }
        if ($request->input('companyAddress')) {
            $updateOrCreateCompanyAddress = new UpdateOrCreateCompanyAddressRequest();
            $updateOrCreateCompanyAddress->setCompanyAddressCompanyId($companyId);
            $updateOrCreateCompanyAddress->setCompanyAddressProvince($request->input('companyAddress.province'));
            $updateOrCreateCompanyAddress->setCompanyAddressCity($request->input('companyAddress.city'));
            $updateOrCreateCompanyAddress->setCompanyAddressArea($request->input('companyAddress.area'));
            if ($request->input('companyAddress.info')) {
                $updateOrCreateCompanyAddress->setCompanyAddressInfo($request->input('companyAddress.info'));
            }
            CompanyAddress::updateOrCreateCompanyAddress($updateOrCreateCompanyAddress);
        }
        //更新主企业信息
        $updateOrCreateCompanyMainRequest = new UpdateOrCreateCompanyMainRequest();
        $updateOrCreateCompanyMainRequest->setUserId(Auth::id());
        $updateOrCreateCompanyMainRequest->setCompanyId($companyId);
        CompanyMain::updateOrCreateCompanyMain($updateOrCreateCompanyMainRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 市场创建企业和意见箱
     */
    public function marketCreate(Request $request): HttpResponseException
    {
        $createCompanyRequest = new CreateCompanyByUserRequest();
        $createCompanyRequest->setUserId(Auth::id());
        $createCompanyRequest->setName($request->input('companyName'));
        $createCompanyRequest->setLogo(config('common.default_company_logo'));
        $createCompanyRequest->setPoster(config('common.default_company_poster'));
        $createCompanyRequest->setStatus(CompanyModel::STATUS_CERTIFIED_DEFAULT);
        $createCompanyRequest->setProfile(config('common.default_company_profile'));
        $createCompanyRequest->setIndustry('个人组织');
        $createCompanyResponse = Company::createCompany($createCompanyRequest);
        $companyId = $createCompanyResponse->getId();
        //企业地址
        $updateOrCreateCompanyAddress = new UpdateOrCreateCompanyAddressRequest();
        $updateOrCreateCompanyAddress->setCompanyAddressCompanyId($companyId);
        $updateOrCreateCompanyAddress->setCompanyAddressProvince($request->input('companyAddress.province'));
        $updateOrCreateCompanyAddress->setCompanyAddressCity($request->input('companyAddress.city'));
        $updateOrCreateCompanyAddress->setCompanyAddressArea($request->input('companyAddress.area'));
        if ($request->input('companyAddress.info')) {
            $updateOrCreateCompanyAddress->setCompanyAddressInfo($request->input('companyAddress.info'));
        }
        CompanyAddress::updateOrCreateCompanyAddress($updateOrCreateCompanyAddress);
        //企业超级管理员
        $user = User::withTrashed()
            ->findOrFail(Auth::id());
        $companyUser = CompanyUser::withTrashed()
            ->firstOrCreate([
                'user_id' => Auth::id(),
                'company_id' => $companyId
            ], [
                'name' => $user->getOriginal('name') ?: substr($user->getAttribute('mobile'), -4, 4),
                'avatar' => $user->getOriginal('avatar') ?: config('common.default_avatar'),
                'role' => config('common.companyUser.role.supperAdministrator'),
            ]);
        if ($companyUser->trashed()) {
            $companyUser->restore();
        }
        //创建意见箱
        $createBoxRequest = new CreateBoxRequest();
        $createBoxRequest->setBoxUserId(Auth::id());
        $createBoxRequest->setBoxCompanyId($companyId);
        $createBoxRequest->setBoxName($request->input('boxName'));
        $content = [];
        Arr::set($content, 'content', (string)'匿名收集意见，欢迎大家踊跃参与');
        $createBoxRequest->setContent($content);
        $createBoxRequest->setBoxOpen(true);
        $createBoxRequest->setBoxAll(false);
        $createBoxRequest->setBoxTransfer(true);
        $transferContent = [];
        Arr::set($transferContent, 'createdName', $user->getOriginal('name') ?: substr($user->getAttribute('mobile'), -4, 4));
        $createBoxRequest->setTransferContent($transferContent);
        $response = boxCreate::createBox($createBoxRequest);
        $boxId = $response->getBoxId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxId', 'companyId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 更新企业
     */
    public function update(Request $request): HttpResponseException
    {
        $updateCompanyRequest = new UpdateCompanyByUserRequest();
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $updateCompanyRequest->setCompanyId($companyId);
        if ($request->input('companyName')) {
            $updateCompanyRequest->setCompanyName($request->input('companyName'));
        }
        if ($request->input('companyProfile')) {
            $updateCompanyRequest->setCompanyProfile($request->input('companyProfile'));
        }
        if ($request->input('companyPoster')) {
            $updateCompanyRequest->setCompanyPoster($request->input('companyPoster'));
        }
        if ($request->input('companyLogo')) {
            CompanyModel::query()
                ->where('id', $companyId)
                ->update(['wx_poster' => '']);
            $updateCompanyRequest->setCompanyLogo($request->input('companyLogo'));
        }
        if ($request->input('companyUserId')) {
            $updateCompanyRequest->setCompanyUserId($request->input('companyUserId'));
        }
        if ($request->input('companyIndustry')) {
            $updateCompanyRequest->setCompanyIndustry($request->input('companyIndustry'));
        }
        if ($request->input('companyAddress')) {
            $updateOrCreateCompanyAddress = new UpdateOrCreateCompanyAddressRequest();
            $updateOrCreateCompanyAddress->setCompanyAddressCompanyId($companyId);
            $updateOrCreateCompanyAddress->setCompanyAddressProvince($request->input('companyAddress.province'));
            $updateOrCreateCompanyAddress->setCompanyAddressCity($request->input('companyAddress.city'));
            $updateOrCreateCompanyAddress->setCompanyAddressArea($request->input('companyAddress.area'));
            if ($request->input('companyAddress.info')) {
                $updateOrCreateCompanyAddress->setCompanyAddressInfo($request->input('companyAddress.info'));
            }
            CompanyAddress::updateOrCreateCompanyAddress($updateOrCreateCompanyAddress);
        }
        $updateCompanyResponse = Company::updateCompany($updateCompanyRequest);
        $companyId = $updateCompanyResponse->getCompanyId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyId'));
        return Response::success($successRequest);
    }

    /**
     * 是否存在企业
     * @return HttpResponseException
     */
    public function exist(): HttpResponseException
    {
        $isExist = CompanyUser::query()
            ->where('user_id', Auth::id())
            ->exists();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('isExist'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 企业申请认证
     */
    public function apply(Request $request): HttpResponseException
    {
        if (!empty($request->input('companyId'))) {
            $companyId = $request->input('companyId');
        } else {
            $getCompanyIdRequest = new GetCompanyIdRequest();
            $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
            $companyId = $getCompanyIdResponse->getCompanyId();
        }
        $companyMsg = CompanyModel::query()
            ->where('id', $companyId)
            ->first();
        if ($companyMsg->status == 'STATUS_CERTIFIED_PASS') {
            throw new MessageException('改企业组织已经被认证。');
        }
        $model = new CompanyApply();
        $model->fill([
            'id_card_face' => $request->input('companyApplyIdCardFace'),
            'id_card_positive' => $request->input('companyApplyIdCardPositive'),
            'business' => $request->input('companyApplyBusiness'),
            'company_id' => $companyId,
            'user_id' => Auth::id()
        ]);
        $model->save();
        $companyApplyId = $model->getKey();
        $company = CompanyModel::query()
            ->findOrFail($companyId);
        $company->fill([
            'status' => CompanyModel::STATUS_CERTIFIED_AUDIT
        ]);
        $company->save();
        Log::info('$runMsg：', [$model]);
        event(new CompanyApplyCreateEvent($model));
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyApplyId'));
        return Response::success($successRequest);
    }

    /**
     * @return HttpResponseException
     * 企业认证状态
     */
    public function applyStatus(): HttpResponseException
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $model = CompanyModel::query()
            ->findOrFail($companyId);
        $companyStatus = $model->getOriginal('status');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyStatus'));
        return Response::success($successRequest);
    }

    /**
     * @return HttpResponseException
     * 认证失败原因
     * @throws MessageException
     */
    public function applyReason(): HttpResponseException
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $model = CompanyApply::withTrashed()
            ->where('company_id', $companyId)
            ->latest()
            ->first();
        if (empty($model)) {
            throw new MessageException('获取拒绝原因异常');
        }
        $data = [
            'companyApplyRejectReason' => $model->getAttribute('reject_reason')
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    /**
     * @return HttpResponseException
     * 获取默认主企业
     */
    public function mainCompany(): HttpResponseException
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $mainCompanyId = $getCompanyIdResponse->getCompanyId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('mainCompanyId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 默认主企业更新
     */
    public function mainCompanyUpdate(Request $request): HttpResponseException
    {
        $getCompanyIdRequest = new UpdateOrCreateCompanyMainRequest();
        $getCompanyIdRequest->setUserId(Auth::id());
        $getCompanyIdRequest->setCompanyId($request->input('mainCompanyId'));
        $getCompanyIdResponse = CompanyMain::updateOrCreateCompanyMain($getCompanyIdRequest);
        $mainCompanyId = $getCompanyIdResponse->getId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('mainCompanyId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 搜索企业
     */
    public function searchList(Request $request): HttpResponseException
    {
        //意见箱
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'company' => $this->searchCompany($request),
            'boxes' => $this->searchCompanyBox($request)
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 天眼查企业数据，可分页
     */
    public function getSearchCompany(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            $this->searchCompany($request),
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 关键字查询企业意见箱
     */
    public function getSearchCompanyBox(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            $this->searchCompanyBox($request),
        ]);
        return Response::success($successRequest);
    }


    public function searchCompany(Request $request)
    {
        $param = [
            'keyword' => $request->input('keyword'),
            'page' => $request->input('page'),
            'pageSize' => $request->input('pageSize')
        ];
        $searches = CompanySearch::withTrashed()
            ->whereJsonContains('request', $param)
            ->where('updated_at', '>=', date('Y-m-d 00:00:00', time()))
            ->first();
        $list = [];
        if (!$searches) {
            $tycUrl = config('common.tianYanCha.hostUrl');
            $headerA = config('common.tianYanCha.token');
            $http = Http::withHeaders([
                'authorization' => $headerA,
            ])
                ->get($tycUrl, [
                    'word' => $request->input('keyword'),
                    'pageSize' => $request->input('pageSize'),
                    'pageNum' => $request->input('page')
                ]);
            $response = $http->json();
            $returnData = empty($response['result']['items']) ? [] : $response['result']['items'];
            $response['result']['total'] = empty($response['result']['total']) ? 0 : $response['result']['total'];
            foreach ($returnData as $company) {
                $list[] = [
                    'companyName' => strip_tags(Arr::get($company, 'name')),
                    'tianYanChaId' => Arr::get($company, 'id')
                ];
            }
        } else {
            $data = Arr::get($searches, 'response');
            $list = Arr::get($data, 'list');
            $response['result']['total'] = Arr::get($data, 'pageInfo.totalNumber');
        }
        $names = Arr::pluck($list, 'companyName');
        $company = CompanyModel::withTrashed()
            ->select('id', 'name', 'user_id', 'logo')
            ->with('company_apply')
            ->where('status', 'STATUS_CERTIFIED_PASS')
            ->whereIn('name', $names)
            ->get();
        $companyIds = [];
        $companyLogos = [];
        $companyUserIds = [];
        $companyApply = [];
        foreach ($company as $item) {
            $name = $item->name;
            $companyIds[$name] = $item->id;
            $companyLogos[$name] = $item->logo;
            $companyUserIds[$name] = $item->user_id;
            $companyApply[$name] = $item->company_apply;
        }
        foreach ($list as &$value) {
            $certifiedStatus = 0;
            $name = Arr::get($value, 'companyName');
            if ((bool)Arr::get($companyUserIds, $name)) {
                $certifiedStatus = 2;
            } elseif ((bool)Arr::get($companyApply, $name)) {
                $certifiedStatus = 1;
            }
            $value['id'] = (int)Arr::get($companyIds, $name);
            $value['companyLogo'] = Arr::get($companyLogos, $name) ?: config('common.default_company_logo');
            $value['isCertified'] = (bool)Arr::get($companyUserIds, $name);
            $value['isApply'] = (bool)Arr::get($companyApply, $name);
            $value['certifiedStatus'] = $certifiedStatus;
        }
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $pageRequest->setTotalPage(ceil($response['result']['total'] / $request->input('pageSize')));
        $pageRequest->setTotalNumber($response['result']['total']);
        $pageRequest->setList($list);
        $pageRequest->execute();
        $model = CompanySearch::withTrashed()
            ->whereJsonContains('request', $param)
            ->first();
        if (empty($model)) {
            $model = new CompanySearch();
        } elseif ($model->trashed()) {
            $model->restore();
        }
        $model->fill([
            'request' => $param,
            'response' => $pageRequest->getData(),
        ]);
        $model->save();
        return $pageRequest->getData();
    }

    public function searchCompanyBox(Request $request)
    {
        $userId = Auth::id();
        $model = Box::query()
            ->select('name', 'id', 'content', 'created_at', 'company_id', 'open')
            ->with('company')
            ->with(['boxes_follows' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }])
            ->where('open', true)
            ->where('name', 'like', '%' . $request->input('keyword') . '%');
        $model->orderBy('created_at', 'desc');
        $commentList = $model->paginate($request->input('pageSize'));
        $items = $commentList->items();
        $boxList = [];
        foreach ($items as $key => $val) {
            $boxList[$key]['boxId'] = $val->id;
            $boxList[$key]['boxName'] = $val->name;
            $boxList[$key]['boxContent'] = empty($val->content) ? '' : $val->content;
            $boxList[$key]['boxCreatedTime'] = date('Y/m/d', strtotime($val->created_at));
            $boxList[$key]['boxCreateUserName'] = empty($val->company->name) ? '推荐引擎' : $val->company->name;
            $boxList[$key]['boxIsFollow'] = count($val->boxes_follows) > 0;
            $boxList[$key]['isOwner'] = $val->user_id == Auth::id();
            $boxList[$key]['boxIsOwner'] = $val->user_id == Auth::id();
            $boxList[$key]['boxIsOpen'] = !empty($val->open);
        }
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $pageRequest->setTotalPage($commentList->lastPage());
        $pageRequest->setTotalNumber($commentList->total());
        $pageRequest->setList($boxList);
        $pageRequest->execute();
        return $pageRequest->getData();
    }

    public function createCompany(Request $request): HttpResponseException
    {
        $companyName = @$request->input('companyName');
        if ($request->input('companyId') && (int)$request->input('companyId') > 0) {
            $company = CompanyModel::withTrashed()
                ->where('id', $request->input('companyId'))
                ->first();
            if (!empty($company->deleted_at)) {
                $company = CompanyModel::query()
                    ->where('name', $company->name)
                    ->first();
            }
        } else {
            $company = CompanyModel::query()
                ->where('name', $companyName)
                ->where('status', 'STATUS_CERTIFIED_PASS')
                ->first();
            Log::info('company', [$companyName, $company]);
        }
        if (empty($company->name)) {//天眼查获取企业信息
            $tycUrl = config('common.tianYanCha.hostMsgUrl');
            $http = Http::withHeaders([
                'authorization' => config('common.tianYanCha.token'),
            ])
                ->get($tycUrl, [
                    'keyword' => $companyName,
                ]);
            $response = $http->json();
            Log::info('企业信息', [$response]);
            $model = CompanyModel::withTrashed()
                ->updateOrCreate([
                    'name' => $response['result']['name'],
                ], [
                    'logo' => config('common.default_company_logo'),
                    'poster' => config('common.default_company_poster'),
                    'profile' => $response['result']['businessScope'],
                    'status' => 'STATUS_CERTIFIED_PRE_COMMIT',
                    'industry' => empty($response['result']['industry']) ? '其他' : $response['result']['industry']
                ]);
            if ($model->trashed()) {
                $model->restore();
            }
            $companyId = $model->getKey();
            //企业地址
            $updateOrCreateCompanyAddress = new UpdateOrCreateCompanyAddressRequest();
            $updateOrCreateCompanyAddress->setCompanyAddressCompanyId($companyId);
            $province = empty(config("common.provinceA." . $response['result']['base'])) ? '直辖市' : config("common.provinceA." . $response['result']['base']);
            $updateOrCreateCompanyAddress->setCompanyAddressProvince($province);
            $updateOrCreateCompanyAddress->setCompanyAddressCity($response['result']['city']);
            $updateOrCreateCompanyAddress->setCompanyAddressArea($response['result']['district']);
            CompanyAddress::updateOrCreateCompanyAddress($updateOrCreateCompanyAddress);
            $companyLogo = config('common.default_company_logo');
            $companyPoster = config('common.default_company_poster');
            $companyProfile = $response['result']['alias'];
            $companyName = $response['result']['name'];
            $companyStatus = 'STATUS_CERTIFIED_PRE_COMMIT';
            $companyIndustry = $response['result']['industry'];
            $companyAddress = array(
                'province' => config("common.provinceA." . $response['result']['base']),
                'city' => $response['result']['city'],
                'area' => $response['result']['district'],
                'info' => ''
            );
            //创建企业平台记录
            $createLogRequest = new CreateLogRequest();
            $createLogRequest->setCompanyId($companyId);
            $createLogRequest->setPlatform(PlatformCode::getPlatformCode());
            PlatformLog::createCompanyPlatformLog($createLogRequest);
            $box = Box::withTrashed()
                ->updateOrCreate([
                    'company_id' => $companyId,
                    'name' => $response['result']['name'],
                    'system' => true,
                ], [
                    'open' => true,
                    'content' => []
                ]);
            if ($box->trashed()) {
                $box->restore();
            }
        } else {
            $companyId = $company->id;
            $companyLogo = $company->logo;
            $companyPoster = $company->poster;
            $companyProfile = $company->profile;
            $companyName = $company->name;
            $companyStatus = $company->status;
            $companyIndustry = $company->industry;
            if (empty($company->getAttribute('province')) || empty($company->getAttribute('city')) || empty($company->getAttribute('area'))) {
                $companyAddress = [];
            } else {
                $companyAddress = array(
                    'province' => $company->province,
                    'city' => $company->city,
                    'area' => $company->area,
                    'info' => $company->address
                );
            }
        }
        $companyMsg = [
            'companyId' => $companyId,
            'companyLogo' => $companyLogo,
            'companyPoster' => $companyPoster,
            'companyProfile' => $companyProfile,
            'companyName' => $companyName,
            'companyStatus' => $companyStatus,
            'companyIndustry' => $companyIndustry,
            'companyAddress' => $companyAddress
        ];
        //意见箱
        $boxList = [];
        $boxModel = Box::query()
            ->select('name', 'id', 'system', 'company_id', 'open', 'content', 'created_at')
            ->where('company_id', $companyId)
            ->where('system', true)
            ->orWhereIn('id', function ($query) use ($companyId) {
                $query->from('boxes')->select('id')
                    ->where('company_id', $companyId)
                    ->where('open', true);
            })
            ->orderBy('system', 'desc')
            ->get();
        foreach ($boxModel as $key => $val) {
            $boxList[$key]['boxName'] = $val->name;
            $boxList[$key]['boxId'] = $val->id;
            $boxList[$key]['boxIsOpen'] = !empty($val->open);
            $boxList[$key]['boxCompanyId'] = $val->company_id;
            $boxList[$key]['boxContent'] = $val->content;
            $boxList[$key]['createdTime'] = $val->getCreatedTimeAttribute('created_at');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'company' => $companyMsg,
            'boxes' => $boxList
        ]);
        return Response::success($successRequest);

    }

}
