<?php

namespace App\Logic\Api;

use App\Events\CommentCreateEvent;
use App\Events\MessageEvent;
use App\Exceptions\MessageException;
use App\Facades\Response;
use App\Events\CommentAdoptEvent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\Box;
use App\Models\Comment;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use App\Models\Comment as CommentModel;

class CommentLogic
{
    public function index(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function total(): HttpResponseException
    {
        $commentTotal = CommentModel::query()
            ->count('id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('commentTotal'));
        return Response::success($successRequest);
    }

    public function next(Request $request): HttpResponseException
    {
        $boxId = $request->input('boxId');
        $commentId = $request->input('commentId');
        $model = CommentModel::query()
            ->where('box_id', $boxId)
            ->where('id', '<', $commentId)
            ->whereDoesntHave('reply')
            ->orderBy('created_at', 'desc');
        $count = CommentModel::query()
            ->where('box_id', $boxId)
            ->whereDoesntHave('reply')
            ->count();
        $commentIsNext = false;
        if ($count > 1) {
            $commentIsNext = true;
        }
        $model = $model->first();
        if (empty($model)) {
            $model = CommentModel::query()
                ->where('box_id', $boxId)
                ->when($commentIsNext, function (Builder $query) {
                    $query->whereDoesntHave('reply');
                })
                ->latest()
                ->first();
        }
        $boxComment = [];
        $boxCommentReplies = [];
        if (!empty($model)) {
            $boxComment = [
                'commentUserName' => $model->user->AliasName,
                'commentIsOwner' => $model->user_id == Auth::id(),
                'commentAvatar' => $model->user->AliasAvatar,
                'commentUserId' => !empty($model->user_id) ? $model->user_id : '',
                'commentId' => !empty($model->id) ? $model->id : '',
                'commentContent' => !empty($model->content) ? $model->content : '',
                'commentCreatedTime' => !empty($model->CreatedTime) ? $model->CreatedTime : '',
                'commentIsAdopt' => (bool)$model->adopt,
                'commentIsNext' => $commentIsNext
            ];
            $boxCommentReplyModel = !empty($model->replies) ? $model->replies->first() : '';
            if (!empty($boxCommentReplyModel)) {
                $boxCommentReplies = [
                    'replyUserName' => $boxCommentReplyModel->user_id == Auth::id() ? '我' : '意见管理员',
                    'replyIsOwner' => $boxCommentReplyModel->user_id == Auth::id(),
                    'replyAvatar' => config('image.default_avatar'),
                    'replyId' => !empty($boxCommentReplyModel->id) ? $boxCommentReplyModel->id : '',
                    'replyContent' => !empty($boxCommentReplyModel->content) ? $boxCommentReplyModel->content : '',
                    'replyUserId' => !empty($boxCommentReplyModel->user_id) ? $boxCommentReplyModel->user_id : '',
                    'replyCreatedTime' => !empty($boxCommentReplyModel->CreatedTime) ? $boxCommentReplyModel->CreatedTime : '',
                ];
            }
        }
        $data = [];
        Arr::set($data, 'boxComment', $boxComment);
        Arr::set($data, 'boxCommentReplies', $boxCommentReplies);
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function store(Request $request): HttpResponseException
    {
        $content = [];
        if ($request->has('commentContent.images')) {
            Arr::set($content, 'images', (array)$request->input('commentContent.images'));
        }
        if ($request->has('commentContent.videos')) {
            Arr::set($content, 'videos', (array)$request->input('commentContent.videos'));
        }
        if ($request->has('commentContent.content')) {
            Arr::set($content, 'content', (string)$request->input('commentContent.content'));
        }
        $box = Box::query()
            ->where('id', $request->input('commentBoxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        $isOnce = $box->getAttribute('once');
        if (!empty($isOnce)) {
            $exists = Comment::query()
                ->where('box_id', $box->id)
                ->where('user_id', Auth::id())
                ->exists();
            if ($exists) {
                throw new MessageException('意见箱只能提一次意见！');
            }
        }
        $boxModel = Box::query()
            ->where('id',$request->input('commentBoxId'))
            ->first();
        $model = new Comment();
        $model->fill([
            'user_id' => Auth::id(),
            'box_id' => $request->input('commentBoxId'),
            'content' => $content,
            'company_id'=>$boxModel->getAttribute('company_id')
        ]);
        $model->save();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'commentId' => $model->getKey(),
            'commentContent' => $content,
            'commentCreatedTime' => date('Y-m-d', time())
        ]);
        event(new CommentCreateEvent($model));
        if(!empty($box->system)){
            event(new MessageEvent('USER-COMPANY-BOX',$box->company_id,$box->id,$box->user_id,''));
        }else{
            event(new MessageEvent('USER-BOX',$box->company_id,$box->id,$box->user_id,''));
        }
        return Response::success($successRequest);
    }

    public function adopt(Request $request): HttpResponseException
    {
        $model = Comment::query()
            ->scopes(['boxView' => (int)Auth::id()])
            ->findOrFail($request->input('commentId'));
        $model->fill([
            'adopt' => true
        ]);
        $model->save();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'commentId' => $request->input('commentId'),
        ]);
        event(new CommentAdoptEvent($model));
        event(new MessageEvent('COMMENT-ADOPT', 0, $model->box_id, $model->user_id, ''));
        return Response::success($successRequest);
    }

    public function cancelAdopt(Request $request): HttpResponseException
    {
        $model = Comment::query()
            ->scopes(['boxView' => (int)Auth::id()])
            ->findOrFail($request->input('commentId'));
        $model->fill([
            'adopt' => false
        ]);
        $model->save();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'commentId' => $request->input('commentId'),
        ]);
        return Response::success($successRequest);
    }


    public function show(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    public function destroy(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }
}
