<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\Faq;
use App\Services\Response\Models\PageRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class FaqLogic
{
    public function index(Request $request): HttpResponseException
    {
        $page = $request->input('page');
        $pageSize = $request->input('pageSize');
        $pageRequest = new PageRequest();
        $pageRequest->setPage($page);
        $pageRequest->setPageSize($pageSize);
        $model = Faq::query()
            ->select('id', 'question', 'answer', 'created_at')
            ->where('suspend', false)
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($pageRequest->getPageSize(), ['*'], 'page', $pageRequest->getPage());
        $items = $model->items();
        $pageRequest->setTotalPage($model->lastPage());
        $pageRequest->setTotalNumber($model->total());
        $list = [];
        foreach ($items as $item) {
            $list[] = [
                'id' => $item->id,
                'question' => $item->question,
                'answer' => $item->answer,
                'created_at' => $item->created_at,
            ];
        }
        $pageRequest->setList($list);
        $pageRequest->execute();
        return Response::page($pageRequest);
    }
}