<?php

namespace App\Logic\Api;

use App\Events\MessageEvent;
use App\Facades\Company;
use App\Facades\CompanyMain;
use App\Facades\CompanyUser;
use App\Facades\DingTalkApi;
use App\Facades\DingTalkUser;
use App\Facades\PlatformCode;
use App\Facades\PlatformLog;
use App\Facades\Response;
use App\Facades\Sms;
use App\Facades\User;
use App\Facades\WeiXinApi;
use App\Facades\WeiXinUser;
use App\Facades\WorkWeiXinCompanyCorp;
use App\Services\Company\Models\UpdateOrCreateCompanyRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use App\Services\CompanyUser\Models\FirstOrCreateCompanyUserRequest;
use App\Services\DingTalkApi\Models\GetContactUserRequest;
use App\Services\DingTalkApi\Models\GetUserAccessTokenRequest;
use App\Services\DingTalkUser\Models\UpdateOrCreateDingTalkUserRequest;
use App\Services\PlatformLog\Models\CreateLogRequest;
use App\Services\Response\Models\ErrorRequest;
use App\Services\Response\Models\SuccessRequest;
use App\Services\Sms\Models\IsValidMobileCodeRequest;
use App\Services\User\Models\GetAccessTokenByUserIdRequest;
use App\Services\User\Models\FirstOrCreateUserRequest;
use App\Services\WeiXinApi\Models\Code2SessionRequest;
use App\Services\WeiXinApi\Models\GetPhoneNumberRequest;
use App\Services\WeiXinUser\Models\UpdateOrCreateWeiXinUserRequest;
use App\Services\WorkWeiXinCompanyCorp\Models\GetCompanyIdByCorpIdRequest;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\CompanyUser as CompanyUserModel;
use App\Models\User as UserModel;

class AuthLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws Exception
     */
    public function login(Request $request): HttpResponseException
    {
        $userId = 0;
        if (PlatformCode::isMpDingTalk()) {
            $userId = $this->dingTalkLogin($request);
        } elseif (PlatformCode::isH5() || PlatformCode::isPc()) {
            $userId = $this->mobileAndCodeLoginAndRegister($request);
        } elseif (PlatformCode::isMpWeiXin() || PlatformCode::isMpWorkWeiXin()) {
            $userId = $this->weiXinLogin($request);
        }
        if (empty($userId)) {
            $errorRequest = new ErrorRequest();
            $errorRequest->setCode(401);
            $errorRequest->setMessage('not login');
            return Response::error($errorRequest);
        }
        $getAccessTokenByUserIdRequest = new GetAccessTokenByUserIdRequest();
        $getAccessTokenByUserIdRequest->setUserId($userId);
        $getAccessTokenByUserIdResponse = User::getAccessTokenByUserId($getAccessTokenByUserIdRequest);
        $accessToken = $getAccessTokenByUserIdResponse->getAccessToken();
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'accessToken' => $accessToken
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws Exception
     */
    public function register(Request $request): HttpResponseException
    {
        $userId = 0;
        if (PlatformCode::isMpDingTalk()) {
            $userId = $this->dingTalkRegister($request);
        } elseif (PlatformCode::isH5() || PlatformCode::isPc()) {
            $userId = $this->mobileAndCodeLoginAndRegister($request);
        } elseif (PlatformCode::isMpWeiXin() || PlatformCode::isMpWorkWeiXin()) {
            $userId = $this->weiXinRegister($request);
        }
        if (empty($userId)) {
            throw new Exception('注册失败');
        }
        $getAccessTokenByUserIdRequest = new GetAccessTokenByUserIdRequest();
        $getAccessTokenByUserIdRequest->setUserId($userId);
        $getAccessTokenByUserIdResponse = User::getAccessTokenByUserId($getAccessTokenByUserIdRequest);
        $accessToken = $getAccessTokenByUserIdResponse->getAccessToken();
        //注册平台记录
        $createLogRequest = new CreateLogRequest();
        $createLogRequest->setUserId($userId);
        $createLogRequest->setPlatform(PlatformCode::getPlatformCode());
        PlatformLog::createUserPlatformLog($createLogRequest);
        event(new MessageEvent('USER-FIRST', 0, 0, $userId, ''));
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'accessToken' => $accessToken
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    private function dingTalkLogin(Request $request): int
    {
        $authCode = $request->input('dingTalkAuthCode');
        if (empty($authCode)) {
            throw new Exception('缺少参数企业授权码authCode');
        }
        $corpId = $request->input('dingTalkCorpId');
        if (empty($corpId)) {
            throw new Exception('缺少参数企业corpId');
        }
        $updateOrCreateDingTalkUserRequest = new UpdateOrCreateDingTalkUserRequest();
        $updateOrCreateDingTalkUserRequest->setCorpId($corpId);
        $updateOrCreateDingTalkUserRequest->setAuthCode($authCode);
        $updateOrCreateDingTalkUserResponse = DingTalkUser::updateOrCreateDingTalkUser($updateOrCreateDingTalkUserRequest);
        $userId = $updateOrCreateDingTalkUserResponse->getUserId();
        $sysLevel = $updateOrCreateDingTalkUserResponse->getSysLevel();
        $dingTalkUserId = $updateOrCreateDingTalkUserResponse->getDingTalkUserId();
        $this->dingTalk($sysLevel, $corpId, $userId, $dingTalkUserId);
        return $userId;
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    public function mobileAndCodeLoginAndRegister(Request $request): int
    {
        $mobile = $request->input('mobile');
        $code = $request->input('code');
        if (empty($mobile)) {
            throw new Exception('手机号不能为空');
        }
        //验证码判断
        if (empty($code)) {
            throw new Exception('验证不能为空');
        }
        $isValidMobileCodeRequest = new IsValidMobileCodeRequest();
        $isValidMobileCodeRequest->setCode($code);
        $isValidMobileCodeRequest->setMobile($mobile);
        $isValidMobileCodeResponse = Sms::isValidMobileCode($isValidMobileCodeRequest);
        if (!$isValidMobileCodeResponse->isValid()) {
            throw new HttpResponseException(response()->json([
                'data' => [
                    'isExpired' => $isValidMobileCodeResponse->isExpired(),
                    'isCorrect' => $isValidMobileCodeResponse->isCorrect(),
                ],
                'code' => 0,
            ]));
        }
        //区分登陆或注册
        $userM = UserModel::query()
            ->where('mobile', $mobile)
            ->first();
        // 查询或创建用户
        $firstOrCreateUserRequest = new FirstOrCreateUserRequest();
        $firstOrCreateUserRequest->setMobile($mobile);
        $firstOrCreateUserResponse = User::firstOrCreateUser($firstOrCreateUserRequest);
        $userId = $firstOrCreateUserResponse->getId();
        if(empty($userM->id)){
            //注册平台记录
            $createLogRequest = new CreateLogRequest();
            $createLogRequest->setUserId($userId);
            $createLogRequest->setPlatform(PlatformCode::getPlatformCode());
            PlatformLog::createUserPlatformLog($createLogRequest);
        }
        return $userId;
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    public function weiXinLogin(Request $request): int
    {
        $code = $request->input('authCode');
        if (empty($code)) {
            throw new Exception('缺少参数临时码authCode');
        }
        $updateOrCreateWeiXinUserRequest = new UpdateOrCreateWeiXinUserRequest();
        $updateOrCreateWeiXinUserRequest->setAuthCode($code);
        $updateOrCreateWeiXinUserResponse = WeiXinUser::updateOrCreateWeiXinUser($updateOrCreateWeiXinUserRequest);
        $userId = $updateOrCreateWeiXinUserResponse->getUserId();
        if (PlatformCode::isMpWeiXin()) {
            return $userId;
        }
        $corpId = $request->input('corpId');
        return $this->workWeiXin($corpId, $userId);
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    private function dingTalkRegister(Request $request): int
    {
        $authCode = $request->input('dingTalkAuthCode');
        if (empty($authCode)) {
            throw new Exception('缺少参数钉钉授权码authCode');
        }
        $corpId = $request->input('dingTalkCorpId');
        if (empty($corpId)) {
            throw new Exception('缺少参数企业corpId');
        }
        $getUserAccessTokenRequest = new GetUserAccessTokenRequest();
        $getUserAccessTokenRequest->setCode($authCode);
        $getAccessTokenByCorpIdResponse = DingTalkApi::getUserAccessToken($getUserAccessTokenRequest);
        $accessToken = $getAccessTokenByCorpIdResponse->getAccessToken();
        if (empty($accessToken)) {
            throw new Exception('钉钉授权企业accessToken不能为空');
        }
        $getUserInfoRequest = new GetContactUserRequest();
        $getUserInfoRequest->setAccessToken($accessToken);
        $getUserInfoResponse = DingTalkApi::getContactUser($getUserInfoRequest);
        $mobile = $getUserInfoResponse->getMobile();
        // 查询或创建用户
        $firstOrCreateUserRequest = new FirstOrCreateUserRequest();
        $firstOrCreateUserRequest->setMobile($mobile);
        $firstOrCreateUserResponse = User::firstOrCreateUser($firstOrCreateUserRequest);
        $userId = $firstOrCreateUserResponse->getId();
        // 查询或创建企业用户
        $updateOrCreateDingTalkUserRequest = new UpdateOrCreateDingTalkUserRequest();
        $updateOrCreateDingTalkUserRequest->setUserId($userId);
        $updateOrCreateDingTalkUserRequest->setUnionId($getUserInfoResponse->getUnionId());
        $updateOrCreateDingTalkUserRequest->setCorpId($corpId);
        $updateOrCreateDingTalkUserResponse = DingTalkUser::updateOrCreateDingTalkUser($updateOrCreateDingTalkUserRequest);
        $sysLevel = $updateOrCreateDingTalkUserResponse->getSysLevel();
        $dingTalkUserId = $updateOrCreateDingTalkUserResponse->getDingTalkUserId();
        $this->dingTalk($sysLevel, $corpId, $userId, $dingTalkUserId);
        return $userId;
    }

    /**
     * @param Request $request
     * @return int
     * @throws Exception
     */
    public function weiXinRegister(Request $request): int
    {
        $authCode = $request->input('authCode');
        if (empty($authCode)) {
            throw new Exception('缺少参数authCode');
        }
        $code2SessionRequest = new Code2SessionRequest();
        $code2SessionRequest->setCode($authCode);
        $code2SessionResponse = WeiXinApi::code2Session($code2SessionRequest);
        $iv = $request->input('iv');
        if (empty($iv)) {
            throw new Exception('缺少参数iv');
        }
        $encryptedData = $request->input('encryptedData');
        if (empty($encryptedData)) {
            throw new Exception('缺少参数encryptedData');
        }
        $getPhoneNumberRequest = new GetPhoneNumberRequest();
        $getPhoneNumberRequest->setSessionKey($code2SessionResponse->getSessionKey());
        $getPhoneNumberRequest->setIv($iv);
        $getPhoneNumberRequest->setEncryptedData($encryptedData);
        $getPhoneNumberResponse = WeiXinApi::getPhoneNumber($getPhoneNumberRequest);
        $mobile = $getPhoneNumberResponse->getMobile();
        if (empty($mobile)) {
            throw new Exception('获取手机号失败');
        }
        // 查询或创建用户
        $firstOrCreateUserRequest = new FirstOrCreateUserRequest();
        $firstOrCreateUserRequest->setMobile($mobile);
        $firstOrCreateUserResponse = User::firstOrCreateUser($firstOrCreateUserRequest);
        $userId = $firstOrCreateUserResponse->getId();
        // 更新用户id
        $updateOrCreateWeiXinUserRequest = new UpdateOrCreateWeiXinUserRequest();
        $updateOrCreateWeiXinUserRequest->setUnionId($code2SessionResponse->getUnionId());
        $updateOrCreateWeiXinUserRequest->setUserId($userId);
        $updateOrCreateWeiXinUserRequest->setSessionKey($code2SessionResponse->getSessionKey());
        $updateOrCreateWeiXinUserRequest->setOpenId($code2SessionResponse->getOpenId());
        WeiXinUser::updateOrCreateWeiXinUser($updateOrCreateWeiXinUserRequest);
        if (PlatformCode::isMpWeiXin()) {
            return $userId;
        }
        $corpId = $request->input('corpId');
        return $this->workWeiXin($corpId, $userId);
    }

    /**
     * @param int $corpId
     * @param int $userId
     * @return int
     * @throws Exception
     */
    private function workWeiXin(int $corpId, int $userId): int
    {
        if (empty($corpId)) {
            throw new Exception('缺少参数corpId');
        }
        $getCompanyIdByCorpIdRequest = new GetCompanyIdByCorpIdRequest();
        $getCompanyIdByCorpIdRequest->setCorpId($corpId);
        $getCompanyIdByCorpIdResponse = WorkWeiXinCompanyCorp::getCompanyIdByCorpId($getCompanyIdByCorpIdRequest);
        $companyId = $getCompanyIdByCorpIdResponse->getCompanyId();
        if (empty($companyId)) {
            throw new Exception('企业未开通应用');
        }
        return $userId;
    }

    /**
     * @param $sysLevel
     * @param $corpId
     * @param $userId
     * @param $dingTalkUserId
     * @return int
     */
    private function dingTalk($sysLevel, $corpId, $userId, $dingTalkUserId): int
    {
        $updateOrCreateCompanyRequest = new UpdateOrCreateCompanyRequest();
        $updateOrCreateCompanyRequest->setDingTalkCorpId($corpId);
        if (100 == $sysLevel && $userId) {
            $updateOrCreateCompanyRequest->setUserId($userId);
        }
        $updateOrCreateCompanyResponse = Company::updateOrCreateCompany($updateOrCreateCompanyRequest);
        $companyId = $updateOrCreateCompanyResponse->getId();
        if (empty($userId)) {
            return $userId;
        }
        // 查询或创建企业成员
        $firstOrCreateCompanyUserRequest = new FirstOrCreateCompanyUserRequest();
        $firstOrCreateCompanyUserRequest->setCompanyId($companyId);
        $firstOrCreateCompanyUserRequest->setUserId($userId);
        $firstOrCreateCompanyUserRequest->setDingTalkUserId($dingTalkUserId);
        $firstOrCreateCompanyUserRequest->setDingTalkCorpId($corpId);
        //没有超管，进来的第一个管理员为超管。
        if ($sysLevel) {
            $isSupperAdmin = CompanyUserModel::query()
                ->where('company_id', $companyId)
                ->where('role', config('common.companyUser.role.supperAdministrator'))
                ->exists();
            $firstOrCreateCompanyUserRequest->setRole($isSupperAdmin ? config('common.companyUser.role.administrator') : config('common.companyUser.role.supperAdministrator'));
            \App\Models\Company::query()
                ->where('id', $companyId)
                ->first()
                ->update([
                    'user_id' => $userId
                ]);
        } else {
            $firstOrCreateCompanyUserRequest->setRole(config('common.companyUser.role.normal'));
        }
//        if (100 == $sysLevel) {
//            $firstOrCreateCompanyUserRequest->setRole(config('common.companyUser.role.supperAdministrator'));
//        } elseif ($sysLevel) {
//            $firstOrCreateCompanyUserRequest->setRole(config('common.companyUser.role.administrator'));
//        } else {
//            $firstOrCreateCompanyUserRequest->setRole(config('common.companyUser.role.normal'));
//        }
        CompanyUser::firstOrCreateCompanyUser($firstOrCreateCompanyUserRequest);
        // 设置主企业
        $updateOrCreateCompanyMainRequest = new UpdateOrCreateCompanyMainRequest();
        $updateOrCreateCompanyMainRequest->setCompanyId($companyId);
        $updateOrCreateCompanyMainRequest->setUserId($userId);
        CompanyMain::updateOrCreateCompanyMain($updateOrCreateCompanyMainRequest);
        return $userId;
    }
}
