<?php


namespace App\Logic\Api;


use App\Events\MessageEvent;
use App\Exceptions\MessageException;
use App\Facades\CompanyMain;
use App\Facades\Response;
use App\Models\Box;
use App\Models\BoxCommentUser;
use App\Models\BoxUser;
use App\Models\Company as CompanyModel;
use App\Models\CompanyUser;
use App\Models\CompanyUserInvite;
use App\Services\Box\Models\MoveByBoxIdsRequest;
use App\Services\CompanyMain\Models\UpdateOrCreateCompanyMainRequest;
use App\Services\Response\Models\SuccessRequest;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\Response\Models\PageRequest;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Facades\Box as BoxFacade;

class CompanyUserLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     * 公司成员列表
     */
    public function index(Request $request): HttpResponseException
    {
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $model = CompanyUser::query()
            ->select('id', 'name', 'avatar', 'user_id', 'role')
            ->where('company_id', $companyId);
        if ($request->input('keyword')) {
            $keyword = $request->input('keyword');
            $model->where('name', 'like', '%' . $keyword . '%');
        }
        //类型
        if ($request->input('type')) {
            $type = $request->input('type');
            switch ($type) {
                case 'administrator':
                    $model->where('role', '!=', CompanyUser::ROLE_NORMAL);
                    break;
                default:
            }
        }
        if ($request->input('isNotOwner')) {
            $model->where('user_id', '!=', Auth::id());
        }
        $model = $model->orderByDesc(DB::raw('case when role = "' . CompanyUser::ROLE_SUPPER_ADMINISTRATOR . '" then 4 when role = "' . CompanyUser::ROLE_ADMINISTRATOR . '" and user_id=' . Auth::id() . ' then 3 when role = "' . CompanyUser::ROLE_ADMINISTRATOR . '"  then 2 when user_id=' . Auth::id() . ' then 1 else 0 end'))
            ->latest()
            ->paginate($pageRequest->getPageSize(), ['*'], 'page', $pageRequest->getPage());
        $items = $model->items();
        $pageRequest->setTotalPage($model->lastPage());
        $pageRequest->setTotalNumber($model->total());
        $list = [];
        foreach ($items as $item) {
            $list[] = [
                'companyUserId' => $item->id,
                'companyUserName' => $item->name,
                'companyUserAvatar' => $item->avatar,
                'companyUserUserId' => $item->user_id,
                'companyUserRole' => $item->role,
            ];
        }
        $pageRequest->setList($list);
        $pageRequest->execute();
        return Response::page($pageRequest);
    }

    /**
     * @throws Exception
     */
    public function supperAdministratorExchange(Request $request): HttpResponseException
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        if ($companyId == 0) {
            throw new MessageException('没有企业');
        }
        $companyUserId = $request->input('companyUserId');
        $companyUserModel = CompanyUser::query()
            ->where('id', $companyUserId)
            ->first();
        if (!Gate::allows('createSupperAdministrator', $companyUserModel)) {
            throw new MessageException('没有权限');
        }
        if (empty($companyUserModel->user->id)) {
            throw new Exception('找不到转移用户id');
        }
        $userId = $companyUserModel->user->id;
        //转移公司创建者
        $companyModel = CompanyModel::query()
            ->findOrFail($companyId);
        $companyModel->fill([
            'user_id' => $userId,
        ]);
        $companyModel->save();
        //转移超管
        $companyUserModel->fill([
            'role' => CompanyUser::ROLE_SUPPER_ADMINISTRATOR,
        ]);
        $companyUserModel->save();
        //转为管理员
        $companyUserOwner = CompanyUser::query()
            ->where('company_id', $companyId)
            ->where('user_id', Auth::id())
            ->first();
        $companyUserOwner->fill([
            'role' => CompanyUser::ROLE_ADMINISTRATOR,
        ]);
        $companyUserOwner->save();
        //转移成员公司所有意见箱
        $boxIds = Box::query()
            ->select('id')
            ->where('company_id', $companyId)
            ->where('user_id', Auth::id())
            ->pluck('id')
            ->toArray();
        $moveByBoxIdsRequest = new MoveByBoxIdsRequest();
        $moveByBoxIdsRequest->setOldUserId(Auth::id());
        $moveByBoxIdsRequest->setNewUserId($userId);
        $moveByBoxIdsRequest->setBoxIds($boxIds);
        BoxFacade::moveByBoxIds($moveByBoxIdsRequest);
        $companyUserId = $companyUserOwner->getKey();
        //系统消息
        event(new MessageEvent('POWER-MOVE', $companyId, 0, $userId, $companyUserOwner->name));
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact($companyUserId));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 企业用户退出或删除
     * @throws MessageException
     */
    public function destroy(Request $request): HttpResponseException
    {
        $companyUserId = $request->input('companyUserId');
        $companyUserModel = CompanyUser::query()
            ->find($companyUserId);
        if (empty($companyUserModel)) {
            //已删除
            $successRequest = new SuccessRequest();
            return Response::success($successRequest);
        }
        $userId = $companyUserModel->getAttribute('user_id');
        $companyId = $companyUserModel->getAttribute('company_id');
        if (!Gate::allows('delete', $companyUserModel)) {
            throw new MessageException('没有权限');
        }
        $supperAdministratorUserId = 0;
        $supperAdministratorModel = CompanyUser::query()
            ->where('company_id', $companyId)
            ->where('role', CompanyUser::ROLE_SUPPER_ADMINISTRATOR)
            ->first();
        if (!empty($supperAdministratorModel)) {
            $supperAdministratorUserId = $supperAdministratorModel->getAttribute('user_id');
        }

        //转移成员公司所有意见箱超管
        $boxIds = Box::query()
            ->select('id')
            ->where('user_id', $userId)
            ->where('company_id', $companyId)
            ->pluck('id')
            ->toArray();
        $moveByBoxIdsRequest = new MoveByBoxIdsRequest();
        $moveByBoxIdsRequest->setOldUserId($userId);
        $moveByBoxIdsRequest->setNewUserId($supperAdministratorUserId);
        $moveByBoxIdsRequest->setBoxIds($boxIds);
        BoxFacade::moveByBoxIds($moveByBoxIdsRequest);

        //删除助手
        BoxUser::query()
            ->whereIn('box_id', function (Builder $query) use ($companyId) {
                $query->select('id')
                    ->from('boxes')
                    ->where('company_id', $companyId);
            })
            ->where('user_id', $userId)
            ->delete();
        //删除公司意见箱指定成员
        BoxCommentUser::query()
            ->whereIn('box_id', function (Builder $query) use ($companyId) {
                $query->select('id')
                    ->from('boxes')
                    ->where('company_id', $companyId);
            })
            ->where('user_id', $userId)
            ->delete();
        //退出企业
        $companyUserModel->delete();
        //更新主企业
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdRequest->setUserId($companyUserModel->getOriginal('user_id'));
        CompanyMain::getCompanyId($getCompanyIdRequest);
        if ($companyUserModel->getOriginal('user_id') == Auth::id()) {//退出企业
            event(new MessageEvent('USER-OUT', $companyId, 0, $supperAdministratorUserId, Auth::id()));
        } else {//移除成员
            event(new MessageEvent('USER-REMOVE', $companyId, 0, $userId, $companyUserModel->getOriginal('name')));
        }
        $successRequest = new SuccessRequest();
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 企业添加管理员
     * @throws MessageException
     */
    public function storeAdmin(Request $request): HttpResponseException
    {
        $companyUserId = $request->input('companyUserId');
        $model = CompanyUser::query()
            ->select('id', 'role', 'company_id')
            ->findOrFail($companyUserId);
        if (!Gate::allows('administratorCreate', $model)) {
            throw new MessageException('没有权限');
        }
        $model->fill([
            'role' => CompanyUser::ROLE_ADMINISTRATOR,
        ]);
        $model->save();
        $companyUserId = $model->getKey();
        //系统消息
        event(new MessageEvent('POWER-SET', $model->getOriginal('company_id'), 0, $companyUserId, Auth::id()));
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyUserId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 企业删除管理员
     * @throws MessageException
     */
    public function destroyAdmin(Request $request): HttpResponseException
    {
        $companyUserId = $request->input('companyUserId');
        $model = CompanyUser::query()
            ->where('role', config('common.companyUser.role.administrator'))
            ->find($companyUserId);
        if (empty($model)) {
            //已删除
            $successRequest = new SuccessRequest();
            return Response::success($successRequest);
        }
        $companyId = $model->getOriginal('company_id');
        if (!Gate::allows('administratorDelete', $model)) {
            throw new MessageException('没有权限');
        }
        $model->fill([
            'role' => CompanyUser::ROLE_NORMAL,
        ]);
        // 将管理员的所有公开意见箱转让给超管
        $supperAdministrator = CompanyUser::query()
            ->where('company_id', $companyId)
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->first();
        $openBoxIds = Box::query()
            ->where('company_id', $companyId)
            ->where('open', true)
            ->pluck('id')
            ->toArray();
        $boxIds = Box::query()
            ->where('open', true)
            ->where('company_id', $companyId)
            ->where('user_id', $model->getOriginal('user_id'))
            ->pluck('id')
            ->toArray();
        $moveByBoxIdsRequest = new MoveByBoxIdsRequest();
        $moveByBoxIdsRequest->setOldUserId($model->getOriginal('user_id'));
        $moveByBoxIdsRequest->setNewUserId($supperAdministrator->getOriginal('user_id'));
        $moveByBoxIdsRequest->setBoxIds($boxIds);
        BoxFacade::moveByBoxIds($moveByBoxIdsRequest);
        BoxUser::query()
            ->where('user_id', $model->getOriginal('user_id'))
            ->whereIn('box_id', $openBoxIds)
            ->delete();
        $model->save();
        $companyUserId = $model->getKey();
        if (Auth::id() == $model->getOriginal('user_id')) {
            //卸任
            event(new MessageEvent('POWER-DOWN-ALL', $companyId, 0, $supperAdministrator->getOriginal('user_id'), Auth::id()));
        } else {
            //系统消息(移除管理眼)
            event(new MessageEvent('POWER-DOWN', $companyId, 0, $companyUserId, Auth::id()));
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyUserId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * 企业成员名称修改
     */
    public function updateName(Request $request): HttpResponseException
    {
        $companyUserId = $request->input('companyUserId');
        $model = CompanyUser::query()
            ->select('id', 'name', 'company_id')
            ->where('id', $companyUserId)
            ->first();
        $model->fill([
            'name' => $request->input('companyUserName')
        ]);
        $model->save();
        $companyUserId = $model->getKey();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyUserId'));
        return Response::success($successRequest);
    }

    /**
     * @throws MessageException
     */
    public function store(Request $request): HttpResponseException
    {
        $companyUserCompanyId = $request->input('companyUserInviteCompanyId');
        $companyUserInviteToken = $request->input('companyUserInviteToken');
        $companyUserName = $request->input('companyUserName');
        $companyUserAvatar = $request->input('companyUserAvatar');
        $companyUserRole = CompanyUser::ROLE_NORMAL;
        if ($request->input('companyUserRole')) {
            $companyUserRole = $request->input('companyUserRole');
        }
        $companyModel = CompanyModel::query()
            ->find($companyUserCompanyId);
        if (empty($companyModel)) {
            throw new MessageException('该企业不存在！');
        }
//        $companyModel = CompanyModel::query()
//            ->where('certified_status', '=', CompanyModel::STATUS_CERTIFIED_PASS)
//            ->find($companyUserCompanyId);
//        if (empty($companyModel)) {
//            $count = CompanyUser::query()
//                ->where('company_id', $companyUserCompanyId)
//                ->count();
//            if ($count >= 20) {
//                throw new MessageException('未认证企业组织至多邀请20个成员');
//            }
//        }
        $companyUserInvite = CompanyUserInvite::query()
            ->where('company_id', $companyUserCompanyId)
            ->where('token', $companyUserInviteToken)
            ->first();
        if (empty($companyUserInvite)) {
            throw new MessageException('无效链接');
        }
//        $tokenExpiredAt = $companyUserInvite->getOriginal('token_expired_at');
//        if ($tokenExpiredAt < Carbon::now()->toDateTimeString()) {
//            throw new MessageException('链接已过期');
//        }
        $companyUserExists = CompanyUser::query()
            ->where('user_id', Auth::id())
            ->where('company_id', $companyUserCompanyId)
            ->exists();
        if ($companyUserExists) {
            //已加入企业
            throw new MessageException('您已加入企业组织');
        }
        $model = CompanyUser::withTrashed()
            ->updateOrCreate([
                'user_id' => Auth::id(),
                'company_id' => $companyUserCompanyId
            ], [
                'name' => $companyUserName,
                'avatar' => $companyUserAvatar ?: Arr::get(config('common.default_avatars'), rand(0, count(config('common.default_avatars')) - 1)),
                'role' => $companyUserRole ?: CompanyUser::ROLE_NORMAL
            ]);
        if ($model->trashed()) {
            $model->restore();
        }
        $companyUserId = $model->getKey();
        $getCompanyIdRequest = new UpdateOrCreateCompanyMainRequest();
        $getCompanyIdRequest->setUserId(Auth::id());
        $getCompanyIdRequest->setCompanyId($companyUserCompanyId);
        CompanyMain::updateOrCreateCompanyMain($getCompanyIdRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('companyUserId'));
        return Response::success($successRequest);
    }
}
