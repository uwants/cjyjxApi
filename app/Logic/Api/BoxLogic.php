<?php

namespace App\Logic\Api;

use App\Events\MessageEvent;
use App\Exceptions\MessageException;
use App\Facades\Box;
use App\Facades\CompanyMain;
use App\Facades\Response;
use App\Models\BoxCommentUser;
use App\Models\BoxFollow;
use App\Models\BoxHot;
use App\Models\BoxUser;
use App\Models\Comment;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\Reply;
use App\Models\OldBox;
use App\Models\Box as Boxes;
use App\Models\User;
use App\Services\Box\Models\CreateBoxRequest;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\Response\Models\PageRequest;
use App\Services\Response\Models\SuccessRequest;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\Box as BoxModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class BoxLogic
{
    public function index(Request $request): HttpResponseException
    {
        //获取默认主公司企业id
        $companyId = $this->getCompanyMainCompanyId();
        return $this->indexPlatform($request, $companyId);
    }

    public function feedback(): HttpResponseException
    {
        $boxId = config('common.box.feedback_box_id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws Exception
     */
    public function editShow(Request $request): HttpResponseException
    {
        $boxId = $request->input('boxId');
        $boxModel = BoxModel::query()
            ->where('id', $boxId)
            ->first();
        if (empty($boxModel)) {
            throw new Exception('找不到该意见箱');
        }
        $boxId = $boxModel->getKey();
        $boxIsHelper = BoxUser::query()
            ->where('box_id', $boxId)
            ->where('user_id', Auth::id())
            ->exists();
        $data = [
            'boxId' => $boxId,
            'boxName' => $boxModel->getAttribute('name'),
            'boxContent' => $boxModel->getAttribute('content'),
            'boxOnce' => $boxModel->getAttribute('once'),
            'boxIsSuspend' => $boxModel->getAttribute('suspend'),
            'boxIsOwner' => $boxModel->getAttribute('user_id') == Auth::id(),
            'boxIsHelper' => $boxIsHelper
        ];
        $boxUserIds = BoxUser::query()
            ->where('box_id', $boxId)
            ->pluck('user_id')
            ->toArray();
        $companyId = $this->getCompanyMainCompanyId();
        //兼容pc的设计
        $pcBoxUser = CompanyUser::query()
            ->select('name', 'user_id')
            ->where('company_id', $companyId)
            ->whereIn('user_id', $boxUserIds)
            ->get()
            ->toArray();
        $boxCommentUserIds = BoxCommentUser::query()
            ->where('box_id', $boxId)
            ->pluck('user_id')
            ->toArray();
        $boxType = 3;
        if ($boxModel->getAttribute('open')) {
            $boxType = 1;//1为公开
        }
        if ($boxModel->getAttribute('all')) {
            $boxType = 2;//2为所有人
        }
        Arr::set($data, 'boxUserIds', $boxUserIds);
        Arr::set($data, 'boxCommentUserIds', $boxCommentUserIds);
        Arr::set($data, 'boxType', $boxType);
        Arr::set($data, 'pcBoxUser', $pcBoxUser);
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function total(): HttpResponseException
    {
        $boxTotal = BoxModel::query()
            ->count('id');
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxTotal'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function store(Request $request): HttpResponseException
    {
        $companyId = $this->getCompanyMainCompanyId();
        if ($companyId == 0) {
            throw new MessageException('没有企业');
        }
        $createBoxRequest = new CreateBoxRequest();
        $createBoxRequest->setBoxUserId(Auth::id());
        $createBoxRequest->setBoxCompanyId($companyId);
        $createBoxRequest->setBoxName($request->input('boxName'));
        $createBoxRequest->setBoxOnce($request->input('boxIsOnlyOnce'));
        $content = [];
        Arr::set($content, 'content', (string)$request->input('boxContent.content'));
        $createBoxRequest->setContent($content);
        $createBoxRequest->setBoxSystem(false);
        if ($request->input('boxShow')) {
            $createBoxRequest->setBoxShow($request->input('boxShow'));
        }
        $createBoxRequest->setBoxUserIds((array)$request->input('boxUserIds'));
        switch ($request->input('boxType')) {
            case 1:
                $createBoxRequest->setBoxOpen(true);
                break;
            case 2:
                $createBoxRequest->setBoxAll(true);
                break;
            case 3:
                //指定人
                $createBoxRequest->setBoxCommentUserIds($request->input('boxCommentUserIds'));
                break;
        }
        $response = Box::createBox($createBoxRequest);
        $boxId = $response->getBoxId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function show(Request $request): HttpResponseException
    {
        $successRequest = new SuccessRequest();
        $successRequest->setData($this->getBoxDetails($request));
        return Response::success($successRequest);
    }

    public function update(Request $request): HttpResponseException
    {
        $createBoxRequest = new CreateBoxRequest();
        $createBoxRequest->setBoxUserId(Auth::id());
        $createBoxRequest->setBoxId($request->input('boxId'));
        $createBoxRequest->setBoxName($request->input('boxName'));
        $createBoxRequest->setBoxOnce($request->input('boxIsOnlyOnce'));
        $content = [];
        Arr::set($content, 'content', (string)$request->input('boxContent.content'));
        $createBoxRequest->setContent($content);
        $createBoxRequest->setBoxSystem(false);
        if ($request->input('boxShow')) {
            $createBoxRequest->setBoxShow($request->input('boxShow'));
        }
        $createBoxRequest->setBoxUserIds((array)$request->input('boxUserIds'));
        BoxCommentUser::query()
            ->where('box_id', $createBoxRequest->getBoxId())
            ->delete();
        BoxUser::query()
            ->where('box_id', $createBoxRequest->getBoxId())
            ->delete();
        switch ($request->input('boxType')) {
            case 1:
                $createBoxRequest->setBoxOpen(true);
                break;
            case 2:
                $createBoxRequest->setBoxAll(true);
                break;
            case 3:
                //指定人
                $createBoxRequest->setBoxCommentUserIds($request->input('boxCommentUserIds'));
                break;
        }
        $response = Box::createBox($createBoxRequest);
        $boxId = $response->getBoxId();
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('boxId'));
        return Response::success($successRequest);
    }

    public function suspend(Request $request): HttpResponseException
    {
        BoxModel::query()
            ->where('id', $request->input('boxId'))
            ->update(['suspend' => true]);
        $successRequest = new SuccessRequest();
        $successRequest->setData(['boxId' => $request->input('boxId')]);
        return Response::success($successRequest);
    }

    public function open(Request $request): HttpResponseException
    {
        BoxModel::query()
            ->where('id', $request->input('boxId'))
            ->update(['suspend' => false]);
        $successRequest = new SuccessRequest();
        $successRequest->setData(['boxId' => $request->input('boxId')]);
        return Response::success($successRequest);
    }


    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function destroy(Request $request): HttpResponseException
    {
        $boxId = $request->input('boxId');
        $model = BoxModel::query()
            ->find($boxId);
        if (empty($model)) {
            throw new MessageException('找不到该意见箱');
        }
        //判断权限
        if (!Gate::allows('delete', $model)) {
            throw new MessageException('没有权限');
        }
        $model->delete();
        $successRequest = new SuccessRequest();
        $boxId = $model->getKey();
        event(new MessageEvent('BOX-DELETE', 0, $boxId, 0, ''));
        $successRequest->setData(compact('boxId'));
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 收藏意见箱
     */
    public function collectionStore(Request $request): HttpResponseException
    {
        $box = BoxModel::query()
            ->select('id', 'company_id')
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        $boxUser = BoxUser::query()
            ->select('id')
            ->where('user_id', Auth::id())
            ->where('box_id', $box->id)
            ->first();
        if ($box->user_id == Auth::id() || !empty($boxUser->id)) {
            throw new MessageException('收藏失败！自己创建的意见箱和属于意见箱助手的都不可以收藏');
        }
        $model = BoxFollow::withTrashed()
            ->firstOrCreate([
                'user_id' => (int)Auth::id(),
                'box_id' => $request->input('boxId')
            ]);
        if ($model->trashed()) {
            $model->restore();
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(['followId' => $model->getKey(), 'boxId' => $request->input('boxId')]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 取消收藏意见箱
     */
    public function collectionDestroy(Request $request): HttpResponseException
    {
        $box = BoxModel::withTrashed()
            ->select('id', 'company_id')
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        $model = BoxFollow::query()
            ->where('user_id', (int)Auth::id())
            ->where('box_id', $request->input('boxId'))
            ->firstOrFail();
        $model->delete();
        $successRequest = new SuccessRequest();
        $successRequest->setData(['followId' => $model->getKey(), 'boxId' => $request->input('boxId')]);
        return Response::success($successRequest);
    }

    public function collectionIndex(Request $request): HttpResponseException
    {
        $model = BoxFollow::query()
            ->with(['box' => function ($query) {
                $query->with('company');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->where('user_id', (int)Auth::id());
        $model->orderBy('id', 'desc');
        $followList = $model->paginate($request->input('pageSize'));
        $items = $followList->items();
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $pageRequest->setTotalPage($followList->lastPage());
        $pageRequest->setTotalNumber($followList->total());
        $list = [];
        foreach ($items as $key => $val) {
            $list[$key]['boxId'] = $val->box_id;
            $list[$key]['boxName'] = $val->box->name;
            $boxUserName = empty($val->box->company->name) ? $val->box->name : $val->box->company->name;
            $boxCompanyLogo = empty($val->box->company->logo) ? config('common.default_company_logo') : $val->box->company->logo;
            $list[$key]['boxCompanyLogo'] = $boxCompanyLogo;
            $list[$key]['boxUserName'] = $boxUserName;
            $list[$key]['boxIsOwner'] = $val->box->user_id == Auth::id();
            $list[$key]['boxIsSuspend'] = !empty($val->box->suspend);//是否暂停
            $list[$key]['boxIsDelete'] = !empty($val->box->deleted_at);
            $list[$key]['boxIsOpen'] = !empty($val->box->open);
            $list[$key]['boxContent'] = $val->box->content;
            $list[$key]['boxCreatedTime'] = $this->getTime($val->box->created_at);
            $list[$key]['followCreatedTime'] = date('Y-m-d', strtotime($val->created_at));
        }
        $pageRequest->setList($list);
        $pageRequest->execute();
        $successRequest = new SuccessRequest();
        $successRequest->setData($pageRequest->getData());
        return Response::success($successRequest);
    }


    private function indexPlatform(Request $request, int $companyId): HttpResponseException
    {
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));

        $model = BoxModel::query()
            ->select('id', 'company_id', 'user_id', 'name', 'suspend', 'once', 'transfer', 'transfer_content', 'system', 'content', 'created_at', 'open');
        //共同过滤
        if (empty($model)) {
            $pageRequest->execute();
            return Response::page($pageRequest);
        }
        $type = $request->input('type');
        //首页过滤
        if (empty($type)) {
            if ($companyId == 0) {
                $pageRequest->execute();
                return Response::page($pageRequest);
            }
            $model
                ->scopes(['view' => (Auth::id())])
                ->where('company_id', $companyId)
                ->where('suspend', false);
        }
        //我的意见箱
        if ($type == 'owner') {
            if ($companyId == 0) {
                $pageRequest->execute();
                return Response::page($pageRequest);
            }
            $model
                ->scopes(['view' => (Auth::id())])
                ->where('company_id', $companyId)
                ->scopes(['owner' => Auth::id()])
                ->orWhere(function (Builder $query) use ($companyId) {
                    $query
                        ->select('user_id')
                        ->scopes(['box_user' => Auth::id()])
                        ->where('company_id', $companyId);
                });
        }
        //我提出的意见箱
        if ($type == 'comment') {
            $model
                ->where(function ($query) {
                    $query
                        ->scopes(['comment' => Auth::id()]);
                })
                ->orderByDesc(
                    Comment::query()
                        ->select('created_at')
                        ->whereColumn('box_id', 'boxes.id')
                        ->where('user_id', Auth::id())
                        ->orderBy('created_at', 'desc')
                        ->limit(1)
                );
        }

        $model->withCount([
            'comments' => function (Builder $query) {
                $query->where('user_id', Auth::id());
            },
            'comments as comments_no_reply_count' => function (Builder $query) {
                $query->doesntHave('reply');
            },
            'box_users' => function ($query) {
                $query->where('user_id', Auth::id());
            },
            'boxes_follows' => function ($query) {
                $query
                    ->where('user_id', Auth::id())
                    ->where('deleted_at', null);
            }
        ])
            ->with([
                'comments' => function ($query) {
                    $query
                        ->select('id', 'box_id', 'user_id', 'content', 'adopt', 'created_at',
                            DB::raw("(select count(id) from replies where replies.comment_id=opinions.id) as replyNum "))
                        ->with(['reply' => function ($query) {
                            $query->orderBy('created_at', 'desc');
                        }])
                        ->orderBy('replyNum', 'asc')
                        ->orderBy('created_at', 'desc');
                }
            ]);
        //首页，我的排序
        if (empty($type) || $type == 'owner') {
            $model->orderByDesc(DB::raw('case when user_id = "' . Auth::id() . '" then 2 when box_users_count > 0 then 1 else 0 end'))
                ->latest();
        }
        $model = $model
            ->paginate($pageRequest->getPageSize(), ['*'], 'page', $pageRequest->getPage());
        $pageRequest->setTotalPage($model->lastPage());
        $pageRequest->setTotalNumber($model->total());
        $items = $model->items();
        $list = [];
        foreach ($items as $val => $item) {
            $boxContent = [
                'content' => Arr::get($item->content, 'content', '')
            ];
            $list[$val] = [
                'boxId' => $item->id,
                'boxName' => empty($item->name) ? '' : $item->name,
                'boxSuspend' => $item->suspend,
                'boxUserId' => $item->user_id,
                'boxCreatedTime' => $item->CreatedTime,
                'boxIsOwner' => $item->user_id == Auth::id(),
                'boxIsOnlyOnce' => (bool)$item->once,
                'boxCompanyLogo' => empty($item->company->logo) ? config('common.default_company_logo') : $item->company->logo,
                'boxIsFollow' => (bool)$item->boxes_follows_count,
                'boxIsOpen' => (bool)$item->open,
                'boxTransfer' => $item->transfer > 0,
                'boxIsTransfer' => Arr::get($item->transfer_content, 'isTransfer') > 0,
                'boxTransferName' => Arr::get($item->transfer_content, 'createdName') ?: '',
            ];
            Arr::set($list[$val], 'boxContent', $boxContent);
            //热门企业问题
            if ($item->user_id == Auth::id()) {
                Arr::set($list[$val], 'boxCreateUserName', '我');
            } elseif (!empty($item->company->name)) {
                Arr::set($list[$val], 'boxCreateUserName', $item->company->name);
            } else {
                Arr::set($list[$val], 'boxCreateUserName', '焦点热点');
            }
            $tagName = '';
            $boxComment = [];
            $boxCommentReplies = [];
            if ($item->user_id == Auth::id() || $item->box_users_count > 0) {
                //我是创建者,显示最新未回复的意见
                $tagName = 'owner';
                if ($item->box_users_count > 0) {
                    $tagName = 'helper';
                }
                $boxCommentModel = '';
                if (!empty($item->comments)) {
                    $boxCommentModel = !empty($item->comments->first()) ? $item->comments->first() : '';
                    if (!empty($boxCommentModel)) {
                        $boxComment = [
                            'commentUserName' => empty($boxCommentModel->user->AliasNames) ? "匿名用户" : $boxCommentModel->user->AliasNames,
                            'commentIsOwner' => $boxCommentModel->user_id == Auth::id(),
                            'commentAvatar' => empty($boxCommentModel->user->AliasAvatars) ? config('common.default_avatar') : $boxCommentModel->user->AliasAvatars,
                            'commentUserId' => !empty($boxCommentModel->user_id) ? $boxCommentModel->user_id : '',
                            'commentId' => !empty($boxCommentModel->id) ? $boxCommentModel->id : '',
                            'commentContent' => !empty($boxCommentModel->content) ? $boxCommentModel->content : '',
                            'commentCreatedTime' => !empty($boxCommentModel->CreatedTime) ? $boxCommentModel->CreatedTime : '',
                            'commentIsAdopt' => (bool)$boxCommentModel->adopt,
                            'commentIsNext' => $item->comments_no_reply_count > 1
                        ];
                    }
                }
                //判断是否有回复
                if (!empty($item->comments)) {
                    $boxCommentReplyModel = !empty($boxCommentModel->reply) ? $boxCommentModel->reply : '';
                    if (!empty($boxCommentReplyModel)) {
                        $boxCommentReplies = $this->getBoxCommentReply($boxCommentReplyModel);
                    }
                }
            }
            if ($item->user_id != Auth::id() && $item->comments_count == 0 && $item->box_users_count == 0) {
                $tagName = 'AllowComment';
                //我不是创建者,未回复
                //意见回复都不显示
            }
            if ($item->user_id != Auth::id() && $item->comments_count > 0 && $item->box_users_count == 0) {
                $tagName = 'HasComment';
                //我不是创建者,已评论,显示我最新提的意见
                $boxCommentModel = '';
                if (!empty($item->comments)) {
                    $boxCommentModel = Comment::query()->where('box_id', $item->id)
                        ->where('user_id', Auth::id())
                        ->with(['reply' => function ($query) {
                            $query->orderBy('created_at', 'desc');
                        }])
                        ->latest()
                        ->first();
                    if (!empty($boxCommentModel)) {
                        $boxComment = [
                            'commentUserName' => empty($boxCommentModel->user->AliasNames) ? "匿名用户" : $boxCommentModel->user->AliasNames,
                            'commentIsOwner' => $boxCommentModel->user_id == Auth::id(),
                            'commentAvatar' => empty($boxCommentModel->user->AliasAvatars) ? config('common.default_avatar') : $boxCommentModel->user->AliasAvatars,
                            'commentUserId' => !empty($boxCommentModel->user_id) ? $boxCommentModel->user_id : '',
                            'commentId' => !empty($boxCommentModel->id) ? $boxCommentModel->id : '',
                            'commentContent' => !empty($boxCommentModel->content) ? $boxCommentModel->content : '',
                            'commentCreatedTime' => !empty($boxCommentModel->CreatedTime) ? $boxCommentModel->CreatedTime : '',
                            'commentIsAdopt' => (bool)$boxCommentModel->adopt,
                            'commentIsNext' => false
                        ];
                    }
                }
                //判断是否有回复
                if (!empty($item->comments)) {
                    $boxCommentReplyModel = !empty($boxCommentModel->reply) ? $boxCommentModel->reply : '';
                    if (!empty($boxCommentReplyModel)) {
                        $boxCommentReplies = $this->getBoxCommentReply($boxCommentReplyModel);
                    }
                }
            }
            Arr::set($list[$val], 'boxTagName', $tagName);
            Arr::set($list[$val], 'boxComment', $boxComment);
            Arr::set($list[$val], 'boxCommentReplies', $boxCommentReplies);
        }
        $isCreator = BoxModel::query()
            ->where('company_id', $companyId)
            ->scopes(['owner' => Auth::id()])
            ->exists();
        $pageRequest->setExtra(compact('isCreator'));
        $pageRequest->setList($list);
        $pageRequest->execute();
        return Response::page($pageRequest);
    }

    /**
     * @param Reply $boxCommentReplyModel
     * @return array
     */
    private function getBoxCommentReply(Reply $boxCommentReplyModel): array
    {
        return [
            'replyUserName' => $boxCommentReplyModel->getOriginal('user_id') == Auth::id() ? '我' : '意见管理员',
            'replyIsOwner' => $boxCommentReplyModel->getOriginal('user_id') == Auth::id(),
            'replyAvatar' => config('common.default_avatar'),
            'replyId' => !empty($boxCommentReplyModel->id) ? $boxCommentReplyModel->id : '',
            'replyContent' => !empty($boxCommentReplyModel->content) ? $boxCommentReplyModel->content : '',
            'replyUserId' => !empty($boxCommentReplyModel->user_id) ? $boxCommentReplyModel->user_id : '',
            'replyCreatedTime' => !empty($boxCommentReplyModel->CreatedTime) ? $boxCommentReplyModel->CreatedTime : '',
        ];
    }

    /**
     * @param $request
     * @return array
     * @throws MessageException
     */
    public function getBoxDetails($request): array
    {
        $box = BoxModel::withTrashed()
            ->select('id', 'user_id', 'name', 'company_id', 'suspend', 'once', 'open', 'transfer', 'transfer_content', 'all', 'show', 'system', 'content', 'created_at', 'updated_at', 'deleted_at')
            ->where('id', $request->input('boxId'))
            ->with(['user', 'company'])
            ->with(['box_users' => function ($query) {
                $query->where('user_id', Auth::id());
            }])
            ->with(['boxes_follows' => function ($query) {
                $query->where('user_id', Auth::id());
            }])
            ->with(['comments' => function ($query) {
                $query->where('user_id', Auth::id());
            }])
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }

        $isPermission = Gate::allows('view', $box);
        $companyId = empty($box->company->id) ? 0 : $box->company->id;
        $userInfo = CompanyUser::query()
            ->select('company_id', 'user_id', 'name', 'avatar', 'role', 'created_at', 'updated_at', 'deleted_at')
            ->where('company_id', $companyId)
            ->where('user_id', Auth::id())
            ->first();
        /* if (($box->user_id == Auth::id() || count($box->box_users) > 0) && $request->input('isShare') == 0 && $box->transfer == 0) {
             $isAllowComment = false;
         } else {
             if ($box->once == 1 && count($box->comments) > 0) {
                 $isAllowComment = false;
             } else {
                 $isAllowComment = true;
             }
         }*/
        if ($box->once == 1 && count($box->comments) > 0) {
            $isAllowComment = false;
        } else {
            $isAllowComment = true;
        }
        $companyId = $box->company_id;
        $commentNumber = Comment::query()
            ->where('box_id', $box->id);
        $commentModel = Comment::query()
            ->where('box_id', $box->id)
            ->groupBy('user_id')
            ->orderByDesc('id');
        if ($box->user_id != Auth::id() && count($box->box_users) <= 0) {
            $commentModel->where('user_id', Auth::id());
            $commentNumber->where('user_id', Auth::id());
        }
        $commentModel->orderBy('created_at', 'desc');
        $commentList = $commentModel->paginate($request->input('pageSize'));
        $items = $commentList->items();
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $pageRequest->setTotalPage($commentList->lastPage());
        $pageRequest->setTotalNumber($commentNumber->count());
        $boxComments = [];
        foreach ($items as $ke => $va) {
            $commentList = Comment::query()
                ->where('box_id', $box->id)
                ->where('user_id', $va->user_id)
                ->with(['replies' => function ($query) use ($companyId) {
                    $query->with(['user' => function ($query) use ($companyId) {
                        $query->with(['company_user' => function ($query) use ($companyId) {
                            $query->where('company_id', $companyId);
                        }]);
                    }])->orderBy('created_at', 'asc');
                }])
                ->orderByDesc('id')->get();
            foreach ($commentList as $key => $val) {
                $boxComments[$ke][$key]['commentReward'] = number_format($val->CommentReward, 2, '.', '');
                $boxComments[$ke][$key]['commentRewardAll'] = number_format($val->CommentRewardAll, 2, '.', '');
                $boxComments[$ke][$key]['commentId'] = $val->id;
                $boxComments[$ke][$key]['userId'] = $val->user_id;
                $boxComments[$ke][$key]['commentContent'] = $val->content;
                $boxComments[$ke][$key]['commentIsAdopt'] = $val->adopt;
                if ($val->user_id != Auth::id()) {
                    $boxComments[$ke][$key]['commentUserName'] = empty($val->user->AliasNames) ? '匿名用户' : $val->user->AliasNames;
                    $boxComments[$ke][$key]['commentUserAvatar'] = empty($val->user->AliasAvatars) ? config('common.default_avatar') : $val->user->AliasAvatars;
                } else {
                    $boxComments[$ke][$key]['commentUserAvatar'] = empty($userInfo->avatar) ? config('common.default_avatar') : $userInfo->avatar;
                    $boxComments[$ke][$key]['commentUserName'] = '我';//empty($userInfo->name) ? '我' : $userInfo->name;
                }
                $reply = [];
                foreach ($val->replies as $k => $v) {
                    $reply[$k]['replyId'] = $v->id;
                    $reply[$k]['replyContent'] = $v->content;
                    $reply[$k]['replyUserId'] = $v->user_id;
                    $reply[$k]['createdTime'] = $this->getTime($v->created_at);
                    $reply[$k]['replyIsOwner'] = $v->user_id == Auth::id();
                    $reply[$k]['replyUserName'] = '意见管理员';
                }
                $boxComments[$ke][$key]['createdTime'] = $this->getTime($val->created_at);
                $boxComments[$ke][$key]['commentIsOwner'] = $val->user_id == Auth::id();
                $boxComments[$ke][$key]['commentReplies'] = $reply;
            }
        }
        $pageRequest->setList($boxComments);
        $pageRequest->execute();
        return array(
            'boxId' => $box->id,
            'boxName' => $box->name,
            'boxSuspend' => (bool)$box->suspend,
            'boxIsAllowComment' => $isAllowComment,
            'boxIsDelete' => !empty($box->deleted_at),
            'boxContent' => $box->content,
            'boxUserId' => $box->user_id,
            'boxUserName' => empty($box->company->name) ? '推荐引擎' : $box->company->name,//企业名字
            'boxCompanyId' => $box->company_id,
            'boxCompanyName' => empty($box->company->name) ? '推荐引擎' : $box->company->name,
            'boxCompanyLogo' => empty($box->company->logo) ? config('common.default_company_logo') : $box->company->logo,
            'boxCreatedTime' => $this->getTime($box->created_at),
            'boxIsOwner' => $box->user_id == Auth::id(),
            'boxIsBoxAssistant' => $box->box_users->count() > 0,
            'boxIsFollow' => count($box->boxes_follows) > 0,
            'boxIsPermission' => $isPermission,
            'boxTransfer' => $box->transfer > 0,
            'boxIsTransfer' => Arr::get($box->transfer_content, 'isTransfer') > 0,
            'boxTransferName' => Arr::get($box->transfer_content, 'createdName') ?: '',
            'boxComments' => $pageRequest->getData(),
            'boxIsOpen' => (bool)$box->open
        );
    }

    public function hotIndex(): HttpResponseException
    {
        $hotBoxIds = BoxHot::query()
            ->pluck('box_id')
            ->toArray();
        $model = BoxModel::query()
            ->select('id', 'name', 'company_id', 'content', 'open', 'created_at')
            ->whereIn('id', $hotBoxIds)
            ->where('suspend', false)
            ->where(function (Builder $query) {
                $query->where('open', true)
                    ->orWhere('show', true)
                    ->orWhere('system', true);
            })
            ->orderByDesc('id')
            ->with('company')
            ->withCount([
                'boxes_follows' => function ($query) {
                    $query
                        ->where('user_id', Auth::id())
                        ->where('deleted_at', null);
                }])
            ->get();
        $data = [];
        foreach ($model as $item) {
            $data [] = [
                'boxId' => $item->getKey(),
                'boxName' => empty($item->name) ? '' : $item->name,
                'boxCreatedTime' => empty($item->CreatedTime) ? '' : $item->CreatedTime,
                'boxCompanyName' => empty($item->company->name) ? '推荐引擎' : $item->company->name,
                'boxCompanyLogo' => empty($item->company->logo) ? config('common.default_company_logo') : $item->company->logo,
                'boxContent' => empty($item->content) ? '' : $item->content,
                'boxIsFollow' => (bool)$item->boxes_follows_count,
                'boxIsOpen' => (bool)$item->open,

            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    public function getTime($time)
    {
        $day = strtotime(date('Y-m-d', time()));
        $yesterday = $day - 24 * 360;
        if (strtotime($time) > $day) {//今天
            $showTime = date('H:i', strtotime($time));
        } elseif (($yesterday < strtotime($time)) && (strtotime($time) < $day)) {
            $showTime = '昨天';
        } else {
            $showTime = date('Y-m-d', strtotime($time));
        }
        return $showTime;

    }

    public function oldBox(Request $request): HttpResponseException
    {
        $uuid = $request->input('uuid');
        $model = OldBox::query()
            ->where('uuid_id', $uuid)
            ->first();
        $companyId = 0;
        $boxId = 0;
        if (strpos($uuid, '-') === false) {
            $companyId = Arr::get($model, 'company_id');
        } else {
            $boxId = Arr::get($model, 'box_id');
        }
        Log::info('旧版跳转', [$request]);
        $successRequest = new SuccessRequest();
        $successRequest->setData(['companyId' => $companyId, 'boxId' => $boxId]);
        return Response::success($successRequest);
    }

    public function getCompany(Request $request): HttpResponseException
    {
        $companyId = 0;
        if ($request->input('boxId')) {
            $model = Boxes::withTrashed()
                ->where('id', $request->input('boxId'))
                ->first();
            $companyId = $model->company_id;
        } elseif ($request->input('companyName')) {
            $model = Company::query()
                ->where('status', 'STATUS_CERTIFIED_PASS')
                ->where('name', $request->input('companyName'))
                ->first();
            $companyId = $model->id;
        }
        Log::info('扫码或连接跳转', [$request]);
        $successRequest = new SuccessRequest();
        $successRequest->setData(['companyId' => $companyId]);
        return Response::success($successRequest);
    }

    private function getCompanyMainCompanyId(): int
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdRequest->setUserId(Auth::id());
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        return $getCompanyIdResponse->getCompanyId();
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function getCommentByUser(Request $request): HttpResponseException
    {
        $box = BoxModel::withTrashed()
            ->select('id', 'user_id', 'name', 'company_id', 'suspend', 'once', 'open', 'all', 'show', 'system', 'content', 'created_at', 'updated_at', 'deleted_at')
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        $companyId = $box->company_id;
        $userInfo = CompanyUser::query()
            ->select('company_id', 'user_id', 'name', 'avatar', 'role', 'created_at', 'updated_at', 'deleted_at')
            ->where('company_id', $companyId)
            ->where('user_id', Auth::id())
            ->first();
        $commentList = Comment::query()
            ->where('box_id', $request->input('boxId'))
            ->where('user_id', $request->input('userId'))
            ->with(['replies' => function ($query) use ($companyId) {
                $query->with(['user' => function ($query) use ($companyId) {
                    $query->with(['company_user' => function ($query) use ($companyId) {
                        $query->where('company_id', $companyId);
                    }]);
                }])->orderBy('created_at', 'asc');
            }]);
        $commentList->orderBy('created_at', 'desc');
        $commentLists = $commentList->paginate($request->input('pageSize'));
        $items = $commentLists->items();
        $pageRequest = new PageRequest();
        $pageRequest->setPage($request->input('page'));
        $pageRequest->setPageSize($request->input('pageSize'));
        $pageRequest->setTotalPage($commentLists->lastPage());
        $pageRequest->setTotalNumber($commentLists->total());
        $boxComments = [];
        foreach ($items as $key => $val) {
            $boxComments[$key]['commentReward'] = number_format($val->CommentReward, 2, '.', '');
            $boxComments[$key]['commentRewardAll'] = number_format($val->CommentRewardAll, 2, '.', '');
            $boxComments[$key]['commentId'] = $val->id;
            $boxComments[$key]['commentKey'] = $val->user_id;
            $boxComments[$key]['commentContent'] = $val->content;
            $boxComments[$key]['commentIsAdopt'] = $val->adopt;
            if ($val->user_id != Auth::id()) {
                $boxComments[$key]['commentUserName'] = empty($val->user->AliasNames) ? '匿名用户' : $val->user->AliasNames;
                $boxComments[$key]['commentUserAvatar'] = empty($val->user->AliasAvatars) ? config('common.default_avatar') : $val->user->AliasAvatars;
            } else {
                $boxComments[$key]['commentUserAvatar'] = empty($userInfo->avatar) ? config('common.default_avatar') : $userInfo->avatar;
                $boxComments[$key]['commentUserName'] = '我';//empty($userInfo->name) ? '我' : $userInfo->name;
            }
            $reply = [];
            foreach ($val->replies as $k => $v) {
                $reply[$k]['replyId'] = $v->id;
                $reply[$k]['replyContent'] = $v->content;
                $reply[$k]['replyUserId'] = $v->user_id;
                $reply[$k]['createdTime'] = $this->getTime($v->created_at);
                $reply[$k]['replyIsOwner'] = $v->user_id == Auth::id();
                $reply[$k]['replyUserName'] = '意见管理员';
            }
            $boxComments[$key]['createdTime'] = $this->getTime($val->created_at);
            $boxComments[$key]['commentIsOwner'] = $val->user_id == Auth::id();
            $boxComments[$key]['commentReplies'] = $reply;
        }
        $pageRequest->setList($boxComments);
        $pageRequest->execute();
        return Response::page($pageRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     * 落地页信息
     */
    public function marketBoxMessage(Request $request): HttpResponseException
    {
        $box = BoxModel::query()
            ->with('user')
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        if (empty($box->transfer)) {
            throw new MessageException('该意见箱不可以转移！');
        }
        if ($request->input('time') + 3600 * 24 * 3 < time()) {
            throw new MessageException('二维码已经失效！');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'boxName' => $box->name,
            'boxUserName' => $box->user->name,
            'boxCreatedTime' => date('Y/m/d', strtotime($box->created_at))
        ]);
        return Response::success($successRequest);
    }

    /**
     * @param Request $request
     * @return HttpResponseException
     * @throws MessageException
     */
    public function moveBox(Request $request): HttpResponseException
    {
        $box = BoxModel::query()
            ->with('user')
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        if (empty($box->transfer)) {
            throw new MessageException('该意见箱已经被转移！');
        }
        if (Auth::id() == $box->user_id) {
            throw new MessageException('自己不可以转移自己的意见箱！');
        }
        $createdTime = Arr::get($box->transfer_content, 'createdTime');
        if (!empty($createdTime) && $createdTime + 3600 * 24 * 3 < time()) {
            throw new MessageException('二维码已经失效，转移失败！');
        }
        $user = User::withTrashed()
            ->findOrFail(Auth::id());
        $companyUser = CompanyUser::withTrashed()
            ->firstOrCreate([
                'user_id' => Auth::id(),
                'company_id' => $box->company_id,
            ], [
                'name' => $user->getOriginal('name') ?: substr($user->getAttribute('mobile'), -4, 4),
                'avatar' => $user->getOriginal('avatar') ?: config('common.default_avatar'),
                'role' => config('common.companyUser.role.administrator'),
            ]);
        if ($companyUser->trashed()) {
            $companyUser->restore();
        }
        $messageContent = $box->transfer_content;
        Arr::set($messageContent, 'isTransfer', 1);
        BoxModel::query()
            ->where('id', $request->input('boxId'))
            ->update(['user_id' => Auth::id(), 'transfer' => 0, 'transfer_content' => $messageContent]);
        //切换主企业
        User::withTrashed()
            ->where('id', Auth::id())
            ->update(['main_company_id' => $box->company_id]);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'boxId' => $box->id,
            'companyId' => $box->company_id,
        ]);
        return Response::success($successRequest);
    }


}
