<?php

namespace App\Logic\Api;

use App\Facades\CompanyMain;
use App\Services\CompanyMain\Models\GetCompanyIdRequest;
use App\Services\User\Models\GetUserInfoByCompanyIdRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Services\Response\Models\SuccessRequest;
use App\Facades\Response;
use App\Facades\User;
use Illuminate\Support\Facades\Auth;

class UserLogic
{

    public function index(): HttpResponseException
    {
        $getCompanyIdRequest = new GetCompanyIdRequest();
        $getCompanyIdResponse = CompanyMain::getCompanyId($getCompanyIdRequest);
        $companyId = $getCompanyIdResponse->getCompanyId();
        $GetUserInfoByCompanyIdRequest = new GetUserInfoByCompanyIdRequest();
        $GetUserInfoByCompanyIdRequest->setCompanyId($companyId);
        $GetUserInfoByCompanyIdRequest->setUserId(Auth::id());
        $res = User::getUserInfoByCompanyId($GetUserInfoByCompanyIdRequest);
        $successRequest = new SuccessRequest();
        $successRequest->setData($res->getData());
        return Response::success($successRequest);
    }
}
