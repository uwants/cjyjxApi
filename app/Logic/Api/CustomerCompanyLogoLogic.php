<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\CustomerCompanyLogo;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomerCompanyLogoLogic
{
    public function index(): HttpResponseException
    {
        $model = CustomerCompanyLogo::query()
            ->select('logo')
            ->where('suspend', false)
            ->orderByDesc('sort')
            ->latest()
            ->get();
        $data = $model->toArray();
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}