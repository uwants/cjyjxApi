<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\Alias;
use App\Models\AliasUser;
use App\Models\User;
use App\Services\Response\Models\SuccessRequest;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AliasLogic
{
    public function index(): HttpResponseException
    {
        $model = Alias::query()
            ->inRandomOrder()
            ->limit(10)
            ->getModels();
        $data = [];
        foreach ($model as $item) {
            $data [] = [
                'aliasId' => $item->getAttribute('id'),
                'aliasName' => $item->getAttribute('name'),
                'aliasAvatar' => $item->getAttribute('avatar')
            ];
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }

    /**
     * @throws Exception
     */
    public function show(): HttpResponseException
    {
        $model = User::query()
            ->where('id', Auth::id())
            ->first();
        $data = [
            'aliasName' => $model->AliasNames,
            'aliasAvatar' => $model->AliasAvatars
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}
