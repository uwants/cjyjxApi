<?php


namespace App\Logic\Api;


use App\Facades\PlatformCode;
use App\Facades\Response;
use App\Models\Banner;
use App\Models\User;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BannerLogic
{
    public function index(): HttpResponseException
    {
        $model = Banner::query()
            ->select('id', 'image', 'url')
            ->where('suspend', false);
        if (PlatformCode::isPc()) {
            $model->where('type', config('common.banner_type.pc'));
        } else {
            $model->where('type', config('common.banner_type.mp'));
        }
        $data = [];
        if (!empty($model)) {
            $data = $model
                ->get()
                ->toArray();
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}