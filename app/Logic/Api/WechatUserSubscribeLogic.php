<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\WeiXinUser;
use App\Models\WeiXinUserSubscribe;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WechatUserSubscribeLogic
{
    public function check(Request $request): HttpResponseException
    {
        $model = WeiXinUser::query()
            ->where('user_id', Auth::id())
            ->first();
        $successRequest = new SuccessRequest();
        if (empty($model)) {
            $successRequest->setData(['isSubscribe' => false]);
            return Response::success($successRequest);
        }
        $isSubscribe = WeiXinUserSubscribe::query()
            ->where('unionid', $model->getAttribute('union_id'))
            ->exists();
        $successRequest->setData(compact('isSubscribe'));
        return Response::success($successRequest);
    }
}