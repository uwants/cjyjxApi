<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ShareUrlLogic
{
    public function show(Request $request): HttpResponseException
    {
        $type = $request->input('type');
        $url = '';
        $tk = 'tk=' . $request->input('tk');
        $id = 'id=' . $request->input('id');
        $str = $tk . '&' . $id;
        $boxId = $request->input('boxId');
        switch ($type) {
            case 'companyInvite':
                $url = config('common.url.company_invite');
                $url = $url . '?scene=' . urlencode($str);
                break;
            case 'boxInvite':
                $url = config('common.url.box_invite');
                $url = $url . '?boxId=' . $boxId . '&isShare=1';
                break;
            default:
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData(compact('url'));
        Response::success($successRequest);
    }
}