<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\Box;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Request;

class BoxPosterLogic
{
    public function show(Request $request)
    {
        $boxId = $request->input('boxId');
        $model = Box::query()
            ->where('id', $boxId)
            ->first();
        $data = [
            'boxId' => empty($model->id) ? $boxId : $model->id,
            'boxPoster' => empty($model->poster) ? '' : $model->poster
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        Response::success($successRequest);
    }

    public function update(Request $request)
    {
        $boxId = $request->input('boxId');
        $boxPoster = $request->input('boxPoster');
        $model = Box::withTrashed()
            ->where('id', $boxId)
            ->update([
                'poster' => $boxPoster
            ]);
        $data = [
            'boxId' => $boxId,
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        Response::success($successRequest);
    }
}
