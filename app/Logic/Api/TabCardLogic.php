<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\TabCard;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class TabCardLogic
{
    public function index():HttpResponseException
    {
        $model = TabCard::query()
            ->select('id', 'image', 'title', 'path')
            ->where('suspend', false)
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        $data = $model->toArray();
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        return Response::success($successRequest);
    }
}