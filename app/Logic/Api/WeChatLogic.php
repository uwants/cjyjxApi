<?php

namespace App\Logic\Api;

use App\Facades\Response;
use App\Facades\WeiXinApi;
use App\Models\AdminAppletSetting;
use App\Models\Box;
use App\Services\Response\Models\SuccessRequest;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class WeChatLogic
{
    /**
     * @param Request $request
     * @return HttpResponseException
     */
    public function qrCode(Request $request): HttpResponseException
    {
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChat.appId'));
        $accessTokenSend->setSecret(config('common.weChat.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $accessToken->getAccessToken();
        $query = 'a=1';
        $with = 430;
        if ($request->input('query')) {
            $query = http_build_query($request->input('query'));
        }
        if ($request->input('with')) {
            $with = $request->input('with');
        }
        $param = [
            'scene' => $query,
            'page' => $request->input('page'),
            'width' => $with
        ];
        Log::info('生成二维码参数:', compact('param'));
        //$contents = Http::Post($url, $param);
        $json_post = json_encode($param, JSON_UNESCAPED_UNICODE);
        $res = $this->curl_post($url, $json_post);
        $filePath = "qrCode/" . gmdate("Y") . "/" . gmdate("m") . "/" . gmdate("d") . "/" . uniqid(8) . '.png';
        Storage::disk('qiniu')->put($filePath, $res);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'image' => base64_encode($res),
            'imageUrl' => config('common.qiniu.QN_CDN_URL') . '/' . $filePath
        ]);
        return Response::success($successRequest);
    }

    public function companyBoxCode(Request $request): HttpResponseException
    {
        $box = Box::query()
            ->where('id', $request->input('boxId'))
            ->first();
        if (empty($box->id)) {
            throw new MessageException('意见箱不存在！');
        }
        if (empty($box->transfer)) {
            throw new MessageException('该意见箱已经被转移！');
        }
        $imageUrl = Arr::get($box->transfer_content, 'imageUrl');
        $createdTime = Arr::get($box->transfer_content, 'createdTime');
        if (!empty($imageUrl) && $createdTime + 3600 * 24 * 3 > time()) {
            $successRequest = new SuccessRequest();
            $successRequest->setData([
                'imageUrl' => $imageUrl,
                'createdTime' => $createdTime
            ]);
            return Response::success($successRequest);
        }
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChat.appId'));
        $accessTokenSend->setSecret(config('common.weChat.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $accessToken->getAccessToken();
        $createdTime = time();
        $query = 'boxId=' . $request->input('boxId') . '&t=' . $createdTime;
        $param = [
            'scene' => $query,
            'page' => $request->input('page'),
            'width' => 400
        ];
        Log::info('生成二维码参数:', compact('param'));
        $json_post = json_encode($param, JSON_UNESCAPED_UNICODE);
        $res = $this->curl_post($url, $json_post);
        $filePath = "qrCode/" . gmdate("Y") . "/" . gmdate("m") . "/" . gmdate("d") . "/" . uniqid(8) . '.png';
        Storage::disk('qiniu')->put($filePath, $res);
        $imageUrl = config('common.qiniu.QN_CDN_URL') . '/' . $filePath;
        //记录数据
        $messageContent = $box->transfer_content;
        Arr::set($messageContent, 'imageUrl', $imageUrl);
        Arr::set($messageContent, 'createdTime', $createdTime);
        Box::query()
            ->where('id', $request->input('boxId'))
            ->update(['transfer_content' => $messageContent]);
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'imageUrl' => $imageUrl,
            'createdTime' => $createdTime
        ]);
        return Response::success($successRequest);
    }

    public function curl_post($url, $curlPost, $headerOpt = [])
    {

        if (empty($headerOpt)) {
            $headerOpt = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
        }
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerOpt);//修改header信息
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);

        $out_put = curl_exec($ch);//运行curl
        curl_close($ch);
        return $out_put;
    }

    public function inviteQrCode(Request $request): HttpResponseException
    {
        $page = $request->input('page');
        $successRequest = new SuccessRequest();
        $successRequest->setData(['image' => $page]);
        return Response::success($successRequest);
    }

    public function workSuiteToken(Request $request): HttpResponseException
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token";
        $dataPost = [
            "suite_id" => 'ww5045c3fb525c0a00',
            "suite_secret" => 'Nqjebi45NHrQFjION4P6L9KtJmtc4uqG-ZTaUXs0iEY',
            "suite_ticket" => 'AMdXvfnYd0g6lzQIRIk8Dgqk3SEJk8LysYVlif9oFLdWocU-OmbVZEJ4KSpsvIWJ',
        ];
        $msg = Http::post($url, $dataPost);
        $msg = $msg->json();
        $suite_access_token = Arr::get($msg, 'suite_access_token');
        $code = "6VsAtr6I5vCcnmJf3cY5mFMZ355t1fbO97jPRSw8Ol8";
        $urlTwo = "https://qyapi.weixin.qq.com/cgi-bin/service/miniprogram/jscode2session?suite_access_token=$suite_access_token&js_code=$code&grant_type=authorization_code";
        //var_dump($urlTwo);
        $two = Http::get($urlTwo);
        $two = $two->json();
        $successRequest = new SuccessRequest();
        $successRequest->setData(['message' => $msg, 'two' => $two]);
        return Response::success($successRequest);
    }

    public function getSkin(Request $request): HttpResponseException
    {
        if ($request->input('appId')) {
            $appId = $request->input('appId');
        } else {
            $appId = config('common.mpWeiXin.appId');
        }
        $model = AdminAppletSetting::query()
            ->where('type', 'SKIN')
            ->where('app_id', $appId)
            ->first();
        if (empty($model->id)) {
            $skin = 'SCHEME_ONE';
        } else {
            $skin = Arr::get($model->setting, 'skinValue');
        }
        $successRequest = new SuccessRequest();
        $successRequest->setData([
            'skin' => $skin,
        ]);
        return Response::success($successRequest);
    }
}
