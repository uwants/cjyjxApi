<?php


namespace App\Logic\Api;


use App\Facades\Response;
use App\Models\Company;
use App\Services\Response\Models\SuccessRequest;
use Illuminate\Http\Request;

class CompanyPosterLogic
{
    public function show(Request $request)
    {
        $companyId = $request->input('companyId');
        $model = Company::query()
            ->where('id', $companyId)
            ->first();
        $data = [
            'companyId' => empty($model->company_id) ? $companyId : $model->id,
            'companyPoster' => empty($model->wx_poster) ? '' : $model->wx_poster
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        Response::success($successRequest);
    }

    public function update(Request $request)
    {
        $companyId = $request->input('companyId');
        $companyPoster = $request->input('companyPoster');
        Company::withTrashed()
            ->where('id', $companyId)
            ->update([
                'wx_poster' => $companyPoster
            ]);
        $data = [
            'companyId' => $companyId,
        ];
        $successRequest = new SuccessRequest();
        $successRequest->setData($data);
        Response::success($successRequest);
    }
}
