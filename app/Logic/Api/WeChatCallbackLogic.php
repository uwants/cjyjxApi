<?php

namespace App\Logic\Api;

use App\Models\WeiXinUserSubscribe;
use App\Services\WeiXinApi\Models\GetAccessTokenRequest;
use Illuminate\Support\Facades\Log;
use App\Facades\WeiXinApi;
use Illuminate\Support\Facades\Http;

class WeChatCallbackLogic
{
    public function index()
    {
        $postStr = file_get_contents("php://input", "r");
        $postObj = (array)simplexml_load_string($postStr, "SimpleXMLElement", LIBXML_NOCDATA);
        $keyword = empty($postObj['Content']) ? '' : trim($postObj['Content']);//用户聊天内容
        $time = time();
        Log::info('$postObj：', [$postObj]);
        //若为事件类型（关注、点击菜单等）
        $content = "您好，欢迎关注超级意见箱！\r\n超级意见箱服务号将会为您推送小程序中的消息!";
        if (trim($postObj['MsgType']) == "event") {
            switch (trim($postObj['Event'])) {
                case'subscribe'://关注事件
                    $mess = $this->wxSetUserInfo($postObj['FromUserName'], 'insert');
                    if ($mess['code'] != 0) {
                        $content = $mess['message'];
                    }
                    break;
                case "unsubscribe"://取消关注
                    $content = "谢谢你的一直陪伴，欢迎下次关注！!";
                    $this->wxSetUserInfo($postObj['FromUserName'], 'del');
                    Log::error('谢谢你的一直陪伴，欢迎下次关注', [$postObj['FromUserName']]);
                    break;
                case'CLICK':
                    //菜单的自定义的key值，可以根据此值判断用户点击了什么内容，从而推送不同信息
                    $EventKey = $postObj['EventKey'];
                    switch ($EventKey) {
                        case'message1':
                            $content = "您好，欢迎！\r\n信息：$EventKey";
                            break;
                        case'message2':
                            $content = "您好，欢迎关注超级意见箱！\r\n信息：$EventKey";
                            break;
                    }
                    break;
            }
            $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
            echo $output;
        } elseif (trim($postObj['MsgType']) == "text") {//若为文字类型
            if ($keyword == "openid") {
                //获取用户信息
                $content = $postObj['FromUserName'];
                $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
                echo $output;
            } else {
                $mess = $this->wxSetUserInfo($postObj['FromUserName'], 'insert');
                if ($mess['code'] != 0) {
                    $content = $mess['message'];
                    $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
                    echo $output;
                } else {
                    $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "image", 'M9A-nAV-h7kzljIwky6RPW4l4nXKAK_ipo17KAR22wY');
                    echo $output;
                }
            }
        } else {
            $content = "您好，欢迎关注超级意见箱！\r\n超级意见箱服务号将会为您推送小程序中的消息。";
            $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
            echo $output;
        }
    }

    public function get_retXml($fromUsername, $toUsername, $time, $msgType, $msg)
    {
        $imageTpl = '';
        if ($msgType == "image") {
            $imageTpl = /** @lang text */
                "
				<xml>
					<ToUserName><![CDATA[%s]]></ToUserName>
					<FromUserName><![CDATA[%s]]></FromUserName>
					<CreateTime>%s</CreateTime>
					<MsgType><![CDATA[%s]]></MsgType>
					<Image>
						<MediaId><![CDATA[%s]]></MediaId>
					</Image>
				</xml>";
        } else if ($msgType == "text") {
            $imageTpl = /** @lang text */
                "
				<xml>
					<ToUserName><![CDATA[%s]]></ToUserName>
					<FromUserName><![CDATA[%s]]></FromUserName>
					<CreateTime>%s</CreateTime>
					<MsgType><![CDATA[%s]]></MsgType>
					<Content><![CDATA[%s]]></Content>
				</xml>";
        }
        return sprintf($imageTpl, $fromUsername, $toUsername, $time, $msgType, $msg);
    }

    public function wxSetUserInfo($openId, $type = 'insert')
    {
        $accessTokenSend = new GetAccessTokenRequest();
        $accessTokenSend->setAppId(config('common.weChatService.appId'));
        $accessTokenSend->setSecret(config('common.weChatService.appSecret'));
        $accessToken = WeiXinApi::getAccessToken($accessTokenSend);
        Log::info('获取token', [$accessToken->getAccessToken()]);
        if (empty($accessToken->getAccessToken())) {
            return ['code' => 1, 'message' => 'token不存在！', 'data' => ['errCode' => 1]];
        }
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" . $accessToken->getAccessToken() . "&openid=$openId&lang=zh_CN";
        $mes = Http::get($url);
        $message = json_decode($mes);
        Log::info('回复信息回调：',[$url,$message]);
        if (empty($message->openid)) {
            return ['code' => -1, 'message' => 'openid数据错误！', 'data' => ['errCode' => 1]];
        }
        if ($type == 'insert') {
            $have = WeiXinUserSubscribe::withTrashed()
                ->where([
                    'openid' => $openId,
                    'unionid' => $message->unionid,
                ])->first();
            if(!empty($have->id)){
                WeiXinUserSubscribe::withTrashed()
                    ->where([
                        'openid' => $openId,
                        'unionid' => $message->unionid,
                    ])->restore();
            }else{
                WeiXinUserSubscribe::query()
                    ->firstOrCreate([
                        'openid' => $openId,
                        'unionid' => $message->unionid,
                    ]);
            }
        } else {
            $del = WeiXinUserSubscribe::query()
                ->where('openid', $openId)
                ->delete();
            Log::error('取消关注', [
                'openid' => $openId,
                'unionid' => $message->unionid,
                'delete' => $del
            ]);
        }
        return ['code' => 0, 'message' => 'success！', 'data' => ['errCode' => '']];
    }

}
