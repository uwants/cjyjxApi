<?php

namespace App\Providers;

use App\Events\CommentAdoptEvent;
use App\Events\CommentCreateEvent;
use App\Events\ReplyCreateEvent;
use App\Listeners\CommentAdoptListener;
use App\Listeners\CommentCreateListener;
use App\Listeners\ReplyCreateListener;
use App\Events\BoxCreateEvent;
use App\Listeners\BoxCreateListener;
use App\Events\CodeErrorEvent;
use App\Listeners\CodeErrorListener;
use App\Events\CompanyApplyCreateEvent;
use App\Listeners\CompanyApplyCreateListener;
use App\Events\MessageEvent;
use App\Listeners\MessageListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ReplyCreateEvent::class => [
            ReplyCreateListener::class
        ],
        CommentCreateEvent::class => [
            CommentCreateListener::class
        ],
        CommentAdoptEvent::class => [
            CommentAdoptListener::class
        ],
        BoxCreateEvent::class => [
            BoxCreateListener::class,
        ],
        CodeErrorEvent::class => [
            CodeErrorListener::class,
        ],
        CompanyApplyCreateEvent::class => [
            CompanyApplyCreateListener::class,
        ],
        MessageEvent::class => [
            MessageListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
