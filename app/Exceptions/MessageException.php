<?php

namespace App\Exceptions;

use Exception;

class MessageException extends Exception
{
    const SYSTEM_ERROR_MESSAGE = '访问人数过多，请稍后重试';
}
