<?php

namespace App\Exceptions;

use App\Events\CodeErrorEvent;
use App\Facades\Response;
use App\Services\Response\Models\ErrorRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e) {
            if ($e instanceof MessageException) {
                $errorRequest = new ErrorRequest();
                $errorRequest->setMessage($e->getMessage());
                event(new CodeErrorEvent($e));
                return Response::error($errorRequest);
            }
            if ($e instanceof ValidationException) {
                $error = $e->errors();
                $errorRequest = new ErrorRequest();
                $errorRequest->setMessage('参数验证异常');
                $errorRequest->setData(compact('error'));
                event(new CodeErrorEvent($e));
                return Response::error($errorRequest);
            }
            if ($e instanceof AuthorizationException) {
                $errorRequest = new ErrorRequest();
                $errorRequest->setMessage('没有权限');
                event(new CodeErrorEvent($e));
                return Response::error($errorRequest);
            }
            if (!$e instanceof HttpResponseException) {
                $errorRequest = new ErrorRequest();
                $errorRequest->setMessage('访问人数过多，请稍后重试');
                $error = $e->getMessage();
                $errorRequest->setData(compact('error'));
                event(new CodeErrorEvent($e));
                return Response::error($errorRequest);
            }
        });
    }

}
