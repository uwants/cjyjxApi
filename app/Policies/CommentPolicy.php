<?php


namespace App\Policies;


use App\Models\Box;
use App\Models\Comment;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Comment $comment): bool
    {
        $boxId = $comment->getAttribute('box_id');
        $box = Box::query()
            ->find($boxId);
        $once = $box->getAttribute('once');
        $isComment = Comment::query()
            ->where('user_id', $comment->getAttribute('user_id'))
            ->where('box_id', $comment->getAttribute('box_id'))
            ->exists();
        //只能提一次,并且提过
        if ($once && $isComment) {
            return false;
        }
        //没有该意见箱
        if (empty($box)) {
            return false;
        }
        //公开
        $open = $box->getAttribute('open');
        if ($open) {
            return true;
        }
        $show = $box->getAttribute('show');
        if ($show) {
            return true;
        }
        $all = $box->getAttribute('all');
        $isInCompany = CompanyUser::query()
            ->where('company_id', $box->getAttribute('company_id'))
            ->where('user_id', $comment->getAttribute('user_id'))
            ->exists();
        if ($all && $isInCompany) {
            return true;
        }
        //指定成员评论
        return CompanyUser::query()
            ->where('box_id', $comment->getAttribute('box_id'))
            ->where('user_id', $comment->getAttribute('user_id'))
            ->exists();
    }
}