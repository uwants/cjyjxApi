<?php


namespace App\Policies;


use App\Exceptions\MessageException;
use App\Models\CompanyUser;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyUserPolicy
{

    use HandlesAuthorization;

    /**
     * @param User $user
     * @param CompanyUser $companyUser
     * @return bool
     */
    public function create(User $user, CompanyUser $companyUser): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @param CompanyUser $companyUser
     * @return bool
     */
    public function administratorCreate(User $user, CompanyUser $companyUser): bool
    {
        return CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->exists();
    }

    /**
     * @param User $user
     * @param CompanyUser $companyUser
     * @return bool
     */
    public function administratorDelete(User $user, CompanyUser $companyUser): bool
    {
        $isSupperAdministrator = CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->exists();
        if ($companyUser->getAttribute('role') == config('common.companyUser.role.supperAdministrator')) {
            return false;
        }
        if ($isSupperAdministrator) {
            return true;
        }
        return $companyUser->getAttribute('user_id') == $user->getKey();
    }

    public function createSupperAdministrator(User $user, CompanyUser $companyUser): bool
    {
        $isSupperAdministrator = CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->exists();
        if ($isSupperAdministrator && $user->getKey() != $companyUser->getAttribute('user_id')) {
            return true;
        }
        return false;
    }

    public function update(User $user, CompanyUser $companyUser): bool
    {
        //超管所有权
        $isSupperAdministrator = CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->exists();
        if ($isSupperAdministrator) {
            return true;
        }
        return $companyUser->getAttribute('user_id') == $user->getKey();
    }

    /**
     * @throws MessageException
     */
    public function delete(User $user, CompanyUser $companyUser): bool
    {
        $role = $companyUser->getAttribute('role');
        //超管不能退出
        $isSupperAdministrator = CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.supperAdministrator'))
            ->exists();
        if ($isSupperAdministrator && $companyUser->getAttribute('user_id') == $user->getKey()) {
            throw new MessageException('超管不能退出企业');
        }
        if ($isSupperAdministrator) {
            return true;
        }
        $isAdministrator = CompanyUser::query()
            ->where('user_id', $user->getKey())
            ->where('company_id', $companyUser->getAttribute('company_id'))
            ->where('role', config('common.companyUser.role.administrator'))
            ->exists();
        if ($isAdministrator && $role == config('common.companyUser.role.normal')) {
            return true;
        }
        if ($companyUser->getAttribute('user_id') == $user->getKey()) {
            return true;
        }
        return false;
    }
}