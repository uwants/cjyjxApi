<?php


namespace App\Policies;

use App\Models\Box;
use App\Models\BoxCommentUser;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\User;
use App\Models\BoxUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class BoxPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Box $box): bool
    {
        $companyId = $box->getAttribute('company_id');
        $isAdministrator = CompanyUser::query()
            ->where('company_id', $companyId)
            ->scopes('administrator')
            ->where('user_id', $user->getKey())
            ->exists();
        $show = $box->getAttribute('show');
        if ($show) {
            return Company::query()
                    ->where($box->getAttribute('company_id'))
                    ->where('status', config('common.company.status.pass'))
                    ->exists() && $isAdministrator;
        }
        $open = $box->getAttribute('open');
        if ($open) {
            return $isAdministrator;
        }
        return CompanyUser::query()
            ->where('company_id', $companyId)
            ->where('user_id', $user->getKey())
            ->exists();
    }

    public function delete(User $user, Box $box): bool
    {
        if ($user->getAttribute('id') == $box->getAttribute('user_id')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Box $box): bool
    {
        //自已的意见箱
        if ($user->getAttribute('id') == $box->getAttribute('user_id')) {
            return true;
        }
        //意见箱助手
        $boxUser = BoxUser::query()
            ->select('id')
            ->where('box_id', $box->getAttribute('id'))
            ->where('user_id', $user->getAttribute('id'))
            ->first();
        if(!empty($boxUser->id)){
            return true;
        }
        //公开的意见箱
        if ($box->getAttribute('open')) {
            return true;
        }
        //可搜索的意见箱
        if ($box->getAttribute('show')) {
            return true;
        }
        //内部公开意见箱
        $companyId = $box->getAttribute('company_id');
        if ($box->getAttribute('all')) {
            return CompanyUser::query()
                ->where('user_id', $user->getKey())
                ->where('company_id', $companyId)
                ->exists();
        }
        //指定人可见意见箱
        return BoxCommentUser::query()
            ->where('box_id', $box->getKey())
            ->where('user_id', $user->getKey())
            ->exists();
    }
}
